
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Download nuclear data from KAERI, http://atom.kaeri.re.kr
# This program works fine with the 2019 version of the web page
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.13 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys
import itertools
import argparse
import multiprocessing

import urllib
import urllib.request

# info
desc  = 'Download nuclear data from KAERI, http://atom.kaeri.re.ikr'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

defs = { 'num_proc': 8,
         'z_min': 0,
         'z_max': 120,
         'a_min': 1,
         'a_max': 300,
         'dir': '.',
         }

def getweb(params):
  if not os.path.isfile(params['file_out']):
    print('# params :: {}'.format(params))
    urllib.request.urlretrieve(params['url'],params['file_out'])
  return

def get_program_args():

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--dir', help='Directory to store files, default "{}"'.format(defs['dir']), type=str, default=defs['dir'], )
  
  parser.add_argument( '--z_min', help='Minimum atomic number, default "{}"'.format(defs['z_min']), type=int, default=defs['z_min'], )
  parser.add_argument( '--z_max', help='Maximum atomic number, default "{}"'.format(defs['z_max']), type=int, default=defs['z_max'], )
  parser.add_argument( '--a_min', help='Minimum mass number, default "{}"'.format(defs['a_min']), type=int, default=defs['a_min'], )
  parser.add_argument( '--a_max', help='Maximum mass number, default "{}"'.format(defs['a_max']), type=int, default=defs['a_max'], )
  
  return parser.parse_args()

def main():

  args = get_program_args()

  z_min = max(args.z_min,0)
  z_max = args.z_max
  
  a_min = max(args.a_min,1)
  a_max = args.a_max

  assert( z_min <= z_max )
  assert( a_min <= a_max )

  # query base string
  query_str = 'http://atom.kaeri.re.kr/nuchart/?z={z}&a={a}&im=&lib=&mat=&mf=&mt=&rg=&zlv=0&ix=&iy=#iproperty'

  # initialize list to store jobs
  list_jobs = []

  # add jobs to the list
  #for zi, ai in itertools.product(range(z_min,z_max+1),range(a_min,a_max+1)):
  for zi in range(z_min,z_max+1):
    
    alo = max(zi/4,1)
    ahi = min(4*zi,a_max)
    ahi = max(ahi,40)
    
    alo = int(alo)
    ahi = int(ahi)

    for ai in range(alo,ahi):
      # define job params
      params = { 'z': zi, 
                 'a': ai,
                 'file_out': '{}/kaeri_z{:03d}_a{:03d}.html'.format(args.dir,zi,ai),
                 }
      params['url'] = query_str.format(** params)
      # add job to the list
      list_jobs.append(params)

  print('# num jobs = {}'.format(len(list_jobs)))

  # launch jobs
  pool = multiprocessing.Pool(defs['num_proc'])
  pool.map(getweb,list_jobs)

  return 0


if __name__=='__main__':
  sys.exit(main())
