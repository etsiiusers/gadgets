
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Read data from database
# This program works fine with the 2019 version of the web page
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.15 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Files need to be sanitized and some importat errors and several XML 
#   non-compliant features of the file must be sanitized
# - add more info to the database about creation date, author, version, ...
# 2019.03.13 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#


import os
import sys
import argparse
import shlex
import subprocess
import json
import datetime
import xml.etree.ElementTree as et

import numpy

# info
desc  = 'Read nuclear data in HTML files from KAERI'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# default program parameters
defs = { 'json': 'dbnuc.json',
         }

# element names
list_names = {'n': 'nitrogen', 'cd': 'cadmium', 'au': 'gold', 'fe': 'iron', 'rb': 'rubidium', 'mg': 'magnesium', 'u': 'uranium', 'rn': 'radon', 'nb': 'niobium', 'te': 'tellurium', 'bi': 'bismuth', 'pd': 'palladium', 'i': 'iodine', 'tl': 'thallium', 'fr': 'francium', 'ca': 'calcium', 'hg': 'mercury', 'ba': 'barium', 'ru': 'ruthenium', 'zr': 'zirconium', 'hf': 'hafnium', 'c': 'carbon', 'at': 'astatine', 'pu': 'plutonium', 'ge': 'germanium', 'cr': 'chromium', 'f': 'fluorine', 'er': 'erbium', 'cs': 'cesium', 'sm': 'samarium', 'th': 'thorium', 'pa': 'protactinium', 'np': 'neptunium', 'ga': 'gallium', 'ti': 'titanium', 'eu': 'europium', 'md': 'mendelevium', 's': 'sulfur', 'pb': 'lead', 'y': 'yttrium', 'am': 'americium', 'hs': 'hassium', 'pm': 'promethium', 'gd': 'gadolinium', 'kr': 'krypton', 're': 'rhenium', 'ni': 'nickel', 'co': 'cobalt', 'sr': 'strontium', 'fm': 'fermium', 'as': 'arsenic', 'in': 'indium', 'sb': 'antimony', 'dy': 'dysprosium', 'ho': 'holmium', 'pt': 'platinum', 'os': 'osmium', 'br': 'bromine', 'sc': 'scandium', 'po': 'polonium', 'se': 'selenium', 'ag': 'silver', 'v': 'vanadium', 'cl': 'chlorine', 'tc': 'technetium', 'zn': 'zinc', 'ds': 'darmstadtium', 'mn': 'manganese', 'ac': 'actinium', 'w': 'tungsten', 'li': 'lithium', 'na': 'sodium', 'ce': 'cerium', 'pr': 'praseodymium', 'cf': 'californium', 'es': 'einsteinium', 'ir': 'iridium', 'k': 'potassium', 'ra': 'radium', 'ne': 'neon', 'be': 'beryllium', 'ta': 'tantalum', 'sn': 'tin', 'yb': 'ytterbium', 'h': 'hydrogen', 'la': 'lanthanum', 'tb': 'terbium', 'cu': 'copper', 'al': 'aluminum', 'db': 'dubnium', 'nd': 'neodymium', 'lr': 'lawrencium', 'lu': 'lutetium', 'cm': 'curium', 'si': 'silicon', 'sg': 'seaborgium', 'rh': 'rhodium', 'o': 'oxygen', 'rf': 'rutherfordium', 'bk': 'berkelium', 'ar': 'argon', 'tm': 'thulium', 'p': 'phosphorus', 'mo': 'molybdenum', 'bh': 'bohrium', 'xe': 'xenon', 'no': 'nobelium', 'b': 'boron', 'he': 'helium', 'mt': 'meitnerium', 'rg': 'roentgenium', 'cn': 'copernicium', }

def is_valid_file(root):
  
  '''
  Check if file has valid data
  '''

  for node in root.iter():
    try:
      if node.attrib['class'] == 'title_l':
        if node.text == 'Directions for Using Table of Nuclides':
          return False
        else: 
          return True
    except KeyError:
      pass
  return True


def load_data_name(root):

  '''
  Load isotope name
  '''
  
  name = ''

  for node in root.iter():
    if 'class' in node.attrib.keys():
      if node.attrib['class'] == 'title_l':
        name = node.text
        break
  
  if name:
    words = name.split('-')

    print('# name :: {}'.format(name))

    z = int(words[0])
    sym = words[1]
    a = int(words[2])

    par = { 'Z': z, 
            'A': a, 
            'sym': sym, 
            'name_short': '{}{}{}'.format(z,sym,a), 
            }
   
    try:
      name = list_names[sym]
      par['name'] = name 
      par['name_long'] = '{}{}{}'.format(z,list_names[sym.lower()],a)
    except KeyError:
      pass
    
    return par

  return None

def load_data_property(root,name):

  '''
  Load isotope property
  '''

  # loop over all elements
  for node in root.iter():

    # check the "tr" elements (row of tables)
    if node.tag == 'tr':
      # get all elements in this row
      rows = node.findall('td')

      # check if there is some element with the property
      has_name = False
      for ri in rows:
        if ri.text == name:
          has_name = True
          break
      
      # read property
      if has_name:
        words = rows[-1].text.split()

        try:
          value = float(words[0])
        except ValueError:
          value = numpy.nan

        params = {}
        try:
          params[name] = { 'value': value,
                           'error': float(words[1]),
                           'units': words[2],
                         }
        except ValueError:
          params[name] = { 'value': value,
                           'units': words[1],
                         }

        
        return params

  return None

def load_data_atomic_mass(root):
  return load_data_property(root,'Atomic mass')

def load_data_excess_mass(root):
  return load_data_property(root,'Mass excess')

def load_data_binding_energy(root):
  return load_data_property(root,'Binding energy / A')

def load_data_abundance(root):
  return load_data_property(root,'Abundance')

def load_data_beta_decay(root):
  return load_data_property(root,'Beta decay energy')

def load_data_decay_eex(text):
  words = text.split()

  value = words[0]
  value = value.replace('#','')

  try:
    par = { 'value': float(value), }
  except ValueError:
    par = { 'value': value, }

  if len(words) > 1:
    err = words[1]
    err = err.replace('(','')
    err = err.replace(')','')
    err = err.replace('#','')
    par['error'] = float(err)

  return par

def load_data_decay_half_life(text):
  words = text.split()
  
  par = {}
 
  if len(words) > 0:
    val = words[0]
    val = val.replace('#','')
    try:
      par['value'] = float(val)
    except ValueError:
      par['value'] = val

  if len(words) > 1:
    par['units'] = words[1]

  if len(words) > 2:
    err = words[2]
    err = err.replace('(','')
    err = err.replace(')','')
    err = err.replace('#','')
    try:
      par['error'] = float(err)
    except ValueError:
      par['error'] = err

  return par

def load_data_decay_mode(text):
  pi = text.find('[')
  pj = text.find(']')
  
  s = text
  r = ''
  if pi >= 0:
    s = text[0:pi] + text[pj+1:-1]
    r = text[pi:pj+1]

  s = s.replace(',',' ')

  num = s.count(':')
  words = s.split()
  
  branches = []

  if num == 0:
    branches.append( { 'type': text, })
  else:
    for i in range(num):
      type = words[2*i]
      type = type.replace(':','')
    
      type = type.replace('β-','b-')
      type = type.replace('β+','b+')
      type = type.replace('α','a')
      type = type.replace(',',' ')
    
      value = words[2*i+1].split('%')

      try:
        ratio = float(value[0])
      except ValueError:
        ratio = value[0]
    
      branches.append( { 'type': type, 'ratio': ratio, })
    
      if len(value) > 1:
        err = value[1]
        if err:
          err = err.replace('(','')
          err = err.replace(')','')
          try:
            branches[-1]['error'] = float(err)
          except ValueError:
            branches[-1]['error'] = err
    
      if r:
        branches[-1]['comment'] = r

  return branches

def load_data_decay(root):
  
  '''
  Load decay data
  '''

  for node in root.iter():
    
    if node.tag == 'table':
      
      has_decay = False
      for ni in node.iter():
        if ni.text == 'Decay Modes':
          has_decay = True
      
      if has_decay:
        rows = node.findall('tr')
        num_rows = len(rows)
        
        list_decay = []
        for i in range(1,len(rows)):
          elem = rows[i].findall('td')
          
          par = {}
          if elem[0].text:
            par['Eex'] = load_data_decay_eex(elem[0].text)
          if elem[1].text:
            par['J'] = elem[1].text.replace('\n',' ').strip()
          if elem[2].text:
            par['half-life'] = load_data_decay_half_life(elem[2].text.replace('\n',' ').strip())
          if elem[3].text:
            par['mode'] = load_data_decay_mode(elem[3].text.replace('\n',' ').strip())
          
          list_decay.append(par)

        return { 'decay': list_decay, }

  return None

def load_data(root):
  
  '''
  Load content from XML document
  '''

  # list functions to load defined parts of the XML document
  list_func = [ load_data_name, 
                load_data_atomic_mass,
                load_data_excess_mass,
                load_data_binding_energy,
                load_data_abundance,
                load_data_beta_decay,
                load_data_decay,
                ]

  # initialize data structure
  params = {}
  
  # loop over data extraction functions
  for func in list_func:
    par = func(root)
    if par:
      params.update(par)
  
  # define a key for this file data
  try:
    key = '{}{}'.format(params['sym'],params['A'])
    # return data
    return { key.lower(): params, }
  except KeyError:
    print('# error :: missing "sym" for isotope')
    print(params)
    return { 'null': None, }

def format(file_name):

  '''
  Sanitize XML content with an external application
  '''

  # This sed command removes a mistake in node attributes nad missing closing 
  # tags
  # sed -i "s/style=width:130px;'/style='width:130px;'/g" *.html
  # sed -i 's:<td colspan=2>::g' *.html
  # sed -i 's:cellpadding=5 ::g' *.html
  # sed -i 's:<br>::g' *.html
  
  # open file and read content
  fd = open(file_name,'r')
  text = fd.read()
  fd.close()

  # replace XML non compliant content
  list_s = [ ( 'style=width:130px;\'', 'style=\'width:130px;\'', ),
             ( 'cellpadding=5 ', '', ),
             ( '<br>', '', ),
             ]
  for si in list_s:
    text = text.replace(si[0],si[1])

  # command to launch
  cmmd = 'xmlstarlet fo -R'

  # launch XML sanitizer
  proc = subprocess.Popen(shlex.split(cmmd),stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.DEVNULL)
  outs, errs = proc.communicate(text.encode())
  
  # return string with XML xontent
  return outs.decode('utf-8')

def get_version():

  '''
  Get current version of this file, return HASH to identify GIT version
  '''
  
  cmmd = 'git log -1'
  proc = subprocess.Popen(shlex.split(cmmd),stdout=subprocess.PIPE,stderr=subprocess.DEVNULL)
  outs, errs = proc.communicate()
  
  words = outs.decode('utf-8').split()

  shash = 'undef'
  for i, wi in enumerate(words):
    if wi == 'commit':
      try:
        shash = words[i+1]
      except IndexError:
        pass
      break
  
  return shash

def get_program_args():

  '''
  Get command line arguments
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( 'inp', help='Input file names', type=str, nargs="*", )
  
  parser.add_argument( '--json', help='Dump data to JSON file, set file name, default "{}"'.format(defs['json']), type=str, default=defs['json'], )
  
  return parser.parse_args()

def main():

  '''
  Program main driver routine
  '''

  # read command line arguments
  args = get_program_args()

  # initialize dictionary with nuclear porperties data
  dbnuc = {}
  
  # add some info to database
  info = { 'name': os.path.basename(__file__),
           'desc': desc,
           'author': author,
           'license': license,
           'date_creation': '{}'.format(datetime.datetime.now()),
           'git_version_hash': get_version(),
           }
  dbnuc['info'] = info

  # load data from all files 
  for file_name in args.inp:
    
    print('# parsing file {} ...'.format(file_name))
    
    # read HTML file and sanitize content
    text = format(file_name)
    if text:
      root = et.fromstring(text)

      # check if the XML document has valid data
      if is_valid_file(root):
        # take data from XML document
        dbnuc.update(load_data(root))

  # print results to screen
  print(dbnuc)

  # save data to JSON file
  file_json = args.json
  if file_json:
    try:
      with open(file_json,'w') as fd:
        json.dump(dbnuc,fd)
    except OSError:
      print('# error :: imposible to create file "{}"'.format(file_json))

  return 0


if __name__=='__main__':
  sys.exit(main())
