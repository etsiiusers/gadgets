
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test database of nuclear properties 
# This program works fine with the 2019 version of the web page
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.15 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys
import argparse
import json

# info
desc  = 'Test database of nuclear properties'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

defs = { 'inp': 'dbnuc.json',
         }

class units_c():

  def __init__(self,name,value):
    self.name = name
    self.value = value
    return

list_units = { 'zs': units_c('zs',1.0e-21),
               'ys': units_c('ys',1.0e-24), 
               'h': units_c('h',3600.0), 
               'My': units_c('My',1.0e+06*365*24*3600.0),
               'm': units_c('m',60.0),
               'Zy': units_c('Zy',1.0e+21), 
               'Py': units_c('Py',1.0e+15),
               'ky': units_c('ky',1.0e+03*365*24*3600.0),
               'μs': units_c('μs',1.0e-06),
               'd': units_c('d',24*3600.0),
               'Gy': units_c('Gy',1.0e+09*365*24*3600.0),
               'Yy': units_c('Yy',1.0e+24*365*24*3600.0),
               'Ty': units_c('Ty',1.0e+12*365*24*3600.0),
               'y': units_c('y',365*24*3600.0),
               's': units_c('s',1.0),
               'as': units_c('as',1.0e-18),
               'ns': units_c('ns',1.0e-09),
               'ps': units_c('ps',1.0e-12),
               'Ey': units_c('Ey',1.0e+18),
               'ms': units_c('ms',1.0e-03),
               }

def get_program_args():

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--inp', help='Database file name, default "{}"'.format(defs['inp']), type=str, default=defs['inp'], )
  
  return parser.parse_args()

def main():

  args = get_program_args()

  db = None

  try:
    with open(args.inp,'r') as fd:
      db = json.load(fd)
  except OSError:
    print('# error :: impossible to open file "{}"'.format(args.inp))

  if db:
    for ki, vi in db.items():
      if ki != 'info':
        try:
          for d in vi['decay']:
            v = d['half-life']['value']
            u = d['half-life']['units']
            try:
              scale = list_units[u]
              print('# {:5s} :: {:15.7e} s'.format(ki,v*scale.value)) 
            except KeyError:
              print('# {:5s} :: {} {} <- Units not identified'.format(ki,v,u)) 
            except:
              print('# {:5s} :: {} {} <- What\'s wrong here?'.format(ki,v,u)) 
        except KeyError:
          pass

  return 0


if __name__=='__main__':
  sys.exit(main())
