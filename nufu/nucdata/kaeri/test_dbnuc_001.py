
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test database of nuclear properties 
# This program works fine with the 2019 version of the web page
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.15 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys
import argparse
import json

# info
desc  = 'Test database of nuclear properties'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

defs = { 'inp': 'dbnuc.json',
         }

def get_program_args():

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--inp', help='Database file name, default "{}"'.format(defs['inp']), type=str, default=defs['inp'], )
  
  return parser.parse_args()

def main():

  args = get_program_args()

  db = None

  try:
    with open(args.inp,'r') as fd:
      db = json.load(fd)
  except OSError:
    print('# error :: impossible to open file "{}"'.format(args.inp))

  list_units = []

  if db:
    for ki, vi in db.items():
      if ki != 'info':
        try:
          for d in vi['decay']:
            u = d['half-life']['units']
            if not '(' in u:
              list_units.append(u)
        except KeyError:
          pass

  if list_units:
    list_units = set(list_units)
    print('# units :: {}'.format(list_units))
    
  return 0


if __name__=='__main__':
  sys.exit(main())
