
#
# Copyright 2019-2020 Universidad Politécnica de Madrid
#
# This file is part of gadgets.
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Version control:
# 2020.12.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - minor changes in module info
# - add comments
# - add command line arguments and help
# 2019.03.29 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys
import argparse
import json

# info
info = { 'name': 'nubase',
         'file': __file__,
         'desc': 'Read NUBASE database (nubtab12.asc.txt, taken from JANIS)',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2020,
         'version': [ 1, 1, 0, ],
         'license': 'Copyright (C) 2019-2020 Universidad Politécnica de Madrid',
         }

# script defaut parameters
defs = { 'inp': 'nubtab12.asc.txt',
         'out': 'dbnubase12.json',
         'verbose': 0,
         }

# list of elements
list_elem = [ 'N', 'Cd', 'Au', 'Fe', 'Rb', 'Mg', 'U', 'Rn', 'Nb', 'Te', 'Bi', 
              'Pd', 'I', 'Tl', 'Fr', 'Ca', 'Hg', 'Ba', 'Ru', 'Zr', 'Hf', 'C', 
              'At', 'Pu', 'Ge', 'Cr', 'F', 'Er', 'Cs', 'Sm', 'Th', 'Pa', 'Np', 
              'Ga', 'Ti', 'Eu', 'Md', 'S', 'Pb', 'Y', 'Am', 'Hs', 'Pm', 'Gd', 
              'Kr', 'Re', 'Ni', 'Co', 'Sr', 'Fm', 'As', 'In', 'Sb', 'Dy', 'Ho', 
              'Pt', 'Os', 'Br', 'Sc', 'Po', 'Se', 'Ag', 'V', 'Cl', 'Tc', 'Zn', 
              'Ds', 'Mn', 'Ac', 'W', 'Li', 'Na', 'Ce', 'Pr', 'Cf', 'Es', 'Ir', 
              'K', 'Ra', 'Ne', 'Be', 'Ta', 'Sn', 'Yb', 'H', 'La', 'Tb', 'Cu', 
              'Al', 'Db', 'Nd', 'Lr', 'Lu', 'Cm', 'Si', 'Sg', 'Rh', 'O', 'Rf', 
              'Bk', 'Ar', 'Tm', 'P', 'Mo', 'Bh', 'Xe', 'No', 'B', 'He', 'Mt', 
              'Rg', ]

# convert units of decaying period
list_lambda_units = [ ( 'ms', 1.0e-03, ),
                      ( 's', 1.0e+00, ),
                      ( 'm', 60.0e+00, ),
                      ( 'h', 3600.0e+00, ),
                      ( 'y', 365*24*3600.0, ),
                      ( 'ky', 1.0e+03*365*24*3600, ),
                      ( 'ys', 1.0e-24, ),
                      ( 'as', 1.0e-18, ),
                      ( 'zs', 1.0e-21, ),
                      ( 'd', 24*3600.0, ),
                      ( 'My', 1.0e+06*365*24*3600.0, ),
                      ( 'ps', 1.0e-12, ),
                      ( 'us', 1.0e-06, ),
                      ( 'Gy', 1.0e+09*365*24*3600.0, ),
                      ( 'Py', 1.0e+12*365*24*3600.0, ),
                      ( 'Zy', 1.0e+18*365*24*3600.0, ),
                      ( 'ns', 1.0e-09, ),
                      ( 'Ey', 1.0e+15*365*3600.0, ),
                      ]

def is_element_sym(w):
  for ei in list_elem:
    if w == ei:
      return True
  return False

def select_sym(w):
  sym = []
  for ei in list_elem:
    if ei in w:
      sym.append(ei)
  if len(sym) > 1:
    s = ''
    for si in sym:
      if len(si) > len(s):
        s = si
    return s
  elif len(sym)==1:
    return sym[0]
  else:
    return ''

def is_lambda_unit(w):
  for li in list_lambda_units:
    if w == li[0]:
      return True
  return False

def skey(key,space):
  sk = '# {' + ':{}s'.format(space+1) +'}{}: '
  return sk.format('',key)

def svalue(value):
  return '{},'.format(value)

def slist(db,space):
  s = ''
  for ki, vi in db.items():
    s += skey(ki,space)
    try:
      sl = slist(vi,space+2)
      s = s + '{' + sl + '}'
    except:
      s = s + svalue(vi) 
  return s 

def print_db(db,space=0):
  s = slist(db,space)
  s = s.replace('#','\n#')
  print(s)
  return

def split_index(words):
  index = words[0]
  num = index[0:4]
  l = ''
  if len(index) > 4:
    l = index[-1]
  z = int(num[0:3])
  return 1, index, num, l, z

def split_sym(words):
  name_short = words[0]
  i = 0
  for j, wj in enumerate(name_short):
    if not wj.isdigit():
      i = j
      break
  a = int(name_short[:j])
  sym = name_short[j:]
  meta = ''
  if len(sym) > 2:
    sym = select_sym(sym)
    meta = name_short[j+len(sym):]
  return 1, a, sym, meta

def split_value(words):
  val = words[0]
  err = words[1]

  est = 'n'
  if '#' in val:
    est = 'y'
  
  val = val.replace('#','')
  err = err.replace('#','')

  inc = 0
  try:
    w = float(val)
    inc += 1
  except ValueError:
    pass
  try:
    w = float(err)
    inc += 1
  except ValueError:
    pass
    
  return inc, val, err, est

def split_me(words):
  inc, val, err, est = split_value(words)
  me = { 'name': 'mass excess',
         'value': val, 
         'error': err,
         'unit': 'keV',
         'estim': est,
         }
  return inc, me

list_methods = [ 'RQ', '*', 'RN', 'p', '2p', 'MD', 'Nm', 'Nm*', 'AD', ]

def split_ex(words):
  inc, val, err, est = split_value(words)
 
  if val == 'non-exist':
    index_extra = 1
    err = ''
  elif val == '*':
    index_extra = 0
    val = ''
    err = ''
    inc = 0
  else:
    index_extra = 2

  extra = ''
  try:
    s = words[index_extra].strip()
    for mi in list_methods:
      if s == mi:
        extra = s
        inc += 1
        break
  except IndexError:
    extra = ''
  
  ex = { 'name': 'excitation energy',
         'value': val,
         'error': err,
         'unit': 'keV',
         'estim': est,
         'method': extra,
         }
  return inc, ex

def split_ex_alt(words):
  if words[0] == '*':
    return 1, { 'method': words[0], }
  else:
    return 0, {}

def split_lambda(words):
  val = ''
  uni = ''
  err = ''

  est = 'n'
  cond = 'undefined'

  inc = 0

  try:
    val = words[0]
    uni = words[1]
    err = words[2]

    if '#' in val:
      est = 'y'
  
    val = val.replace('#','')
    err = err.replace('#','')

    cond = 'unstable'
    if val == 'stbl':
      cond = 'stable'
      if '+' in err or '-' in err:
        val = uni
        err = ''
        uni = ''
        inc = 2
      else:
        err = ''
        uni = ''
        inc = 1
    elif 'p-unst' in val:
      inc = 1
      err = ''
      uni = ''
    else:
      if '+' in val or '-' in val:
        val = ''
        err = ''
        uni = ''
        inc = 0
      elif not is_lambda_unit(uni):
        uni = ''
        err = ''
        inc = 1
      else:
        if '+' in err or '-' in err:
          err = ''
          inc = 2
        else:
          inc = 3
  except IndexError:
    val = ''
    uni = ''
    err = ''
    cond = 'undef'
    est = ''
  
  l = { 'type': cond,
        'value': val,
        'error': err,
        'unit': uni,
        'estim': est,
        }

  return inc, l

def split_spin(words):
  s = ''
  t = ''
  est = 'n'
  frg = ''
  
  inc = 0

  try:
    s = words[0]
    t = words[1]
  
    if '+' in s or '-' in s:
      inc += 1
      if '#' in s:
        est = 'y'
      s = s.replace('#','')
      if 'frg' in t:
        frg = 'frg'
        t = t.replace('frg','')
        inc += 1
        if not 'T' in t:
          t = words[2]
          inc += 1
      else:
        if not 'T' in t:
          t = ''
        else:
          inc += 1
    elif 'low' in s:
      if '#' in s:
        est = 'y'
      s = s.replace('#','')
      inc += 1
      if not 'T' in t:
        t = ''
      else:
        inc += 1
  except IndexError:
    pass

  spin = { 'value': s,
           'estim': est,
           'T': t,
           'frg': frg,
           }
           
  return inc, spin

def split_info(words):
  
  def check_year(w):
    if len(w) == 2:
      status = True
      for wi in w:
        if not wi.isdigit():
          status = False
          break
      return status
    else:
      return False

  def check_reference(w):
    if len(w) >= 6:
      return True
    elif w == 'Imme':
      return True
    else:
      return False

  def check_discover(w):
    if len(w) == 4:
      if w[0:2]=='20' or w[0:2]=='19':
        return True
    return False

  info = {}
  inc = 0

  flag = False

  try:
    w = words[0]
    if check_year(w):
      info['year_ensdf'] = w
      inc += 1
  except IndexError:
    return inc, info

  try:
    w = words[0+inc]
    if check_reference(w):
      if w == 'Imme':
        info['reference'] = 'Imme  e'
        inc += 2
      elif len(w) >= 9:
        flag = True
        info['reference'] = w[:-4]
      else:
        info['reference'] = w
        inc += 1
  except IndexError:
    return inc, info

  try:
    if flag:
      n = len(w)
      w = w[n-4:n]
    else:
      w = words[0+inc]
    if check_discover(w):
      info['year_discovered'] = w
      inc += 1
  except IndexError:
    return inc, info

  return inc, info

def split_decay(words):
  try:
    return words[0:]
  except IndexError:
    return ''

def split_amass(words):
  an = int(words[0])
  return 1, an

def load(line):
  words = line.split()

  i = 0
  inc = 0

  inc, an = split_amass(words[i:])
  i += inc
  
  inc, index, num, l, zn = split_index(words[i:]) 
  i += inc
  
  inc, a, sym, meta = split_sym(words[i:])
  i += inc
 
  inc, me = split_me(words[i:])
  i += inc

  ex = {}
  inc = 0
  if meta:
    inc, ex = split_ex(words[i:])
  else:
    inc, ex = split_ex_alt(words[i:])
  i += inc

  inc, lm = split_lambda(words[i:])
  i += inc
    
  inc, spin = split_spin(words[i:])
  i += inc
  
  inc, info = split_info(words[i:])
  i += inc

  decay = split_decay(words[i:])
 
  elem_info = { 'A': an, 
                'Z': zn,
                'sym': sym,
                'index': index,
                'nl': (num, l, ),
                'meta': meta,
                'me': me,
                'ex': ex,
                'lambda': lm, 
                'spin': spin,
                'info': info,
                'decay': decay,
                }
                
  return elem_info


def get_args(info):

  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, {email}'.format(** info))

  parser.add_argument( '--inp', help='Define input file with NUBASE database in ASCII format, default "{}"'.format(defs['inp']), default=defs['inp'], type=str, )
  parser.add_argument( '--out', help='Define output file name (JSON format), default "{}"'.format(defs['out']), default=defs['out'], type=str, )
  
  parser.add_argument( '--verbose', help='Define level of verbodity, default "{}"'.format(defs['verbose']), default=defs['verbose'], type=int, )

  return parser.parse_args()
  

def main():
  
  args = get_args(info)

  # set input file name
  file_inp = args.inp

  # general info for the DB
  info_general = {}
  info_general.update(info)
  info_general['file'] = file_inp
  
  db = { 'info': info_general, }

  # load data from file
  try:
    with open(file_inp,'r') as fd:
     for i, line in enumerate(fd):
       elem = load(line)
       if elem:
         key = '{}{}{}'.format(elem['A'],elem['sym'],elem['meta'])
         db[key] = elem
  except OSError:
    print('# error :: impossible to open file "{}"'.format(file_name),file=sys.stderr)

  # print data to screen
  if args.verbose > 0:
    print_db(db) 

  # save to json
  file_json = args.out
  try:
    with open(file_json,'w') as fd:
      json.dump(db,fd)
  except OSError:
    print('# error :: impossible to open file "{}"'.format(file_json))

  return

if __name__ == '__main__':
  sys.exit(main())
