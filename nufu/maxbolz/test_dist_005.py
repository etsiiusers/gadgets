
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module dist.py
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.03.16 :: Manue Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy 
import scipy.integrate

import libnufu.dist as dist
import libnufu.mesh as mesh

desc = 'Test program for distributions'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

def main():

  m = 1.0e+00
  kt  = 1.0e+00

  a = numpy.sqrt(kt/2.0e+00)

  f1 = dist.maxboltz_c(a) 
  f2 = dist.maxboltz_ene_c(kt,m)
  
  ene = { 'lo': f1.mean*1.0e-03,
          'hi': f1.mean*1.0e+01,
          'size': 100,
          'type': 'log',
          }
  ene['values'] = mesh.build(ene)

  for ei in ene['values']:
    
    factor = numpy.sqrt(ei)*2.0e+00
    
    print(' {:17.9e}'.format(ei),end='')
    print(' {:17.9e}'.format(f1.pdf(numpy.sqrt(ei))/factor),end='')
    print(' {:17.9e}'.format(f2.pdf(ei)),end='')

    print()

if __name__=='__main__':
	sys.exit(main())
