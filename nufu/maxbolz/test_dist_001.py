
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module dist.py
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.03.16 :: Manue Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy 
import scipy.integrate

import libnufu.dist as dist
import libnufu.mesh as mesh

desc = 'Test program for distributions'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

def main():

  a = { 'lo': 1.0e-02,
        'hi': 1.0e+02,
        'size': 10,
        'type': 'log',
        }
  a['values'] = mesh.build(a)

  x = { 'lo': 1.0e-06,
        'hi': 1.0e+03,
        'size': 1000,
        'type': 'log',
        }
  x['values'] = mesh.build(x)

  func_dist = [ dist.maxboltz_c(ai) for ai in a['values'] ]

  for xi in x['values']:
    print(' {:17.9e}'.format(xi),end='')
    for fi in func_dist:
      print(' {:17.9e}'.format(fi.pdf(xi)),end='')
    for fi in func_dist:
      print(' {:17.9e}'.format(fi.cdf(xi)),end='')
    print()

if __name__=='__main__':
	sys.exit(main())
