
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module dist.py
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.03.16 :: Manue Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy 
import scipy.integrate

import libnufu.dist as dist
import libnufu.mesh as mesh

desc = 'Test program for distributions'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

def main():

  m = 1.0e+00
  kt  = 1.0e+00

  a = numpy.sqrt(m*kt)

  f1 = dist.maxboltz_c(a) 
  f2 = dist.maxboltz_momxyz_c(kt,m)
  
  px = { 'lo': f1.mean*1.0e-03,
         'hi': f1.mean*1.0e+01,
         'size': 100,
         'type': 'log',
         }
  px['values'] = mesh.build(px)

  f0 = numpy.power(2.0e+00*numpy.pi*f1.a*f1.a,-1.5)/numpy.sqrt(2.0e+00/numpy.pi)

  for pxi in px['values']:
    
    p = numpy.array( [ pxi, 0.0e+00, 0.0e+00, ])

    factor = pxi*pxi/f1.a/f1.a/f1.a/f0
    
    print(' {:17.9e}'.format(pxi),end='')
    print(' {:17.9e}'.format(f1.pdf(pxi)/factor),end='')
    print(' {:17.9e}'.format(f2.pdf(p)),end='')

    print()

if __name__=='__main__':
	sys.exit(main())
