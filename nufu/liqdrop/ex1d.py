
import os
import sys

import sympy 
import sympy.abc

# define some constants
mass_neu = 939.5654133 # MeV
mass_pro = 938.2720813 # MeV
mass_ele = 0.5109989461 # MeV

class ld_func():

  '''
  Symbolic function to get the nuclear binding energy

  NOTE:
  - All energy/mass quantities are in MeV
  '''
  
  # liquid drop model constants, in MeV
  _Avol = 15.5
  _Asur = -16.8
  _Acou = -0.72
  _Asym = -23.2
  _Apar = 34.0 
  _Aele = 20.8e-06 
  
  # mode of functions
  _mode_a_odd = 'odd'
  _mode_a_even_z_even = 'even-even'
  _mode_a_even_z_odd = 'even-odd'

  # object initialization
  def __init__(self,mode=_mode_a_odd):
    
    '''
    Object construction
    '''

    # define variables
    #self.a, self.z = sympy.symbols('a z', integer=True)
    self.a, self.z = sympy.symbols('a z')

    # define partial functions for binding energy
    self.func_b_vol = ld_func._Avol*self.a
    self.func_b_sur = ld_func._Asur*(self.a**(2.0/3.0))
    self.func_b_cou = ld_func._Acou*self.z*(self.z - 1)/(self.a**(1.0/3.0))
    self.func_b_sym = ld_func._Asym*(self.a - 2.0*self.z)*(self.a - 2.0*self.z)/self.a

    self.func_b_par = 0.0
    if mode == ld_func._mode_a_even_z_even:
      self.func_b_par = ld_func._Apar/(self.a**(3.0/4.0))
    elif mode == ld_func._mode_a_even_z_odd:
      self.func_b_par = -ld_func._Apar/(self.a**(3.0/4.0))
    
    # define total binding energy
    self.func_b = self.func_b_vol + self.func_b_sur + self.func_b_cou + self.func_b_sym + self.func_b_par

    # define binding energy per nucleon
    self.func_ba  = self.func_b/self.a

    # define electronic binding energy
    self.func_b_ele  = ld_func._Aele*sympy.Pow(self.z,7.0/3.0)
    self.func_ba_ele = self.func_b_ele/self.a

    # define nuclear mass
    self.func_nmass = (self.a - self.z)*mass_neu + self.z*(mass_pro) - self.func_b
    self.func_amass = (self.a - self.z)*mass_neu + self.z*(mass_pro + mass_ele) - self.func_b - self.func_b_ele

    return
  
  def _eval(self,func,av,zv):
    return func.evalf(subs={ self.z: zv, self.a: av, })

  def eval_b_n(self,av,zv):
    '''
    Nuclear binding energy evaluation
    '''
    return self._eval(self.func_b,av,zv)
  
  def eval_ba_n(self,av,zv):
    '''
    Nuclear binding energy per nucleon evaluation
    '''
    return self._eval(self.func_ba,av,zv)
  
  def eval_nmass(self,av,zv):
    '''
    Nuclear mass evaluation
    '''
    return self._eval(self.func_nmass,av,zv)
  
  def eval_b_a(self,av,zv):
    '''
    Atomic binding energy evaluation
    '''
    return self._eval(self.func_b,av,zv) + self._eval(self.func_b_ele,av,zv)
  
  def eval_ba_a(self,av,zv):
    '''
    Atomic binding energy per nucleon evaluation
    '''
    return self._eval(self.func_ba,av,zv) + self._eval(self.func_ba_ele,av,zv)
  
  def eval_amass(self,av,zv):
    '''
    Atomic mass evaluation
    '''
    return self._eval(self.func_amass,av,zv)

  def dbda(self):
    return sympy.diff(self.func_b,self.a)

  def dbdz(self):
    return sympy.diff(self.func_b,self.z)

  def stable(self):
    '''
    Select the most stable nucleid, it means (A,Z) for min(B/A)
    '''
    
    # f1(a,z) = df/dz
    f1 = self.dbdz()
  
    # f1(a,z) = 0 => zm = zm(a)
    zm = sympy.solve_linear(f1,symbols=[self.z])
  
    # f2(a,z) = df/da
    f2 = self.dbda()
  
    # g2(a) = f2(a,zm(a)) 
    g2 = f2.subs(self.z,zm[1])
  
    # g2(a) = 0 => am 
    am = sympy.nsolve(g2,( 0.01, ))
    
    # evaluation
    am_value = am
    zm_value = zm[1].evalf( subs={ self.a: am_value, })

    return am_value, zm_value


def main():

  # create evaluators
  fo  = ld_func(mode=ld_func._mode_a_odd)
  fee = ld_func(mode=ld_func._mode_a_even_z_even)
  feo = ld_func(mode=ld_func._mode_a_even_z_odd)

  # compute B/A for a defined nucleid
  a = 56.0
  z = 26.0
  ba = fo.eval_ba_n(a,z)
  print('# 1: ba = {} MeV (A odd)'.format(ba))
  ba = feo.eval_ba_n(a,z)
  print('# 2: ba = {} MeV (A even, Z odd)'.format(ba))
  ba = fee.eval_ba_n(a,z)
  print('# 3: ba = {} MeV (A even, Z even)'.format(ba))
  print()

  # solve minimum
  ao, zo = fo.stable()
  print('# case A-odd, a, z = {}, {}'.format(ao,zo))
  print()
  sys.stdout.flush()
  
  aee, zee = fee.stable()
  print('# case A-even Z-even, a, z = {}, {}'.format(aee,zee))
  print()
  sys.stdout.flush()
  
  aeo, zeo = feo.stable()
  print('# case A-evem Z-odd, a, z = {}, {}'.format(aeo,zeo))
  print()
  sys.stdout.flush()
 
  # print table with stability curve
  a_lo = 1
  a_hi = 300
  z_lo = 0
  z_hi = 100

  for ai in range(a_lo,a_hi+1):
    for zi in range(z_lo,z_hi+1):
      for qij in [ zi, ai, fo.eval_ba_a(ai,zi), fee.eval_ba_a(ai,zi), feo.eval_ba_a(ai,zi), ]:
        print(' {:15.7e}'.format(qij),end='')
      print()
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
