
#
# Copyright (C) 2019-2021  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS. If not, see <https://www.gnu.org/licenses/>.
#
# Liquid drop model for nuclear binding energy
#
# Version control:
# 2019.09.17 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include nuclear database
# - include common modules for unit conversion and constants
# 2019.09.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import json

import numpy

import utils.units as units
import utils.prnt as prnt
import ld

# info
info = { 'name': 'main_ld',
         'desc': 'Compute atomic mass using the Liquid-drop model',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2020,
         'version': [ 1, 1, 0, ],
         'license': 'Copyright (C) 2019-2021 UNIVERSIDAD POLITÉCNICA DE MADRID',
         }

# program default parameters
defs = { 'db': 'dbnubase12.json',
         }

# get command line arguments
def get_args(info,defs):

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license} ({email})'.format(** info))

  parser.add_argument( 'A', help='Mass number', type=int, )
  parser.add_argument( 'Z', help='Atomic number', type=int, )
  
  parser.add_argument( '--db', help='Define file name with nuclear database (JSON format), default "{}"'.format(defs['db']), type=str, default=defs['db'], )

  return parser.parse_args()


# load database of nuclear data
def dbload(file_name):
  return json.load(open(file_name,'r'))


# select nucleid from database
def select_nucleid(a,z,db):
  
  '''
  Select nucleid from nuclear database

  -> in
  a: mass number
  z: atomic number
  db: dictionary with database

  <- out
  1) nucleid id
  2) dictionary with nucleid data
  '''

  # loop over all entries in DB
  for ki, vi in db.items():
    # protect againts missing keys
    try:
      # check if this entry is the desired one
      if vi['A']==a and vi['Z']==z:
        # return id and data for this nucleid
        return ki, vi
    except KeyError:
      pass
  return


# driver function
def main():

  # read command line arguments
  args = get_args(info,defs)

  # print program header
  print(prnt.wrinit(info,args))

  # get parameters
  z = args.Z
  a = args.A

  # load nuclear DB
  db = dbload(args.db)

  # select nulceid
  id, nuc = select_nucleid(a,z,db)
 
  # get excess mass and change units from keV to MeV
  delta = float(nuc['me']['value'])*units.conversion_factor('k','M')

  # get mass from excess mass
  m_db = ld.convert_me_to_mass(delta,a)
  
  print('# data from DB')
  print('# id = {}'.format(id))
  for ki, vi in nuc.items():
    print('# nuc :: {} = {}'.format(ki,vi))
  print('# M({},{})   = {:15.7e} MeV'.format(a,z,m_db))
  print()
  
  # get liquid-drop properties
  b_ld = ld.binding_energy(a,z)
  m_ld = ld.mass(a,z)

  print('# data from liquid-drop model')
  print('# B({},{})   = {:15.7e} MeV'.format(a,z,b_ld[0]))
  print('# B({},{})/A = {:15.7e} MeV'.format(a,z,b_ld[0]/a))
  print('# M({},{})   = {:15.7e} MeV'.format(a,z,m_ld))
  print()
  
  err = numpy.abs(m_db - m_ld)/m_db
  print('# |M_exp - M_ld|/M_exp   = {:15.7e} ({:15.7e} %)'.format(err,err*100.0))
  print()

  return
    

if __name__=='__main__':
  sys.exit(main())
