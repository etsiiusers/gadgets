
#
# Copyright (C) 2019-2021  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS. If not, see <https://www.gnu.org/licenses/>.
#
# Liquid drop model for nuclear binding energy
#
# Version control:
# 2021.01.20 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - move main to other file
# - use this file as module
# 2019.09.17 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include nuclear database
# - include common modules for unit conversion and constants
# 2019.09.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import numpy

import utils.units as units
import utils.cphys as cphys

# info
info = { 'name': 'ld',
         'desc': 'Liquid drop model for nuclear binding energy',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2020,
         'version': [ 1, 1, 0, ],
         'license': 'Copyright (C) 2019-2021 UNIVERSIDAD POLITÉCNICA DE MADRID',
         }

# define constants
mass_proton   = cphys.proton.mass.value   * units.mass.kg_to_MeV # MeV
mass_neutron  = cphys.neutron.mass.value  * units.mass.kg_to_MeV # MeV
mass_electron = cphys.electron.mass.value * units.mass.kg_to_MeV # MeV

# constants for liquid-drop model
kv =  15.5e+00 # MeV
ks = -16.8e+00 # MeV
kc =  -0.72e+00 # MeV
ka = -23.2e+00 # MeV
kp =  34.0e+00 # MeV
ke =  20.8e-06 # MeV

# convert from mass excess in MeV to mass in MeV
def convert_me_to_mass(delta,a):
  return delta + a*units.mass.amu_to_MeV

# convert binding energy to mass
def convert_be_to_mass(a,z,be):
  return (a-z)*mass_neutron + z*(mass_proton + mass_electron) - be

# evaluate liquid-drop binding energy
def binding_energy(a,z):

  # volume term
  bv = kv*a
  
  # surface term
  bs = ks*numpy.power(a,2.0/3.0)
  
  # coulomb term
  bc = kc*(z*(z-1))/numpy.power(a,1.0/3.0)
  
  # asymmetry term
  ba = ka*(a-2.0*z)*(a-2.0*z)/a

  # parity term
  bp = 0.0e+00
  if a%2==0 and z%2==0:
    bp = kp
  elif a%2==1 and z%2==1:
    bp = -kp
  bp = bp/numpy.power(a,3.0/4.0)

  # electron binding energy
  be = ke*numpy.power(z,7.0/3.0)

  # total nuclear binding energy
  bn = bv + bs + bc + ba + bp 
  
  # total atomic binding energy
  b = bn + be

  # return total and partial binding energy
  return numpy.array([ b, bv, bs, bc, ba, bp, be, ])

def mass(a,z):

  # liquid drop binding energy values
  b_values= binding_energy(a,z)
  be = b_values[0]

  # atomic mass
  return convert_be_to_mass(a,z,be)
