
#
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# collision.py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# collision.py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with collision.py. If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2021.04.24 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys
import argparse

import numpy
import scipy.interpolate

# program info
info = { 'name': 'target',
         'desc': 'Load target density and temperature profiles',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }

# program default arguments
defs = { 'den_cx': 0,
         'den_cf': 1,
         'tem_cx': 0,
         'tem_cf': 1,
         }

# write program info 
def wrinfo(info):
  s = '# info ::\n'
  for ki, vi in info.items():
    s = s + '# {:>24s} = {}\n'.format(ki,vi)
  return s

# write program command line arguments
def wrargs(args):
  s = '# args ::\n'
  for ki, vi in vars(args).items():
    s = s + '# {:>24s} = {}\n'.format(ki,vi)
  return s

def get_args(info,defs):
  
  # create argument parser
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, {email}'.format(** info))

  # positional arguments
  parser.add_argument( 'file_den', help='Density data file', type=str, )
  parser.add_argument( 'file_tem', help='Temperature data file', type=str, )
  
  # optional arguments
  parser.add_argument( '--den_cx', help='Select column for position in density data file, default "{}"'.format(defs['den_cx']), default=defs['den_cx'], type=int, )
  parser.add_argument( '--den_cf', help='Select column for value of density in data file, default "{}"'.format(defs['den_cf']), default=defs['den_cf'], type=int, )
  parser.add_argument( '--tem_cx', help='Select column for position in temperature data file, default "{}"'.format(defs['tem_cx']), default=defs['tem_cx'], type=int, )
  parser.add_argument( '--tem_cf', help='Select column for value of temperature in data file, default "{}"'.format(defs['tem_cf']), default=defs['tem_cf'], type=int, )
  
  return parser.parse_args()

def load_file(file_name,cx,cf):
  data = numpy.loadtxt(file_name,encoding='utf8',usecols=(cx,cf,))
  return data[:,0], data[:,1]

def driver(dx,df,tx,tf):
  
  fden = scipy.interpolate.interp1d(dx,df,fill_value='extrapolate')
  ftem = scipy.interpolate.interp1d(tx,tf,fill_value='extrapolate')

  s = ''
  for xi in numpy.linspace(0.0,200.0,4000):
    for wi in ( xi, fden(xi), ftem(xi), ):
      s = s + ' {:15.7e}'.format(wi)
    s = s + '\n'
  print(s)

  return

def main(info,defs):
  
  args = get_args(info,defs)

  print(wrinfo(info))
  print(wrargs(args))

  den_x, den_f = load_file(args.file_den,args.den_cx,args.den_cf)
  tem_x, tem_f = load_file(args.file_tem,args.tem_cx,args.tem_cf)

  return driver(den_x,den_f,tem_x,tem_f)

if __name__=='__main__':
  sys.exit(main(info,defs))
