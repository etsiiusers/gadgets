
#
# Copyright (C) 2020 Universidad Politécnica de Madrid
#
# collision.py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# collision.py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with collision.py. If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2022.04.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix error with random selection of directions
# 2022.03.21 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - change way to manage indexes in 4-vectors
# 2021.04.06 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - improve residual calculation
# 2020.12.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - move lorentz transformation to new module
# 2020.11.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import numpy
import scipy.optimize

import utils.linalg as linalg
import utils.lorentz as lorentz

from utils.lorentz import js

# module info
info = { 'name': 'collision',
         'desc': 'Solve a collision of two particles with reaction',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2022,
         'version': [ 1, 3, 1, ],
         'license': 'Copyright (C) 2020-2022 Universidad Politécnica de Madrid',
         }


# axis of the system in the COM frame
def com_axis(q):
  # get system axis
  ax = q[js.mom]
  if numpy.linalg.norm( ax, ) == 0.0:
    ax = linalg.random_dir(1)
  return ax

# solve collision in COM frame
def solve_com(q1_com,q2_com,m3,m4,theta,phi):
  
  '''
  Solve binary collision in the COM frame

  -> in
  q1_com: 4-vector momentum for particle 1 in COM frame, array [4] of floats
  q2_com: 4-vector momentum for particle 2 in COM frame, array [4] of floats
  m3: rest mass of product particle 3, float
  m4: rest mass of product particle 4, float
  theta: dispersion angle of reaction products relative to indicent particles 
         in COM frame, float
  phi: dispersion angle of reaction products, rotation around incident 
       particles in COM frame, floats

  <- out
  (1) q3_com, 4-vector momentum for particle 3 in COM frame, array [4] of floats
  (2) q4_com, 4-vector momentum for particle 4 in COM frame, array [4] of floats

  NOTE:
  - Due to the axis symmetry of the problem, angle phi is not well defined. It
    is no possible to define a system of coordinates to define phi properly.
  '''
  
  # validation of rest masses
  assert m3>=0.0 and m4>=0.0, 'wrong rest masses'

  # validation of momentum in COM frame
  dp = numpy.linalg.norm( q1_com[js.mom] + q2_com[js.mom], )
  tol = 1.0e-12
  assert numpy.all( dp < tol, ), 'total momentum in COM frame is not 0.0'

  # get total energy in COM frame
  e_com = q1_com[js.ene] + q2_com[js.ene]

  # solve collision
  def _helper_solve_collision(e0,m3,m4):
    # define function for energy conservation
    fun = lambda pc: lorentz.pc2ene(m3,pc) + lorentz.pc2ene(m4,pc) - e0
    # find modulus of momentum that holds energy conservation, pc for fun(pc) = 0.0
    return scipy.optimize.brentq( fun, 0.0, e0, maxiter=200, )

  # apply solver to all states
  pc = _helper_solve_collision(e_com,m3,m4)
  
  # define system axis
  ax = com_axis(q1_com)
 
  # compute direction of propagation of products
  du = linalg.array2dir( ax, )
  dv = linalg.array2dir( linalg.array_ortho(du), )

  dw = linalg.rotation( du, dv, theta, )
  dw = linalg.rotation( dw, du, phi, )

  # 4-vector momenta
  q3_com = lorentz.pc2vector( m3,  pc*dw, )
  q4_com = lorentz.pc2vector( m4, -pc*dw, )

  return q3_com, q4_com


# solve collision in Lab frame
def solve_lab(q1_lab,q2_lab,m3,m4,theta,phi):
  
  '''
  Solve binary collision with reaction to produce two new particles
  in the LAB frame

  -> in
  q1_lab: 4-vector momentum for particle 1 in LAB frame, array [4] of floats
  q2_lab: 4-vector momentum for particle 2 in LAB frame, array [4] of floats
  m3: rest mass of product particle 3, float
  m4: rest mass of product particle 4, float
  theta: dispersion angle of reaction products relative to indicent particles 
         in COM frame, float
  phi: dispersion angle of reaction products, rotation around incident 
       particles in COM frame, float

  <- out
  (1) q3_lab, 4-vector momentum for particle 3 in LAB frame, array [4] of floats
  (2) q4_lab, 4-vector momentum for particle 4 in LAB frame, array [4] of floats

  NOTE:
  - Due to the axis symmetry of the problem, angle phi is not well defined. It
    is no possible to define a system of coordinates to define phi properly.
  '''
  
  # compute transformation from LAB to COM frame
  bs = lorentz.boost_to_com(q1_lab,q2_lab)

  # to COM frame 
  q1_com, q2_com = lorentz.lab_to_com(bs,q1_lab,q2_lab)

  # solve collision in COM frame
  q3_com, q4_com = solve_com(q1_com,q2_com,m3,m4,theta,phi)

  # back from COM to LAB frame
  q3_lab, q4_lab = lorentz.com_to_lab(bs,q3_com,q4_com)

  return q3_lab, q4_lab


# solve collision
def solve(m1,m2,m3,m4,k1,k2,d1,d2,theta,phi):
  
  '''
  Solve binary collision with reaction to produce two new particles

  -> in
  m1: rest mass of incident particle 1, float
  m2: rest mass of incident particle 2, float
  m3: rest mass of product particle 3, foat
  m4: rest mass of product particle 4, float
  k1: kinetic energy or particle 1, array [:] of floats
  k2: kinetic energy or particle 2, array [:] of floats
  d1: propagation direction of particle 1, array [:,3] of floats
  d2: propagation direction of particle 2, array [:,3] of floats
  theta: dispersion angle of reaction products relative to indicent particles 
         in COM frame, array [:] float
  phi: dispersion angle of reaction products, rotation around incident 
       particles in COM frame, array [:] of floats

  <- out
  (1) q1_lab, 4-vector momentum for particle 1
  (2) q2_lab, 4-vector momentum for particle 2
  (3) q3_lab, 4-vector momentum for particle 3
  (4) q4_lab, 4-vector momentum for particle 4

  NOTE:
  - Due to the axis symmetry of the problem, angle phi is not well defined. It
    is no possible to define a system of coordinates to define phi properly.
  '''
  
  # validate parameters
  assert m1>=0.0 and m2>=0.0 and m3>=0.0 and m4>=0.0, 'wrong rest masses'
  assert numpy.all( k1>=0.0 ), 'kinetic energies must greater or equal to 0.0'
  assert numpy.all( k2>=0.0 ), 'kinetic energies must greater or equal to 0.0'
  
  # reaction heat
  q = (m1 + m2) - (m3 + m4)
  assert numpy.all( q + k1 + k2 > 0.0 ), 'endothermic reaction, there is not enough kinetic energy for the reaction to happen'

  # initial momentum
  pc1_lab = linalg.array2dir(d1)*lorentz.kin2pc(m1,k1)[:,None] 
  pc2_lab = linalg.array2dir(d2)*lorentz.kin2pc(m2,k2)[:,None]

  # 4-vector momentum in lab frame
  q1_lab = lorentz.pc2vector(m1,pc1_lab) 
  q2_lab = lorentz.pc2vector(m2,pc2_lab) 
  
  # allocate 4-vector mpomentums for products
  q3_lab = numpy.zeros_like(q1_lab)
  q4_lab = numpy.zeros_like(q1_lab)
 
  # solve collisions
  for q1i, q2i, q3i, q4i, ti, pi in zip( q1_lab, q2_lab, q3_lab, q4_lab, theta, phi, ):
    q3i[:], q4i[:] = solve_lab(q1i,q2i,m3,m4,ti,pi)
  
  # validate results
  tol = 1.e-12

  q_prev = q1_lab - q2_lab
  q_curr = q1_lab - q2_lab

  de = 1. - q_curr[js.ene]/q_prev[js.ene]
  assert numpy.all( de < tol ), 'energy conservation is broken'

  dp = numpy.linalg.norm( q_prev[js.mom] - q_curr[js.mom], axis=-1, )/numpy.linalg.norm( q_prev[js.mom], axis=-1, )
  assert numpy.all( dp < tol ), 'momentum conservation is broken'

  return q1_lab, q2_lab, q3_lab, q4_lab
