
#
# Copyright 2020 Universidad Politécnica de Madrid
#
# This file is part of gadgets
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2022.04.05 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - update 
# 2020.12.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - update script to last changes in modules
# 2020.11.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys
import argparse
import itertools
import json

import numpy

from utils.space import dim, sl
import utils.units as units
import utils.linalg as linalg
import utils.lorentz as lorentz

import collision 

# info
info = { 'name': 'main_collision_v01',
         'desc': 'Solve collision in a binary nuclear reaction',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2022,
         'version': [ 1, 2, 0, ],
         'license': 'Copyright 2020-2022 Universidad Politécnica de Madrid',
         }

# program default parameters
defs = { 'db': 'dbnuc.json',
         'angle': numpy.pi/6.0,
         }

# keys to explore database
ks = argparse.Namespace(
  
  # isotope names
  iso1 = 'h2',
  iso2 = 'h3',
  iso3 = 'n1',
  iso4 = 'he4',
  
  # elements in DB
  mass = 'mass_value',

  )

# read command line arguments
def get_args(info):

  '''
  Define command line arguments adn parse them
  '''

  # create argument parser
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, {email}'.format(** info))

  # positional arguments
  parser.add_argument( 'kin', help='Define kinetic energy of incident particle (in MeV)', type=float, )

  # optional arguments
  parser.add_argument( '--db', help='Select nuclear database file, default "{}"'.format(defs['db']), default=defs['db'], type=str, )

  return parser.parse_args()

# load database of nuclear data
def dbload(file_name):
  return json.load(open(file_name,'r'))

# driver function
def main():

  # read command line arguments
  args = get_args(info)

  # load database
  data = dbload(args.db)

  # get mass of reactives from DB
  m1 = data[ks.iso1][ks.mass]*units.mass.amu_to_MeV # deuterium
  m2 = data[ks.iso2][ks.mass]*units.mass.amu_to_MeV # tritium

  # get mass of products from DB
  m3 = data[ks.iso3][ks.mass]*units.mass.amu_to_MeV # neutron
  m4 = data[ks.iso4][ks.mass]*units.mass.amu_to_MeV # alpha
 
  # get reaction heat
  q = (m1 + m2) - (m3 + m4)

  # define number of samples
  num_samples = 100

  # get kinetic energy
  k1 = args.kin*numpy.ones( ( num_samples, ), )
  k2 = numpy.zeros( ( num_samples, ), )

  # allocate memory and define direction of propagation of incident particles
  d1 = numpy.zeros( ( num_samples, dim.num, ), )
  d2 = numpy.zeros( ( num_samples, dim.num, ), )
  d1[sl.x] =  1.
  d2[sl.x] = -1.
 
  # solve for different deflection angles
  theta_lo, theta_hi, theta_num = 0.0, numpy.pi, num_samples
  theta = numpy.linspace(theta_lo,theta_hi,theta_num)
  phi = numpy.zeros( ( num_samples, ), )

  # solve collision
  q1, q2, q3, q4 = collision.solve(m1,m2,m3,m4,k1,k2,d1,d2,theta,phi)

  # kinetic energy of products
  k3 = q3[lorentz.js.ene] - m3
  k4 = q4[lorentz.js.ene] - m4

  # direction of propagation of reaction products
  d3 = linalg.array2dir(q3[lorentz.js.mom])
  d4 = linalg.array2dir(q4[lorentz.js.mom])
  
  # angle between products
  w = numpy.sum( d3*d4, axis=-1, )
 
  # print results 
  print( '\n'.join( [ ''.join( [ f' {xi:15.7e}' for xi in itertools.chain( [ m1, m2, m3, m4, q, ], xs, ) ], ) for xs in zip( k1, k2, k3, k4, theta/numpy.pi, w, ) ], ), )

  return

# launch program
if __name__ == '__main__':
  sys.exit(main())
