
#
# Copyright 2020 Universidad Politécnica de Madrid
#
# This file is part of gadgets
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets. If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2020.12.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - adapt program to last changes in module organization
# 2020.11.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys
import argparse
import json

import numpy

import utils.units as units
import utils.linalg as linalg
import utils.lorentz as lorentz

import collision

# info
info = { 'name': 'main_collision',
         'desc': 'Solve collision in a binary nuclear reaction',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2020,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright 2020 Universidad Politécnica de Madrid',
         }

# program default parameters
defs = { 'db': 'dbnuc.json',
         'angle': numpy.pi/6.0,
         }

# read command line arguments
def get_args(info):

  '''
  Define command line arguments adn parse them
  '''

  # create argument parser
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, {email}'.format(** info))

  # positional arguments
  parser.add_argument( 'kin', help='Define kinetic energy of incident particles (in MeV)', nargs=2, type=float, )

  # optional arguments
  parser.add_argument( '--db', help='Select nuclear database file, default "{}"'.format(defs['db']), default=defs['db'], type=str, )
  parser.add_argument( '--angle', help='Define angle of dispersion of products in radians, default "{}"'.format(defs['angle']), default=defs['angle'], type=float, )

  return parser.parse_args()

# load database of nuclear data
def dbload(file_name):
  return json.load(open(file_name,'r'))

# driver function
def main():

  # read command line arguments
  args = get_args(info)

  # load database
  data = dbload(args.db)

  # get mass of reactives from DB
  m1 = data['h2']['mass_value'] # deuterium
  m2 = data['h3']['mass_value'] # tritium

  # get mass of products from DB
  m3 = data[ 'n1']['mass_value'] # neutron
  m4 = data['he4']['mass_value'] # alpha
 
  # change units, AMU to MeV
  m1 = m1*units.mass.amu_to_MeV
  m2 = m2*units.mass.amu_to_MeV
  m3 = m3*units.mass.amu_to_MeV
  m4 = m4*units.mass.amu_to_MeV

  # get reaction heat
  q = (m1 + m2) - (m3 + m4)

  # get kinetic energy
  k1 = numpy.array( [ args.kin[0], ], )
  k2 = numpy.array( [ args.kin[1], ], )
  
  # define angle of dispersion of products
  theta = numpy.array( [ args.angle, ], )
  phi   = 2.0*numpy.pi*numpy.random.random( ( 1, ), )

  # define direction of propagation of incident particles (TODO!!!)
  d1 = linalg.array2dir(numpy.array( [ [ 1.0,  1.0, 0.0, ], ], ))
  d2 = linalg.array2dir(numpy.array( [ [ 1.0, -1.0, 0.0, ], ], ))
  
  # solve collision
  q1, q2, q3, q4 = collision.solve(m1,m2,m3,m4,k1,k2,d1,d2,theta,phi)

  # kinetic energy of products from 4-vector momentum
  k3 = q3[lorentz.js.ene] - m3
  k4 = q4[lorentz.js.ene] - m4

  # direction of propagation of reaction products
  d3 = linalg.array2dir(q3[lorentz.js.mom])
  d4 = linalg.array2dir(q4[lorentz.js.mom])
  
  # angle between products
  w = numpy.sum( d3*d4, axis=-1, )
  
  # print results
  print( ''.join( [ ''.join( [ f' {xi:15.7e}' for xi in [ m1, m2, m3, m4, q, k1i, k2i, k3i, k4i, wi, ] ], ) for k1i, k2i, k3i, k4i, wi in zip( k1, k2, k3, k4, w, ) ], ), )
    
  return

# launch program
if __name__=='__main__':
  sys.exit(main())
