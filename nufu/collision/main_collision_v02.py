
#
# Copyright 2020 Universidad Politécnica de Madrid
#
# This file is part of gadgets
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2022.04.05 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - update code
# 2021.04.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add more output
# - use Maxwell-Boltzmann distribution for particle energy
# - move things to functions
# 2021.04.06 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - complete distributions, use uniform distribution for energy
# - include more info in output
# 2020.12.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - update script to last changes in modules
# 2020.11.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import os
import sys
import argparse
import itertools
import json

import numpy

import utils.units as units
import utils.linalg as linalg
import utils.lorentz as lorentz
import utils.maxwell as maxwell

from utils.lorentz import js

import collision

# program info
info = { 'name': 'main_collision_v02',
         'desc': 'Solve collision in a binary nuclear reaction',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 3, 0, ],
         'license': 'Copyright (C) 2020-2021 Universidad Politécnica de Madrid',
         }

# program default command line arguments
defs = { 'db': 'dbnuc.json',
         'samples': 10000,
         'kt': 1.0e-02, # in MeV
         }

# keys to explore database
ks = argparse.Namespace(
  
  # isotope names
  iso1 = 'h2', # deuterium
  iso2 = 'h3', # tritium
  iso3 = 'n1', # neutron
  iso4 = 'he4', # alpha
  
  # fields for each isotope ion DB
  mass = 'mass_value',

)

# read command line arguments
def get_args(info):

  '''
  Define command line arguments adn parse them
  '''

  # create argument parser
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, {email}'.format(** info))

  # optional arguments
  parser.add_argument( '--db', help='Select nuclear database file, default "{}"'.format(defs['db']), default=defs['db'], type=str, )
  parser.add_argument( '--samples', help='Define number of samples, default "{}"'.format(defs['samples']), default=defs['samples'], type=int, )
  parser.add_argument( '--kt', help='Temperature of the media in MeV, default "{}"'.format(defs['kt']), default=defs['kt'], type=float, )

  # read arguments
  args = parser.parse_args()

  # validation
  assert os.path.isfile( args.db ), 'missing DB file'
  assert args.samples > 0, 'number of samples must be greater than 0'
  assert args.kt > 0., 'temperature must be greater than 0.'

  return args

# load database of nuclear data
def dbload(file_name):
  return json.load(open(file_name,'r'))

# write program info 
def wrdict(ds):
  return ''.join( [ f'# {ki:>24s} = {vi}\n' for ki, vi in ds.items() ], )

def wrinfo(info):
  return '# info ::\n' + wrdict(info)

# write program command line arguments
def wrargs(args):
  return '# args ::\n' + wrdict(vars(args))

# write program parameters
def wrparticles(* ps):
  func_name = lambda i: f'p{i}'
  return '# particles ::\n' + ''.join( [ f'# {func_name(i):>24s} = {pi}\n' for i, pi in enumerate(ps) ], )
  
def wrmasses(* ms):
  func_mass = lambda i: f'm{i}'
  return '# masses ::\n' + ''.join( [ f'# {func_mass(i):>24s} = {mi:17.9e} MeV\n' for i, mi in enumerate(ms) ], )
  
def wrheat(q):
  name_q = 'Q'
  return '# reaction heat ::\n' + f'# {name_q:>24s} = {q:17.9e} MeV\n'

# write description of program output
def wrdesc():
  ls = [ 'kinetic energy for particle 1, MeV',
         'kinetic energy for particle 2, MeV',
         'kinetic energy for particle 3, MeV',
         'kinetic energy for particle 4, MeV',
         'cosine of the dispersion angle between products, 1 - |cos(d3,d4)|',
         'residual, res = |((E1 + E2) - (E3 + E4))/(E1 + E2)|',
         ]
  
  func_col = lambda i: f'column_{i:02d}'
  return '# descriptipon ::\n' + ''.join( [ f'# {func_col(i):>24s} = {li}\n' for i, li in enumerate(ls) ], )

# get masses from DB
def load_mass(db,* ps):
  return [ db[pi]['mass_value']*units.mass.amu_to_MeV for pi in ps ]

# random samples 
def sample_ene(kt,n):
  return maxwell.dist_maxwell_c(kt).sample(n)

def sample_angle_theta( n, theta_lo=0., theta_hi=numpy.pi, ):
  f_lo, f_hi = 0.5*(1.0 - numpy.cos(theta_lo)), 0.5*(1.0 - numpy.cos(theta_hi)) 
  return numpy.arccos(1.0 - 2.0*(f_lo + (f_hi - f_lo)*numpy.random.random(n)))

def sample_angle_phi( n, phi_lo=0., phi_hi=2.*numpy.pi, ):
  return phi_lo + (phi_hi - phi_lo)*numpy.random.random(n)

def sample_dir(n):
  cs = numpy.cos(sample_angle_theta(n))
  sn = numpy.sqrt(1. - cs*cs)
  phi = sample_angle_phi(n) 
  return linalg.array2dir( numpy.array( [ sn*numpy.cos(phi), sn*numpy.sin(phi), cs, ]), ).T

# driver function
def main():

  # read command line arguments
  args = get_args(info)

  # print program info
  print( '\n'.join( [ wrinfo(info), wrargs(args), ], ), )

  # load database
  data = dbload(args.db)

  # get atomic mass from DB in MeV
  m1, m2, m3, m4 = load_mass(data,ks.iso1,ks.iso2,ks.iso3,ks.iso4)

  # get reaction heat
  q = (m1 + m2) - (m3 + m4)
  
  # print parameters and column description
  print( '\n'.join( [ wrparticles(ks.iso1,ks.iso2,ks.iso3,ks.iso4), wrmasses(m1,m2,m3,m4), wrheat(q), wrdesc(), ],  ), )

  # get kinetic energy of incident particles in LAB frame
  k1 = sample_ene(args.kt,args.samples)
  k2 = sample_ene(args.kt,args.samples)

  # define direction of propagation of incident particles in LAB frame
  d1 = sample_dir(args.samples)
  d2 = sample_dir(args.samples)
  
  # define angle of dispersion of products in COM frame
  theta = sample_angle_theta(args.samples)
  phi   = sample_angle_phi(args.samples)

  # solve collision
  q1, q2, q3, q4 = collision.solve(m1,m2,m3,m4,k1,k2,d1,d2,theta,phi)
  
  # validate results
  ene_tot_1 = q1[js.ene] + q2[js.ene]
  ene_tot_2 = q3[js.ene] + q4[js.ene]
  res = numpy.where( ene_tot_1 != 0., numpy.abs(1.0 - ene_tot_2/ene_tot_1), ene_tot_2, )

  # kinetic energy of products
  k3 = q3[js.ene] - m3
  k4 = q4[js.ene] - m4

  # direction of propagation of reaction products
  d3 = linalg.array2dir(q3[js.mom])
  d4 = linalg.array2dir(q4[js.mom])
  
  # cosine of the angle between products
  w = 1. - numpy.abs(numpy.sum( d3*d4, axis=-1, ))
 
  # print results
  print( '\n'.join( [ ''.join( [ f' {xi:17.9e}' for xi in xs], ) for xs in zip( k1, k2, k3, k4, w, res, ) ], ), )

  return

# launch program
if __name__=='__main__':
  sys.exit(main())
