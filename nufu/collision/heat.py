
#
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# This file is part of gadgets
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets. If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2021.07.29 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys
import argparse
import json

import numpy

import utils.units as units

# info
info = { 'name': 'heat',
         'desc': 'Compute reaction heat',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }

# program default parameters
defs = { 'db': 'dbnuc.json',
         'angle': numpy.pi/6.0,
         }

# read command line arguments
def get_args(info):

  '''
  Define command line arguments adn parse them
  '''

  # create argument parser
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, {email}'.format(** info))

  # positional arguments
  parser.add_argument( 'reaction', help='Define nuclear reaction in format X(a,b)Y', type=str, )
  
  # optional arguments
  parser.add_argument( '--db', help='Select nuclear database file, default "{}"'.format(defs['db']), default=defs['db'], type=str, )

  return parser.parse_args()

# load database of nuclear data
def dbload(file_name):
  return json.load(open(file_name,'r'))

def reaction_split(r):
  
  l, r = r.split(',')
  
  l = l.replace('(',' ')
  l = l.replace('+',' ')
  
  r = r.replace(')',' ')
  r = r.replace('+',' ')
  
  return l.split(), r.split()

def lookup_mass(ps,db):
  ms = []
  for pi in ps:
    try:
      ms.append(db[pi]['mass_value'])
    except KeyError:
      print('# error :: missing isotope "{}"'.format(pi),file=sys.stderr)
      sys.exit(-1)
  return numpy.array(ms) * units.mass.amu_to_MeV

def qvalue(reaction,db):
  
  # split reaction
  lhs, rhs = reaction_split(reaction)

  # get mass of reactives from DB
  m_init = lookup_mass(lhs,db)

  # get mass of products from DB
  m_exit = lookup_mass(rhs,db)
 
  # get reaction heat
  return numpy.sum(m_init) - numpy.sum(m_exit)

# driver function
def main():

  # read command line arguments
  args = get_args(info)

  # load database
  db = dbload(args.db)

  # get reaction heat
  q = qvalue(args.reaction,db)

  print('# reaction heat Q = {:15.7e} MeV'.format(q))

  return

# launch program
if __name__=='__main__':
  sys.exit(main())
