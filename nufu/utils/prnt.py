
#
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# This file is part of gadgets.
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Module with helper functions for printing
#
# Version control 
# 2021.01.20 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

# script info
info = { 'name': 'prnt',
         'desc': 'Helper module to print',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }


# write a dictionaty
def wrdict(d,header=''):
  s = ''
  if header:
    s = s + '# {} ::\n'.format(header)
  for ki, vi in d.items():
    s = s + '# {:>24s} = {}\n'.format(ki,vi)
  return s

# write info
def wrinfo(info):
  return wrdict(info,'info')

# write arguments
def wrargs(args):
  return wrdict(vars(args),'args')

# write parameters from a dictionary
def wrparam(d):
  return wrdict(d,'parameters')

# initial message of a program
def wrinit(info,args):
  return '\n' + wrdict(info) + '\n' + wrargs(args)

