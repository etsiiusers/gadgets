
#
# Copyright 2020 Universidad Politécnica de Madrid
#
# This file is part of gadgets.
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2022.04.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - vectorize functions
# 2020.12.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys

import numpy

try:
  from .space import dim, sl, dim_x, dim_y, dim_z, num_dim
except ImportError:
  from space import dim, sl, dim_x, dim_y, dim_z, num_dim

info = { 'name': 'linalg',
         'desc': 'Module for linear algebra',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2022,
         'version': [ 1, 1, 0, ],
         'license': 'Copyright (C) 2020-2022 Universidad Politécnica de Madrid',
         }


# normalize an array
def array2dir(w):
  if w.ndim > 1:
    wmod = numpy.linalg.norm( w, axis=-1, )
    return numpy.where( wmod[:,None] != 0., w/wmod[:,None], w, )
  else:
    wmod = numpy.linalg.norm( w, )
    if wmod != 0.:
      return w/wmod
    else:
      return w

# get random direction
def random_dir(num):
    u = 2.*numpy.random.random(num) - 1
    w = numpy.sqrt(1. - u*u)
    phi = 2.*numpy.pi*numpy.random.random(num)
    xs = numpy.array( [ w*numpy.cos(phi), w*numpy.sin(phi), u, ])
    return xs.T

# build rotation matrix for a defined angle around a defined axis
def rotmat(axis,angle):
  
  ax = array2dir(axis)

  cs = numpy.cos(angle)
  sn = numpy.sin(angle)

  q = 1.0 - cs

  uxy = ax[dim_x]*ax[dim_y]
  uxz = ax[dim_x]*ax[dim_z]
  uyz = ax[dim_y]*ax[dim_z]

  r = numpy.zeros( ( num_dim, num_dim, ))
  
  r[0,0] = cs + ax[dim_x]*ax[dim_x]*q
  r[0,1] = uxy*q - ax[dim_z]*sn
  r[0,2] = uxz*q + ax[dim_y]*sn

  r[1,0] = uxy*q + ax[dim_z]*sn 
  r[1,1] = cs + ax[dim_y]*ax[dim_y]*q
  r[1,2] = uyz*q - ax[dim_x]*sn
  
  r[2,0] = uxz*q - ax[dim_y]*sn
  r[2,1] = uyz*q + ax[dim_x]*sn
  r[2,2] = cs + ax[dim_z]*ax[dim_z]*q

  return r

# apply a rotation to vector u for a defined angle and around a defined axis
def rotation(u,axis,angle):
  try:
    return numpy.array( [ numpy.matmul(rotmat(axi,ani),ui) for ui, axi, ani in zip(u,axis,angle) ], )
  except TypeError:
    return numpy.matmul(rotmat(axis,angle),u)

# get a general orthogonal 3-vector
def array_ortho(x):
  return numpy.array( [ x[sl.y] - x[sl.z], 
                        x[sl.z] - x[sl.x], 
                        x[sl.x] - x[sl.y], 
                        ])

#
# test functions
#

def test_rot():

  u  = numpy.array( [ 1.0, 0.0, 0.0, ])
  ax  = numpy.array( [ 0.0, 0.0, 1.0, ])
  phi = numpy.pi/2.0

  v = rotation(u,ax,phi)

  print('# v = {}'.format(v))

  return

if __name__=='__main__':

  # print message
  print('# warning :: this is a python module, it is not intended for standalone run')
  print('#            use "python tester.py {name}" to run test.'.format(** info))
  
  # exit program
  sys.exit()
