
#
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# This file is part of gadgets.
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Version control:
# 2021.04.13 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import numpy
import scipy.stats

# module info
info = { 'name': 'maxwell',
         'desc': 'Define Maxwell-Boltzmann distribution',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }

# Class for Maxwell-Boltzmann distribution based on gamma distributions from 
# scipy
class dist_maxwell_c():
  
  '''
  Maxwell-Boltzmann distribution of particle energy
  '''
  
  def __init__(self,kt):
    

    '''
    Initialization of class

    -> in
    kt: temperature (scale parameter)

    NOTE: 
    - Units of scale parameter must be consistent with the distribution 
      variables. If we use temperature in keV we must pass particle energies 
      in keV to this class
    '''
    
    # save parameters
    self.kt = kt
    assert( self.kt >= 0.0 )

    # create distribution
    self.func = scipy.stats.gamma(a=1.5,scale=self.kt)

    return

  def pdf(self,x):
    return self.func.pdf(x)

  def cdf(self,x):
    return self.func.cdf(x)

  def sample(self,size=1):
    return self.func.rvs(size=size)


# test function for Maxwell-Boltzmann distributions
def test_dist():
  
  '''
  Test Maxwell-Boltzmann distribution
  '''

  kt = 1.0e+00

  fmb = dist_maxwell_c(kt)

  ene = numpy.exp(numpy.linspace(numpy.log(1.0e-03*kt),numpy.log(1.0e+03*kt),100))
  f = fmb.pdf(ene)
  F = fmb.cdf(ene)

  s = ''
  for wi in zip(ene,f,F):
    for wij in wi:
      s = s + ' {:15.7e}'.format(wij)
    s = s + '\n'
  print(s)

  return

# create a sample of values with a Maxwell-Boltzmann distribution
def test_sample():
  
  kt = 1.0e+00

  fmb = dist_maxwell_c(kt)

  sample = fmb.sample(20000)

  hst, edges = numpy.histogram(sample,bins='auto',density=True)
  
  s = ''
  for i in range(len(hst)):
    s = s + ' {:15.7e} {:15.7e} {:15.7e} {:15.7e}\n'.format(edges[i],edges[i+1],hst[i],fmb.pdf(0.5*(edges[i]+edges[i+1])))
  print(s)

  return
    

if __name__=='__main__':
  
  # print message
  print('# warning :: this is a python module, it is not intended for standalone run')
  print('#            use "python tester.py {name}" to run test.'.format(** info))
  
  # exit program
  sys.exit()
