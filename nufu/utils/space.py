
#
# Copyright (C) 2020-2022 Universidad Politécnica de Madrid
#
# This file is part of gadgets.
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2022.01.19 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - minor changes
# 2020.12.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys
import argparse

import numpy

info = { 'name': 'space',
         'desc': 'Module to manage space dimensions',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2022,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2020-2022 Universidad Politécnica de Madrid',
         }

dim = argparse.Namespace(
  x = 0, y = 1, z = 2,
  num = 3,
  )

sl = argparse.Namespace(
  x =  numpy.s_[...,0],
  y =  numpy.s_[...,1],
  z =  numpy.s_[...,2],
  )

dim_x = dim.x
dim_y = dim.y
dim_z = dim.z

num_dim = dim.num

if __name__=='__main__':

  # print message
  print('# warning :: this is a python module, it is not intended for standalone run')
  print('#            use "python tester.py {name}" to run test.'.format(** info))
  
  # exit program
  sys.exit()
