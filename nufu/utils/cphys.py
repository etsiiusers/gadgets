
#
# Copyright (C) 2020 Universidad Politécnica de Madrid
#
# This file is part of gadgets.
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Version control:
# 2019.03.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include several common constants
# 2019.02.23 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import numpy

# module info
info = { 'name': 'cphys',
         'desc': 'Define physical constants',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2020,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2019 Universidad Politécnica de Madrid',
         }

# common class to store constants
class value_c():
  
  '''
  Class to store common physics constants
  '''
  
  def __init__(self,value,error=numpy.nan,units='',sym='',desc=''):

    self.value = value
    self.error = error
    self.units = units

    self.sym   = sym
    self.desc  = desc

    return

  def __str__(self):
    return '{:15.7e} {:15.7e} {:8s} {:8s} {}'.format(self.value,self.error,self.units,self.sym,self.desc)


# speed of light
c_light = value_c( value = 2.99792458e+08, error = numpy.nan, units = 'm/s', sym = 'c', desc = 'speed of light', )

# Avogadro's number
n_avog = value_c( value = 6.02214076e+23, error = numpy.nan, units = 'mol', sym='N', desc = 'Avogadro constant', )

# perfect gas constant
r_univ = value_c( value = 8.314472, error = numpy.nan, units = 'J/K/mol', sym = 'R', desc = 'universal gas constant', )

# planck's constant
h_planck = value_c( value = 6.62607015e-34, error = numpy.nan, units = 'J*s', sym = 'h', desc = 'Planck constant', )
hbar_planck = value_c( value = h_planck.value/2.0e+00/numpy.pi, error = numpy.nan, units = 'J*s', sym = 'hbar', desc = 'reduced Planck constant', )

# elemental charge
elemental_charge = value_c( value = 1.60217662e-19, error = numpy.nan, units = 'C', sym = 'q', desc = 'elemental charge', )

# electron properties
class electron():
  mass   = value_c( value = 9.10938356e-31, error = numpy.nan, units = 'kg', sym = 'me', desc = 'electron mass', )
  charge = value_c( value = -elemental_charge.value, 
                    error = elemental_charge.error, 
                    units = elemental_charge.units, 
                    sym = 'qe', 
                    desc = 'electron charge', )

# proton properties
class proton():
  mass   = value_c( value = 1.6726219e-27, error = numpy.nan, units = 'kg', sym = 'mp', desc = 'proton mass', )
  charge = value_c( value = elemental_charge.value, 
                    error = elemental_charge.error, 
                    units = elemental_charge.units, 
                    sym = 'qp', 
                    desc = 'proton charge', )

# neutron properties
class neutron():
  mass   = value_c( value = 1.674927471e-27, error = numpy.nan, units = 'kg', sym='mn', desc='neutron mass', )
  charge = value_c( value = 0.0e+00, error = numpy.nan, units = 'C', sym = 'qn', desc = 'neutron charge', )

# vacuum EM properties
class vacuum():
  permeability = value_c( value = (4.0e+00*numpy.pi) * 1.00000000082e-07, error = numpy.nan,  units = 'H/m', sym='mu', desc='vacuum permeability', )
  permitivity  = value_c( value = 1.0e+00/c_light.value/c_light.value/permeability.value, error = numpy.nan, units = 'F/m', sym='eps', desc='vacuum permitivity', )

# fine-structure constant
fine_structure = value_c( value = 0.0072973525664, error = numpy.nan, units = '', sym='alpha', desc='fine structure constant', )

# standard gravity
standard_gravity = value_c( value = 9.80665e+00, error = numpy.nan, units = 'm/s/s', sym='g', desc='standdard gravity', )


def test_values():

  '''
  Print a list of variables
  '''

  list_val = [ c_light, 
               r_univ, 
               n_avog, 
               h_planck, hbar_planck, 
               elemental_charge, 
               electron.mass, electron.charge, 
               proton.mass , proton.charge, 
               neutron.mass, neutron.charge, 
               vacuum.permeability, vacuum.permitivity, 
               fine_structure, 
               standard_gravity, 
               ]

  s = ''
  for vi in list_val:
    s = s + ' {}\n'.format(vi)
  print(s)
  
  return

def test_available():
  
  '''
  Get list of available constants
  '''

  def loop_vars(ls):
    s = ''
    for ki, vi in ls.items():
      if isinstance(vi,value_c):
        s = s + '# {:>24s} = {}\n'.format(ki,vi)
      elif type(vi) == type(object):
        s = s + loop_vars(vars(vi))
    return s

  ls = globals()
  
  print(loop_vars(ls))

  return

  

if __name__=='__main__':
  
  # print message
  print('# warning :: this is a python module, it is not intended for standalone run')
  print('#            use "python tester.py {name}" to run test.'.format(** info))
  
  # exit program
  sys.exit()
