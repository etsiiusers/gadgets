
#
# Copyright 2020 Universidad Politécnica de Madrid
#
# This file is part of gadgets.
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2020.10.20 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - show info variable included in each module, if available
# 2020.10.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import os
import sys
import argparse
import importlib

# script info
info = { 'name': 'tester',
         'desc': 'Helper script to evaluate all available test in a module',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2020,
         'version': [ 1, 1, 0, ],
         'license': 'Copyright (C) 2020 Universidad Politécnica de Madrid',
         }


# script default parameters
defs = { 'key': 'test',
         }


# write dictionary
def wrdict(info):
  s = ''
  for ki, vi in info.items():
    s = s + '# {:24s} :: {}\n'.format(ki,vi)
  return s


# create message for a module
def message_on_test(name,info):
  s = '''
# 
# Testing python module "{}"
#

# module info ::

'''.format(name)
  s = s + wrdict(info)
  return s


# do a test
def dotest(name,key,vars,info={}):

  # print some info of the module
  if info:
    print(message_on_test(name,info))

  # create list of test functions from vars
  list_test = { ki : vi for ki, vi in vars.items() if ki.startswith(key) }

  # loop over all test functions available and run them
  for name, func in list_test.items():
    # print info about test function
    print('\n# test :: {}\n'.format(name))
    # launch test function
    func()
  
  return


# add folder to path
def update_path(name_module):
  ws = name_module.split('.')
  dr = './'
  for wi in ws[:-1]:
    dr = os.path.join(dr,wi)
  sys.path.append(dr)
  return


# import module
def load_module(module):
  return importlib.import_module(module)


# read command line arguments and validate them
def get_args(info):

  # cretae argument parser
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, {email}'.format(** info))

  # define positional parameters
  parser.add_argument( 'module', help='Define module name', type=str, )

  # define optional parameters
  parser.add_argument( '--key', help='Define key to detect test functions in module, default "{}"'.format(defs['key']), default=defs['key'], type=str, )

  # parse command line args
  return parser.parse_args()


def launch_test(module,key):
  
  # update sys.path variable to include all folders and subfolders
  update_path(module)

  # import module
  lib = load_module(module)
  
  # run test functions available in module
  dotest(module,key,vars(lib),lib.info)

  return


def main():

  # read arguments
  args = get_args(info)

  # print info
  print(wrdict(info) + '\n' + wrdict(vars(args)))

  # launch test functions inside module
  launch_test(args.module,args.key)
  
  return

# execute program
if __name__=='__main__':
  sys.exit(main())
