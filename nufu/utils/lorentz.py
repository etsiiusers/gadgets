
#
# Copyright 2020 Universidad Politécnica de Madrid
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets. If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2022.04.05 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - vectorize operations
# - fix minor errors
# - redefine com_to_lab and lab_to_com
# 2022.03.21 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - remove unused modules
# - include namespace with indices
# 2020.12.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys
import argparse

import numpy

try:
  from .space import dim, sl, dim_x, dim_y, dim_z, num_dim
except ImportError:
  from space import dim, sl, dim_x, dim_y, dim_z, num_dim

# module info
info = { 'name': 'lorentz',
         'desc': 'Module with Lorentz transformations',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2020,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2020 Universidad Politécnica de Madrid',
         }

#
# units
#
# This script works with all magnitudes in terms of energy
#
# momentum: (pc)
# energy: E
# mass: (mc^2)
# velocity: (v/c), 0 <= v <= 1
#

#
# variables
#

js = argparse.Namespace(
  
  ene = numpy.s_[...,0],
  
  px = numpy.s_[...,1], 
  py = numpy.s_[...,2], 
  pz = numpy.s_[...,3],
  
  mom = numpy.s_[...,1:],
  
  num = 4,
  )

# 
# relativistic parameters
#

# squared beta parameter, beta^2 = (v/c)^2
def beta2(v):
  return numpy.sum( v*v, axis=-1, )

# beta parameter, beta = v/c
def beta(v):
  return numpy.sqrt(beta2(v))

# gamma parameter, gamma = 1/sqrt(1 - b^2), where b is the beta parameter
def gamma(v):
  return 1.0/numpy.sqrt(1.0 - beta2(v))

# return beta and gamma parameters
def bg(v):
  b2 = beta2(v)
  g = 1.0/numpy.sqrt(1.0 - b2)
  return numpy.sqrt(b2), g

#
# Lorentz transformations
#

# Lorentz transformation matrix for a boost in arbitrary direction
def boost_matrix(v):
  
  '''
  Compute the Lorentz transformation matrix for an arbotrary boost

  -> in
  v: boost of the Lorentz transformation, array(3) of floats

  <- out
  (1) l, matrix for the transformation, array(4,4) of floats
  '''
  
  l = numpy.zeros( ( dim.num + 1, dim.num + 1, ), )
  
  b, g = bg(v)
  gm1 = g - 1.0
  
  n = numpy.copy(v)
  
  n2 = numpy.dot(n,n)
  if n2 != 0.0:
    n = n/numpy.sqrt(n2)

  nxy = n[dim.x]*n[dim.y]
  nxz = n[dim.x]*n[dim.z]
  nyz = n[dim.y]*n[dim.z]

  l[0,0] = g
  l[0,1] = -b*g*n[dim.x]
  l[0,2] = -b*g*n[dim.y]
  l[0,3] = -b*g*n[dim.z]
  
  l[1,0] = -b*g*n[dim.x]
  l[1,1] = 1.0 + gm1*n[dim.x]*n[dim.x]
  l[1,2] = gm1*nxy
  l[1,3] = gm1*nxz
  
  l[2,0] = -b*g*n[dim.y]
  l[2,1] = gm1*nxy
  l[2,2] = 1.0 + gm1*n[dim.y]*n[dim.y]
  l[2,3] = gm1*nyz

  l[3,0] = -b*g*n[dim.z]
  l[3,1] = gm1*nxz
  l[3,2] = gm1*nyz
  l[3,3] = 1.0 + gm1*n[dim.z]*n[dim.z]
  
  return l

# class to evaluate Lorentz transformations between two inertial frames
class boost_c():
  
  '''
  Compute the Lorentz transformation for an arbitrary boost
  '''
  
  def __init__(self,v):
    
    '''
    Initialize transformation matrix

    -> in
    v: boost of the Lorentz transformation, array [3] of floats

    <- out
    (1) l, matrix for the transformation, array [4,4] of floats
    '''
    
    self.v = v

    self.dm = boost_matrix( v)
    self.im = boost_matrix(-v)

    return
  
  def deval(self,* qs):
    
    '''
    Apply Lorentz direct transformation of frame for a defined boost

    -> in
    qs: tuple of 4-vectors to transform

    <- out
    (1) tuple of transformed 4-vectors
    '''

    return [ numpy.matmul(self.dm,qi) for qi in qs ]

  def ieval(self,* qs):
    
    '''
    Apply Lorentz inverse transformation of frame for a defined boost

    -> in
    qs: tuple of 4-vectors to transform

    <- out
    (1) tuple of transformed 4-vectors
    '''
   
    return [ numpy.matmul(self.im,qi) for qi in qs ]


# conver 3-vector momentum to 4-vector coordinates
def pc2vector(m,pc):
  return numpy.array( [ pc2ene(m,pc), pc[sl.x], pc[sl.y], pc[sl.z], ]).T


# get energy from momentum and rest mass
def pc2ene(m,pc):
  assert( m >= 0.0 )
  return numpy.sqrt(m*m + numpy.sum( pc*pc, axis=-1, ))

# get momentum from energy and rest mass
def ene2pc(m,e):
  assert( m >=0.0 and e >= m )
  return numpy.sqrt(e*e - m*m)

# get kinetic energy from momentum and rest mass
def pc2kin(m,pc):
  assert( m >= 0.0 )
  kin = pc2ene(m,pc) - m
  assert( kin >= 0.0 )
  return kin

# get momentum from kinetic energy and rest mass
def kin2pc(m,kin):
  assert( m >= 0.0 )
  return numpy.sqrt(kin*(kin + 2.0*m))



# 
# metric tensor
#

def metric_tensor(): 
  g = numpy.identity(js.num)
  g[0,0] = -1.0
  return -g

def metric_dot(u,v):
  g = metric_tensor()
  return numpy.matmul(u.T,numpy.matmul(g,v))


#
# change of frame
#

# get tarnsformation operation from LAB to COM
def boost_to_com(* qs):
  # 4-vector momentum of COM in lab frame
  qc_lab = numpy.sum( numpy.stack( [ qi for qi in qs ], axis=0, ), axis=0, )
  # get velocity of COM (boost for lorentz transformation) in lab frame
  vc_lab = qc_lab[js.mom]/qc_lab[js.ene]
  # boost to transform from Lab frame to COM frame
  return boost_c(vc_lab)

# from LAB to COM
def lab_to_com(bs,* qs):
  return bs.deval(* qs)

# from COM to LAB
def com_to_lab(bs,* qs):
  return bs.ieval(* qs)

#
# Test functions
#

def test_frame2():
  
  '''
  Test changes of frame
  '''
  
  def qrand():
    mc2 = 2.0*numpy.random.random_sample() + 1.0
    pc  = 2.0*numpy.random.random_sample( ( num_dim, )) - 1.0
    return pc2vector(mc2,pc)

  q1_lab = qrand()
  q2_lab = qrand()
  
  bs = boost_to_com(q1_lab,q2_lab)

  q1_com, q2_com = lab_to_com(bs,q1_lab,q2_lab)

  q11_lab, q22_lab = com_to_lab(bs,q1_com,q2_com)

  dq1 = q1_lab - q11_lab
  dq2 = q2_lab - q22_lab

  print('# dq1 = {}'.format(dq1))
  print('# dq2 = {}'.format(dq2))

  return

def test_frame3():
  
  '''
  Test changes of frame
  '''
  
  def qrand():
    mc2 = 2.0*numpy.random.random_sample() + 1.0
    pc  = 2.0*numpy.random.random_sample( ( num_dim, )) - 1.0
    return pc2vector(mc2,pc)

  q1_lab = qrand()
  q2_lab = qrand()
  q3_lab = qrand()
  
  bs = boost_to_com(q1_lab,q2_lab,q3_lab)

  q1_com, q2_com, q3_com = lab_to_com(bs,q1_lab,q2_lab,q3_lab)

  q11_lab, q22_lab, q33_lab = com_to_lab(bs,q1_com,q2_com,q3_com)

  dq1 = q1_lab - q11_lab
  dq2 = q2_lab - q22_lab
  dq3 = q3_lab - q33_lab

  print('# dq1 = {}'.format(dq1))
  print('# dq2 = {}'.format(dq2))
  print('# dq3 = {}'.format(dq3))

  return


if __name__ == '__main__':

  # print message
  print('# warning :: this is a python module, it is not intended for standalone run')
  print('#            use "python tester.py {name}" to run test.'.format(** info))
  
  # exit program
  sys.exit()
