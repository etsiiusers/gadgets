
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for rates module
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.22 :: Manuel Cotelo Ferreiro
# - initial version
#

import sys

import argparse

import numpy
import scipy.integrate

import libnufu.nc as nc
import libnufu.xs as xs
import libnufu.rates as rates

#info
desc  = 'Test program for rates module'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'dbxs': './data_xs/dbxs.h5',
         'dbnc': './data_nc/dbnuc.json',
         }

def get_program_args():

  '''
  Define command line arguments
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--dbxs', help='Select cross section database', type=str, default=defs['dbxs'], )
  parser.add_argument( '--dbnc', help='Select nuclear database', type=str, default=defs['dbnc'], )

  return parser.parse_args()


def main():
  
  # read command line argument
  args = get_program_args()

  # load databases
  db_nc = nc.db_c(args.dbnc)
  db_xs = xs.db_c(args.dbxs)

  # select all available reactions in database
  list_reac = db_xs.keys()

  # option, keep temperature constant and do not update it with deposited energy
  tem_static = True

  # build object to compute rates
  func_rates = rates.rates_c(list_reac,db_nc,db_xs,tem_static)

  # define composition
  x_h2 = 0.50 # 50% of particles is D
  x_h3 = 0.50 # 50% of particles is T
  n = 1.0e+30 # total density of particles, particles/m3
  n_h2 = n*x_h2 # density of D
  n_h3 = n*x_h3 # density of T

  # select initial particles
  p_h2 = func_rates.list_particles['h2']
  p_h3 = func_rates.list_particles['h3']
  
  # initialize state
  u_init = func_rates.array_zeros()
  u_init[p_h2.id] = n*x_h2
  u_init[p_h3.id] = n*x_h3
  u_init[-1] = 1.0e+04 # initial temperature, in eV

  # integrate rates
  t_begin = 0.0e+00
  t_end   = 1.0e-06 # in s
  #ans = scipy.integrate.solve_ivp(func_rates,( t_begin, t_end, ),u_init)
  ans = scipy.integrate.solve_ivp(func_rates,( t_begin, t_end, ),u_init,method='BDF',jac=None)
  #ans = scipy.integrate.solve_ivp(func_rates,( t_begin, t_end, ),u_init,method='LSODA',jac=None)

  # print solution
  for i, ti in enumerate(ans.t):
    print(' {:15.7e}'.format(ti),end='')
    utot = numpy.sum(ans.y[:-1,i])
    for uij in ans.y[:-1,i]:
      print(' {:15.7e}'.format(uij/utot),end='')
    print(' {:15.7e}'.format(ans.y[-1,i]),end='')
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
