
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module nc.py
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.02.25 :: Manue Cotelo Ferreiro
# - initial version
#

import sys
import argparse

import libnufu.nc as nc

# info
desc = 'Test program for module nc'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# default program parameters
defs = { 'db': './data_nc/dbnuc.json',
         }

def get_program_args():
  
  '''
  Parse command line arguments and define program allowed arguments
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)
  parser.add_argument( '--db', help='Select database file, default "{}"'.format(defs['db']), type=str, default=defs['db'], )

  return parser.parse_args()

def main():
  
  # read command line arguments
  args = get_program_args()

  # load database
  data = nc.db_c(args.db)

  # make list of elements
  list_elem = {}
  for ki, vi in data.items():
    s = vi['sym']
    ss = s[0].upper() + s[1:]
    list_elem[ss] = ''

  s = '[ '
  for ki in list_elem.keys():
    s += '\'{}\', '.format(ki)
  s += ']'
  print(s)

  return 0

if __name__=='__main__':
  sys.exit(main())
