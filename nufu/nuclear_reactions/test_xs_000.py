
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module xs
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.23 :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

import numpy

import libnufu.xs as xs

# program info
desc = 'Test module xs'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

def print_db(db,fd=sys.stdout):
  for ki in db:
    item = db[ki]
    sr = '{}'.format(item['reac'])
    sp = '{}'.format(item['prod'])
    print('# reaction :: {:24s} {:24s} {:24s}'.format(ki,sr,sp),file=fd)
  print()
  return

def driver(file_name):
  db = xs.db_c(file_name)
  print_db(db)
  return

def main():
  list_files = [ './data_xs/xs.xml', './data_xs/dbxs.h5', ]
  list(map(driver,list_files))
  return 0

if __name__=='__main__':
  sys.exit(main())
