
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Program to convert CSV data to XML format
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Versin control:
# 2019.02.24 :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

import csv
import xml.etree.cElementTree as et

import util.xs as xs

xsdata = {}

def add_data(fname):
	key, data = read_data_csv(fname)
	xsdata[key] = data 
	return

def read_data_csv(fname):
	try:
		with open(fname,'r') as fd:
			return get_csv_reac(fd), get_csv_xs(fd)
	except OSError:
		print(' ERROR :: impossible to open XS file {}'.format(fname))
	return

def is_csv_comment(line):
	if line[0] == '#':
		return True
	return False

def get_csv_reac(fd):
	key_reac = 'reac'
	fd.seek(0)
	for line in fd:
		if is_csv_comment(line) and (key_reac in line):
			words = line.split(':',1)
			return words[-1].strip()
	return None

def get_csv_xs(fd):
	fd.seek(0)
	rdr = csv.reader(fd,delimiter=';')
	x = []
	for row in rdr:
		if not is_csv_comment(row[0]):
			x.append((float(row[0]), float(row[1]), ))
	return x 

def tostr(x):
	return '{:.6e}'.format(x)

def main():
	basedir = './data_xs'
	csv_suffix = '.csv'

	files_csv = [ os.path.join(basedir,fl) for fl in os.listdir(basedir) if csv_suffix in fl ]
	
	for fl in files_csv:
		add_data(fl)	

	root = et.Element('doc')
	
	for r in xsdata:
		item = et.SubElement(root, xs.__k_item, {xs.__k_reaction: r, })
		unit = et.SubElement(item, xs.__k_xs_unit, {xs.__k_energy_uni: 'eV', xs.__k_sigma_uni: 'b', })
		for p in xsdata[r]:
			point = et.SubElement(item, xs.__k_xs_val, {xs.__k_energy: tostr(p[0]), xs.__k_sigma: tostr(p[1]), })

	file_xml = os.path.join(basedir,'xs.xml')

	tree = et.ElementTree(root)
	tree.write(file_xml,xml_declaration=True)

	return 0

if __name__=='__main__':
	sys.exit(main())
