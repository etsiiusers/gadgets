
#
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with it.  If not, see <https://www.gnu.org/licenses/>.
#
# Versin control:
# 2021.07.02 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import os
import sys
import argparse
import csv
import json

import numpy

# program info
info = { 'name': '',
         'desc': '',
         'author': '',
         'email': 'manuel.cotelo@upm.es',
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }

# default parameters
defs = { 'base': 'files_csv', 
         'suffix': 'csv',
         'out': 'xs.json',
         }

def read_data_csv(file_name):
	try:
		with open(file_name,'r') as fd:
			return get_csv_reac(fd), get_csv_xs(fd)
	except OSError:
		print(' ERROR :: impossible to open XS file {}'.format(file_name))
	return

def is_csv_comment(line):
	if line[0] == '#':
		return True
	return False

def get_csv_reac(fd):
	key_reac = 'reac'
	fd.seek(0)
	for line in fd:
		if is_csv_comment(line) and (key_reac in line):
			words = line.split(':',1)
			return words[-1].strip()
	return None

def get_csv_xs(fd):
	fd.seek(0)
	rdr = csv.reader(fd,delimiter=';')
	x = []
	for row in rdr:
		if not is_csv_comment(row[0]):
			x.append( [ float(row[0]), float(row[1]), ])
	return numpy.array(x) 

def load_data(file_name):

  k, v = read_data_csv(file_name)

  rs, ps = k.split(',')

  rs = rs.replace('+',' ')
  rs = rs.replace('(',' ')
  
  ps = ps.replace('+',' ')
  ps = ps.replace(')',' ')

  data = { 'energy': [ vi for vi in v[:,0] ],
           'energy_unit': 'eV',
           'sigma': [ vi for vi in v[:,1] ],
           'sigma_unit': 'b',
           'reac': rs.split(),
           'prod': ps.split(),
           }

  return { k: data, }

def select_files(base,suffix):
  end = '.{}'.format(suffix)
  return [ os.path.join(base,fl) for fl in os.listdir(base) if fl.endswith(end) ]

# get command line arguments
def get_args(info,defs):
  
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license} ({email})'.format(** info))

  parser.add_argument('--base', help='', default=defs['base'], type=str, )
  parser.add_argument('--suffix', help='', default=defs['suffix'], type=str, )
  parser.add_argument('--out', help='', default=defs['out'], type=str, )

  return parser.parse_args()

# driver functions
def main():
  
  args = get_args(info,defs)

  xsdata = {}
  for fl in select_files(args.base,args.suffix):
    xsdata.update(load_data(fl))

  try:
    with open(args.out,'w') as fd:
      json.dump(xsdata,fd)
  except OSError:
    print('# error :: impossible to create file "{}"'.format(args.out),file=sys.stderr)

  return 0

if __name__=='__main__':
  sys.exit(main())
