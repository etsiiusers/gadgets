
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for XS average
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.22 :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

import argparse

import numpy

import libnufu.nc as nc
import libnufu.xs as xs
import libnufu.units as units
import libnufu.mesh as mesh

desc  = 'Test program for XS average with temperature'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

defs = { 'inpxs': './data_xs/dbxs.h5',
         'inpnc': './data_nc/dbnuc.json',
         'id_reac': 'h2(h3,n1)he4',
         }

def get_program_args(parser=argparse.ArgumentParser()):
  '''
  Define command line arguments
  '''
  parser.add_argument( '--inpxs', help='Select cross section database', type=str, default=defs['inpxs'], )
  parser.add_argument( '--inpnc', help='Select nuclear database', type=str, default=defs['inpnc'], )
  parser.add_argument( '--id_reac', help='Select reaction', type=str, default=defs['id_reac'], )
  return parser.parse_args()

def main():
  
  # read command line argument
  args = get_program_args(argparse.ArgumentParser(description=desc,epilog=eplg))

  # load XS DB
  list_xs = xs.db_c(args.inpxs)

  # load Nuclear DB
  list_nc = nc.db_c(args.inpnc)

  # select data for a defined reaction
  id_reac = args.id_reac
  sigma = list_xs[id_reac]
  func_sigma = sigma['interp']

  # set reduced mass of the reactives
  p1 = sigma['reac'][0]
  p2 = sigma['reac'][1]
  m1 = list_nc[p1]['mass_value']*units.mass.amu_to_eV
  m2 = list_nc[p2]['mass_value']*units.mass.amu_to_eV
  mu = (m1*m2)/(m1+m2)
  
  # build a temperature mesh
  kt = { 'size': 200,
         'lo': 1.0e+00, # in eV
         'hi': 1.0e+06, # in eV
         'type': 'log',
         }
  kt['values'] = mesh.build(kt)

  # average XS for different temperatures
  for kti in kt['values']:
    sv = xs.xs_average(mu,kti,func_sigma)
    print(' {:15.7e} {:15.7e}'.format(kti,sv))

  return 0

if __name__=='__main__':
  sys.exit(main())
