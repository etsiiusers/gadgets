
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module nc.py
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.04.03 :: Manue Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import libnufu.nc as nc

# info
desc = 'Test program for module nc'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# default program parameters
defs = { 'db': './data_nc/dbnubase12.json',
         }

def get_program_args():
  
  '''
  Parse command line arguments and define program allowed arguments
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( 'z', help='Select atomic number', type=int, )
  parser.add_argument( '--db', help='Select database file, default "{}"'.format(defs['db']), type=str, default=defs['db'], )

  return parser.parse_args()

def main():
  
  # read command line arguments
  args = get_program_args()

  # load database
  data = nc.db_nubase_c(args.db)

  # select isotopes with defined Z
  for ki, vi in data.items():
    if ki != 'info':
      if vi['Z'] == args.z:
        print('# sym z mass be :: {:5} {:3} {:15.7e} {:15.7e}'.format(ki,vi['Z'],data.mass(ki),data.be(ki)))

  return 0

if __name__=='__main__':
  sys.exit(main())
