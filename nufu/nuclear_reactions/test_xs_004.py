
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for XS models
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.22 :: Manuel Cotelo Ferreiro
# - initial version
#

import sys

import argparse

import libnufu.units as units
import libnufu.mesh as mesh
import libnufu.xs as xs
import libnufu.xsmodel as xsmodel

#info
desc  = 'Test program for XS models'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'inpxs': './data_xs/dbxs.h5',
         'id_reac': 'h2(h2,n1)he3',
         }

def get_program_args():

  '''
  Define command line arguments
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--inpxs', help='Select cross section database', type=str, default=defs['inpxs'], )
  parser.add_argument( '--id_reac', help='Select reaction', type=str, default=defs['id_reac'], )

  return parser.parse_args()


def main():
  
  # read command line argument
  args = get_program_args()

  # load database
  db = xs.db_c(args.inpxs)

  # get reaction data
  id_reac = args.id_reac

  # XS interpolator
  func_data = db[id_reac]['interp']

  # build cross-section model
  func_m1 = xsmodel.list_douane[id_reac]
  func_m2 = xsmodel.list_xs[id_reac]

  # create energy mesh
  ene = { 'size': 1000,
          'lo': 1.0e+00, # in eV
          'hi': 1.0e+08, # in eV
          'type': 'log',
          }
  ene['values'] = mesh.build(ene)

  for ei in ene['values']:
    fi = func_data(ei)
    g1i = func_m1(ei*units.energy.eV_to_keV)
    g2i = func_m2(ei*units.energy.eV_to_keV)
    print(' {:15.7e} {:15.7e} {:15.7e} {:15.7e}'.format(ei,fi,g1i,g2i))
  print()

  '''
  x = db[id_reac]['values']
  for ei, si in zip(x[:,0],x[:,1]):
    print(' {:15.7e} {:15.7e}'.format(ei,si))
  print()
  '''

  return 0

if __name__=='__main__':
  sys.exit(main())
