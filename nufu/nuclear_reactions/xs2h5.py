
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Program to convert old nuclear fusion cross sections database in XML format 
# to HDF5 
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.23 :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

import argparse

import h5py
import numpy

import libnufu.xs as xs

# program info
desc = 'Convert nuclear DB in XML format to HDF5'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default values
defs = { 'file_inp': 'xs.xml',
         'file_out': 'dbxs.h5',
         }

def get_program_args():

  '''
  Get command line arguments and define program options
  '''

  # create parser
  parser = argparse.ArgumentParser(description=desc,epilog=eplg)
  
  # define allowed program options
  parser.add_argument( '--inp', help='input file name with DB in XML format, default "{}"'.format(defs['file_inp']), type=str, default=defs['file_inp'], )
  parser.add_argument( '--out', help='outut file name, default "{}"'.format(defs['file_out']), type=str, default=defs['file_out'], )
  
  # parse command dlien arguments
  return parser.parse_args()

def main():

  '''
  Main program function
  '''

  # read command line arguments
  args = get_program_args()

  # nuclear data initialization from XML file
  db_xs = xs.db_c(args.inp)

  # open HDF5 file
  h5f = h5py.File(args.out,'w')

  # iterate over data and print to HDF5 file
  for ki, vi in db_xs.db.items():
    # save data
    dset = h5f.create_dataset(ki, data=numpy.array(vi['values']), compression="gzip", )
    # add info
    dset.attrs['name'] = ki
    dset.attrs['energy_unit'] = vi['energy_unit']
    dset.attrs['sigma_unit'] = vi['sigma_unit']

  return 0

if __name__=='__main__':
  sys.exit(main())
