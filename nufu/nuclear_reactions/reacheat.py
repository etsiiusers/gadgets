
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Program to compute reaction heat
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.23 :: Manuel Cotelo Ferreiro
# - initial version
#

import sys
import itertools
import argparse

import libnufu.units as units
import libnufu.nc as nc
import libnufu.xs as xs

# program info
desc = 'Compute reaction heat'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'inpnc': './data_nc/dbnuc.json',
         }

def mass_delta(r,p,db):
  
  '''
  Compute mass difference between reactives and reaction products
  
  -> in
  r: list of reactives
  p: list of products
  db: nuclear database

  <- out
  mass difference, in amu
  '''

  # get total mass of reactives
  mr = 0.0e+00
  for ri in r:
    mr += db[ri]['mass_value']
  # get total mass of reaction products
  mp = 0.0e+00
  for pi in p:
    mp += db[pi]['mass_value']
  # mass difference
  dm = mr - mp
  return dm

def driver(id_reac,db):
  
  '''
  Compute reaction heat and print results

  -> in
  id_reac: string with reaction description
  db: database with nuclear data

  <- out
  reaction heat, in MeV
  '''

  # get list of particles
  rp = xs.particles(id_reac)
  # reaction heat, based in mass difference
  dm = mass_delta(rp['reac'],rp['prod'],db)
  q = dm*units.mass.amu_to_keV
  # print results
  sr = '{}'.format(rp['reac'])
  sp = '{}'.format(rp['prod'])
  print('# {:24s} :: {:24s} {:24s} -> {:17.9e} keV ({:17.9e}) amu'.format(id_reac,sr,sp,q,dm))
  # return reaction heat
  return q

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( 'id', help='Nuclear reaction description', type=str, nargs='*', )
  parser.add_argument( '--inpnc', help='Select nuclear database', type=str, default=defs['inpnc'], )

  return parser.parse_args()

def main():

  '''
  Program main driver routine
  '''

  # read command line arguments
  args = get_program_args()
  assert( len(args.id) > 0 )

  # load nuclear database
  db = nc.db_c(args.inpnc)

  # print basic info
  print()
  print('# {}'.format(desc))
  print('# {}'.format(eplg))
  print()

  # apply driver to all reactions
  list(map(driver,args.id,itertools.repeat(db)))

  return 0

if __name__ == '__main__':
    sys.exit(main())
