
#
# This file is part of GADGETS
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module nc.py
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.04.03 :: Manue Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

import libnufu.nc as nc

desc = 'Test program for module nc and read nuclear properties from database'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

def main():
  # list of different files with nuclear database
  file_name = 'data_nc/dbnubase12.json'
  # load DB
  data = nc.db_nubase_c(file_name)
  # print DB
  print(data)
  return 0

if __name__=='__main__':
  sys.exit(main())
