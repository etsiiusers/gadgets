
import os
import sys

import libnufu.mesh
import libnufu.interp

desc = 'Test program for module interp'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

def func(x):
  return x*(x + 1.0)

def main():

  x = { 'lo': 1.0e-03,
        'hi': 1.0e+03,
        'size': 200,
        'type': 'log',
        }
  x['values'] = util.mesh.build(x)

  xi = x['values']
  fi = func(x['values'])

  list_func = [ ( 'orig', func, ),
                ( 'lili', util.interp.func_interp_lili_c(xi,fi), ),
                ( 'lilo', util.interp.func_interp_lilo_c(xi,fi), ),
                ( 'loli', util.interp.func_interp_loli_c(xi,fi), ),
                ( 'lolo', util.interp.func_interp_lolo_c(xi,fi), ),
                ]

  x0 = x
  x0['size'] = 4*x0['size']
  x0['values'] = util.mesh.build(x0)

  for xi in x0['values']:
    print(' {:17.9e}'.format(xi),end='')
    for ni, fi in list_func:
      print(' {:17.9e}'.format(fi(xi)),end='')
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
