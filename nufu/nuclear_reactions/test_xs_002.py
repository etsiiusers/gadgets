
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for XS average
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.22 :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

import argparse

import libnufu.mesh as mesh
import libnufu.units as units
import libnufu.nc as nc
import libnufu.xs as xs

# info
desc  = 'Test program for XS average with temperature'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'inpxs': './data_xs/dbxs.h5',
         'inpnc': './data_nc/dbnuc.json',
         }

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--inpxs', help='Select cross section database', type=str, default=defs['inpxs'], )
  parser.add_argument( '--inpnc', help='Select nuclear database', type=str, default=defs['inpnc'], )

  return parser.parse_args()

def main():
  
  # read command line argument
  args = get_program_args()

  # load nuclear database
  nudb = nc.db_c(args.inpnc)

  # load XS file
  list_xs = xs.db_c(args.inpxs)

  # set reduced mass of the reactives
  mu = (2.0e+00*930.0e+06)/2.0e+00 # in eV
  
  # build a temperature mesh
  kt = { 'size': 100,
         'lo': 1.0e-01, # in eV
         'hi': 1.0e+08, # in eV
         'type': 'log',
         }
  kt['values'] = mesh.build(kt)

  # get reduced mass
  list_mu = {}
  for ki, vi in list_xs.items():
    # get reduced mass, in eV
    reac = vi['reac']
    m1 = nudb[reac[0]]['mass_value']*units.mass.amu_to_eV
    m2 = nudb[reac[0]]['mass_value']*units.mass.amu_to_eV
    list_mu[ki] = m1*m2/(m1 + m2)

  # print output heeader
  count = 0
  print('# column_{:03d} = energy, in eV'.format(count))
  count += 1
  for ki in list_xs:
    print('# column_{:03d} = cross section for reaction {}, in b'.format(count,ki))
    count += 1

  # average XS for different temperatures for all available reactions
  for kti in kt['values']:
    # print energy, in eV
    print(' {:15.7e}'.format(kti),end='')
    # iterate over available reactions
    for ki, vi in list_xs.items():
      # do average
      sv = xs.xs_average(list_mu[ki],kti,vi['interp'])
      # print averaged cross section, in m3/s
      print(' {:15.7e}'.format(sv),end='')
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
