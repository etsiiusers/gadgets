
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Define an analytical model for fusion XS
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
#
# 2019.02.22 :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

import numpy 

desc = 'Analytical models for fusion XS'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

class model_douane_c():
  
  def __init__(self,par):
    '''
    Construct average XS evaluator

    -> in
    par: parameters for the Douane model
    '''
    self.par = par
    return

  def eval(self,kt):
    '''
    Averaged XS evaluation: XS in averaged with a Maxwell-Boltzmann 
    distribution of the reacting particles at a defined temperature

    -> in
    kt: temperature, in keV

    <- out
    Averaged XS, in b
    '''
    assert( kt > 0.0e+00 )
    w = self.par[3] - self.par[2]*kt
    w = self.par[4] + self.par[1]/(1.0e+00 + w*w)
    w = w/(kt*(numpy.exp(self.par[0]/numpy.sqrt(kt)) - 1.0e+00))
    return w

  def __call__(self,kt):
    return self.eval(kt)

list_douane = { 'h2(h2,h1)h3': model_douane_c( [ 46.097, 372.0, 4.36e-04, 1.22, 0.0, ]),
                'h2(h2,n1)he3': model_douane_c( [ 47.88, 482.0, 3.08e-04, 1.177, 0.0, ]),
                'h2(h3,n1)he4': model_douane_c( [ 45.95, 50200.0, 1.368e-02, 1.076, 409.0, ]),
                'h2(he3,p1)he4': model_douane_c( [ 89.27, 25900.0, 3.98e-03, 1.297, 647.0, ]),
                }

class model_xs_c():

  def __init__(self,s0,b):
    '''
    Construct model evaluation

    -> in:
    s0: constant for the astrophysical function, in barn*keV
    b: Gamow peak parameter, in (keV)^(1/2)
    '''
    self.s0 = s0 
    self.b = b
    return

  def eval(self,ene):
    '''
    XS evaluation

    -> in
    ene: energy, in keV

    <- out
    XS, in barn
    '''
    assert( ene > 0.0e+00 )
    return (self.s0/ene)*numpy.exp(- self.b/numpy.sqrt(ene))

  def __call__(self,ene):
    return self.eval(ene)

list_xs = { 'h2(h2,p)h3': model_xs_c(53.0,31.4),
            'h2(h2,n1)he3': model_xs_c(53.0,31.4),
            'h2(h3,n1)he4': model_xs_c(11000.0,34.4),
            'h2(he3,p1)he4': model_xs_c(7000.0,88.8),
            'li6(p,he3)he4': model_xs_c(5500,87.2),
            'li6(h2,he4)he4': model_xs_c(3060.0,104.0),
            'li7(h1,he4+he4)he4': model_xs_c(125.0,88.1),
            'b11(h1,he4+he4)he4': model_xs_c(100000.0,150.3),
            }
