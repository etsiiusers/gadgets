
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Compute nuclear reaction rates
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.25 :: Manuel Cotelo Ferreiro
# - initial version
#

import sys

import numpy

try:
  import nc
except ImportError:
  import libnufu.nc as nc

try:
  import xs
except ImportError:
  import libnufu.xs as xs

try:
  import xsmodel
except ImportError:
  import libnufu.xsmodel as xsmodel

try:
  import units
except ImportError:
  import libnufu.units as units

try:
  import mesh
except ImportError:
  import libnufu.mesh as mesh

try:
  import interp
except ImportError:
  import libnufu.interp as interp

try:
  import cphys
except ImportError:
  import libnufu.cphys as cphys

# module info
desc = 'Compute reaction rates'
author = 'Manuel Cotelo Ferrreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

class RatesError(Exception):

  def __init__(self,msg):
    Exception.__init__(self,msg)

class particle_c():

  def __init__(self,name,id,mass):
    self.name = name
    self.id = id
    self.mass = mass
    return

  def __str__(self):
    return ' {:3d} {:12s} {:17.9e}'.format(self.id,self.name,self.mass)

class reaction_c():

  def __init__(self,name,ridx,pidx,func_coeff):
    self.name = name
    self.ridx = ridx
    self.pidx = pidx
    self.func_coeff = func_coeff
    return

  def __str__(self):
    sr = '{}'.format(self.ridx)
    sp = '{}'.format(self.pidx)
    return ' {:18s} {:10s} {:10s} {}'.format(self.name,sr,sp,self.func_coeff)

  def coeff(self,u):
    return self.func_coeff(u)

class cross_c():
 
  _kt_default = { 'lo': 1.0e-02, # in eV
                  'hi': 1.0e+06, # in eV
                  'size': 120,
                  'type': 'log',
                  }

  _sigma_min = 1.0e-64

  def __init__(self,mu,func_sigma,kt=_kt_default):
    # compute energy mesh
    kt['values'] = mesh.build(kt)
    # compute averaged cross sections
    x = []
    f = []
    for kti in kt['values']:
      val = xs.xs_average(mu,kti,func_sigma)
      if (kti > 0.0e+00 ) and (val > self._sigma_min):
        x.append(kti)
        f.append(val)
    # create an interpolator
    if (len(x) > 2) and (len(x) == len(f)):
      self.func = interp.func_interp_lolo_c( x, f, fill='extrapolate', )
    else:
      self.func = interp.func_interp_lolo_c( [ 1.0e-09, 1.0e+09, ], [ self._sigma_min, self._sigma_min, ], fill='extrapolate', )
    return

  def __call__(self,x):
    return self.func(x)

class rates_c():

  '''
  Object to compute rates of nuclear reactions
  '''

  def __init__(self,list_reac,db_nuc,db_xs,tem_static=False):
   
    # flag to update temperature based on deposited energy
    self.tem_static = tem_static

    # initialize lists
    self.list_particles = {}
    self.list_reactions = []
    
    # initialize counter of particles
    num_particles = 0
    
    # make list of particles and reactions
    for reac in list_reac:
      
      rp = xs.particles(reac)
      
      # update list of particles
      for _, pi in rp.items():
        for pij in pi:
          if not pij in self.list_particles:
            mass = db_nuc[pij]['mass_value']*units.mass.amu_to_eV
            self.list_particles[pij] = particle_c(pij,num_particles,mass)
            num_particles += 1
      
      # update list of reactions
      ridx = [] # list of reactives
      for ri in rp['reac']:
        ridx.append(self.list_particles[ri].id)
      pidx = [] # list of products
      for pi in rp['prod']:
        pidx.append(self.list_particles[pi].id)
      
      # build XS functors
      mn = 1.0e+00
      md = 0.0e+00
      for ri in rp['reac']:
        mi = self.list_particles[ri].mass
        mn *= mi
        md += mi
      mu = mn/md
      fxs = cross_c(mu,db_xs[reac]['interp'])
      
      # add reaction
      self.list_reactions.append(reaction_c(reac,ridx,pidx,fxs))

    # some output
    print('# num_particles = {}'.format(len(self.list_particles)))
    for ki, pi in self.list_particles.items():
      print('# particle :: {}'.format(pi))
    print()
    print('# num_reactions = {}'.format(len(self.list_reactions)))
    for ri in self.list_reactions:
      print('# reaction :: {}'.format(ri))
    print()

    # set number of equations, one for each particle and one more (the last 
    # one) for the energy
    self.num_dim = len(self.list_particles) + 1
    
    # set energy self.deposition factor, fraction of produced energy deposited 
    # in the plasma
    self.qfactor = 3.5/17.6

    # specific head
    self.gamma = 5.0/3.0
    self.cv_ref = cphys.r_univ.value/(self.gamma - 1.0e+00)
    self.cv_ref /= units.mass.mol_to_particle
    self.cv_ref /= units.temperature.K_to_eV
    self.cv_ref *= units.energy.j_to_eV

    return

  def array_zeros(self):
    return numpy.zeros( ( self.num_dim, ))
  
  def matrix_zeros(self):
    return numpy.zeros( ( self.num_dim, self.num_dim, ))

  def cv(self,u):
    return self.cv_ref*numpy.sum(u[:-1])

  def eval(self,t,u):
    
    # initialize rates
    udot = self.array_zeros()
    
    # get current temperature
    tem = u[-1]

    # compute rates
    for reac in self.list_reactions:
    # compute coefficient
      w = reac.coeff(tem)
      w0 = w
      for i in reac.ridx:
        w *= u[i]
      # add rate to the defined rates
      for i in reac.ridx:
        udot[i] -= w
      for i in reac.pidx:
        udot[i] += w

    # compute energy derivative
    if not self.tem_static:
      q = 0.0e+00
      for ki, vi in self.list_particles.items():
        q -= vi.mass*udot[vi.id]
      udot[-1] = (self.qfactor*q)/self.cv(u)
    
    # print for debug purposes
    '''
    print('#',end='')
    for v in u:
      print(' {:13.5e}'.format(v),end='')
    for v in udot:
      print(' {:13.5e}'.format(v),end='')
    print()
    '''

    # check values for debug
    '''
    if any(numpy.isnan(u)):
      print('# nan in u!!!')
      sys.exit(-1)
    if any(numpy.isnan(udot)):
      print('# nan in udot!!!')
      sys.exit(-1)
    '''

    return udot

  def jac(self,t,u):
    # initialize rates
    j = self.matrix_zeros()
    
    # compute rates
    for reac in self.list_reactions:
      # compute coefficient
      w = reac.coeff(u[-1])
      
    raise RatesError('Not implemented')

    return j

  def __call__(self,t,u):
    return self.eval(t,u)

if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
