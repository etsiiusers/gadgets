
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module for simple utility functions
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.25 :: Manuel Cotello Ferreiro
# - initial version
#

import itertools

import xml.etree.cElementTree as et

def toint(line):
	try:
		return int(line)
	except ValueError:
		return None

def tofloat(line):
	try:
		return float(line)
	except ValueError:
		return None

def tostr(value):
	if isinstance(value,float):
		return '{:15.8e}'.format(value)
	else:
		return str(value)

def att2array(line):
	if isinstance(line,numpy.ndarray):
		return line
	elif not isinstance(line,str):
		raise Exception('bad data type for conversion')
	return numpy.array(list(map(tofloat,filter(None,line.split(',')))))

def array2att(vals):
	line = ''
	try:
		for vi in vals:
			line += '{:15.8e},'.format(vi)
		return line
	except TypeError:
		print('# vals :: {}'.format(vals))
		raise Exception('bad types in convertsion of array to string')

def check_xml_type(root_node,key,type):
	try:
		if root_node.attrib[key] == type:
			return True
		return False
	except KeyError:
		raise Exception('wrong node {} or missing mandatory attribute {}'.format(root_node,key))
	except AttributeError:
		raise Exception('bad XML tree node {}'.format(root_node))

if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
