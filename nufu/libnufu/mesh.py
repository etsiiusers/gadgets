
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Mesh construction module
#
# Version control
#
# 2019.02.25 :: Manuel Cotelo Ferreiro
# - initial version
#

import itertools

import numpy 

def MeshError(Exception):

  def __init__(self,msg):
    Exception.__init__(self,msg)
    return

desc = 'Module for meshes'
author = 'Manue Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

class mesh_ctor_c():

  '''
  Object for mesh construction
  '''
  
  def __init__(self,func_from=lambda x: x,func_to=lambda x: x,func_check=lambda x: True):
    '''
    Construct object

    -> in
    func_from: functor for transformation from real to scaled space
    func_to: functor for transformation from scaled to real space (inverse function of func_from)
    func_check: check conditions on mesh bounds
    '''
    self.func_from = func_from
    self.func_to = func_to
    self.func_check = func_check
    return

  def __call__(self,xl,xr,nx):
    '''
    Build a 1D mesh

    -> in
    xl, xr: left and right bounds of the mesh
    nx: number of elements in mesh

    <- out
    array with mesh values
    '''
    assert( self.func_check(xl) and self.func_check(xr) )
    return self.func_to(numpy.linspace(self.func_from(xl),self.func_from(xr),nx))

# list of default mesh constructors
_list_mesh_ctor = { 'lin': mesh_ctor_c(),
                    'log': mesh_ctor_c(numpy.log,numpy.exp),
                    'quad': mesh_ctor_c(numpy.sqrt,lambda x: x*x),
                    }

def build(par):
  '''
  Build a mesh based on parameters from dictionary

  -> in
  par: dictionary, must contain keys 'type', 'lo', 'hi' and 'size'

  <- out
  array with mesh values
  '''
  try:
    return _list_mesh_ctor[par['type']](par['lo'],par['hi'],nx=par['size'])
  except KeyError:
    raise MeshError('unknown type of mesh')

def build_sphmesh(rlo=0.0,rhi=1.0,nrad=10,ntheta=10,nphi=10):
	r     = numpy.linspace(rlo,      rhi,num=nrad  ,endpoint=True )
	theta = numpy.linspace(0.0,    numpy.pi,num=ntheta,endpoint=True )
	phi   = numpy.linspace(0.0,2.0*numpy.pi,num=nphi  ,endpoint=False)
	return numpy.array(__aux_sphmesh__(r,theta,phi))

def build_sphmesh_shell(radius=1.0,ntheta=10,nphi=10):
	theta = numpy.linspace(0.0,    numpy.pi,num=ntheta,endpoint=True )
	phi   = numpy.linspace(0.0,2.0*numpy.pi,num=nphi  ,endpoint=False)
	return numpy.array(__aux_sphmesh_shell__(radius,theta,phi))

def __aux_sphmesh__(r,theta,phi):
	x = []
	for ri in r:
		if ri==0.0:
			x.append([ri, 0.0, 0.0, ])
		else:
			x.extend(aux_sphmesh_shell(ri,theta,phi))
	return x

def __aux_sphmesh_shell__(r,theta,phi):
	x = []
	for ti in theta:
		if ti==0.0 or ti==numpy.pi:
			x.append([r, ti, 0.0, ])
		else:
			for pi in phi:
				x.append([r, ti, pi, ])
	return x

def crd_xyz2rtp(xin):
	'''
	Change of coordinates from cartesian to spheric

	spheric coordinates:
	r in [0.0, \infty)
	theta in [0.0, \pi]
	phi in [0.0, 2 \pi)

	x: set of three values, vector from the origin of coordinates to the point
	'''
	def aux_crd_xyz2rtp(px,pr):
		x1 = px[0]
		x2 = px[1]
		x3 = px[2]
		r     = numpy.sqrt(x1*x1 + x2*x2 + x3*x3)
		theta = numpy.arccos(x3/r)
		phi   = numpy.arccos(x1/(r*numpy.sin(theta)))
		if x2 < 0.0:
			phi = -phi
		pr[0] = r
		pr[1] = theta
		pr[2] = phi
		return 
	rout = numpy.zeros(xin.shape)
	if len(xin.shape)>1:
		for xi, ri in zip(xin,rout):
			aux_crd_xyz2rtp(xi,ri)
		return rout
	else:
		aux_crd_xyz2rtp(xin,rout)

def crd_rtp2xyz(rin):
	'''
	Change of coordinates from spherical to cartesian

	spheric coordinates:
	r in [0.0, \infty)
	theta in [0.0, \pi]
	phi in [0.0, 2 \pi)

	x: set of three values, 1) radius, 2) altitude, 3) longitude
	'''
	def aux_crd_rtp2xyz(pr,px):
		r     = pr[0]
		theta = pr[1]
		phi   = pr[2]
		ct = numpy.cos(theta)
		st = numpy.sin(theta)
		cp = numpy.cos(phi)
		sp = numpy.sin(phi)
		px[0] = r*st*cp
		px[1] = r*st*sp
		px[2] = r*ct
		return
	xout = numpy.zeros(rin.shape)
	if len(rin.shape)>1:
		for ri, xi in zip(rin,xout):
			aux_crd_rtp2xyz(ri,xi)
		return xout
	else:
		return aux_crd_rtp2xyz(rin,xout)

def dirs_leg(order):
	'''
	Obtain a set of points in a spherical shell of radius 1 using the roots of 
	the Legendre polynomials of a defined order

	order: integer value greater than 2 with the order of the chebyshev 
	       polynomial used for the distribution
	'''
	def get_order_array(order):
		x = numpy.zeros(order+1)
		x[-1] = 1.0
		return x
	if order < 2:
		raise Exception('distribution of ray components imust be at least of order 2!!!')
	r = numpy.polynomial.legendre.Legendre(get_order_array(order)).roots()
	drs = numpy.array(list(itertools.product(r,r,r)))
	for vi in drs:
		vi /= numpy.linalg.norm(vi)
	return drs

def dirs_cheb(order):
	'''
	Obtain a set of points in a spherical shell of radius 1 using the roots of 
	the Chebyshev polynomials of a defined order

	order: integer value greater than 2 with the order of the chebyshev 
	       polynomial used for the distribution
	'''
	def get_order_array(order):
		x = numpy.zeros(order+1)
		x[-1] = 1.0
		return x
	if order < 2:
		raise Exception('distribution of ray components imust be at least of order 2!!!')
	r = numpy.polynomial.chebyshev.chebroots(get_order_array(order))
	drs = numpy.array(list(itertools.product(r,r,r)))
	for vi in drs:
		vi /= numpy.linalg.norm(vi)
	return drs

if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
