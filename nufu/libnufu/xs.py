
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Manage a database for fusion cross sections
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.02.27 :: Manuel Cotelo Ferreiro
# - inherit native dict in XS database
# 2019.02.22 :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

import h5py 

import numpy 
import scipy.interpolate
import scipy.integrate

import xml.etree.cElementTree as et

desc = 'Manage a database for fusion cross sections'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

try:
    import cphys
except ImportError:
    import libnufu.cphys as cphys

try:
    import units
except ImportError:
    import libnufu.units as units

try:
    import interp
except ImportError:
    import libnufu.interp as interp

class XSError(Exception):

  def __init__(self,msg):
    Exception.__init__(self,msg)

class db_c(dict):

  # keys to navigate through XML files
  _k_doc = 'doc'
  _k_type = 'type'
  _k_item = 'item'
  _k_reaction = 'reaction'
  _k_xs_val = 'xs_val'
  _k_xs_unit = 'xs_unit'
  _k_uni = '_uni'
  _k_log = '_log'
  _k_energy = 'energy'
  _k_energy_uni = _k_energy + _k_uni
  _k_energy_log = _k_energy + _k_log
  _k_sigma = 'sigma'
  _k_sigma_uni = _k_sigma + _k_uni
  _k_sigma_log = _k_sigma + _k_log
  _k_interp = 'interp'

  def __init__(self,file_name=''):
#    self.db = {}
    if file_name:
      self.load(file_name)
    return

  def _load_xml_item(self,item):
    # key "reaction" is mandatory for all items
    try:
      key = item.attrib[self._k_reaction]
    except KeyError:
      raise XSError('missing mandatory key value {} in XML file'.format(self._k_reaction))
    # read XS data from XML file
    e = []
    s = []
    for p in item.iter(self._k_xs_val):
      ei = float(p.attrib[self._k_energy])
      si = float(p.attrib[self._k_sigma])
      if ( ei > 0.0e+00 and si > 0.0e+00 ):
        e.append(ei)
        s.append(si)
    # read unit information
    uni_info = item.find(self._k_xs_unit)
    # data chek
    assert(not e or not s or not uni_info)
    # join all data about a reaction in a dictionary
    val = numpy.zeros( ( len(e), 2, ))
    val[:,0] = e
    val[:,1] = s
    self[key] = { 'values': val,
                  'interp': interp.func_interp_lolo_c(val[:,0],val[:,1],fill='extrapolate'),
                  'energy_unit': uni_info.attrib[self._k_energy_uni],
                  'sigma_unit': uni_info.attrib[self._k_sigma_uni],
                  'name': key,
                  }
    self[key].update(particles(key))
    return

  def _load_xml(self,file_name):
    root = et.parse(file_name).getroot()
    for item in root.iter(self._k_item):
      self._load_xml_item(item)
    return
  
  def _load_h5(self,file_name):
    h5f = h5py.File(file_name,'r')
    for ki in h5f:
      x = numpy.array(h5f[ki])
      item = { 'values': x,
               'interp': interp.func_interp_lolo_c(x[:,0],x[:,1],fill='extrapolate'),
               }
      item.update(h5f[ki].attrs)
      self[ki] = item
      self[ki].update(particles(ki))
    return

  def load(self,file_name):
    if os.path.isfile(file_name):
      if file_name.endswith('.xml'):
        self._load_xml(file_name)
      elif file_name.endswith('.h5'):
        self._load_h5(file_name)
      else:
        raise XSError('unknown format for XS data file "{}"'.format(file_name))
    else:
      raise XSError('file "{}" does not exist'.format(file_name))
    return

def particles(id_reac):
  
  '''
  Get particles from reaction string

  -> in
  id_reac: string with reaction description

  <- out
  dictionary with two lists, one with reactives and other with reaction 
  products
  '''

  # initialize data structure
  p = { 'reac': [],
        'prod': [],
        }
  
  # split in reactives and products
  reac, prod = id_reac.split(',')
  
  # reactives
  words = reac.split('(')
  p['reac'].append(words[0])
  p['reac'].extend(words[1].split('+'))
  
  # products
  words = prod.split(')')
  p['prod'].append(words[-1])
  p['prod'].extend(words[0].split('+'))
  
  return p



class func_xs_c():
  
  '''
  Functor for XS average with Maxwell-Boltzmann distribution. This class is
  intended for auxiliar calculations, not for direct use
  '''

  def __init__(self,kt,func_sigma):
    '''
    Constructor

    -> in
    func_sigma: XS evaluation function, XS in b
    kt: temperature, in eV
    '''
    self.func_sigma = func_sigma
    self.kt = kt
    assert( self.kt > 0.0e+00 )
    return

  def __call__(self,ene):
    '''
    Evaluation of integral function for XS average with Maxwell-Boltzmann 
    distribution

    -> in
    ene: collision energy, in eV

    <- out
    evaluation of integral function, in (eV^2)*b
    '''
    return ene*self.func_sigma(ene)*numpy.exp(-ene/self.kt)


def xs_average(mu,kt,func_sigma):
  '''
  Average XS with a Maxwell-Boltzmann distribution

  -> in
  mu: reduced mass, in eV
  kt: temperature, in eV
  func_sigma: functor for XS evaluation

  <- out
  averaged xs, in m3/s
  '''
 
  func_avg = func_xs_c(kt,func_sigma)
  
  e1 = 0.0e+00
  e2 = kt
  e3 = 1.0e+02*kt
  e4 = numpy.inf

  ans1, res1 = scipy.integrate.quad(func_avg,e1,e2,limit=2000,)
  ans2, res2 = scipy.integrate.quad(func_avg,e2,e3,limit=2000,)
  ans3, res3 = scipy.integrate.quad(func_avg,e3,e4,limit=2000,)

  ans = ans1 + ans2 + ans3
  res = res1 + res2 + res3

  ans = numpy.sqrt(8.0e+00/numpy.pi/mu)*numpy.power(1.0/kt,1.5e+00)*ans
  ans = ans*units.xs.barn_to_m2*cphys.c_light.value
 
  return ans

if __name__ == '__main__':
    print('# warning :: this file is not intended for standalone run')
    sys.exit(-1)
