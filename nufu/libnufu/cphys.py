
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Definition of several useful physics constants
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include several common constants
# 2019.02.23 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

import numpy

# module info
info = { 'desc': 'Define physical constants',
         'author': 'Manuel Cotelo Ferrreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID',
         }

# speed of light
class c_light():
  value = 2.99792458e+08
  units = 'm/s'

# perfect gas constant
class r_univ():
  value = 8.314472
  units = 'J/K/mol'

# planck's constant
class h_planck():
  value = 6.62607015e-34
  units = 'J*s'

class hbar_planck():
  value = h_planck.value/2.0e+00/numpy.pi
  units = 'J*s'

# electron properties
class electron():
  class mass():
    value = 9.10938356e-31 
    units = 'kg'
  class charge():
    value = -1.60217662e-19
    units = 'C'

# proton properties
class proton():
  class mass():
    value = 1.6726219e-27
    units = 'kg'
  class charge():
    value = 1.60217662e-19
    units = 'C'

# neutron properties
class neutron():
  class mass():
    value = 1.674927471e-27
    units = 'kg'
  class charge():
    value = 0.0e+00
    units = 'C'

# mu_0
class vacuum_permeability():
  value = (4.0e+00*numpy.pi) * 1.00000000082e-07
  units = 'H/m'
  #value = 1.2566370614e-06
  #units = 'N/A^2'

# epsiolon_0
class vacuum_permitivity():
  value = 1.0e+00/c_light.value/c_light.value/vacuum_permeability.value
  #value = 8.854187817e-12
  units = 'F/m'

# fine-structure constant
class fine_structure():
  value = 0.0072973525664
  units = ''

# standard gravity
class standard_gravity():
  value = 9.80665e+00
  units = 'm/s/s'

if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
