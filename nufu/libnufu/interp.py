
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Interpolation helper classes
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.02.23 :: Manue Cotelo Ferreiro
# - initial version
#

import os
import sys

import numpy
import scipy.interpolate

# info
desc = 'Interpolation helper functors'
author = 'Manue Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# default parameters of the module
_defs = { 'fill': numpy.nan,
           }

class func_interp_base_c():
  
  '''
  Base interpolation functor
  '''
  
  def __init__(self,x,fx,fill=_defs['fill']):
    '''
    Build interpolation functor base data

    -> in
    x: coordinates
    fx: value of the interpolated function in the coordinates
    fill: flag to define what to do for coordinates out of the original data range
    '''
    # get dimensions
    if len(x) != len(fx):
      print('# warning :: different sizes for x and fx, take the lower size.')
    num = min(len(x),len(fx))
    assert( num > 1 )
    # build array and fill it
    w = []
    for i in range(num):
      w.append( [ x[i], fx[i], ])
    # sort array
    w = numpy.array(sorted(w,key=lambda x: x[0]))
    # create functor
    self.func = scipy.interpolate.interp1d(self.from_x(w[:,0]),self.from_f(w[:,1]),copy=True,bounds_error=False,fill_value=fill,)
    return

  '''
  Data default transformations before/after interpolation
  '''
  def from_x(self,x):
    return x
  def from_f(self,x):
    return x
  def to_x(self,x):
    return x
  def to_f(self,x):
    return x

  def __call__(self,x):
    '''
    Overload call operator, interpolation evaluation
    '''
    return self.to_f(self.func(self.from_x(x)))


class func_interp_lili_c(func_interp_base_c):

  '''
  Lin-Lin interpolation
  '''

  def __init__(self,x,fx,fill=_defs['fill']):
    func_interp_base_c.__init__(self,x,fx,fill)
    return

class func_interp_lilo_c(func_interp_base_c):

  '''
  Lin-Log interpolation
  '''
  
  def __init__(self,x,fx,fill=_defs['fill']):
    assert( numpy.amin(fx) > 0.0e+00 )
    func_interp_base_c.__init__(self,x,fx,fill)
    return
  
  def from_f(self,x):
    return numpy.log(x)
  def to_f(self,x):
    return numpy.exp(x)


class func_interp_loli_c(func_interp_base_c):

  '''
  Log-Lin interpolation
  '''
  
  def __init__(self,x,fx,fill=_defs['fill']):
    assert( numpy.amin(x) > 0.0e+00 )
    func_interp_base_c.__init__(self,x,fx,fill)
    return
  
  def from_x(self,x):
    return numpy.log(x)
  def to_x(self,x):
    return numpy.exp(x)

class func_interp_lolo_c(func_interp_base_c):

  '''
  Log-Log interpolation
  '''
  
  def __init__(self,x,fx,fill=_defs['fill']):
    assert( numpy.amin(x) > 0.0e+00 )
    assert( numpy.amin(fx) > 0.0e+00 )
    func_interp_base_c.__init__(self,x,fx,fill)
    return
  
  def from_x(self,x):
    return numpy.log(x)
  def from_f(self,x):
    return numpy.log(x)
  def to_x(self,x):
    return numpy.exp(x)
  def to_f(self,x):
    return numpy.exp(x)

if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
