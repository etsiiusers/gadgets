
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Manage nuclear database
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.03 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include class to manage NUBASE database for nuclear properties
# 2019.02.27 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - remove old formats, keep just JSON
# - inherit native python dict in database class
# 2019.02.23 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

import json

try:
  import units as units
except ImportError:
  import libnufu.units as units

try:
  import cphys as cphys
except ImportError:
  import libnufu.cphys as cphys

# info
desc = 'Manage a database for nuclear properties'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

class NucDBError(Exception):

  def __init__(self,msg):
    Exception.__init__(self,msg)
    return


class _db_base_c(dict):

  def __init__(self,file_name):
    self.load(file_name)
    return

  def load(self,file_name):
    if os.path.isfile(file_name):
      if file_name.endswith('.json'):
        self._load_json(file_name)
      else:
        raise NucDBError('unknown database file format')
    else:
      raise NucDBError('file "{}" does not exist'.format(file_name))
  
  def _load_json(self,file_name):
    try:
      with open(file_name) as fd:
        self.update(json.load(fd))
    except OSError:
      raise NucDBError('impossible to read database file "{}"'.format(file_name))
    return


class db_c(_db_base_c):

  def __init__(self,file_name):
    _db_base_c.__init__(self,file_name)
    return


class db_nubase_c(_db_base_c):

  def __init__(self,file_name):
    _db_base_c.__init__(self,file_name)
    return

  def select(self,name):
    try:
      return self[name]
    except KeyError:
      return None
  
  def _mass(self,data):
    try:
      return float(data['me']['value']) + data['A']*units.mass.amu_to_keV
    except ( KeyError, ValueError, ):
      return numpy.nan

  def mass(self,name):
    return self._mass(self.select(name))
  
  def be(self,name):
    data =  self.select(name)
    
    try:
      m = self._mass(data)
      z = data['Z']
      a = data['A']
      n = a - z
      mp = cphys.proton.mass.value*units.mass.kg_to_keV
      mn = cphys.neutron.mass.value*units.mass.kg_to_keV
      me = cphys.electron.mass.value*units.mass.kg_to_keV
      return (z*(mp + me) + n*(mn) - m)/a
    except ( KeyError, ValueError, ):
      return numpy.nan



if __name__ == '__main__':
    print('# warning :: this file is not intended for standalone run')
    sys.exit()
