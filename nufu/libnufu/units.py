
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Unit conversion module
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control
# 2019.09.17 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include comments
# - minor changes in messages
# - include few unit conversion factors
# 2019.03.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add new constants for unit conversion
# 2019.02.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os

# module info
info = { 'desc': 'Units conversion module',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID',
         }


class magnitude_c():

  '''
  Object to describe order of magnitude
  '''
  
  def __init__(self,order,name,sym):
    
    '''
    Construct descriptor of order of magnitude

    -> in
    order: order integer respect to base units
    name: prefix
    sym: symbol
    '''
    
    self.order = order
    self.factor = float('1.0e{}'.format(self.order))
    self.name = name
    self.sym = sym
    
    return

# list of available orders of magnitude
list_order = {  'Y': magnitude_c(  21, 'yotta',  'Y', ),
                'Z': magnitude_c(  18, 'zetta',  'Z', ),
                'E': magnitude_c(  15,   'exa',  'E', ),
                'T': magnitude_c(  12,  'tera',  'T', ),
                'G': magnitude_c(   9,  'giga',  'G', ),
                'M': magnitude_c(   6,  'mega',  'M', ),
                'k': magnitude_c(   3,  'kilo',  'k', ),
                'h': magnitude_c(   2, 'hecta',  'h', ),
               'da': magnitude_c(   1,  'deca', 'da', ),
                 '': magnitude_c(   0,      '',   '', ),
                'd': magnitude_c(  -1,  'deci',  'd', ),
                'c': magnitude_c(  -2, 'centi',  'c', ),
                'm': magnitude_c(  -3,  'mili',  'm', ),
               'mu': magnitude_c(  -6, 'micro', 'mu', ),
                'n': magnitude_c(  -9,  'nano',  'n', ),
                'p': magnitude_c( -12,  'pico',  'p', ),
                'f': magnitude_c( -15, 'femto',  'f', ),
                'a': magnitude_c( -18,  'atto',  'a', ),
                'z': magnitude_c( -21, 'zepto',  'z', ),
                'y': magnitude_c( -24, 'yocto',  'y', ),
                }

# get conversion factor between orrders of magnitude
def conversion_factor(order_from,order_to):
  return list_order[order_from].factor/list_order[order_to].factor

class length():

  '''
  Common unit conversion for length
  '''

  m_to_dm = conversion_factor('','d')
  m_to_cm = conversion_factor('','c')
  m_to_mm = conversion_factor('','m')
  
  dm_to_m = 1.0e+00/m_to_dm
  cm_to_m = 1.0e+00/m_to_cm
  mm_to_m = 1.0e+00/m_to_mm

  m_to_micron = conversion_factor('','mu')
  micron_to_m = 1.0e+00/m_to_micron

class area():
  
  '''
  Common unit conversion for area
  '''
 
  m2_to_dm2 = length.m_to_dm*length.m_to_dm
  m2_to_cm2 = length.m_to_cm*length.m_to_cm
  m2_to_mm2 = length.m_to_mm*length.m_to_mm
  
  dm2_to_m2 = 1.0e+00/m2_to_dm2 
  cm2_to_m2 = 1.0e+00/m2_to_cm2
  mm2_to_m2 = 1.0e+00/m2_to_mm2

class vol():
  
  '''
  Common unit conversion for volume
  '''

  m3_to_cm3 = area.m2_to_cm2*length.m_to_cm 
  m3_to_mm3 = area.m2_to_mm2*length.m_to_mm 

  cm3_to_m3 = 1.0e+00/m3_to_cm3
  mm3_to_m3 = 1.0e+00/m3_to_mm3

class mass():
  
  '''
  Common unit conversion for mass
  '''
  
  amu_to_eV  = 931.49406121e+06
  amu_to_keV = amu_to_eV*conversion_factor('','k')
  amu_to_MeV = amu_to_eV*conversion_factor('','M')
  amu_to_GeV = amu_to_eV*conversion_factor('','G')

  eV_to_aum  = 1.0e+00/amu_to_eV
  keV_to_aum = 1.0e+00/amu_to_keV
  MeV_to_aum = 1.0e+00/amu_to_MeV
  GeV_to_aum = 1.0e+00/amu_to_GeV

  amu_to_kg = 1.66053904e-27
  kg_to_amu = 1.0e+00/amu_to_kg

  eV_to_kg  = 1.782662e-36
  keV_to_kg = eV_to_kg*conversion_factor('k','')
  MeV_to_kg = eV_to_kg*conversion_factor('M','')

  kg_to_eV = 1.0e+00/eV_to_kg
  kg_to_keV = 1.0e+00/keV_to_kg
  kg_to_MeV = 1.0e+00/MeV_to_kg

  g_to_kg = conversion_factor('','k')
  kg_to_g = 1.0e+00/g_to_kg

  mol_to_particle = 6.02214199e+23
  particle_to_mol = 1.0e+00/mol_to_particle

class energy():

  '''
  Common unit conversion for energy
  '''

  eV_to_j = 1.6021766208e-19 
  keV_to_j = eV_to_j*conversion_factor('k','')
  
  j_to_eV = 1.0/eV_to_j
  j_to_keV = 1.0/keV_to_j

  eV_to_keV = conversion_factor('','k')
  keV_to_eV = conversion_factor('k','')

class xs():
  
  '''
  Common unit conversion for cross sections
  '''

  barn_to_cm2 = 1.0e-24
  barn_to_m2  = barn_to_cm2*area.cm2_to_m2
  
  cm2_to_barn = 1.0e+00/barn_to_cm2
  m2_to_barn = 1.0e+00/barn_to_m2

class temperature():

  '''
  Common unit conversion for temperature
  '''
  
  eV_to_K = 11604.0
  K_to_eV = 1.0/eV_to_K

class charge():

  '''
  Unit conversion for number of elementary particles to Coulombs
  '''

  z_to_c = 1.60217662e-19
  c_to_z = 1.0e+00/z_to_c

# warning
if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
