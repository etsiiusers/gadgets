
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module with common distributions
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.16 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - improve Maxwell-Boltzmann distributions
# 2019.02.25 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy
import scipy.special

try:
  import cphys
except ImportError:
  import libnufu.cphys as cphys

# module info
desc = 'Define some well known distributions'
author = 'Manuel Cotelo Ferrreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)


class DistError(Exception):
  
  def __init__(self,msg='undef'):
    Exception.__init__(self,msg)
    return


class dist_base_c():

  def pdf(self,x):
    
    '''
    Evaluate probability distribution function

    -> in
    x: independent variable

    <- out
    (1) pdf, units of 1/x
    '''
    
    raise DistError('not implemented !!!')

  def cdf(self,x):
    
    '''
    Evaluate cummulative distribution function, 
    
    F(x0) = P(x>x0)

    -> in
    x: independent variable

    <- out
    (1) cdf, no dimensions
    '''
    
    raise DistError('not implemented !!!')


class maxboltz_c(dist_base_c):

  def __init__(self,a):
    # parameter
    self.a = a
    # auxiliar variables
    self.c0 = numpy.sqrt(2.0e+00/numpy.pi)/self.a
    self.p0 = self.c0/self.a/self.a
    self.scale_x = numpy.sqrt(2.0e+00)*self.a
    self.scale_avg = 1.0e+00*numpy.sqrt(2.0e+00/numpy.pi)
    self.scale_mode = numpy.sqrt(2.0e+00)
    self.scale_s2 = (3.0e+00*numpy.pi - 8.0e+00)/numpy.pi
    return

  def scale(self,x):
    return x/self.scale_x

  def pdf(self,x):
    w = self.scale(x)
    return self.p0*x*x*numpy.exp(-w*w)

  def cdf(self,x):
    w = self.scale(x)
    return scipy.special.erf(w) - self.c0*x*numpy.exp(-w*w)

  @property
  def mean(self):
    return  self.scale_avg*self.a

  @property
  def mode(self):
    return self.scale_mode*self.a

  @property
  def variance(Self):
    return self.scale_s2*self.a*self.a


class maxboltz_momxyz_c(dist_base_c):

  '''
  Maxwell-Boltzmann distribution based on momentum vector
  '''

  def __init__(self,kt,mass):
    # parameters
    self.kt = kt
    self.mass = mass
    # base functions
    self.func = maxboltz_c(numpy.sqrt(self.mass*self.kt))
    self.scale_pdf = numpy.power(2.0e+00*numpy.pi*self.func.a*self.func.a,-1.5)/numpy.sqrt(2.0e+00/numpy.pi)
    self.scale_pdf = (self.func.a*self.func.a*self.func.a)/self.scale_pdf
    return 

  def pdf(self,p):
    
    '''
    Evaluate probability distribution function

    -> in
    p: momentum (p*c) array, in keV

    <- out
    (1) pdf, in 1/keV
    '''
    
    p2 = numpy.dot(p,p)
    return self.func.pdf(numpy.sqrt(p2))/p2/self.scale_pdf


class maxboltz_mom_c(dist_base_c):

  '''
  Maxwell-Boltzmann distribution based on momentum vector
  '''

  def __init__(self,kt,mass):
    return 


class maxboltz_ene_c(dist_base_c):
  
  '''
  Maxwell-Boltzmann distribution based on particle kinetic energy
  '''

  def __init__(self,kt,mass=0.0e+00):
    # parameters
    self.kt = kt
    self.mass = mass
    # PDF functions
    self.func = maxboltz_c(numpy.sqrt(self.kt/2.0e+00))
    return

  def pdf(self,ene):
   
    '''
    Evaluate probability distribution function

    -> in
    ene: energy, in keV

    <- out
    (1) pdf value, in 1/keV
    '''

    return self.func.pdf(numpy.sqrt(ene))/numpy.sqrt(ene)/2.0e+00
  
  def cdf(self,ene):
   
    '''
    Evaluate cummulative distribution function

    -> in
    ene: energy, in keV

    <- out
    (1) cdf value, no dimensions
    '''

    return 0.5*self.func.cdf(numpy.sqrt(ene))


class maxboltz_vel_c(dist_base_c):

  '''
  Maxwell-Boltzmann distribution based on particle velocity value
  '''

  def __init__(self,kt,mass):
    
    '''
    kt: temperature, in keV
    mass: mass of the particle, in keV
    '''
    # parameters
    self.kt = kt
    self.mass = mass
    # PDF function
    self.func = maxboltz_c(numpy.sqrt(self.kt/self.mass))
    return

  def pdf(self,x):
    return self.func.pdf(x)

  def cdf(self,x):
    return self.func.cdf(x)
  
  @property
  def mean(self):
    return  self.func.mean

  @property
  def mode(self):
    return self.func.mode

  @property
  def variance(Self):
    return self.func.variance



class maxboltz_velxyz_c(dist_base_c):
  
  '''
  Maxwell-Boltzmann distribution based on particle velocity vector
  '''

  def __init__(self,kt,mass):
    
    '''
    Build Maxwell-Boltzmann distribution based in velocity vector

    kt: temperature, in keV
    mass: mass of the particle, in keV
    '''

    # parameters
    self.kt = kt
    self.mass = mass
    # PDF functions
    self.func = maxboltz_c(numpy.sqrt(self.kt/self.mass))
    self.scale_pdf = 4.0e+00*numpy.pi
    return
    
  def pdf(self,v):

    '''
    Evaluate probability distribution function

    -> in
    v: velocity array v/c, no dimensions

    <- out 
    (1) pdf value, no dimensions
    '''

    v2 = numpy.dot(v,v)
    return self.func.pdf(numpy.sqrt(v2))/v2/self.scale_pdf


if __name__=='__main__':
  print('# warning :: this module is not intended for standalone run')
  sys.exit(-1)
