
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# This file is part of gadgets
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# This program computes ingnition criteria for a nuclear fusion 
# reaction
#
# Version control 
# 2021.02.11 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys
import argparse

import numpy

import libnufu.nc as nc
import libnufu.xs as xs

import utils.units as units

# module info
info = { 'name': 'icrit',
         'desc': 'Compute ingnition criteria for a nuclear fusion reaction',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2020 Universidad Politécnica de Madrid',
         }

# default parameters
defs = { 'dbnc': 'dbnubase12.json',
         'dbxs': 'xs.xml',
         }

# get command line arguments
def get_args(info,defs):

  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, ({email})'.format(** info))
  
  # optional parameters
  parser.add_argument( '--dbnc', help='Set file name with nuclear database, default "{dbnc}"'.format(** defs), default=defs['dbnc'], type=str, )
  parser.add_argument( '--dbxs', help='Set file name with cross section database, default "{dbxs}"'.format(** defs), default=defs['dbxs'], type=str, )
  
  return parser.parse_args()

# get mass from isotope data from DB
def mass(data):
  return float(data['me']['value'])*units.energy.keV_to_eV + data['A']*units.mass.amu_to_eV

# compute energy source
def esrc(qc,sv,n1,n2):
  return qc*sv*n1*n2*units.energy.eV_to_j

# compute energy losses
def eloss(kt,n1,n2,z1,z2):
  ne = (n1 + n2)/1.0e+03
  s1 = n1*z1*z1/1.0e+03
  s2 = n2*z2*z2/1.0e+03
  return 5.35e-31*ne*(s1 + s2)*numpy.sqrt(kt/1.0e+03)

# main function
def main():

  # set reaction
  r1 = '2H'
  r2 = '3H'
  p1 = '1n'
  p2 = '4He'
  reaction = 'h2(h3,n1)he4'
  
  # get energy of charged particles, in eV
  qc = 3.5e+06

  # particle density, in 1/m3
  n_total = 1.0e+21

  # atomic fraction of reactants in fuel
  x1 = 0.5
  x2 = 1.0 - x1

  assert( x1 >= 0.0 and x1 <= 1.0 )
  assert( x2 >= 0.0 and x2 <= 1.0 )
  assert( numpy.abs(1.0 - x1 - x2) < 1.0e-09 )

  # particle density of each reactant, in 1/m3
  n1 = x1*n_total
  n2 = x2*n_total

  z1 = 1.0
  z2 = 1.0
  
  # read arguments
  args = get_args(info,defs)

  # load nuclear DB
  dbnc = nc.db_c(args.dbnc)

  # load XS DB
  dbxs = xs.db_c(args.dbxs)

  # select XS from DB
  try:
    sigma = dbxs[reaction]
    sigma_func = sigma['interp']
  except KeyError:
    print('# error :: XS for reaction "{}" is not available'.format(reaction),file=sys.stderr)
    print('# available reactions are :: {}'.format(dbxs.keys()),file=sys.stderr)
    return -1

  # get reduced mass of reactives
  try:
    r1_mass = mass(dbnc[r1])
    r2_mass = mass(dbnc[r2])
    mu = r1_mass*r2_mass/(r1_mass + r2_mass)
  except KeyError:
    print('# error :: isotope not avaiable in DB'.format(r1))
    print('# available isotopes are :: {}'.format(dbnc.keys()))
    return -2

  # create temperature mesh
  kt = numpy.exp(numpy.linspace(numpy.log(1.0e+02),numpy.log(1.0e+07),200))

  # compute ignition criteria
  s = ''
  for kti in kt:

    sv = xs.xs_average(mu,kti,sigma_func)

    es = esrc(qc,sv,n1,n2)
    el = eloss(kti,n1,n2,z1,z2)

    de = es - el

    for wi in ( n1, n2, kti, sv, es, el, de, ):
      s = s + ' {:15.7e}'.format(wi)
    s = s + '\n'

  # print results
  print(s)

  return 0

# run main function
if __name__ == '__main__':
  sys.exit(main())
