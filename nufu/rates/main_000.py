
#
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with it. If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2021.07.02 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import os
import sys
import argparse
import json

import numpy
import scipy.interpolate
import scipy.integrate
import scipy.stats

import utils.units as units
import utils.cphys as cphys
import utils.prnt as prnt

import rates

# program info
info = { 'name': 'main_000',
         'desc': 'Compute average cross sections for nucclear fusion',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }

# program default arguments
defs = { 'db_xs': 'dbxs.json',
         'db_nuc': 'dbnuc.json',
         'kt_lo': 1.0e+00,
         'kt_hi': 1.0e+06,
         'kt_size': 100,
         'kt_type': [ 'log', 'lin', ],
         }


def select_xs(db,reac_name):
  try:
    return db[reac_name]
  except KeyError:
    return ''

def show_available_reactions(db):
  s = '# available reactions in DB ::\n'
  for ki in db:
    s = s + '{}\n'.format(ki)
  return s

def build_mesh_lin(a,b,size):
  return numpy.linspace(a,b,size)

def build_mesh_log(a,b,size):
  assert( a > 0.0 and b > 0.0 )
  return numpy.exp(build_mesh_lin(numpy.log(a),numpy.log(b),size))

def build_mesh(a,b,size,type):
  if type == 'lin':
    return build_mesh_lin(a,b,size)
  elif type == 'log':
    return build_mesh_log(a,b,size)
  else:
    print('# error :: unknown type of mesh "{}"'.format(type),file=sys.stderr)
    return ''

def get_reduced_mass(rs,db):
  ms = numpy.array( [ db[ri]['mass_value']*units.mass.amu_to_eV for ri in rs])
  return scipy.stats.hmean(ms)

def wrdata(kt,xs):
  s = prnt.wrdict( { 'column_000': 'temperature, in eV',
                     'column_001': 'averaged cross section <sigma·v>, in m^3/s',
                     },
                     'description')
  s = s + '\n'
  for wi in zip(kt,xs):
    for wij in wi:
      s = s + '{:15.7e}'.format(wij)
    s = s + '\n'
  
  return s

# get command line arguments
def get_args(info,defs):

  parser = argparse.ArgumentParser(description='',epilog='')

  # positional arguments
  parser.add_argument( 'reac', help='', type=str, )

  # optional arguments
  parser.add_argument( '--db_xs', help='', default=defs['db_xs'], type=str, )
  parser.add_argument( '--db_nuc', help='', default=defs['db_nuc'], type=str, )
  parser.add_argument( '--kt_lo', help='', default=defs['kt_lo'], type=float, )
  parser.add_argument( '--kt_hi', help='', default=defs['kt_hi'], type=float, )
  parser.add_argument( '--kt_size', help='', default=defs['kt_size'], type=int, )
  parser.add_argument( '--kt_type', help='', default=defs['kt_type'][0], type=str, choices=defs['kt_type'], )

  return parser.parse_args()

# driver functions
def main(info,defs):

  # get command line arguments
  args = get_args(info,defs)

  # print header
  print(prnt.wrinfo(info))
  print(prnt.wrargs(args))

  # argument validation
  assert( os.path.exists(args.db_nuc) )
  assert( os.path.exists(args.db_xs) )
  assert( args.kt_lo > 0.0 and args.kt_hi > 0.0 and args.kt_lo <= args.kt_hi )
  assert( args.kt_size > 1 )
  
  # load nuclear properties
  db_nuc = json.load(open(args.db_nuc,'r'))
  
  # load XS
  db_xs = json.load(open(args.db_xs,'r'))

  # select XS for reaction
  xs = select_xs(db_xs,args.reac) 
  if not xs:
    # missing cross sections, exit
    print('# error :: missing cross sections for reaction "{}"'.format(args.reac),file=sys.stderr)
    print(show_available_reactions(db))
    return -1

  # compute reduced mass
  mu = get_reduced_mass(xs['reac'],db_nuc)
  print(prnt.wrparam( { 'mu': mu, }))

  # create array of temperatures
  kt = build_mesh(args.kt_lo,args.kt_hi,args.kt_size,args.kt_type)
  assert( len(kt) > 0 )

  # build average object
  func_xsv = rates.xsv_c(xs['energy'],xs['sigma'])

  # evaluate averages XS
  xsv = numpy.array([ func_xsv(kti,mu) for kti in kt ])

  # print results
  print(wrdata(kt,xsv))

  return 0

if __name__=='__main__':
  sys.exit(main(info,defs))
