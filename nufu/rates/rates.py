
#
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with it. If not, see <https://www.gnu.org/licenses/>.
#
# Version control 
# 2021.07.02 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys

import numpy
import scipy.interpolate
import scipy.integrate

import utils.units as units
import utils.cphys as cphys

# module info
info = { 'name': 'rates',
         'desc': 'Integrate average cross sections for nuclear fusion reactions',
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }

class xsv_c():
  
  '''
  Averaged cross section <sigma*v> considering a system in thermodynamic 
  equilibrium and hence the velocity distribution takes the Maxwell-Boltzmann 
  shape.
  '''

  def __init__(self,ene,sigma):
    
    '''
    Initialization

    -> in
    ene: energy in eV, array of floats
    sigma: cross sections in barns, array of floats of same size as "ene"
    '''
    
    # save parameters
    self.ene   = numpy.array(ene)
    self.sigma = numpy.array(sigma)
    
    # validation
    assert( self.ene.ndim == 1 and self.ene.ndim == self.sigma.ndim )
    assert( self.ene.shape[0] > 1 and self.ene.shape[0] == self.sigma.shape[0] ) 
    assert( all(self.ene[i] <= self.ene[i+1] for i in range(self.ene.shape[0]-1)) )

    # create interpolator for XS
    self.func_sigma = scipy.interpolate.interp1d(self.ene,self.sigma,copy=False,kind='linear',fill_value="extrapolate",)

    return

  def supp_lo(self):
    
    '''
    Return the energy lower bound fo the suppoort of XS

    <- out
    (1) energy in eV, float
    '''

    return self.ene[0]

  def supp_hi(self):
    
    '''
    Return the energy upper bound for the XS
    
    <- out
    (1) energy in eV, float
    '''

    return self.ene[-1]

  def supp(self):
    
    '''
    Return energy support of the cross sections
    
    <- out
    (1) energy for the lower bound in eV, float
    (2) energy for the upper bound in eV, float
    '''

    return ( self.supp_lo(), self.supp_hi(), )
  
  def func_integ(self,kt):

    '''
    Function for averaging

    -> in
    kt: temperature in eV, float

    <- out
    (1) functor to evaluate inegrand for averaging, accepts energy in eV as 
        input and returns distribution in eV*barn
    '''

    return lambda ene: ene*self.func_sigma(ene)*numpy.exp(-ene/kt)

  def eval(self,kt,mu):
    
    '''
    Evaluate averaged cross section

    -> in
    kt: temperature in eV, float
    mu: reduced mass in eV, float

    <- out 
    (1) average cross section <sigma·v> in m3/s, float
    '''
    
    # create function to evaluate itegrand for averaging
    func_avg = self.func_integ(kt)
    
    # chop the integral in parts, define limits for each part
    e1 = 0.0e+00
    e2 = min(self.supp_hi(),kt)
    e3 = min(self.supp_hi(),1.0e+02*kt)
    e4 = self.supp_hi()

    # numerical integration of each interval
    ans1, res1 = scipy.integrate.quad( func_avg, e1, e2, limit=2000, )
    ans2, res2 = scipy.integrate.quad( func_avg, e2, e3, limit=2000, )
    ans3, res3 = scipy.integrate.quad( func_avg, e3, e4, limit=2000, )
 
    # accumulate results
    ans = ans1 + ans2 + ans3
    res = res1 + res2 + res3

    # apply constants
    ans = ans*numpy.sqrt(8.0e+00/numpy.pi/mu)*numpy.power(1.0/kt,1.5e+00)
    ans = ans*units.xs.barn_to_m2*cphys.c_light.value

    return ans

  def __call__(self,kt,mu):
    
    '''
    See xsv_c.eval
    '''

    return self.eval(kt,mu)

def test_xs():
  
  '''
  Test for cross section averaging (<sigma·v>) with artificial cross sections 
  (sigma)
  '''

  ene   = numpy.exp(numpy.linspace(numpy.log(1.0e+00),numpy.log(1.0e+08),1000))
  _x = 1.0e+03 - 1.0e-12*numpy.power(ene - 1.0e+06,2.0)
  sigma = numpy.piecewise(_x,( _x<0.0, _x>=0.0, ),( lambda x: 0.0, lambda x: x, ),)

  xs = xsv_c(ene,sigma)

  mu = 1.0e+00
  kt = 1.0e+04

  fsigma = xs.func_sigma
  finteg = xs.func_integ(kt)
  
  s = '\n'
  s = s + '# <xs·v> = {:15.7e}\n'.format(xs.eval(kt,mu))
  s = s + '\n'
  for ei in ene:
    for wi in ( ei, fsigma(ei), finteg(ei), ):
      s = s + ' {:15.7e}'.format(wi)
    s = s + '\n'
  print(s)

  return

if __name__=='__main__':

  # print message
  print('# warning :: this is a python module, it is not intended for standalone run')
  print('#            use "python tester.py {name}" to run test.'.format(** info))
  
  # exit program
  sys.exit()
