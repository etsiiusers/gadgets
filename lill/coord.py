
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module to manage coordinates
# Copyright (C) 2019-2021 Universidad Politécnica de Madrid
#
# Version control:
# 2022.10.18: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix problems in "renorm" for non-vectorized calculations
# - check if we divide by 0.0
# - add tests
# 2022.07.29: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix error in to_cart
# 2022.07.27: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - use namespaces
# 2021.12.30: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add variables to include indices and slices
# 2021.12.28: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - split functions
# - use module utils
# - remove old version of coordinate transformation
# 2021.11.23: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add new methods to transform coordinates
# 2021.03.25: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - wrap module info in a dictionary
# - remove exceptions
# 2019.03.07: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy

import utils

# module info
info = argparse.Namespace(
  name = 'coord',
  desc = 'Module to convert coordinates',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  version = [ 1, 3, 0, ],
  year = 2022,
  copyright = 'Copyright (C) 2019-2022 Universidad Politécnica de Madrid',
)

# number of dimensions
num_dim = 3

# indices
js = argparse.Namespace(
  
  # cartesian
  x = 0, y = 1, z = 2,
  
  # spherical
  r = 0, theta = 1, phi = 2, 

)

# slices
sl = argparse.Namespace(
  
  # cartesian
  x = numpy.s_[...,js.x],
  y = numpy.s_[...,js.y],
  z = numpy.s_[...,js.z],
  
  # spherical
  r = numpy.s_[...,js.r],
  theta = numpy.s_[...,js.theta],
  phi = numpy.s_[...,js.phi],

)

def split_cart(xs):
  return xs[sl.x], xs[sl.y], xs[sl.z]

def split_sph(ss):
  return ss[sl.r], ss[sl.theta], ss[sl.phi]

def to_cart(ss):
  
  r, theta, phi = split_sph(ss)
  
  st = numpy.sin( theta )
  
  return numpy.stack( 
    [ r * st * numpy.cos( phi ), 
      r * st * numpy.sin( phi ), 
      r * numpy.cos( theta ), 
      ], 
    axis=-1, 
  )

def to_sph(xs):
  r = numpy.sqrt( numpy.sum( xs*xs, axis=-1, ), )
  return numpy.stack( 
    [ r,
      numpy.arccos( numpy.where( r > 0, xs[sl.z]/r, 0., ), ), 
      numpy.arctan2(xs[sl.y],xs[sl.x]),
      ], 
    axis=-1, 
  )

def append_cart(rtp):
  return numpy.hstack( [ rtp, to_cart(rtp), ], )

def append_sph(xyz):
  return numpy.hstack( [ xyz, to_sph(xyz), ], )

def renorm( x, nrm=1., ):
  if x.ndim == 1:
    q = numpy.linalg.norm( x, )
    if q > 0.:
      return nrm*x/q
    else:
      return x
  else:
    q = numpy.linalg.norm( x, axis=-1, )
    return nrm * numpy.where( q[:,None] > 0., x/q[:,None], x, )

#
# Testing
#

def test_to_cart():
  
  import itertools

  ts = numpy.linspace( 0., 1., 7, )
  ps = numpy.linspace( 0., 2., 9, )

  ss = numpy.array( [ [ 1., ti*numpy.pi, pi*numpy.pi, ] for ti, pi in itertools.product(ts,ps) ], )
  
  xs = to_cart(ss)

  print('\n'.join([ ' '.join( [ f'{qi:15.7e}' for qi in itertools.chain(xi,si) ], ) for xi, si in zip(xs,ss) ], ), )
  
  return

def test_to_sph():
  
  import itertools
  
  dx = numpy.array( [ [  1., 0., 0., ], [  0., 0., 0., ], [ -1.,  0.,  0., ], ], )
  dy = numpy.array( [ [  0., 1., 0., ], [  0., 0., 0., ], [  0., -1.,  0., ], ], )
  dz = numpy.array( [ [  0., 0., 1., ], [  0., 0., 0., ], [  0.,  0., -1., ], ], )

  xs = numpy.array( [ qi+qj+qk for qi, qj, qk in itertools.product(dx,dy,dz) ], )

  ss = to_sph(xs)

  print('\n'.join([ ' '.join( [ f'{qj:15.7e}' for qj in itertools.chain(xi,si) ], ) for xi, si in zip(xs,ss) ], ), )

  return

def test_change():
  
  import itertools
  
  xs_orig = 2.*numpy.random.random_sample( ( 1000, 3, ), ) - 1.
  xs = numpy.copy( xs_orig, )

  print( '\n# average error:', )

  num_iter = 100
  for i in range(num_iter):
    
    xs = to_cart(to_sph(xs))
    ds = numpy.linalg.norm( xs - xs_orig, axis=-1, )
    
    if (i+1)%10 == 0:
      print( f'# {i+1:3d} -> {numpy.average(ds):15.7e}', )


def test_renorm():
  
  import itertools
  
  xs = numpy.array(
    [ [  2.,  0.,  0., ],
      [  0.,  0.,  0., ],
      [ -2.,  0.,  0., ],
      [  1.,  1.,  1., ],
    ], 
  )

  qs = renorm(xs)
  
  print('\n'.join([ ' '.join( [ f'{w:15.7e}' for w in itertools.chain(xi,qi) ], ) for xi, qi in zip(xs,qs) ], ), )

  return

def test_renorm_random():
  
  import itertools
  
  xs = 2.*numpy.random.random_sample( ( 20, 3, ), ) - 1.

  qs = renorm(xs)
  
  print('\n'.join([ ' '.join( [ f'{w:15.7e}' for w in itertools.chain(xi,qi) ], ) for xi, qi in zip(xs,qs) ], ), )

  return


if __name__ == '__main__':
  utils.show_message()
  sys.exit( utils.run_test(info,globals()), )
else:
  print( utils.write_disclaimer(info), )
