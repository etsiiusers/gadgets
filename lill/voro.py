
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Voronoi decomposition of points in a sphere
#
# Copyright (C) 2021 Universidad Politécnica de Madrid',
#
# Version control:
# 2022.07.27: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - update to changes in module utils
# - use namespaces
# 2021.12.29: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - minor changes in "write_" functions
# 2021.09.22: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include transformation to unit sphere
# 2021.04.09: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add command line arguments
# - get points from file
# - improve output
# - include calculation of areas of each region
# 2021.03.26: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse
import itertools

import numpy
import scipy.spatial

import utils

# module info 
info = argparse.Namespace(
  name = 'voro',
  desc = 'Create Voronoi decomposition from points distributed in a unit sphere centered in the origin',
  author = 'Manuel Cotello Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 3, 1, ],
  copyright = 'Copyright (C) 2021-2022 Universidad Politécnica de Madrid',
)

# default module parameters
defs = argparse.Namespace(
  colx = 0,
  coly = 1,
  colz = 2,
  root = 'svor',
)

# print format for reals
def to_string(value):
    return f'{value:15.7e}'

# helper write functions
def write_points(sv,fm=to_string):
  return '\n# original points:\n\n' + '\n'.join( [ ' '.join( [ fm(xij) for xij in xi ], ) for xi in sv.points ], )

def write_vertex(v,fm=to_string):
  return ' '.join( [ fm(vi) for vi in v ], )

def write_vertices(sv,fm=to_string):
  return '\n# vertices:\n\n' + '\n'.join( [ write_vertex(vi,fm) for vi in sv.vertices ], )

def write_regions(sv,fm=to_string):
  s = ''
  s += '\n# regions:\n\n'
  for ri in sv.regions:
    it = itertools.chain( ri, [ ri[0], ], )
    s += '\n'.join( [ write_vertex(sv.vertices[k],fm) for k in it ], ) + '\n'
  return s

def write_areas(sv,fm=to_string):
  s = ''
  if getattr(sv, "calculate_areas", None):
    s = '\n'.join( [ fm(ai) for ai in sv.calculate_areas() ], )
  else:
    s = s + '# area calculation is not available in this scipy version {}'.format(scipy.version.version)
  return s

def dump(svor,header='',root='svor',fm=to_string):
  print(header + write_points(svor,fm),file=open('{}_points.txt'.format(root),'w'))
  print(header + write_vertices(svor,fm),file=open('{}_vertices.txt'.format(root),'w'))
  print(header + write_regions(svor,fm),file=open('{}_region.txt'.format(root),'w'))
  print(header + write_areas(svor,fm),file=open('{}_areas.txt'.format(root),'w'))
  return

def load_points(file_name,colx=0,coly=1,colz=2):
  return numpy.loadtxt(file_name,usecols=(colx,coly,colz),)

def to_unit_sphere(x):
  # get center
  x0 = numpy.average(x,axis=0)
  # traslate points
  x = x - x0
  # get norm for each point
  l = numpy.linalg.norm(x,axis=1)
  # renorm points to unit sphere
  x = x/l[:,None]
  return x

def doit(xs,is_unit=False):
  
  # create points in a centered unit sphere
  if not is_unit:
    x = to_unit_sphere(xs)

  # voronoi decomposition
  svor = scipy.spatial.SphericalVoronoi(x)
  svor.sort_vertices_of_regions()

  return svor

# get command line arguments
def get_args(info,defs):
  
  parser = argparse.ArgumentParser( description=f'{info.desc}', epilog=f'{info.copyright} ({info.email})', )
  
  # positional arguments
  parser.add_argument( 'inp', help='Input file name, in XML format', type=str, )

  # optional arguments
  parser.add_argument( '--colx', help=f'Select column for X coordinate in data file, default "{defs.colx}"', default=defs.colx, type=int, )
  parser.add_argument( '--coly', help=f'Select column for Y coordinate in data file, default "{defs.coly}"', default=defs.coly, type=int, )
  parser.add_argument( '--colz', help=f'Select column for Z coordinate in data file, default "{defs.colz}"', default=defs.colz, type=int, )
  
  parser.add_argument( '--root', help=f'Select root for output file names, default "{defs.root}"', default=defs.root, type=str, )

  # read arguments
  args = parser.parse_args()

  # validation
  assert ( os.path.isfile(args.inp) ), 'positional argument must be a valid file name'
  assert ( args.colx >= 0 and args.coly >= 0 and args.colz >= 0 ), 'column indices must be equal or greater than 0'

  return args

# main function
def main():
  
  # read command line arguments
  args = get_args(info,defs)

  # create info header
  header = utils.write_init(info,args)
  print(header)

  # load points
  x = load_points(args.inp,args.colx,args.coly,args.colz)
  
  # voronoi decomposition
  svor = doit(x)
  
  # print data
  dump(svor,header,args.root)
  
  return

# run this script
if __name__ == '__main__':
  sys.exit( main(), )
else:
  print( utils.write_disclaimer(info), )
