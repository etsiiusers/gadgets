
#
# Copyright (C) 2021 Universidad Politécnica de Madrid
#
# This file is part of gadgets.
#
# gadgets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gadgets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with gadgets.  If not, see <https://www.gnu.org/licenses/>.
#
# Module with utility functions to manage module/program info and 
# run test inside the module
#
# Version control 
# 2022.10.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - minor improvements
# 2022.07.27 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - change function names
# 2022.01.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include GIT reversion info
# 2021.12.30 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include date and time 
# 2021.10.21 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - Initial version
#

import sys
import argparse
import datetime
import shlex
import subprocess

# 
# script info
#

info = argparse.Namespace(
  name = 'utils',
  desc = 'Module with utility functions',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 0, 2, ],
  copyright = 'Copyright (C) 2021-2022 Universidad Politécnica de Madrid',
)

# 
# functions to write info messages
#

def write_dict(d):
  return '\n'.join([ '# {:>18s} = {}'.format(ki,vi) for ki, vi in d.items() ])

def write_ns(ns,use_filter=False):
  if use_filter:
    return write_dict( { name: value for name, value in vars(ns).items() if not name.startswith('_') })
  else:
    return write_dict(vars(ns))

def write_info(info):
  return '\n# info:\n' + write_ns(info)

def write_args(args):
  return '\n# args:\n' + write_ns(args)

def write_date():
  return '\n# date:\n' + write_dict( { 'now': datetime.datetime.now(), }, )

def write_git():
  cmmd = 'git log -1'
  proc = subprocess.Popen(shlex.split(cmmd),stdout=subprocess.PIPE,)
  outs, errs = proc.communicate()
  ls = outs.decode('utf8').splitlines()
  # get commit hash
  try:
    hash = ls[0].split()[1].strip()
    author = ls[1].split(' ',1)[1].strip()
    d = ls[2].split(' ',1)[1].strip()
    return '\n# GIT revision:\n' + write_dict( { 'hash': hash, 'author': author, 'date': d, }, )
  except:
    return '\n# GIT revision:\n' + write_dict( { '': 'not available', }, )

def write_init(info,args):
  return write_info(info) + '\n' + write_args(args) + '\n' + write_date() + '\n' + write_git() + '\n'

def write_disclaimer(info):
  return f'# {info.copyright} hereby disclaims all interest in program "{info.name}"'

def write_test(ls):
  return '\n# available test = ' + ', '.join(ls.keys()) + '\n'

#
# progress messages
#

class progress_message_c():
  
  level = 0

  prefix = '# '
  suffix = ' ... '
  tab = '  '

  def __init__( self, msg, verbose=1, fd=sys.stdout, use_flush=True, ):
    # save parameters
    self.verbose, self.fd, self.use_flush = verbose, fd, use_flush
    

    # set level
    self.level = progress_message_c.level 
    if self.verbose > 0:
      progress_message_c.level += 1
    
    # set tabulator
    self.tab = ''.join(self.level*[progress_message_c.tab])
    self.header = '\n' + progress_message_c.prefix + self.tab
    
    # send message
    self.open( msg, )
    
    return
    
  def send( self, msg, ):
    if self.verbose > 0:
      print( f'{msg}', end='', file=self.fd, )
      if self.use_flush:
        self.fd.flush()
    return
  
  def open( self, msg, ):
    self.send( self.header + msg + progress_message_c.suffix, )
    return

  def close( self, ):
    self.send( 'done\n', )
    return

  def __del__(self):
    if self.level+1 == progress_message_c.level:
      self.close()
    progress_message_c.level = 0
    return


# 
# other functions
#

def nmsanitizer(name):
  return name.strip().lower()

#
# functions for testing
#
def filter_tests( vars, key_test, ):
  return { name: func for name, func in vars.items() if name.startswith(key_test) and callable(func) }

def run_test( info, vars, key_test='test_', iosync=True, ):

  # create list of test functions
  list_test = filter_tests(vars,key_test)
  
  # print some info
  print(write_info(info))

  # print list of test
  print(write_test(list_test))

  # run test
  for name, func in list_test.items():

    # print test name
    print( f'\n# run test "{name}" ...', file=sys.stdout, )

    # run test
    func()

    # sync I/O
    if iosync:
      sys.stdout.flush()

  print() 

  return 0

def show_message():
  print('\n# warning :: this file is not intended for standalone run, launch tests ...',file=sys.stderr,)
  return
 

#
# testing
#

def test_progress_message():
  
  _ = progress_message_c( 'testing in verbose mode', verbose=1, )
  del(_)
  
  _ = progress_message_c( 'testing in verbose mode', verbose=0, )
  del(_)
  
  return

def test_progress_message_chain():

  progress_message_c( 'message 1', verbose=1, )
  progress_message_c( 'message 2', verbose=1, )
  progress_message_c( 'message 3', verbose=1, )
  progress_message_c( 'message 4', verbose=1, )
  
  _ = progress_message_c( 'message 1', verbose=1, )
  _ = progress_message_c( 'message 2', verbose=1, )
  _ = progress_message_c( 'message 3', verbose=1, )
  _ = progress_message_c( 'message 4', verbose=1, )
  
  a = progress_message_c( 'message 1', verbose=1, )
  b = progress_message_c( 'message 2', verbose=1, )
  c = progress_message_c( 'message 3', verbose=1, )
  d = progress_message_c( 'message 4', verbose=1, )

  return

# run this script
if __name__ == '__main__':
  show_message()
  sys.exit( run_test( info, globals(), ), )
else:
  print( write_disclaimer(info), )


