
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module to manage some volumes
# 
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2022.10.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - use Namespace for variable "info"
# 2021.12.29 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - new method to load shapes
# 2021.09.07 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - update code
# 2019.03.06 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy

import utils
import ioxml

# module info
info = argparse.Namespace( 
  name = 'volume',
  desc = 'Module to manage common geometries',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 1, 1, ],
  copyright = 'Copyright (C) 2019-2022 Universidad Politécnica de Madrid',
)

# names for XML io

tag = argparse.Namespace(
  shape = 'shape',
)

att = argparse.Namespace(
  type = 'type',
)

#
# shapes
#

class plane_c():
 
  # class name
  name = 'plane'

  # for XML io
  tag = argparse.Namespace(
    point = 'point',
    normal = 'normal',
    )

  att = argparse.Namespace(
    value = 'value',
    )
  
  def __init__(self,x,n):
    
    '''
    Constructor for an object of class plane_c
    
    All in cartesian coordinates
    
    -> in
    x: point of the plane, array of 3 floats
    n: normal vector to the plane, array of 3 floats
    '''
    
    # parameter validation
    assert ( numpy.linalg.norm(n) > 0.0 ) , 'bad definition of plane normal vector'

    # save parameters
    self.x, self.n = numpy.array(x), numpy.array(n)

    # renormalization
    self.n = self.n/numpy.linalg.norm(self.n)

    return

  def inside(self,x,tol=1.0e-15):
    dx = x - self.x
    dx0 = numpy.linalg.norm(dx)
    if dx0 > tol:
      res = numpy.dot(dx,self.n)/dx0
      return ( res < tol )
    else:
      return True

  def cross(self,x,v):
     
    # validation
    assert numpy.all( [ numpy.linalg.norm(vi) > 0. for vi in v ], ), 'bad definition for line direction'
    
    a = numpy.dot(v,self.n)
    
    return numpy.piecewise( a, [ a != 0., ], [ numpy.dot(self.x - x,self.n)/a, numpy.nan, ], )
    '''
    if a != 0.0:
      b = numpy.dot(x - self.x,self.n)
      return numpy.array( [ -b/a, ], )
    else:
      return numpy.array( [ ], )
    '''
  
  @staticmethod
  def fromxml(root):
    elem_point = ioxml.find_unique(root,plane_c.tag.point)
    elem_normal = ioxml.find_unique(root,plane_c.tag.normal)
    return plane_c( ioxml.read_attrib(elem_point,plane_c.att.value,float,3),
                    ioxml.read_attrib(elem_normal,plane_c.att.value,float,3),
                    )

  def toxml(self):
    root = ioxml.create_element( tag.shape, { att.type: self.name, }, )
    elem_point = ioxml.create_element( self.tag.point, { self.att.value: self.x, }, )
    elem_normal = ioxml.create_element( self.tag.normal, { self.att.value: self.n, }, )
    return ioxml.append_element(root,elem_point,elem_normal)


class sphere_c():
  
  # class name
  name = 'sphere'
  
  # tags and attributes for XML
  tag = argparse.Namespace(
    radius = 'radius',
    position = 'position',
    )

  att = argparse.Namespace( 
    value = 'value',
    )

  def __init__(self,pos,radius):
    # parameters
    self.pos = numpy.array(pos)
    self.radius = radius
    assert ( self.pos.ndim == 1 and self.pos.shape[0] == 3 ), 'bad dimensions in sphere_c ctor'
    assert ( self.radius > 0.0e+00 ), 'radius must be a positive defined value'
    # auxiliar variables
    self.r2 = self.radius*self.radius
    return

  def inside(self,x):
    return numpy.linalg.norm(x - self.pos) < self.radius

  def cross(self,x,v):
    
    # validation
    assert ( numpy.linalg.norm(v) > 0. ), 'bad definition for line direction'

    # compute position difference
    dx = x - self.pos
    
    # compute equation coefficients
    c = numpy.dot(dx,dx) - self.r2
    b = 2.0e+00*numpy.dot(dx,v)
    a = numpy.dot(v,v)
    
    # solve equation
    ans = []
    disc = b*b - 4.0e+00*a*c
    if disc > 0.0e+00:
      disc = numpy.sqrt(disc)
      return numpy.array( [ ((-b + disc)/a/2.0e+00), ((-b - disc)/a/2.0e+00), ], )
    elif disc == 0.0:
      return numpy.array( [ ((-b)/a/2.0e+00), ], )
    else:
      return numpy.array( [], )

  @staticmethod
  def fromxml(node):
    np = ioxml.find_unique(node,sphere_c.tag.position)
    nr = ioxml.find_unique(node,sphere_c.tag.radius)
    return sphere_c( ioxml.read_attrib(np,sphere_c.att.value,float,3),
                     ioxml.read_attrib(nr,sphere_c.att.value,float,1)[0],
                     )

  def toxml(self,):
    root = ioxml.create_element( tag.shape, { att.type: self.name, }, )
    np = ioxml.create_element( self.tag.position, { self.att.value: self.pos, }, )
    nr = ioxml.create_element( self.tag.radius, { self.att.value: self.radius, }, )
    return ioxml.append_element(root,np,nr)


# list of available shapes

_list_shapes = { plane_c.name: plane_c.fromxml, 
                 sphere_c.name: sphere_c.fromxml, 
                 }

def select_loader(name):
  return _list_shapes[name.strip()]

# load shape from XML node

def _helper_load(node):
  tp = ioxml.read_attrib(node,att.type,)
  assert ( len(tp) == 1 ), f'wrong number of definitions of shape type, just one allowed ({len(tp)})'
  return select_loader(tp[0])(node)

def load(node):
  return [ _helper_load(ni) for ni in ioxml.find_all(node,tag.shape) ]

#
# Testing
#

def test_plane_c():
  pos = [ 0., 0., 0., ]
  dir = [ 0., 0., 1., ]
  pl = plane_c(pos,dir)
  print(utils.write_ns(pl))
  return

def test_sphere_c():
  pos = [ 0., 0., 0., ]
  radius = 1.
  sp = sphere_c(pos,radius)
  print(utils.write_ns(sp))
  return

def test_plane_fromxml():

  s = '''
  <shape type="plane">
    <point value="1.; 2.; 3.;" units="m" />
    <normal value="3.; 2.; 1.;" />
  </shape>
  '''
  
  root = ioxml.fromstring(s)
  
  pl = plane_c.fromxml(root)
  
  print(s)
  print(utils.write_ns(pl))

  return

def test_sphere_fromxml():

  s = '''
  <shape type="sphere">
    <position value="1.; 2.; 3.;" units="m" />
    <radius value="4.;" units="m" />
  </shape>
  '''

  root = ioxml.fromstring(s)

  sp = sphere_c.fromxml(root)
  
  print(s)
  print(utils.write_ns(sp))

  return

def test_plane_toxml():
 
  # create a plane
  pos = [ 0., 0., 0., ]
  dir = [ 0., 0., 1., ]
  pl = plane_c(pos,dir)
  
  # dump
  print(ioxml.tostring(pl.toxml()))

  return

def test_sphere_toxml():
 
  # create a plane
  pos = [ 0., 0., 0., ]
  r = 1.
  sp = sphere_c(pos,r)
  
  # dump
  print(ioxml.tostring(sp.toxml()))

  return

if __name__ == '__main__':
  utils.show_message()
  sys.exit(utils.run_test(info,globals()))
else:
  print( utils.write_disclaimer(info), )
