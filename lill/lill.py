
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Program to compute the illumination of a pshere by several lasers
#
# Copyright (C) 2019-2022 Universidad Poliecnica de Madrid
#
# Version control:
# 2022.07.27 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add comments
# - minor changes in global variables
# 2022.06.10 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix error in the energy deposition, need to include geometric attenuation 
#   factor
# - include check: none of the mesh points should be coincident with the beam 
#   focus to avoid a singular point in the laser deposited intensity
# 2022.03.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix names of output file
# - fix error in print format for touch counts
# 2022.02.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix units in header
# 2022.01.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - code updated to last changes in modules
# - vectorization
# - reshape write functions
# 2021.12.30 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include new scheme of XML io
# 2021.12.29 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - update to last changes in modules
# 2021.11.23 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include functions for validation
# - reshape some write_ functions
# 2021.09.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include some validation functions; check number of times that each point is 
#   touched by a laser anf average angle of incidence os lasers in each point
# - add command line arguments
# - add info messages based on a level of verbosity
# 2021.09.07 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - move projection of illumination point to laser base plane to procile_c 
#   class
# - create Voronoi decomposition to get area associated to each node
# 2021.04.09 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - changes in prints
# 2021.03.25 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - improve criteria for relative position between ray-target-point
# 2019.03.11 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - improve laser illumination of the spherical target, now focus can take any 
#   position respect to the laser plane and target
# - improve output
# 2019.03.08 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - load laser parameters from file and create laser profiles
# - add command line argument
# 2019.03.06 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse
import itertools

import numpy
import scipy.spatial

import utils
import printer
import coord
import ioxml
import mesh
import dist
import beam
import volume
import harmonics
import voro 

# program info
info = argparse.Namespace(
  name = 'lill',
  desc = 'Laser illumination of a spherical target',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version =  [ 3, 1, 0, ],
  copyright = 'Copyright (C) 2019-2022 Universidad Politécnica de Madrid',
)

# program default parameters
defs = argparse.Namespace(
  nodes = 1000,
  l_min = 0,
  l_max = 10,
  verbose = 1,
)

# tags to select data from XML file
tag = argparse.Namespace(
  target = 'target',
  laser = 'laser',
)

# empty printer
pr_silent = printer.empty_c()

# 
# helper functions to write data
#

def write_data( p, e, hfunc, ):

  '''
  Print data to file

  -> in
  p: cartesian coordinates of the points, in m
  e: intensity, in W
  hfunc: functor to evaluate spherical harmonics expansion
  '''
  
  # column descriptors
  lh = [ 'point coordinate X, in m',
         'point coordinate Y, in m',
         'point coordinate Z, in m',
         'point coordinate R, in m',
         'point coordinate Theta, in radians',
         'point coordinate Phi, in radians',
         'laser intensity, in W/m2',
         'approximated laser intensity using the harmonics expansion, in W/m2',
         ]
  
  return ''.join( [ 
    '\n# column description:\n',
    ''.join( [ f'# column_{i:02d} = {li}\n' for i, li in enumerate(lh) ], ) + '\n',
    '\n'.join( [ ''.join( [ f' {wj:17.9e}' for wj in itertools.chain( pi, ri, [ ei, hfunc(ri).real, ],) ], ) for pi, ri, ei in zip(p,coord.to_sph(p),e) ], ) + '\n',
    ], )


def write_harm(f,hm,l_min,l_max):
  return ''.join( [ 
    '\n# spherical harmonics coefficients:\n',
    ''.join( [ ''.join( [ f'# l m flm :: {l:4d} {m:4d} {flm.real:17.9e} {flm.imag:17.9e}j\n' for m, flm in fl.items() ], ) for l, fl in f.items() ], ),
    '\n# spectral power of the spherical harmonics expansion:\n',
    ''.join( [ f'# l sl ql :: {l:4d} {hm.power(l):17.9e} {hm.power_norm(l):17.9e}\n' for l in range(l_min,l_max+1) ], ),
    ], )
  

def write_touch_points( xs, cs):
  return '\n' + ''.join( [ ''.join( [ f' {q:15.7e}' for q in itertools.chain(xi,ri) ], ) + f' {ci:6d}\n' for xi, ri, ci in zip(xs,coord.to_sph(xs),cs) ], )


def write_touch_laser(list_laser,cs):
  return '\n' + ''.join( [ f' {i:4d}' + ' '.join( [ f' {pij:15.7e}' for pij in li.base.base ]) + f' {ci:6d}\n' for ( i, li, ), ci in zip(enumerate(list_laser),cs) ], )


def write_angle(ps,ang):
  return '\n' + '\n'.join( [ ' '.join( [ '{:15.7e}'.format(q) for q in itertools.chain(pi,wi) ]) for pi, wi in zip(ps,ang) ], )


def write_pointing(xs):
  return '\n' + '\n'.join( [ ' '.join( [ '{:15.7e}'.format(xij) for xij in xi ]) for xi in coord.append_sph(xs) ] )


# 
# helper functions to load data
#

def load_input( file_name, pr, ):
  _ = pr.child( 'info: loading input file', )
  tree = ioxml.load(file_name)
  return tree.getroot()


def load_volume( root, pr, ):
  _ = pr.child( 'info: loading target', )
  ls = volume.load(ioxml.find_unique(root,tag.target))
  assert len(ls) == 1, f'wrong number of targets defined, just one allowed ({len(ls)})'
  return ls[0]


def load_laser( root, pr, ):
  _ = pr.child( 'info: loading lasers', )
  def _helper_load_laser(elem_laser):
    elem_beam = ioxml.find_unique(elem_laser,beam.beam_c.name)
    return beam.load(elem_beam)
  return [ _helper_load_laser(elem) for elem in ioxml.find_all(root,tag.laser) ]

#
# mesh creation
#

def build_mesh( num_nodes, pr, ):
  _ = pr.child( 'info: generating mesh of nodes', )
  return mesh.build_rays( num_nodes,'fibonacci')

#
# voronoi decomposition
#

def build_voro( nodes, pr, ):
  _ = pr.child('info: build Voronoi decomposition', )
  return voro.doit(nodes)

def build_areas( radius, nodes, svor, pr, ):
  try:
    _ = pr.child('info: compute Voronoi areas', )
    areas = svor.calculate_areas()
  except AttributeError:
    _ = pr.child('info: approximate Voronoi areas', )
    areas = numpy.ones( ( nodes.shape[0], ))
  areas *= 4.0*numpy.pi*radius*radius/numpy.sum(areas)
  return areas 

def dump_voro( svor, header, root, pr, ):
  _ = pr.child( 'info: dump voronoi decomposition', )
  voro.dump( svor, header, 'lill_voro', )
  return

# 
# harmonics analysis
#

def analyze_harmonics( xs, edep, areas, pr, ):
  _ = pr.child('info: do analysis')
  return harmonics.moments_c( xs, edep, areas, )

# 
# evaluation functions
#
def check_mesh(ps,list_laser):

  def __helper_check_mesh(ps,lp,tol=1.e-15):
    if lp.is_focused:
      l = ps - lp.focus
      l = numpy.sum( l*l, axis=-1, )
      return numpy.all( l > tol )
    else:
      return True
  
  return all( [ __helper_check_mesh(ps,lp) for lp in list_laser ], )

def eval_edep(ps,lp,target):

  # get equivalent point to pi in laser plane
  x, v, w = lp(ps)
  
  # normal vector to the target, renormalize to 1.
  n = coord.renorm(ps - target.pos[None,:])

  # check if point is in the illuminated side of the target
  cs = numpy.sum( n*v, axis=-1, )

  # apply geometric attenuation factor
  if lp.is_focused:
    l2 = ps - lp.focus
    l2 = numpy.sum( l2*l2, axis=-1, )
    w = w/l2
  
  return numpy.where( cs < 0., -cs*w, 0. ) 
  

def eval_ang(ps,lp,target):

  # get equivalent point to pi in laser plane
  x, v, w = lp(ps)
  
  # normal vector to the target
  n = ps - target.pos
  n = n/numpy.linalg.norm( n, axis=-1, )[:,None]

  # check if point is in the illuminated side of the target
  cs = numpy.sum( n*v, axis=-1, )

  return numpy.arccos(- cs)


def laserdep( list_laser, points, target, pr, ):
  
  '''
  Compute laser intensity in a defined target of a spherical target.

  -> in
  list_laser: list of available laser beam, type procile_c
  points: list of points on the target to compute intensity, in cartesian 
          coordinates, in m
  target: parameters of the target, type sphere_c

  <- out
  (1) intensity in each point, array of floats
  '''
  
  _ = pr.child( 'info: compute laser energy deposition', )
  
  return numpy.sum( 
    numpy.array( [ eval_edep(points,lp,target) for lp in list_laser ], ),
    axis=0, 
    )

def lasertouch( list_laser, points, target, pr, ):
  
  '''
  Compute how many lasers illuminates each point. This function is 
  intended for validation purposes
  
  -> in
  list_laser: list of available laser beam, type procile_c
  points: list of points on the target to compute intensity, in cartesian 
          coordinates, in m
  <- out
  (1) array of integers, counts of lasers per point
  (2) array of integers, counts of points per laser
  '''
  
  _ = pr.child( 'info: compute laser hits', )
  
  eval_touch = lambda ps, lp, target: numpy.where( eval_edep(ps,lp,target) > 0., 1, 0, )
  
  return numpy.sum( 
    numpy.array( [ eval_touch(points,lp,target) for lp in list_laser ], ),
    axis=0, 
    dtype=int,
    )
  
def laserangle( list_laser, points, target, pr, ):
  
  '''
  Compute average incidence angle of the lasers on each point
  
  -> in
  list_laser: list of available laser beam, type procile_c
  points: list of points on the target to compute intensity, in cartesian 
          coordinates, in m

  <- out
  (1) average incidence angle and cosine of incidence angle for each 
      point, array of floats
  '''
  
  _ = pr.child( 'info: compute laser hit angles', )
  
  return numpy.array( [ eval_ang(points,lp,target) for lp in list_laser ], )


def laser_pointing( list_laser, target, pr, ):

  def _cross_axis_target(las,target):

    ax = las.axis
    dx = las.xref() - target.pos
    d2 = numpy.dot(dx,dx)
    r2 = target.radius*target.radius

    ans = numpy.array( numpy.nan * numpy.ones( ( 3, )) )

    if d2 > r2:
      
      b = 2.*numpy.dot(ax,dx)
      c = d2 - r2
    
      disc = b*b - 4.*c

      if disc > 0.:
        disc = numpy.sqrt(disc)
        lm, lp = (-b - disc)/2., (-b + disc)/2.
        l = min(lm,lp)
      elif disc == 0.:
        l = -b/2.

      if l > 0.0:
        ans = las.xref() + l*ax

    return ans

  _ = pr.child( 'info: compute laser pointing', )
  
  return numpy.array( [ _cross_axis_target(li,target) for li in list_laser ] )


#
# read command line arguments
#

def get_args(info,defs):

  '''
  Define command line arguments and parse them
  '''
  
  # build command line parser
  parser = argparse.ArgumentParser( description=f'{info.desc}', epilog=f'{info.copyright} ({info.email})', )
  
  # positional arguments
  parser.add_argument( 'inp', help='Input file name, in XML format', type=str, )
  
  # optional arguments
  parser.add_argument( '--verbose', help=f'Define level of verbosity, default "{defs.verbose}"', type=int, default=defs.verbose, )
  
  parser.add_argument( '--nodes', help=f'Define number of nodes in tagret, default "{defs.nodes}"', type=int, default=defs.nodes, )

  parser.add_argument( '--l_min', help=f'Define minimum L number for harmonic projection, default "{defs.l_min}"', type=int, default=defs.l_min, )
  parser.add_argument( '--l_max', help=f'Define maximum L number for harmonic projection, default "{defs.l_max}"', type=int, default=defs.l_max, )
  
  parser.add_argument( '--print_voronoi', help='Print Voronoi decomposition', action='store_true', )
  parser.add_argument( '--touch', help='Compute touched points and print results', action='store_true', )
  parser.add_argument( '--pointing', help='Compute pointing coordinates of the laser over the target', action='store_true', )

  # parse arguments
  args = parser.parse_args()

  # validations
  assert args.nodes > 0, 'wrong number of nodes'
  assert args.l_min >= 0 and args.l_max >= args.l_min, 'wrong L number limits'

  return args


# 
# main driver routine
#

def main(info,defs):

  #numpy.warnings.filterwarnings( 'error', category=numpy.VisibleDeprecationWarning, ) 

  # read command line arguments
  args = get_args(info,defs)

  # print program info
  header = utils.write_init(info,args)
  print(header)
  
  # select printer
  bl = printer.select_builder( args.verbose, )
  pr = bl( 'begin program', )

  # read XML input file and return DOM
  root = load_input( args.inp, pr, )

  # initialize target description
  target = load_volume( root, pr, )

  # create mesh of nodes in the outer surface of the target
  nodes = build_mesh( args.nodes, pr, )

  # build voronio decomposition with nodes
  svor = build_voro( nodes, pr, )
  
  # get area associated with each point
  areas = build_areas( target.radius, nodes, svor, pr, )

  # create points from nodes
  ps = target.pos + target.radius*svor.points

  # print data for voronoi decomposition
  if args.print_voronoi:
    dump_voro( svor, header, pr, )

  # load laser parameters from file
  list_laser = load_laser( root, pr, )
  
  # check if some any mesh point is coincident with the focus
  assert check_mesh(ps,list_laser), "mesh node coincident with beam focus, this will produce a singularity en deposited intensity in that node"

  # print laser pointing positions
  if args.pointing:
    xc = laser_pointing( list_laser, target, pr, )
    print(header + write_pointing(xc),file=open('lill_laser_pointing.txt','w'))

  # compute deposited energy
  edep = laserdep( list_laser, ps, target, pr, )

  # get touched points
  if args.touch:
    # get touch
    cs = lasertouch( list_laser, ps, target, pr, )
    print( header + write_touch_points(ps,cs), file=open('lill_touch_points.txt','w'), )
    # get angles
    ang = laserangle( list_laser, ps, target, pr, )
    print(header + write_angle(ps,ang),file=open('lill_angle_points.txt','w'))

  # spectral analysis
  hm = analyze_harmonics( ps, edep, areas, pr, )
  
  assert (args.l_min >= 0) and (args.l_max >= args.l_min), 'wrong L number limits' 

  # coefficients of the spherical harmonics expansion
  flm = hm(args.l_min,args.l_max)
  
  # create spherical harmonics expansion
  fh = harmonics.functor_c(svor.points,edep,args.l_max)

  # print spherical harmonics
  print(write_harm(flm,hm,args.l_min,args.l_max))

  # print laser intensity
  print(write_data(ps,edep,fh))

  return 0

# run program
if __name__ == '__main__':
  sys.exit(main(info,defs))
else:
  print( utils.write_disclaimer(info), )

