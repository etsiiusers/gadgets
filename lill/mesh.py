
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module to create a pherical mesh
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2022.07.29 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - use namespaces
# 2021.12.29 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - simplify code
# - create common interface
# - add testing
# 2021.03.25 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add distribution of random points
# - wrap module info in a dictionary
# - remove exception class
# - fix comments
# 2019.03.07 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include the calculation of points in the unit sphere using Legendre or 
#   Chebyshev polynomials
# - include equaly distributed points in a sphere based on the Fibonacci 
#   algorithm and on the spiral algorithm
# 2019.03.06 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse
import itertools

import numpy

import utils

# module info
info = argparse.Namespace(
  name = 'mesh',
  desc = 'Module to create distributions of points in a sphere',
  author = 'Manuel Cotelo Ferreiro', 
  email = 'manuel.cotelo@upm.es',
  year = 2021,
  version = [ 2, 0, 0, ],
  copyright = 'Copyright (C) 2019-2021 Universidad Politécnica de Madrid',
)

class rays_uniform_c():
  
  '''
  Obtain a set of points in a spherical shell of radius 1 using a uniform 
  distribution in theta and phi
  '''

  name = 'uniform'
  
  def __init__(self,):
    # define parameters
    self.theta_lo, self.theta_hi = 0., numpy.pi
    self.phi_lo, self.phi_hi = 0., 2.*numpy.pi
    return

  def eval(self,num_theta,num_phi):
    
    '''
    Compute points in a centered unit sphere 

    -> in
    num_theta: number of divisions of azimutal angle
    num_phi: numbe of division of longitudinal angle

    <- out
    (1) points in the centered unit sphere
    '''

    # validation
    assert num_theta > 0, 'bad sizes for theta angle'
    assert num_phi > 0, 'bad sizes for phi angle'
    
    # grid
    theta = numpy.linspace( self.theta_lo, self.theta_hi, num_theta, endpoint=True, )
    phi = numpy.linspace( self.phi_lo, self.phi_hi, num_phi, endpoint=False, )

    list_nodes = []

    for ti in theta:
    
      ct = numpy.cos(ti)
      st = numpy.sqrt(1.0e+00 - ct*ct)

      if st != 0.0:
        for pi in phi:
          cp, sp = numpy.cos(pi), numpy.sin(pi)
          ri = numpy.array( [ st*cp, st*sp, ct, ], )
          list_nodes.append(ri/numpy.linalg.norm(ri))
      else:
        list_nodes.append(numpy.array( [ 0., 0., 1., ], ))

    return numpy.array(list_nodes)

  def __call__(self,num_rays):
    # validation
    assert num_rays > 0, 'number of rays must be greater than 0'
    # define number of divisions in theta and phi
    num_theta = numpy.ceil(numpy.sqrt(num_rays))
    num_phi = numpy.ceil(num_rays/num_theta)
    return self.eval(int(num_theta),int(num_phi))


class rays_uniform_area_c():
  
  '''
  Obtain a set of points in a spherical shell of radius 1 using an uniform 
  distribution of phi and a distribution on theta that produce sections of 
  the sphere of equal area
  '''

  name = 'uniform_area'
  
  def __init__(self):
    self.phi_lo, self.phi_hi = 0., 2.*numpy.pi
    return

  def eval(self,num_theta,num_phi):
    
    '''
    Compute points in a centered unit sphere 

    -> in
    num_theta: number of divisions of azimutal angle
    num_phi: numbe of division of longitudinal angle

    <- out
    (1) points in the centered unit sphere
    '''
    
    # validation
    assert num_theta > 0, 'bad sizes for theta angle'
    assert num_phi > 0, 'bad sizes for phi angle'
    
    # grid in phi angle
    phi = numpy.linspace( self.phi_lo, self.phi_hi, num_phi, endpoint=False, )
  
    ai = 4.0e+00*numpy.pi/num_theta
    delta = ai/2.0e+00/numpy.pi
  
    list_nodes = []
  
    ct_last = None

    for i in range(num_theta):
    
      ct = ct_last
      if ct == None:
        ct = 1.0e+00
      else:
        ct = ct - delta
      ct_last = ct

      st = numpy.sqrt(1.0e+00 - ct*ct)
    
      if st != 0.0:
        for pi in phi:
          cp, sp = numpy.cos(pi), numpy.sin(pi)
          ri = [ st*cp, st*sp, ct, ]
          list_nodes.append(ri/numpy.linalg.norm(ri))
      else:
        list_nodes.append( numpy.array( [ 0., 0., 1., ], ) )

    return numpy.array(list_nodes)

  def __call__(self,num_rays):
    # validation
    assert num_rays > 0, 'number of rays must be greater than 0'
    # define number of divisions in theta and phi
    num_theta = numpy.ceil(numpy.sqrt(num_rays))
    num_phi = numpy.ceil(num_rays/num_theta)
    return self.eval(int(num_theta),int(num_phi))


class rays_legendre_c():
  
  '''
  Obtain a set of points in a spherical shell of radius 1 using the roots of 
  the Legendre polynomials of a defined order
  '''

  name = 'legendre'
  
  def __init__(self):
    return

  def eval(self,order):
    
    '''
    Compute points in a centered unit sphere 
    
    -> in
    order: integer value greater than 2 with the order of the chebyshev 
	       polynomial used for the distribution

    <- out
    (1) points in the unit sphere
    '''

    # validation
    assert order > 1, 'order must be greater than 0'
  
    # initialize coefficients, just keep the highest order
    # F(x) = sum a_{i} * P_{i}(x)
    # all a_{i} = 0.0 except a_{order} = 1.0e+00
    x = numpy.zeros(order+1)
    x[-1] = 1.0e+00

    # get zeros of F(x)
    r = numpy.polynomial.legendre.Legendre(x).roots()
  
    # combine in 3D to create directions
    list_nodes = numpy.array(list(itertools.product(r,r,r)))

    # normalize directions
    return numpy.array( [ ni/numpy.linalg.norm(ni) for ni in list_nodes ], )

  def __call__(self,num_rays):
    # validation
    assert num_rays > 0, 'number of rays must be greater than 0'
    order = int(numpy.ceil(numpy.cbrt(num_rays)))
    return self.eval(order)


class rays_chebyshev_c():
  
  '''
  Obtain a set of points in a spherical shell of radius 1 using the roots of 
  the Chebyshev polynomials of a defined order
  '''

  name = 'chebyshev'

  def __init__(self,):
    return

  def eval(self,order):
    
    '''
    Compute points in a centered unit sphere 

    -> in
    order: integer value greater than 2 with the order of the chebyshev 
	       polynomial used for the distribution

    <- out
    (1) point in the unit sphere
    '''

    # validation
    assert order > 1, 'order must be greater than 0'
  
    # initialize coefficients, just keep the highest order
    # F(x) = sum a_{i} * P_{i}(x)
    # all a_{i} = 0.0 except a_{order} = 1.0e+00
    x = numpy.zeros(order+1)
    x[-1] = 1.0e+00

    r = numpy.polynomial.chebyshev.chebroots(x)
  
    list_nodes = numpy.array(list(itertools.product(r,r,r)))

    return numpy.array( [ ni/numpy.linalg.norm(ni) for ni in list_nodes ], )

  def __call__(self,num_rays):
    # validation
    assert num_rays > 0, 'number of rays must be greater than 0'
    # guess order
    order = int(numpy.ceil(numpy.cbrt(num_rays)))
    return self.eval(order)


class rays_fibonacci_c():

  '''
  Obtain a set of points in a spherical shell of radius 1 using the Fibonaci 
  algorithm
  '''

  name = 'fibonacci'
  
  def __init__(self,randomize=True):
    self.randomize = randomize
    self.inc = numpy.pi*(3.0e+00 - numpy.sqrt(5.0e+00))
    return

  def __call__(self,num_rays):
    
    '''
    Compute points in a centered unit sphere 

    -> in
    num_rays: number of points to generate

    <- out
    (1) point in the unit sphere
    '''
    
    # validation
    assert num_rays > 0, 'number of rays must be greater than 0'
  
    rnd = 1.0e+00
    if self.randomize:
      rnd = numpy.random.random() * num_rays

    offset = 2.0e+00/num_rays

    def _helper_eval(i):
      
      y = ((i*offset) - 1.) + (offset/2.);
      r = numpy.sqrt(1.0 - y*y)
      phi = ((i + rnd)%num_rays)*self.inc

      x, z = r*numpy.cos(phi), r*numpy.sin(phi)

      node = numpy.array( [ x, y, z, ], )

      return node/numpy.linalg.norm(node)

    return numpy.array( [ _helper_eval(i) for i in range(num_rays) ], )


class rays_spiral_c():
  
  '''
  Obtain a set of points in a spherical shell of radius 1 using the spiral
  algorithm
  '''
  
  name = 'spiral'

  def __init__(self,):
    return

  def __call__(self,num_rays):

    '''
    Compute points in a centered unit sphere 

    -> in
    num_rays: number of points to generate

    <- out
    (1) point in the unit sphere
    '''
  
    # validation
    assert num_rays > 0, 'number of rays must be greater than 0'
  
    js = numpy.arange(0, num_rays, dtype=float) + 0.5

    phi = numpy.arccos(1. - 2.*js/num_rays)
    theta = (numpy.pi*numpy.sqrt(5.))*js
    
    ct, st = numpy.cos(theta), numpy.sin(theta)
    cp, sp = numpy.cos(phi), numpy.sin(phi)

    return numpy.stack( [ ct*sp, st*sp, cp, ], axis=-1, )
  

class rays_random_c():
  
  '''
  Obtain a random sample of points in a centered unit sphere
  '''
  
  name = 'random'

  def __init__(self):
    return

  def __call__(self,num_rays):
    
    '''
    Compute points in a centered unit sphere 

    -> in
    num_rays: number of points to generate

    <- out
    (1) point in the unit sphere
    '''
    
    # validation
    assert num_rays > 0, 'number of rays must be greater than 0'
  
    c = 1.0 - 2.0*numpy.random.random( [ num_rays, ])
    s = numpy.sqrt(1.0 - c*c)
    phi = 2.0*numpy.pi*numpy.random.random( [ num_rays, ])
  
    return numpy.stack( [ s*numpy.cos(phi), s*numpy.sin(phi), c], axis=-1, )


class cell_c():
  
  '''
  Class to store a triangular spherical sector
  '''

  def __init__(self,vx):
    # save parmeters
    self.vx  = numpy.array(vx)
    # store center
    self.c = numpy.sum( self.vx, axis=0, )
    self.c = self.c/numpy.linalg.norm(self.c) 
    # store area
    self.area = 0.5*numpy.linalg.norm(numpy.cross(self.vx[0]-self.vx[1],self.vx[0]-self.vx[2]))
    # store solid angle
    w = numpy.array( [ numpy.dot(self.vx[0],self.vx[1]),
                       numpy.dot(self.vx[0],self.vx[2]),
                       numpy.dot(self.vx[1],self.vx[2]), 
                       ], )
    w = numpy.arccos(w)/2.0e+00
    ws = numpy.sum(w)/2.0e+00
    w = ws - w
    self.sangle = numpy.tan(ws)*numpy.tan(w[0])*numpy.tan(w[1])*numpy.tan(w[2])
    self.sangle = numpy.sqrt(self.sangle)
    self.sangle = 4.0e+00*numpy.arctan(self.sangle)
    return


class rays_triang_c():
  
  name = 'triangle'

  # define base nodes
  _e1 = numpy.array( [ 1.0e+00, 0.0e+00, 0.0e+00, ])
  _e2 = numpy.array( [ 0.0e+00, 1.0e+00, 0.0e+00, ])
  _e3 = numpy.array( [ 0.0e+00, 0.0e+00, 1.0e+00, ])

  # define sphere cells for order 0
  _mesh_init = [ cell_c( [  _e1,  _e2,  _e3, ]), 
                 cell_c( [  _e1,  _e2, -_e3, ]),
                 cell_c( [  _e1, -_e2,  _e3, ]),
                 cell_c( [  _e1, -_e2, -_e3, ]),
                 cell_c( [ -_e1,  _e2,  _e3, ]), 
                 cell_c( [ -_e1,  _e2, -_e3, ]),
                 cell_c( [ -_e1, -_e2,  _e3, ]),
                 cell_c( [ -_e1, -_e2, -_e3, ]),
                 ]

  def __init__(self):
    return

  @staticmethod
  def refine(mi):
    
    v1, v2, v3 = mi.vx[0], mi.vx[1], mi.vx[2]

    w12 = 0.5*(v1 + v2)
    w12 = w12/numpy.linalg.norm(w12)
    
    w13 = 0.5*(v1 + v3)
    w13 = w13/numpy.linalg.norm(w13)
    
    w23 = 0.5*(v2 + v3)
    w23 = w23/numpy.linalg.norm(w23)

    return [ cell_c( [  v1, w12, w13, ]),
             cell_c( [  v2, w12, w23, ]),
             cell_c( [  v3, w13, w23, ]),
             cell_c( [ w12, w13, w23, ]),
             ]

  def refine_level(self,ms,tol=3.):
  
    list_cell = []

    for mi in ms:
      list_cell.extend(self.refine(mi))
   
    amin = numpy.amin( [ ci.area for ci in list_cell ], )

    mf = []
    for ci in list_cell:
      if ci.area/amin > tol:
        mf.extend(self.refine(ci))
      else:
        mf.append(ci)

    return mf

  def eval(self,order):
    # validation
    assert order > 1, 'order must be greater than 0'
    # initialize cells
    ms = self._mesh_init
    # refine cells
    for i in range(order):
      ms = self.refine_level(ms)
    return numpy.array( [ mi.c for mi in ms ], )

  def __call__(self,num_rays):
    assert num_rays > 0, 'number of rays must be greater than 0'
    order = int(numpy.ceil(numpy.log(num_rays/8.)/numpy.log(4.)))
    return self.eval(order)


# define manager
_list_models = { rays_uniform_c.name: rays_uniform_c,
                 rays_uniform_area_c.name: rays_uniform_area_c,
                 rays_legendre_c.name: rays_legendre_c,
                 rays_chebyshev_c.name: rays_chebyshev_c,
                 rays_fibonacci_c.name: rays_fibonacci_c,
                 rays_spiral_c.name: rays_spiral_c,
                 rays_random_c.name: rays_random_c,
                 rays_triang_c.name: rays_triang_c,
                 }

def build_model(name):
  name_clean = name.strip().lower()
  try:
    return _list_models[name_clean]() 
  except KeyError:
    print('# error :: missing model "{}", using default model "{}"'.format(name,rays_random_c.name),file=sys.stderr,)
    return rays_random_c()

def build_rays( num_rays, name='default', ):
  return build_model(name)(num_rays)

#
# Testing
#

def test_list_models():
  print('\n# list of available models:')
  for ki in _list_models:
    print(ki)
  return

def test_models_ctor():
  for ki, fi in _list_models.items():
    fi()
  return

def test_model_builder():
  
  print('\n# check with available models...')
  for ki in _list_models:
    build_model(ki)

  print('\n# check with different names')
  list_names  = [ 'kk', 'LEGENDRE', 'Spiral', 'Spiral  ', '  Spiral  ', 'www', ]
  for ni in list_names:
    build_model(ni)

  return


def test_ray_builder():
  num_rays = 200
  for ki in _list_models:
    print('# in model {} ...'.format(ki))
    ns = build_rays(num_rays,ki)
  return


def test_plot():
  
  import matplotlib.pyplot as plt

  num_rays = 1000
  for ki in _list_models:
    print('# in model {} ...'.format(ki))
    ns = build_rays(num_rays,ki)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(ns[:,0],ns[:,1],ns[:,2])
    plt.show()
  
  return


# run this script
if __name__ == '__main__':
  utils.show_message()
  sys.exit(utils.run_test(info,globals()))
else:
  print(utils.write_disclaimer(info))
