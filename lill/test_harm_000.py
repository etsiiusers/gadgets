
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Program to compute the illumination of a pshere by several lasers
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2021.03.26 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.special

import liblaser.coord as coord
import liblaser.mesh as mesh
import liblaser.dist as dist
import liblaser.profile as profile
import liblaser.volume as volume
import liblaser.harmonics as harmonics

# info
info = { 'name': 'test_harm_000',
         'desc': 'Test spherical harmonics projection',
         'author': 'Manuel Cotello Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }

# program default parameters
defs = { 'nodes': 1000,
         'l_min': 0,
         'l_max': 6,
         }

def print_data(p,e,hfunc,fd=sys.stdout):

  '''
  Print data to file

  -> in
  p: cartesian coordinates of the points, in m
  e: intensity, in W
  hfunc: functor to evaluate spherical harmonics expansion
  fd: output stream
  '''

  count = 0
  
  print('# column_{:02d} = point coordinate X, in m'.format(count),file=fd)
  count += 1
  print('# column_{:02d} = point coordinate Y, in m'.format(count),file=fd)
  count += 1
  print('# column_{:02d} = point coordinate Z, in m'.format(count),file=fd)
  count += 1
  print('# column_{:02d} = point coordinate R, in m'.format(count),file=fd)
  count += 1
  print('# column_{:02d} = point coordinate Theta, in radians'.format(count),file=fd)
  count += 1
  print('# column_{:02d} = point coordinate Phi, in radian'.format(count),file=fd)
  count += 1
  print('# column_{:02d} = laser intensity, in W/m2'.format(count),file=fd)
  count += 1
  print('# column_{:02d} = approximated laser intensity using the harmonics expansion, in W/m2'.format(count),file=fd)
  count += 1
  
  # loop over point and associated deposited energy
  s = ''
  for pi, ei in zip(p,e):
     
     # spherical coordinates
    r = coord.x2r(pi)

    # evaluate harmonic expansion
    fij = hfunc(r).real

    # print cartesian coordinates
    for pij in pi:
      s = s + ' {:17.9e}'.format(pij)
    # print spherical coordinates
    for rij in r:
      s = s + ' {:17.9e}'.format(rij)
    # print energy
    s = s + ' {:17.9e} {:17.9e}'.format(ei,fij)
    # new line 
    s = s + '\n'

  print(s,file=fd)

  return

def eval_mesh(points,func):
  
  # initialize array of illumination intensity
  fs = []

  # loop over each point in the target
  for i, pi in enumerate(points):
   
    # evaluate function in point
    fs.append(func(pi))

  return numpy.array(fs)


def get_program_args(info,defs):

  '''
  Define command line arguments adn parse them
  '''
  
  # build command line parser
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license} ({email})'.format(** info))
  
  # optional arguments
  parser.add_argument( '--nodes', help='Define number of nodes in tagret, default "{}"'.format(defs['nodes']), type=int, default=defs['nodes'], )
  parser.add_argument( '--l_min', help='Define minimum L number for harmonic projection, default "{}"'.format(defs['l_min']), type=int, default=defs['l_min'], )
  parser.add_argument( '--l_max', help='Define maximum L number for harmonic projection, default "{}"'.format(defs['l_max']), type=int, default=defs['l_max'], )

  # parse arguments
  return parser.parse_args()

def write_info(info,args):
  s = '\n'
  s  = s + '# info ::\n'
  for ki, vi in info.items():
    s = s + '# {:>18s} = {}\n'.format(ki,vi)
  s = s + '\n'
  s  = s + '# args ::\n'
  for ki, vi in vars(args).items():
    s = s + '# {:>18s} = {}\n'.format(ki,vi)
  s = s + '\n'
  return s

def ylm(x,l,m):
  r = coord.x2r(x)
  return scipy.special.sph_harm(m,l,r[2],r[1]).real 

# main driver routine
def main():
  
  # read command line arguments
  args = get_program_args(info,defs)

  # print program info
  print(write_info(info,args))

  # create centered mesh of nodes in the outer surface of the unit sphere
  nodes = mesh.rays_fib(args.nodes)
  
  # build voronio decomposition with nodes
  svor = scipy.spatial.SphericalVoronoi(nodes)
  svor.sort_vertices_of_regions()

  # define function
  func = lambda x: ylm(x,0,0) + ylm(x,1,0) + ylm(x,2,0)

  # compute deposited energy
  fs = eval_mesh(svor.points,func)

  # spectral analysis
  hm = harmonics.moments_c(svor.points,fs,areas=svor.calculate_areas())
  
  assert( (args.l_min >= 0) and (args.l_max >= args.l_min) )

  # coefficients of the spherical harmonics expansion
  flm = hm(args.l_min,args.l_max)
  
  # create spherical harmonics expansion
  fh = harmonics.functor_c(svor.points,fs,args.l_max)

  # print spherical harmonics
  print('# spherical harmonics coefficients')
  for l in flm:
    for m in flm[l]:
      pi = flm[l][m]
      fi = hm.scale*pi
      print('# l m flm :: {:4d} {:4d} {:17.9e} j{:17.9e}'.format(l,m,fi.real,fi.imag))
  print()
  # print spectral power
  print('# spectral power of the spherical harmonics expansion')
  for l in range(args.l_min,args.l_max+1):
    sl = hm.power(l)
    ql = hm.power_norm(l)
    print('# l sl :: {:4d} {:17.9e} {:17.9e}'.format(l,sl,ql))
  print()
  # print laser intensity
  print_data(svor.points,fs,fh)

  return 0

if __name__=='__main__':
  sys.exit(main())
