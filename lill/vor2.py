
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Program to compute the illumination of a pshere by several lasers
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.11 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - improve laser illumination of the spherical target, now focus can take any 
#   position respect to the laser plane and target
# - improve output
# 2019.03.08 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - load laser parameters from file and create laser profiles
# - add command line argument
# 2019.03.06 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

import numpy as np
import scipy.spatial

from matplotlib import colors
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
from scipy.spatial import SphericalVoronoi
from mpl_toolkits.mplot3d import proj3d

import liblaser.mesh as mesh

def main():
  
  # create uniform points in sphere
  points = mesh.rays_fib(100)

  # voronoi decomposition
  radius = 1.0
  center = np.zeros( ( 3, ))
  sv = SphericalVoronoi(points, radius, center)
  # sort vertices (optional, helpful for plotting)
  sv.sort_vertices_of_regions()
  # generate plot
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  # plot the unit sphere for reference (optional)
  u = np.linspace(0, 2 * np.pi, 100)
  v = np.linspace(0, np.pi, 100)
  x = np.outer(np.cos(u), np.sin(v))
  y = np.outer(np.sin(u), np.sin(v))
  z = np.outer(np.ones(np.size(u)), np.cos(v))
  ax.plot_surface(x, y, z, color='y', alpha=0.1)
  # plot generator points
  ax.scatter(points[:, 0], points[:, 1], points[:, 2], c='b')
  # plot Voronoi vertices
  ax.scatter(sv.vertices[:, 0], sv.vertices[:, 1], sv.vertices[:, 2], c='g')
  # indicate Voronoi regions (as Euclidean polygons)
  for region in sv.regions:
    random_color = colors.rgb2hex(np.random.rand(3))
    polygon = Poly3DCollection([sv.vertices[region]], alpha=1.0)
    polygon.set_color(random_color)
    ax.add_collection3d(polygon)
  
  plt.show()

  return

if __name__=='__main__':
  sys.exit(main())
