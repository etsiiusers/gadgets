
#
# Copyright (C) 2019-2021 Universidad Politécnica de  Madrid
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module to obtain spherical harmonics moments
#
# Version control:
# 2022.10.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - use Namespace for variable "info"
# 2022.01.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - use slices from module coord.py
# - fix numpy.VisibleDeprecationWarning
# - change output
# 2021.12.28 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include test
# - remove old modules
# 2021.09.08 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include areas associated with each point
# 2021.03.25 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - simplify code, vectorize operations
# - remove exceptions
# - wrap module info in a dictionary
# 2019.03.08 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.special

from utils import show_message, run_test, write_disclaimer, progress_message_c
from coord import sl, to_sph

# module info
info = argparse.Namespace(
  name = 'harmonics',
  desc = 'Module for spherical harmonics expansion',
  author = 'Manuel Cotelo Ferreiro', 
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 0, 3, ],
  copyright = 'Copyright (C) 2019-2022 Universidad Politécnica de Madrid',
)

# namespace to manage variables
js = argparse.Namespace(

  # define indexes
  x = 0, y = 1, z = 2, # cartesian coordinates XYZ
  r = 3, t = 4, p = 5, # spherical coordinates r, theta and phi
  a = 6, # areas
  f = 7, # function value

  # define slices
  slc = numpy.s_[...,0:3],
  sls = numpy.s_[...,3:6],
  sla = numpy.s_[...,6],
  slf = numpy.s_[...,7],

  )

num = argparse.Namespace(
  dim = 3, # number of dimensions
  var = js.f + 1, # number of variables
  )

class moments_c():

  def __init__( self, x, f, areas=None, ):
    
    # input data validation
    assert( x.shape[0] == f.shape[0] ), 'inconsistency of sizes between grid and function evaluatons'
    assert( x.shape[1] == num.dim ), 'wrong number of dimensions'

    # allocate array
    self.data = numpy.zeros( ( f.shape[0], num.var, ))

    # store position n XYZ coordinates
    self.data[js.slc] = x[js.slc]

    # store position in sphercal coordinates
    self.data[js.sls] = to_sph(x)
    
    # store areas
    if areas is None:
      self.data[js.sla] = 1.0
    else:
      self.data[js.sla] = areas
    # renorm areas
    self.data[js.sla] *= (4.0*numpy.pi/numpy.sum(self.data[js.sla]))
    
    # store functions
    self.data[js.slf] = f

    # rescale data
    self.scale = numpy.linalg.norm(self.data[js.slf])
    if self.scale > 0.0e+00:
      self.data[js.slf] /= self.scale

    # internal variable to speed up calculations
    self.fa = self.data[js.sla]*self.data[js.slf]
    
    return

  def proj(self,l,m):
    
    assert( l >= 0 ), 'bad l-value, must be positive defined'
    assert( numpy.abs(m) <= l ), 'wrong m-value, must be in range -l <= m <= l'
    
    y = scipy.special.sph_harm(m,l,self.data[...,js.p],self.data[...,js.t]) 
    return numpy.sum(self.fa*numpy.conj(y))
  
  def eval(self,l,m):
    return self.proj(l,m)
  
  def __call__(self,l1,l2):
    
    l_min, l_max = min(l1,l2), max(l1,l2)
    
    # data validation
    assert( l_min >= 0 ), 'wrong minimum of l-value, must be equal or greater than 0'
    
    return { l: { m: self.proj(l,m) for m in range(-l,l+1) } for l in range(l_min,l_max+1) }

  def _power(self,l,func_eval):
    
    assert( l >= 0 ), 'wrong l-value, must be equal or greater than 0'
    
    def _helper_eval(l,m,func):
      flm = func(l,m)
      flm = numpy.absolute(flm)
      return flm*flm

    return numpy.sum( [ _helper_eval(l,m,func_eval) for m in range(-l,l+1) ], )/(2*l + 1)

  def power(self,l):
    return self._power(l,self.eval)
  
  def power_norm(self,l):
    return self._power(l,self.proj)


class functor_c():

  # default maximum order of expansion
  _default_l_max = 3

  def __init__(self,x,f,areas=None,l_max=_default_l_max):

    # store maximum L number
    self.l_max = l_max

    # validation
    assert( self.l_max >= 0 ), 'wrong l-value, must be equal or greater than 0'

    # get comments
    self.mom = moments_c(x,f,areas)
    self.flm = self.mom(0,self.l_max)
    
    return

  def __call__(self,ss):
    theta, phi = ss[sl.theta], ss[sl.phi]
    def _helper_eval_on_l(li,fi):
      return numpy.sum( numpy.array( [ fij*scipy.special.sph_harm(mj,li,phi,theta) for mj, fij in fi.items() ], ), axis=0, ) 
    fs = numpy.sum( numpy.array( [ _helper_eval_on_l(li,fi) for li, fi in self.flm.items() ]), axis=0, )
    return self.mom.scale*fs


def sphgrid(num_samples):

  # taken from module "mesh", copied to avoid import module
  
  c = 1.0 - 2.0*numpy.random.random( [ num_samples, ])
  s = numpy.sqrt(1.0 - c*c)
  phi = 2.0*numpy.pi*numpy.random.random( [ num_samples, ])
  
  return numpy.stack( [ s*numpy.cos(phi), s*numpy.sin(phi), c], axis=-1, )

def ylm(ss,l,m):
  return scipy.special.sph_harm(m,l,ss[...,2],ss[...,1])

def _helper_moments_c(func,num_samples=4000,l_min=0,l_max=6,):
  
  import scipy.spatial

  # create grid of nodes
  nodes = sphgrid(num_samples)
  
  # build voronio decomposition with nodes
  svor = scipy.spatial.SphericalVoronoi(nodes)
  svor.sort_vertices_of_regions()
  
  # compute function values
  fs = func(svor.points)

  # spectral analysis
  hm = moments_c(svor.points,fs,areas=svor.calculate_areas())
  
  # coefficients of the spherical harmonics expansion
  flm = hm(l_min,l_max)
  
  # write data
  s = ''.join( [
    # write spherical harmonics
    '\n# spherical harmonics coefficients:\n',
    ''.join( [ ''.join( [ f'# l m flm.real flm.imag :: {l:4d} {m:4d} {qlm.real:17.9e} {qlm.imag:17.9e}\n' for m, qlm in ql.items() ], ) for l, ql in flm.items() ], ),
    # write spectral power
    '\n# spectral power of the spherical harmonics expansion:\n',
    ''.join( [ '# l sl :: {l:4d} {hm.power(l):17.9e} {hm.powwer_norm(l):17.9e}\n' for l in range(l_min,l_max+1) ], ),
    ], )
  
  print(s)
  
  return flm

def test_moments_uniform():
  func = lambda x: numpy.ones( ( x.shape[0], ), )
  return _helper_moments_c(func)

def test_moments_half():
  func = lambda x: numpy.piecewise( x[...,2], [ x[...,2] >= 0., ], [ 1., 0., ], )
  return _helper_moments_c(func)

def test_moments_ylm():
  # define function, linear combination of spherical harmonics 
  func = lambda x: numpy.real(ylm(x,0,0) + ylm(x,1,0) + ylm(x,2,0))
  return _helper_moments_c(func)

def _helper_functor_c(func,num_samples=400,l_min=0,l_max=12):
  
  # create grid of nodes
  nodes = sphgrid(num_samples)
  
  # build voronio decomposition with nodes
  svor = scipy.spatial.SphericalVoronoi(nodes)
  svor.sort_vertices_of_regions()
  
  # evaluate function
  xs = svor.points
  fs = func(xs) 

  # create functor
  hf = functor_c(xs,fs,l_max=l_max,areas=svor.calculate_areas())
  gs = hf(xs)

  # print results
  print( '\n'.join( [ ' '.join( [ '{:15.7e}'.format(wi) for wi in ws ], ) for ws in zip(xs[:,0],xs[:,1],xs[:,2],fs,numpy.real(gs)) ]), )

  return xs, fs, gs

def test_functor_uniform():
  func = lambda x: numpy.ones( ( x.shape[0], ), )
  return _helper_functor_c(func)

def test_functor_half():
  func = lambda x: numpy.piecewise( x[...,2], [ x[...,2] >= 0., ], [ 1., 0., ], )
  return _helper_functor_c(func)

def test_functor_random():
  func = lambda x: numpy.random.random( x.shape[0], )
  return _helper_functor_c(func)

if __name__ == '__main__':
  show_message()
  sys.exit( run_test( info, globals(), ), )
else:
  print( write_disclaimer(info), )
