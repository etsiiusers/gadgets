
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module to load laser data
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2022.10.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - use Namespace for variable "info"
# 2021.12.29 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - change interface with XML
# 2021.03.25 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - remove exceptions
# - wrap module info in a dictionary
# 2019.03.08 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse
import xml.etree.ElementTree as et

import utils

# module info
info = argparse.Namespace(
  name = 'ioxml',
  desc = 'Module with helper functions IO in XML format',
  author = 'Manuel Cotelo Ferreiro', 
  email = 'manuel.cotelo@upm.es',
  year = 2021,
  version = [ 2, 0, 1, ],
  copyright = 'Copyright (C) 2019-2022 Universidad Politécnica de Madrid',
)


def check_num(ls,num_values):
  if num_values > 0:
    n = len(ls)
    if n > num_values:
      print( '# warning :: excess of values, slicing to the demanded size', file=sys.stderr, )
      return ls[:num_values]
    elif n < num_values:
      print( '# error :: missing values', file=sys.stderr, )
  return ls

def read_attrib(node,key,func_cast=lambda x: x,num_values=0,):
  try:
    ls = [ func_cast(vi) for vi in filter(lambda x: bool(x),node.attrib[key].split(';')) ]
    return check_num(ls,num_values)
  except KeyError:
    print( '# error :: missing attribute "{}"'.format(key), file=sys.stderr, )
    return []

def totext(xs):
  
  s = ''
  if isinstance( xs, ( str, ),  ):
    s = xs
  elif isinstance( xs, ( bytes, bytearray, ),  ):
    s = xs.decode('utf8')
  else:
    try:
      s = ''.join( [ ' {};'.format(xi) for xi in xs ], )
    except:
      s = ' {};'.format(xs)
  
  return s

def add_attrib(node,key,xs,use_append=False,):
  
  # convert to string
  s = totext(xs)
  
  # append attribute to node
  if key in node.attrib:
    if use_append:
      node.attrib[key] += s
    else:
      print( '# error :: attribute "{}" already exists and append is forbiden, do nothing.'.format(key), file=sys.stderr, )
      return
  else:
    node.attrib[key] = s
  
  return

def find_unique(node,tag):
  ln = node.findall(tag)
  assert len(ln) == 1, f'tag "{tag}" is not unique in DOM ({len(ln)})'  
  return ln[0]

def find_all(node,tag):
  return node.findall(tag)

def create_element(tag,att={},):
  elem = et.Element(tag)
  for ki, vi in att.items():
    add_attrib(elem,ki,vi)
  return elem

def append_element(root,* list_nodes):
  for node in list_nodes:
    root.append(node)
  return root

def fromstring(s):
  return et.fromstring(s)

def tostring(root):
  return et.tostring(root)

def load(file_name):
  return et.parse( source=open( file_name, 'r', encoding='utf8', ), parser=et.XMLParser( encoding='utf8', ), )

def opendom(meta={}):

  # create root element
  root = et.Element('input')

  # add meta element
  if meta:
    
    elem_meta = create_element("meta")
    
    for ki, vi in meta.items():
      elem = create_element( ki, { 'value': vi, }, )
      append_element(elem_meta,elem)

    append_element(root,elem_meta)

  return root

def dump( root, file_name='', ):
  # define file descriptor
  fd = sys.stdout
  if file_name:
    fd = open(file_name,'w')
  # print
  #s = et.tostring( root, xml_declaration=True,)
  s = et.tostring( root, encoding='utf8', xml_declaration=True,)
  print( s.decode('utf8'), file=fd, )
  return


# run this script
if __name__ == '__main__':
  utils.show_message()
  sys.exit(utils.run_test(info,globals()))
else:
  print( utils.write_disclaimer(info), )
