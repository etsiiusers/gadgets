
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module to define laser beam
# 
# Copyright (C) 2021-2022 Universidad Politécnica de Madrid
#
# Version control:
# 2022.10.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix errors
# - use Namespace for "info" variable
# 2022.03.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix bug in beam_c for focus in the origin
# - fix bug in beam_c to select ray direction
# include method xref to get axis reference point
# 2022.01.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix minor errors in test functions
# 2021.12.30 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - implement XML io
# 2021.12.29 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - minor changes in interface
# - fix minor errors
# 2021.11.30 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse
import itertools

import numpy

import utils
import ioxml
import volume
import dist
import coord

# module info
info = argparse.Namespace( 
  name = 'beam',
  desc = 'Module to manage laser beam description',
  author = 'Manuel Cotelo Ferreiro', 
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 1, 2, ],
  copyright = 'Copyright (C) 2021-2022 Universidad Politécnica de Madrid',
)

# names for XML io

tag = argparse.Namespace(
  beam = 'beam',
)


class beam_c():
  
  '''
  Class to model a laser beam
  '''

  name = 'beam'

  tag = argparse.Namespace(
    focus = 'focus',
    )

  att = argparse.Namespace(
    value = 'value',
    )
  
  # define number of dimensions of the space
  num_dim = 3

  # default parameters
  _focus_default = numpy.zeros( ( num_dim, ), )
  _fx_default = dist.uniform_c()

  def __init__( self, pl, focus=_focus_default, fx=_fx_default, is_focused=True, ):
    
    '''
    Constuctor of an object of class beam_c
    
    -> in
    pl: laser lense plane, plane_c class
    focus: focal point in cartesian coordinates, array of 3 floats
    fx: energy distribution, functor class
    is_focused: whether use a focused beam or parallel rays, bool
    '''
    
    # save parameters
    self.pl, self.focus, self.fx, self.is_focused = pl, focus, fx, is_focused

    # validation
    assert callable(self.fx), 'distribution object must be callable'

    # check focus
    if self.pl.inside(self.focus):
      self.is_focused = False

    # define axis
    if self.is_focused:
      self.axis = self.focus - self.pl.x
      assert numpy.linalg.norm(self.axis) > 0., 'bad definition for ray axis in a focused beam'
      self.axis /= numpy.linalg.norm(self.axis)
    else:
      self.axis = self.pl.n/numpy.linalg.norm(self.pl.n)

    # define trace function
    if self.is_focused:
      self.func_line = self._ray_focused
    else:
      self.func_line = self._ray_parall

    return
 
  def xref(self,):
    return self.pl.x

  def cross(self,x,v):
    # intersection length between Focus-Point line and laser base plane
    l = self.pl.cross(x,v)
    # intersection point
    return x + l[:,None]*v
  
  def trace(self,x,v):
    # intersection point
    p = self.cross(x,v)
    # vector from laser base plane to point
    w = x - p
    return p, coord.renorm(w)

  def _ray_focused(self,x):
    # define ray direction and normalize to 1.0
    v = x - self.focus
    return x, coord.renorm(v)
  
  def _ray_parall(self,x):
    ax = numpy.zeros_like(x)
    ax[...] = self.axis[None,:]
    return x, ax

  def __call__(self,x):
    px, pv = self.trace(* self.func_line(x))
    r = numpy.linalg.norm( px - self.pl.x, axis=-1, )
    return px, pv, self.fx(r)

  @staticmethod
  def fromxml(root):
    elem_focus = ioxml.find_unique(root,beam_c.tag.focus)
    return beam_c( volume.load(root)[0],
                   focus = ioxml.read_attrib(elem_focus,beam_c.att.value,float,3),
                   fx = dist.load(root)[0],
                   )
  
  def toxml(self):
    root = ioxml.create_element( tag.beam, )
    elem_plane = self.pl.toxml()
    elem_focus = ioxml.create_element( self.tag.focus, { self.att.value: self.focus, }, )
    elem_xdist = self.fx.toxml()
    return ioxml.append_element(root,elem_plane,elem_focus,elem_xdist)


def load(root):
  return beam_c.fromxml(root)

#
# Testing
#

def _helper_beam_default():
  
  # define a base plane for the laser beam
  xref = numpy.array( [ 0., 0., 0., ], )
  n = numpy.array( [ 0., 0., 1., ], )
  pl = volume.plane_c(xref,n)
  
  # define an homogeneous intensity in a disc
  l = 2.
  fx = dist.uniform_c(0.,l)
  
  return beam_c(pl,fx=fx,is_focused=False)

# test function for class beam_c
def test_beam():
  
  # create beam
  b = _helper_beam_default()

  # evaluate laser intensity in another target plane
  xref = numpy.array( [ 0., 0., 3., ], )
  e1 = numpy.array( [ 1., 0., 0., ], )
  e1 /= numpy.linalg.norm(e1)
  e2 = numpy.array( [ 0., 1., 1., ], )
  e2 /= numpy.linalg.norm(e2)

  # uniform distribution of points in target plane
  q = numpy.linspace(-2.,2.,16)

  # evaluate intensity
  ws = numpy.array( [ xref + q1*e1 + q2*e2 for q1, q2 in itertools.product(q,q) ], )
  xs, vs, fs = b(ws)

  # print
  print( '\n'.join( [ f'{wi[0]:15.7e} {wi[1]:15.7e} {wi[2]:15.7e} {fi:15.7e}' for wi, fi in zip(ws,fs) ], ), )

  return

def test_beam_fromxml():

  s = '''
  <beam>
    <shape type="plane">
      <point value=" 0.0; 0.0; 0.0;" />
      <normal value=" 0.0; 0.0; 1.0;" />
    </shape>
    <focus value=" 0.0; 0.0; 2.0;" />
    <dist type="uniform">
      <xref value=" 0.0;" />
      <width value=" 2.0;" />
      <area value=" 1.0;" />
    </dist>
  </beam>
  '''
  
  b = beam_c.fromxml(ioxml.fromstring(s))
  print(ioxml.tostring(b.toxml()))
  
  return

def test_beam_toxml():
  b = _helper_beam_default()
  print(ioxml.tostring(b.toxml()))
  return

def test_beam_xml():
  b1 = _helper_beam_default()
  b2 = beam_c.fromxml(b1.toxml())
  return

# run this script
if __name__  == '__main__':
  utils.show_message()
  sys.exit(utils.run_test(info,globals()))
else:
  print( utils.write_disclaimer(info), )
