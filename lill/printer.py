
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module to print info messages in program
#
# Copyright (C) 2022 Universidad Politécnica de Madrid
# Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
#
# Version control:
# 2022.10.20 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys
import argparse

import utils

info = argparse.Namespace(
  name = 'printer',
  desc = 'Manage info messages in program',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = '2022',
  version = [ 0, 1, 0, ],
  copyright = 'Copyright (C) 2022 Universidad Politécnica de Madrid',
)

defs = argparse.Namespace(
  verbose = 1, 
)

#
# print info messages and close
#

class printer_c():
  
  prefix = '# '
  suffix = ' ... '
  tab = '  '

  def __init__( self, msg, verbose=1, level=0, fd=sys.stdout, use_flush=True, use_close=True, ):

    # save parameters
    self.verbose, self.level, self.fd, self.use_flush, self.use_close = verbose, level, fd, use_flush, use_close
    
    # set tabulator
    self.tab = ''.join(self.level*[printer_c.tab])
    self.header = printer_c.prefix + self.tab
    
    # send opening message
    self.open( msg, )

    return
  
  def child( self, msg, ):
    self.send( '\n', )
    self.use_close = False
    return printer_c( msg, verbose=self.verbose, level=self.level+1, fd=self.fd, use_flush=self.use_flush, use_close=True, )

  def open( self, msg, ):
    self.send( self.header + msg + printer_c.suffix, )
    return

  def close( self, ):
    self.send( 'done', )
    return

  def send( self, msg, ):
    if self.verbose > 0:
      print( f'{msg}', end='', file=self.fd, )
      if self.use_flush:
        self.fd.flush()
    return

  def __del__(self):
    #if self.use_close:
    #  self.close()ç
    if self.level == 0:
      self.send(  '\n', )
    return 


#
# empty printer
#
class empty_c():

  def __init__( self, msg='', ** kargs):
    return

  def child( self, msg='', ):
    return self


#
# create info messenger
#

def select_builder( verbose = 0, ):
  if verbose > 0:
    return printer_c
  else:
    return empty_c

def build_empty():
  return select_builder(0)()

#
# testing
#

def test_printer_000():
  pa = printer_c('first')
  return

def test_printer_001():
  qa = printer_c('first')
  qaa = qa.child('inner A')
  qaa = qa.child('inner B')
  return

def test_printer_002():
  qa = printer_c( 'first', use_close=False, )
  qaa = qa.child( 'inner A', )
  qab = qa.child( 'inner B', )
  return

def test_empty():
  
  pa = empty_c()
  pb = empty_c()

  paa = pa.child()

  return

def test_select_builder():
  
  bl0 = select_builder(verbose=0)
  print(bl0)
  bl1 = select_builder(verbose=1)
  print(bl1)

  pr = bl1( 'hello', )

  return

# run script

if __name__ == '__main__':
  utils.show_message()
  sys.exit( utils.run_test(info,globals()), )
else:
  print( utils.write_disclaimer(info), )
