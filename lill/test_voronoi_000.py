
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Voronoi decomposition of points in a sphere
#
# Copyright (C) 2021 Universidad Politécnica de Madrid',
#
# Version control:
# 2021.03.26 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

import numpy
import scipy.spatial

import liblaser.mesh as mesh

import matplotlib.pyplot as plt

# program info 
info = { 'name': 'test_voronio_000',
         'desc': 'Create voronoi decomposition from poinst distributed in a sphere',
         'author': 'Manuel Cotello Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 1, 0, ],
         'license': 'Copyright (C) 2021 Universidad Politécnica de Madrid',
         }

def write_points(sv):
  try:
    areas = sv.calculate_area()
  except:
    areas = numpy.zeros(sv.points.shape[0])
  s = ''
  for xi, ai in zip(sv.points,areas):
    for xij in xi:
      s =  s + ' {:13.5e}'.format(xij)
    s =  s + ' {:13.5e}'.format(ai)
    s = s + '\n'
  return s

def write_vertices(sv):
  s = ''
  for vi in sv.vertices:
    for xj in vi:
      s = s + ' {:13.5e}'.format(xj)
    s = s + '\n'
  return s

def write_regions(sv):
  s = ''
  for ri in sv.regions:
    for k in ri:
      s = s + write_vertices( ( sv.vertices[k], ))
    k = ri[0]
    s = s + write_vertices( ( sv.vertices[k], ))
    s = s + '\n'
  return s

def main():
  
  # create points in a centered unit sphere
  x = [ [  1.0,  0.0,  0.0, ],
        [ -1.0,  0.0,  0.0, ],
        [  0.0,  1.0,  0.0, ],
        [  0.0, -1.0,  0.0, ],
        [  0.0,  0.0,  1.0, ],
        [  0.0,  0.0, -1.0, ],
        ]
  x = numpy.array(x)

  # voronoi decomposition
  svor = scipy.spatial.SphericalVoronoi(x)
  svor.sort_vertices_of_regions()

  # print data
  print(write_points(svor),file=open('points.txt','w'))
  print(write_vertices(svor),file=open('vertices.txt','w'))
  print(write_regions(svor),file=open('region.txt','w'))
  
  return

if __name__=='__main__':
  sys.exit(main())
