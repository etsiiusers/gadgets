
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Program to create XML file with NIF laser specificsations
#
# Copyright (C) 2019-2022 Universidad Poliecnica de Madrid
#
# Version control:
# 2022.10.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - update laser parameters
# - add missing rings under the equator
# 2022.03.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix minor errors
# 2022.02.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse
import datetime

import numpy

import utils
import coord
import ioxml
import dist
import beam
import volume

# info, global
info = argparse.Namespace(
  name = 'laser_nif',
  desc = 'Create a set of laser parameters for NIF',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 0, 2, ],
  copyright = 'Copyright (C) 2022 Universidad Politécnica de Madrid',
)

# program default parameters, global
defs = argparse.Namespace(
  verbose = 1,
)

def deg2rad(angle_deg):
  return numpy.pi*angle_deg/180.0

# tags to select data from XML file
tag = argparse.Namespace(
  target = 'target',
  laser = 'laser',
)

# parameters for NIF lasers
nif = argparse.Namespace(
  
  # each port has 4 beams, each is square with 37 cm side and a clear aperture 
  # of 40 cm
  lbeam  = 0.37,
  lclear = 0.40,

  # four-beam overlap to form f/8 quads.
  
  # chanber center
  center = numpy.array( [ 0., 0., 0., ], ),

  # chamber radius
  radius =  6.,

  # focal length
  focal_length = 9.,

  # profile
  type = 'gauss',
  #type = 'uniform',
  fwhm = 0.37,
  ex = 4.,
  
  # positions of each ring of quads
  ring1 = argparse.Namespace(
    num = 4,
    theta = deg2rad(23.5),
    angle_ref = deg2rad(0.),
  ),
  
  ring2 = argparse.Namespace(
    num = 4,
    theta = deg2rad(30.),
    angle_ref = deg2rad(45.),
  ),
  
  ring3 = argparse.Namespace(
    num = 8,
    theta = deg2rad(44.5),
    angle_ref = deg2rad(2.*45./8.),
  ),
  
  ring4 = argparse.Namespace(
    num = 8,
    theta = deg2rad(50.),
    angle_ref = deg2rad(3.*45./8.),
  ),

)

def build_dist(params):
  if params.type == dist.uniform_c.name:
    return dist.uniform_c( 0., width=params.fwhm, area=1., )
  elif params.type == dist.ggauss_c.name:
    sc = dist.ggauss_c.fwhm2scale(params.fwhm,params.ex) 
    return dist.ggauss_c( 0., scale=sc, shape=params.ex, area=1., )
  else:
    raise 'Unknown type of distribution'
  

def build_beams(params):
  
  def _helper_build_beam(params,theta,phi,fi):
    
    sph = numpy.array( [ params.radius, theta, phi, ], )
    
    pos = coord.to_cart(sph)
    dir = coord.renorm(params.center - pos)

    fc = pos + params.focal_length*dir
      
    pl = volume.plane_c( pos, dir, ) 
    
    return beam.beam_c( pl, focus=fc, fx=fi, )

  # define intensity distribution
  fi = build_dist(params)

  # create list of rings
  lr = [ params.ring1, params.ring2, params.ring3, params.ring4, ]

  # initialize list
  lb = []
  
  # create beams
  for ri in lr:
    
    ang_lo = ri.angle_ref
    ang_hi = ang_lo + 2.*numpy.pi
    ang = numpy.linspace( ang_lo, ang_hi, ri.num, endpoint=False, )
    
    for ai in ang:
      # north hemisphere
      lb.append( _helper_build_beam( params, ri.theta, ai, fi, ), )
      # south hemisphere
      lb.append( _helper_build_beam( params, numpy.pi - ri.theta, ai, fi, ), )

  return lb

def tofile( list_beams, file_name='laser.xml', meta={}, ):
  
  # create DOM and get root element
  root = ioxml.opendom(meta) 
  
  # create target
  target = volume.sphere_c(pos=[ 0., 0., 0., ],radius=1.e-3,)
  
  # add target to DOM
  elem = ioxml.create_element(tag.target)
  ioxml.append_element(elem,target.toxml())
  ioxml.append_element(root,elem)

  # add lasers
  for bi in list_beams:
    elem = ioxml.create_element(tag.laser) 
    ioxml.append_element(elem,bi.toxml())
    ioxml.append_element(root,elem)
  
  ioxml.dump(root,file_name)

  return

def get_args( info, defs, ):

  '''
  Define command line arguments and parse them
  '''
  
  # build command line parser
  parser = argparse.ArgumentParser( 
    description = f'{info.desc}', 
    epilog = f'{info.copyright} ({info.email})', 
  )
  
  # positional arguments
  
  # optional arguments
  parser.add_argument( 
    '--verbose', 
    help = f'Define level of verbosity, default "{defs.verbose}"', 
    default = defs.verbose, 
    type=int, 
  )
  
  # parse arguments
  args = parser.parse_args()

  # validations

  return args

# main driver routine
def main( info, defs, ):

  # read command line arguments
  args = get_args( info, defs, )

  # print program info
  header = utils.write_init( info, args, )
  print(header)

  # build lasers
  list_beams = build_beams(nif)

  # print to XML file
  tofile( list_beams, meta=vars(info), )

  return 0

# run program
if __name__ == '__main__':
  sys.exit(main(info,defs))
