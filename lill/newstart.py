
# Copyright (C) 2022 Universidad Politécnica de Madrid
# Author: Miguel González Torre, Manuel Cotelo Ferreiro

import sys
import argparse
import itertools

import numpy
import scipy.spatial
import scipy.optimize

import utils
import coord
import ioxml
import mesh
import dist
import beam
import volume
import harmonics
import voro 

import lill

rings_nif = [
  argparse.Namespace( angle=numpy.radians( 23.5), num=4, ref=0., ),
  argparse.Namespace( angle=numpy.radians( 30.0), num=4, ref=numpy.radians(45.0), ),
  argparse.Namespace( angle=numpy.radians( 44.5), num=8, ref=0., ),
  argparse.Namespace( angle=numpy.radians( 50.0), num=8, ref=0, ),
  argparse.Namespace( angle=numpy.radians(130.0), num=8, ref=0., ),
  argparse.Namespace( angle=numpy.radians(135.5), num=8, ref=0., ),
  argparse.Namespace( angle=numpy.radians(150.0), num=4, ref=0., ),
  argparse.Namespace( angle=numpy.radians(156.5), num=4, ref=0., ),
]

def build_ring(radius,list_rings):
  
  def _helper_build_ring(radius,ring):
    phi = ring.ref + numpy.linspace(0.,2.*numpy.pi,ring.num,endpoint=False)
    return [ [ radius, ring.angle, pi, ] for pi in phi ]
  
  qc = []
  for ring in list_rings:
    qc.extend(_helper_build_ring(radius,ring))
  
  return coord.to_cart(numpy.array(qc))


def build_quad(qc):
  return numpy.array( [ qi for qi in qc for _ in range(4) ], )
 

def build_laser(xs,ls,es,fwhm):
  
  def _helper_build_laser(xi,ni,li,fwhm,ei):
    pl = volume.plane_c(xi,ni)
    focus = xi + li*ni
    fx = dist.uniform_c( xref=0., width=fwhm, area=ei, )
    #fx = dist.ggauss_c( xref=0., scale=fwhm, shape=2., area=ei, )
    return beam.beam_c(pl,focus,fx)
  
  return [ _helper_build_laser(xi,ni,li,fwhm,ei) for xi, ni, li, ei in zip(xs,coord.renorm(xs),ls,es) ]
  

def lasergen(params):
   
  # ring description, keep things together
  list_rings = rings_nif

  # define parameters
  l, fwhm, rc = params[0], 0.3, 6.

  # option 1: just one focal length
  ls = 192*[ l, ] # -> [ l, l, l, l, l, .... ]
  '''
  # option 2: focal length per ring
  ls = []
  for li, ring in zip(l,list_rings):
        ls.extend(4*ring.num*[ li, ])
  # option 3: focal length per quad
  ls = []
  j = 0
  for ring in list_rings:
    for i in range(ring.num): 
        ls.extend(4*[ l[j], ])
        j += 1
  # option 4: focal length per laser
  ls = l
  '''
  
  # same for energy...
  es = 192*[ 1., ]

  # get center of each quad
  qc = build_ring(rc,list_rings)
  
  # get center of each laser
  lc = build_quad(qc)

  # generate lasers
  list_laser = build_laser(lc,ls,es,fwhm)

  return list_laser


def eval_laser(list_laser,ps,areas,target,l_min=0,l_max=10):
  
  # evaluate laser deposition
  return 

            
def objective(params,ps,areas,target,verbose=1):
    
  list_laser = lasergen(params)
    
  edep = lill.laserdep(list_laser,ps,target) 
    
  edep_mu = numpy.average( edep, weights=areas, )
  de = edep - edep_mu
  edep_var = numpy.average( de*de, weights=area, )
    
  ans = numpy.sqrt(edep_var)/edep_mu
    
  if verbose > 0:
      
    print( f'# params = {params}', )
    print( f'# ans = {format(ans)}', )

  return ans


def write_sol(params,ps,edep,target):
  qs = coord.to_sph(ps)
  return '\n'.join( [ f'{pi[0]:15.7e} {pi[1]:15.7e} {pi[2]:15.7e} {qi[0]:15.7e} {qi[1]:15.7e} {qi[2]:15.7e} {ei:15.7e}' for pi, qi, ei in zip(ps,qs,edep) ], )


def collapse_phi( ps, edep, areas, num=100, ):
  qs = coord.to_sph(ps)
  edges = numpy.arccos(1. -2.*numpy.linspace(0.,1.,num))

  eacc = numpy.zeros_like(edges)
  aacc = numpy.zeros_like(edges)

  ts = qs[coord.sl.theta]

  for ti, ei, ai in zip(ts,edep,areas):
    j = numpy.searchsorted(edges,ti)
    eacc[j-1] += ei*ai
    aacc[j-1] += ai

  eacc = numpy.divide(eacc,aacc,where=aacc>0.)

  return eacc, edges

def optimizer():

  # initialize target description
  xc = numpy.array( [ 0., 0., 0., ], )
  radius = 1.2e-3
  target = volume.sphere_c(xc,radius)

  # create mesh of nodes in the outer surface of the target
  num_nodes = 10000
  nodes = mesh.build_rays(num_nodes,'fibonacci')

  # build voronio decomposition with nodes
  svor = voro.doit(nodes)

  # get area associated with each point
  try:
    areas = svor.calculate_areas()
  except :
    areas = numpy.ones( ( nodes.shape[0], ))
  areas *= 4.0*numpy.pi*target.radius*target.radius / numpy.sum(areas)

  # create points from nodes
  ps = target.pos + target.radius*svor.points
    
  # define geometric parameters
  Rc = 6. #Target Chamber radius
  Rb = 1.2e-3 #Target radius
  Rmin, Rmax = 0., 6e-4 #pag 29 "Description of NIF laser"
  Lmin = Rc - 5.*Rb #minimum set to be distance from laser plane to sphere raduius (6m aprox, target radius 0.1mm)
  #Lmax = Rc*(1 + 1/(numpy.sqrt(0.9)*Rmax/Rb - 1)) #assuming uniform laser, origin 40cm in diameter and target radius measuring 0.1mm, I have to change this for any laser form
  Lmax = 2.*Rc
    
  # build target function
  func = lambda params: objective(params,ps,areas,target)

  # minimization
  bnds = scipy.optimize.Bounds(Lmin,Lmax)
  #x_init = [ 0.5*(Lmin + Lmax), ] 
  x_init = [ Lmax, ] 
  sol = scipy.optimize.minimize( func, x_init, bounds=bnds, tol=1.e-12, )
    
  # imprimir solución                 
  list_laser = lasergen(sol.x)
  edep = lill.laserdep(list_laser,ps,target)
  
  print( f'# params: {sol.x}', )
  print( write_sol(sol.x,ps,edep,target), )

  bins, edges = collapse_phi( ps, edep, areas, 80, )

  print( '\n'.join( [ f'{bi:15.7e} {ei:15.7e}' for bi, ei in zip(bins,edges)], ), file=open('edep_collapse_phi.txt','w'), )

  return
                           
# run program
if __name__ == '__main__':
  sys.exit(optimizer())                            
 
