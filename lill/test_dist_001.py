
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Test program for distributions
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2021.03.26 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - wrap info in dictionary
# 2019.03.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy

import liblaser.dist as dist

# info
info = { 'name': 'test_dist_001',
         'desc': 'Test program for distributions',
         'author': 'Manuel Cotello Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2019-2021 Universidad Politécnica de Madrid',
         }

# program default parameters
defs = { 'pos': ( 0.0e+00, 0.0e+00, ),
         'sigma': ( 1.0e+00, 1.0e+00, ),
         'k': 2.0e+00,
         }

def get_program_args(info,defs):

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license} ({email})'.format(** info))

  parser.add_argument( '--pos', help='Reference point'.format(defs['pos']), nargs=2, type=float, default=defs['pos'], )
  parser.add_argument( '--sigma', help='Standard deviations'.format(defs['sigma']), nargs=2, type=float, default=defs['sigma'], )
  parser.add_argument( '--k', help='Distribution exponent'.format(defs['k']), type=float, default=defs['k'], )

  return parser.parse_args()

def main():

  args = get_program_args(info,defs)

  args.pos = numpy.array(args.pos)
  args.sigma = numpy.array(args.sigma)

  fd = dist.hgauss_ell2d_c(args.sigma,args.pos,args.k)
  
  finc = 3.0e+00

  x_lo = args.pos - finc*args.sigma
  x_hi = args.pos + finc*args.sigma
  x_size = [ 100, 100, ]

  x = [ numpy.linspace(x_lo[0],x_hi[0],x_size[0]),
        numpy.linspace(x_lo[1],x_hi[1],x_size[1]),
        ]

  for xi in x[0]:
    for yj in x[1]:
      w = numpy.array( [ xi, yj, ])
      fij = fd.pdf(w)
      print(' {:15.7e} {:15.7e} {:15.7e}'.format(xi,yj,fij))
      #gij = fd.cdf(xi,yj)
      #print(' {:15.7e} {:15.7e} {:15.7e} {:15.7e}'.format(xi,yj,fij,gij))
      sys.stdout.flush()
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
