
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Module with generalized gaussian distribution in 2D
#
# Copyright (C) 2019-2021 Universidad Politécnica de Madrid
#
# Version control:
# 2022.10.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
#- use Namespace for variable "info"
# 2021.12.30 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include XML io
# 2021.12.28 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - simplify distros
# 2021.09.08 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add new distribution uniform_c
# 2021.03.25 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - wrap module info in dictionary
# - remove exceptions
# 2019.03.11 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - fix error in 2D hyper-gaussian elliptic distribution
# 2019.03.08 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - modify interface to dist_2d_c
# 2019.03.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.stats

import utils
import ioxml

# module info
info = argparse.Namespace(
  name = 'dist',
  desc = 'Module with common distributions of laser intensity',
  author = 'Manuel Cotelo Ferreiro', 
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 1, 1, ],
  copyright = 'Copyright (C) 2019-2022 Universidad Politécnica de Madrid',
)

# names for XML io

tag = argparse.Namespace(
  dist = 'dist',
  )

att = argparse.Namespace(
  type = 'type',
  )

# uniform distribution
class uniform_c():

  name = 'uniform'
  
  tag = argparse.Namespace(
    xref  = 'xref',
    width = 'width',
    area  = 'area',
    )
  
  att = argparse.Namespace(
    value = 'value',
    )

  def __init__( self, xref=0., width=1., area=1., ):
    # save parameters
    self.xref, self.width, self.area = xref, width, area
    # validation
    assert ( self.width > 0. ), 'distribution width must be greater than 0.0'
    # compute return values
    self.values = numpy.array( [ self.area/self.width, 0., ], )
    # auxiliar variables
    self.dx = 0.5*self.width
    return 

  def pdf(self,x):
    return numpy.piecewise( x, [ numpy.abs(x - self.xref) <= self.dx, ], self.values, )

  def cdf(self,x):
    l = (x - self.xref + self.dx)/self.dx/2.
    return numpy.area*numpy.piecewise( [ l < 0., l > 1., ], [ 0., 1., ], default=l, )

  def __call__(self,x):
    return self.pdf(x)

  @staticmethod
  def fromxml(root):
    elem_xref  = ioxml.find_unique(root,uniform_c.tag.xref)
    elem_width = ioxml.find_unique(root,uniform_c.tag.width)
    elem_area  = ioxml.find_unique(root,uniform_c.tag.area)
    return uniform_c( xref=ioxml.read_attrib(elem_xref,uniform_c.att.value,float,1)[0],
                      width=ioxml.read_attrib(elem_width,uniform_c.att.value,float,1)[0],
                      area=ioxml.read_attrib(elem_area,uniform_c.att.value,float,1)[0],
                      )
    return

  def toxml(self):
    root = ioxml.create_element( tag.dist, { att.type: self.name, }, )
    elem_xref  = ioxml.create_element( self.tag.xref,  { self.att.value: self.xref, }, )
    elem_width = ioxml.create_element( self.tag.width, { self.att.value: self.width, }, )
    elem_area  = ioxml.create_element( self.tag.area,  { self.att.value: self.area, }, )
    return ioxml.append_element(root,elem_xref,elem_width,elem_area)


# generalized gaussian distribution
class ggauss_c():

  name = 'gauss'

  tag = argparse.Namespace(
    xref  = 'xref',
    scale = 'scale',
    shape = 'shape',
    area  = 'area',
    )

  att = argparse.Namespace(
    value = 'value',
    )
  
  def __init__( self, xref=0., scale=1., shape=2., area=1., ):
    # save parameters
    self.xref, self.scale, self.shape, self.area = xref, scale, shape, area
    # validation
    assert ( self.scale > 0. ), 'scale parameter must be positive defined (>0.0)'
    assert ( self.shape > 0. ), 'shape parameter must be positive defined (>0.0)'
    # build distro
    self.ds = scipy.stats.gennorm( beta=self.shape, )
    return
  
  @staticmethod
  def sfactor(shape=2.):
    return numpy.power(numpy.log(2.),1./shape)

  @staticmethod
  def hwhm2scale(hwhm,shape=2.):
    return hwhm/ggauss_c.sfactor(shape)
  
  @staticmethod
  def fwhm2scale(fwhm,shape=2.):
    return ggauss_c.hwhm2scale(0.5*fwhm,shape)
  
  @staticmethod
  def scale2hwhm(scale,shape=2.):
    return scale*ggauss_c.sfactor(shape)
  
  @staticmethod
  def scale2fwhm(scale,shape=2.):
    return 2.*ggauss_c.scale2hwhm(scale,shape)
  
  def qs(self,x):
    return numpy.abs((x - self.xref)/self.scale)

  def pdf(self,x):
    return self.ds.pdf(self.qs(x))/self.scale
  
  def cdf(self,x):
    return self.ds.cdf(self.qs(x))
  
  def __call__(self,x):
    return self.pdf(x)
  
  @staticmethod
  def fromxml(root):
    elem_xref  = ioxml.find_unique(root,ggauss_c.tag.xref)
    elem_scale = ioxml.find_unique(root,ggauss_c.tag.scale)
    elem_shape = ioxml.find_unique(root,ggauss_c.tag.shape)
    elem_area  = ioxml.find_unique(root,ggauss_c.tag.area)
    return ggauss_c( xref=ioxml.read_attrib(elem_xref,ggauss_c.att.value,float,1)[0],
                     scale=ioxml.read_attrib(elem_scale,ggauss_c.att.value,float,1)[0],
                     shape=ioxml.read_attrib(elem_shape,ggauss_c.att.value,float,1)[0],
                     area=ioxml.read_attrib(elem_area,ggauss_c.att.value,float,1)[0],
                      )

  def toxml(self):
    root = ioxml.create_element( tag.dist, { att.type: self.name, }, )
    elem_xref  = ioxml.create_element( self.tag.xref,  { self.att.value: self.xref, }, )
    elem_scale = ioxml.create_element( self.tag.scale, { self.att.value: self.scale, }, )
    elem_shape = ioxml.create_element( self.tag.shape, { self.att.value: self.shape, }, )
    elem_area  = ioxml.create_element( self.tag.area,  { self.att.value: self.area, }, )
    return ioxml.append_element(root,elem_xref,elem_scale,elem_shape,elem_area)

  @staticmethod
  def conversion_factor(shape=2.):
    assert ( shape > 0. ), 'shape parameter must be positive defined (>0.0)'
    return numpy.power(numpy.log(2.),1./shape)

  @staticmethod
  def hwhm2scale(self,hwhm,shape=2.,):
    return hwhm/ggauss_c.conversion_factor(shape)
  
  @staticmethod
  def fwhm2scale(self,fwhm,shape=2.,):
    return ggauss_c.hwhm2scale(.5*fwhm,shape)
  
  @staticmethod
  def scale2hwhm(self,scale,shape=2,):
    return scale*ggauss_c.conversion_factor(shape)
  
  @staticmethod
  def scale2fwhm(self,scale,shape=2,):
    return 2.*ggauss_c.scale2hwhm(scale,shape)

# 
# Manager
# 

_list_models = { uniform_c.name: uniform_c.fromxml,
                 ggauss_c.name: ggauss_c.fromxml,
                 }

def select(name):
  return _list_models[name]

def _helper_load(elem):
  tp = ioxml.read_attrib(elem,att.type,)
  assert ( len(tp) == 1 ), 'multiple definitions of distribution type, just one allowed'
  return select(tp[0])(elem)

def load(root):
  return [ _helper_load(elem) for elem in ioxml.find_all(root,tag.dist) ]

#
# Testing
#

def _helper_check_tol(x1,x2,tol=1.0e-09):
  x12 = x1 + x2
  if x12 < tol:
    res = numpy.abs(x1 - x2)
  else:
    res = numpy.abs(2.0*(x1 - x2)/x12)
  return res < tol

def test_uniform_c():
  
  # define parameters
  xref = 0.
  dx = 1.

  # build distro
  ds = uniform_c(xref,dx)

  # create sample
  q = 3.
  xs = xref + q*dx*numpy.linspace(-1.,1.,100)

  # evaluate distro
  fs = ds(xs)

  # plot results
  import matplotlib.pyplot as plt

  plt.figure()
  plt.plot(xs,fs)
  plt.show()

  return


def test_ggauss_c():

  # define parameters
  xref = 0.
  fwhm = 2.
  beta = 3.

  # build distro
  ds = ggauss_c(xref,ggauss_c.fwhm2scale(fwhm,beta),beta)

  # create sample
  q = 5.
  xs = xref + 0.5*q*fwhm*numpy.linspace(-1.,1.,100)

  # evaluate distro
  fs = ds(xs)

  # plot results
  import matplotlib.pyplot as plt

  plt.figure()
  plt.plot(xs,fs)
  plt.show()
  
  return


def test_beta():

  # define parameters
  xref = 0.
  fwhm = 2.
  beta_lo, beta_hi = 0.5, 8.5

  # create sample
  q = 5.
  xs = xref + 0.5*q*fwhm*numpy.linspace(0.,1.,100)

  # plot results
  import matplotlib.pyplot as plt
  
  plt.figure()
  
  for beta in numpy.linspace(beta_lo,beta_hi,9):
    # build distro
    ds = ggauss_c(xref,ggauss_c.fwhm2scale(fwhm,beta),beta)
    # plot
    plt.plot(xs,ds(xs),label='beta = {:5.1f}'.format(beta),lw=1.5,alpha=0.6,)
  
  plt.legend()
  plt.show()
  
  return

def _helper_test_fromxml(s,cl):
  ds = cl.fromxml(ioxml.fromstring(s))
  print(s)
  print(utils.write_ns(ds))
  return ds

def _helper_test_toxml(cl):
  # create distro
  ds = cl()
  # dump
  print(ioxml.tostring(ds.toxml()))
  return ds

def _helper_test_xml(cl):
  ds1 = cl()
  ds2 = cl.fromxml(ds1.toxml())
  return ds1, ds2

def test_uniform_fromxml():
  
  s = '''
  <dist type="uniform">
    <xref value="1.;" />
    <width value="2.;" />
    <area value="1.;" />
  </dist>
  '''

  return _helper_test_fromxml(s,uniform_c)

def test_uniform_toxml():
  return _helper_test_toxml(uniform_c)

def test_uniform_xml():
  du1, du2 = _helper_test_xml(uniform_c)
  cond = all( [ _helper_check_tol(du1.xref,du2.xref),
                _helper_check_tol(du1.width,du2.width),
                _helper_check_tol(du1.area,du2.area),
                ], )
  assert ( cond )
  return cond

def test_ggauss_fromxml():
  
  s = '''
  <dist type="ggauss">
    <xref value="1.;" />
    <scale value="2.;" />
    <shape value="2.;" />
    <area value="1.;" />
  </dist>
  '''

  return _helper_test_fromxml(s,ggauss_c)

def test_ggauss_toxml():
  return _helper_test_toxml(ggauss_c)

def test_ggauss_xml():
  dg1, dg2 = _helper_test_xml(ggauss_c)
  cond = all( [ _helper_check_tol(dg1.xref,dg2.xref),
                _helper_check_tol(dg1.scale,dg2.scale),
                _helper_check_tol(dg1.shape,dg2.shape),
                _helper_check_tol(dg1.area,dg2.area),
                ], )
  assert( cond )
  return cond

def test_manager():
  
  du1 = uniform_c()
  dg1 = ggauss_c()
  
  root = ioxml.create_element('root')
  ioxml.append_element(root,du1.toxml(),dg1.toxml())
  
  ds = load(root)
  
  print()
  for i, di in enumerate(ds):
    print('# dist {}:'.format(i))
    print(utils.write_ns(di))
    print()

  return

# run this script
if __name__ == '__main__':
  utils.show_message()
  sys.exit(utils.run_test(info,globals()))
else:
  print( utils.write_disclaimer(info), )
