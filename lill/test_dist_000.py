
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Test program for distributions
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2021.03.26 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - wrap info in dictionary
# 2019.03.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy

import liblaser.dist as dist

# info
info = { 'name': 'test_dist_000',
         'desc': 'Test program for distributions',
         'author': 'Manuel Cotello Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'year': 2021,
         'version': [ 1, 0, 0, ],
         'license': 'Copyright (C) 2019-2021 Universidad Politécnica de Madrid',
         }

# program default parameters
defs = { 'mu': 0.0e+00,
         'sigma': 1.0e+00,
         'k': 2.0e+00,
         }

def get_program_args(info,defs):

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license} ({email})'.format(** info))

  parser.add_argument( '--mu', help='Distribution mean'.format(defs['mu']), type=float, default=defs['mu'], )
  parser.add_argument( '--sigma', help='Distribution standard deviation'.format(defs['sigma']), type=float, default=defs['sigma'], )
  parser.add_argument( '--k', help='Distribution exponent'.format(defs['k']), type=float, default=defs['k'], )

  return parser.parse_args()

def main():

  args = get_program_args(info,defs)

  x0 = args.mu
  sx = args.sigma
  k = args.k

  fd = dist.hgauss_1d_c(x0,sx,k)
  
  x_lo = x0 - 3.0e+00*sx
  x_hi = x0 + 3.0e+00*sx
  x_size = 100

  x = numpy.linspace(x_lo,x_hi,x_size)
  f = fd.pdf(x)
  i = fd.cdf(x)

  for xi in zip(x,f,i):
    for xij in xi:
      print(' {:17.9e}'.format(xij),end='')
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
