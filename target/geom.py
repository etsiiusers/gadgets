
import os
import sys

import argparse

import numpy

# program info
info = { 'name': 'geom',
         'file': os.path.basename(__file__),
         'desc': 'Create geomtry for fusion targets',
         'year': 2020,
         'version' : [ 0, 1, 0, ],
         'author': 'Manuel Cotelo Ferreiro',
         'email': 'manuel.cotelo@upm.es',
         'license': 'Copyright 2020 Universidad Politécnica de Madrid'
         }

#program default parameters
defs = { 'out': '',
         }

# parse comand line arguments
def get_args(info,defs):
  parser = argparse.ArgumentParser(description='{desc}'.format(** info),epilog='{license}, {email}'.format(** info))
  parser.add_argument( '--out', help='Define output file name, default stdout', default=defs['out'], type=str, )
  return parser.parse_args()

# put in a string the points
def write_shape(li):
  s = ''
  for xi in li['points']:
    for xij in xi:
      s = s + ' {:15.7e}'.format(xij)
    s = s + '\n'
  return s

# put in a string the points for all shapes
def write_shapes(lc):
  s = ''
  for li in lc:
    s = s + write_shape(li) + '\n'
  return s

# write to a string a dictionary
def write_items(dc):
  s = ''
  for ki, vi in dc.items():
    s = s + '# {:16s} :: {}\n'.format(ki,vi)
  return s

# output shapes
def print_ans(info,args,lc,file_name):
  s = write_items(info) + '\n' + write_items(vars(args)) + '\n' + write_shapes(lc)
  if args.out:
    try:
      with open(args.out,'w') as fs:
        print(s,file=fd)
    except OSError:
      print('# error :: impossible to open file "{}"'.format(args.out),file=sys.stderr)
  else:
    print(s)
  return

# draw a shell
def draw_circle(radius,theta_lo,theta_hi,theta_num):
  x = numpy.zeros( ( theta_num, 2, ), )
  theta = numpy.linspace(theta_lo,theta_hi,theta_num)
  x[:,0] = numpy.cos(theta)
  x[:,1] = numpy.sin(theta)
  return radius*x

def draw_shell(ri,ro,num_points=2000):
  '''
  Draw a shell

  -> in:
  ri: inner radius
  ro: outer radius
  num_points: number of points for the contour

  <- out:
  (1) 2D array with points with the closed contour
  '''
  
  num_circ = int((num_points - 2)/2)
  print('# num_circ = {}'.format(num_circ))
  
  # create array to store points
  x = numpy.zeros( ( 2*num_circ+2, 2, ), )
  
  # draw inner and outer circles
  x[         0:    num_circ,:] = draw_circle(ri,0.0,2.0*numpy.pi,num_circ)
  x[num_circ+1:2*num_circ+1,:] = draw_circle(ro,0.0,2.0*numpy.pi,num_circ)
  
  # define join segments
  x[num_circ,0] = ro
  x[num_circ,1] = 0.0e+00
  
  x[2*num_circ+1,0] = ri
  x[2*num_circ+1,1] = 0.0e+00
  
  return x

# driver routine
def driver():

  radius_inner = 1.0
  radius_outer = 2.0

  lc = [ { 'points': draw_shell(radius_inner,radius_outer), } ]

  return lc

# main program
def main():
  
  global info
  global defs

  # read command line arguments
  args = get_args(info,defs)

  # get shapes
  lc = driver()

  # print data
  print_ans(info,args,lc,args.out)

  return 0

if __name__=='__main__':
  sys.exit(main())
