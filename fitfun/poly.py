
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module to manage polynomials, interface to numpy.polynomial.poly1d
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.06  :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import argparse

import numpy

# program description
desc = 'Module for polynomic functors'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)


class cpoly_c(numpy.poly1d):

  def __init__(self,c):
    numpy.poly1d.__init__(self,list(reversed(c)),r=False)
    return

class zpoly_c(numpy.poly1d):
  
  def __init__(self,r):
    numpy.poly1d.__init__(self,sorted(r),r=True)
    return

if __name__ == '__main__':
    print('# warning :: this file is not intended for standalone run')
    sys.exit(-1)
