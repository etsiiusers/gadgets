
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module to manage rational functions F(x) = P(x)/Q(x), where P and Q are 
# polynomials
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.06  :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

import poly

# program description
desc = 'Module for polynomic functors'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

class rational_c():

  @staticmethod
  def cbuild(cp,cq,scale=1.0e+00):
    return rational_c(poly.cpoly_c(cp),poly.cpoly_c(cq),scale)
  
  @staticmethod
  def zbuild(zp,zq,scale=1.0e+00):
    return rational_c(poly.zpoly_c(zp),poly.zpoly_c(zq),scale)

  def __init__(self,fp,fq,scale=1.0e+00):
    # save parameters
    self.fp = fp
    self.fq = fq
    self.scale = scale
    # get functor
    self.func = self.fp/self.fq
    return

  def __call__(self,x):
    return self.scale*self.fp(x)/self.fq(x)

if __name__ == '__main__':
    print('# warning :: this file is not intended for standalone run')
    sys.exit(-1)
