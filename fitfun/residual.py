
#
# This file is part of GADGETS
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module to compute residual of fitting 
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.23  :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import argparse

import numpy

# program description
desc = 'Module to compute residual of fitting'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)


class ResidualError(Exception):
  
  def __init__(self,msg):
    Exception.__init__(self,msg)
    return 


class residual_c():

    '''
    Residual evaluation

    Load an array of pairs (x_{i},f_{i}) and compute the residual respet to a function F(x)

    E^{2} = sum e_{i}^{2} = sum w_{i}*(f_{i} - F(x_{i}))^{2}
    '''

    def __init__( self, x, fx, build_func=None, weight=None, scale=1.0e+00, ):

        '''
        Initialize object

        -> in
        x: coordinates
        fx: value of the function in the coordinates
        build_func: function to create functor object for evaluation
        weight: weight factors for each coordinate value
        '''

        assert(len(x) == len(fx))
        # set arrays
        self.x = x
        self.fx = fx
        # set scale factor
        self.scale = scale
        # set weights
        if weight and (len(weight) == len(self.x)):
            self.use_weight = True
            self.w = numpy.array(weight)
            self.w_tot = numpy.sum(self.w)
        else:
            self.use_weight = False
            self.w = numpy.ones(self.x.shape)
            self.w_tot = 1.0e+00
        # set functor builder
        self.build_func = build_func
        return

    def eval(self, func):

        '''
        Evaluate residual with a given function

        -> in
        func: functor for evaluation F(x_{i})

        <- out
        value of the residual, E^2 = sum w_{i}*(f_{i} - F(x_{i}))^2
        '''

        # get individual errors
        df = (self.fx - func(self.x))
        # compute quadratic errors
        df = numpy.multiply(df, df)
        # apply weight factors and compute the residual
        if self.use_weight:
            df = numpy.dot(df, self.w)
        else:
            df = numpy.sum(df)
        # normalize and scale residual
        return df / self.w_tot / self.scale

    def peval(self, param):

        '''
        Evaluate residual with some given parameters

        This function takes some parameters to build a functor object based on 
        these parameters.

        -> in
        param: meaning depends on self.build_func functor ctor

        <- out
        residual evaluation, E^2 = sum w_{i}*(f_{i} - F(x_{i}))^2
        '''
        
        res = numpy.nan
        if self.build_func != None:
          res = self.eval(self.build_func(param))
        else:
          raise ResidualError('functor builder is not available')
        
        return res

if __name__ == '__main__':
    print('# warning :: this file is not intended for standalone run')
    sys.exit(-1)
