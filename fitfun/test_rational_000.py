
#
# This file is part of GADGETS
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module rational
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.23  :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys
import argparse

import numpy
import poly
import rational

# program description
desc = 'Test program for module rational'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# default parameters
defs = { 'cp': [ 0.0e+00, ],
         'cq': [ 1.0e+00, ],
         'lo': -1.0e+00,
         'hi': 1.0e+00,
         'size': 200,
         }

def get_program_args():
    
    '''
    Read command line arguments and defined allowed program options
    '''
    
    parser = argparse.ArgumentParser(description=desc, epilog=eplg)
    
    parser.add_argument('--cp', help='Numerator P(x) polynomial coefficients for F(x)=P(x)/Q(x), default "{}"'.format(defs['cp']), type=float, default=defs['cp'], nargs="*", )
    parser.add_argument('--cq', help='Denominator Q(x) polynomial coefficients for F(x)=P(x)/Q(x), default "{}"'.format(defs['cq']), type=float, default=defs['cq'], nargs="*", )
    
    parser.add_argument('--lo', help='Lower bound, default "{}"'.format(defs['lo']), type=float, default=defs['lo'], )
    parser.add_argument('--hi', help='Upper bound, default "{}"'.format(defs['hi']), type=float, default=defs['hi'], )
    parser.add_argument('--size', help='Size of the sample, default "{}"'.format(defs['size']), type=int, default=defs['size'], )
    
    return parser.parse_args()


def main():
    
    '''
    Program main function
    '''
    
    # get command line arguments
    args = get_program_args()
    
    # build polynomials, must be the same
    fp = poly.cpoly_c(args.cp)
    fq = poly.cpoly_c(args.cq)
    
    print('# P(x) coeff :: {}'.format(fp.c))
    print('# P(x) roots :: {}'.format(fp.r))
    print('# Q(x) coeff :: {}'.format(fq.c))
    print('# Q(x) roots :: {}'.format(fq.r))

    fr = rational.rational_c(fp,fq)

    # evaluate polynomials
    x = numpy.linspace(args.lo,args.hi,args.size)
   
    wp = fp(x)
    wq = fq(x)
    wr = fr(x)

    # print data
    for w in zip(x,wp,wq,wr):
      for wi in w:
        print(' {:15.7e}'.format(wi),end='')
      print()

    return 0

if __name__ == '__main__':
    sys.exit(main())
