
#
# This file is part of GADGETS
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test program for module poly
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.02.23  :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys
import argparse

import numpy
import poly

# program description
desc = 'Test program for module poly'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# default parameters
defs = { 'lo': -1.0e+00,
         'hi': 1.0e+00,
         'size': 200,
         }

def get_program_args():
    
    '''
    Read command line arguments and defined allowed program options
    '''
    
    parser = argparse.ArgumentParser(description=desc, epilog=eplg)
    
    parser.add_argument('cp', help='Polynomial coefficients', type=float, nargs="*", )
    
    parser.add_argument('--lo', help='Lower bound, default "{}"'.format(defs['lo']), type=float, default=defs['lo'], )
    parser.add_argument('--hi', help='Upper bound, default "{}"'.format(defs['hi']), type=float, default=defs['hi'], )
    parser.add_argument('--size', help='Size of the sample, default "{}"'.format(defs['size']), type=int, default=defs['size'], )
    
    return parser.parse_args()


def main():
    
    '''
    Program main function
    '''
    
    # get command line arguments
    args = get_program_args()
    
    # build polynomials, must be the same
    func_c = poly.cpoly_c(args.cp)
    func_z = poly.zpoly_c(func_c.r)
    
    print('# coeff :: {}'.format(func_c.c))
    print('# roots :: {}'.format(func_c.r))

    # evaluate polynomials
    x = numpy.linspace(args.lo,args.hi,args.size)
    
    fc = func_c(x)
    fz = func_z(x)
    
    dfunc_c = func_c.deriv(1)
    dfunc_z = func_z.deriv(1)

    dfc = dfunc_c(x)
    dfz = dfunc_z(x)

    # print data
    for w in zip(x,fc,fz,dfc,dfz):
      for wi in w:
        print(' {:15.7e}'.format(wi),end='')
      print()

    return 0

if __name__ == '__main__':
    sys.exit(main())
