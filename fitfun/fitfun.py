
#
# This file is part of Gadgets.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Program to perform data fitting using rational functions
# F(x) = P(x)/Q(x), where P and Q are polynomials
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.06 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - move classes to separate files
# - more extensive use of numpy
# - improve fitting, reduce the number of parameters to adjust
# 2019.02.23 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import argparse

import numpy
import scipy
import scipy.optimize

import poly
import rational
import residual

# program description

info = { 'description': 'Perform rational fitting of data F(x) = P(x)/Q(x), where P and Q are polynomial of order np and nq', 
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)', 
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID',
         }


# default parameters

defs = {'col_x': 0,
        'col_f': 1,
        'ilo': 0,
        'ihi': -1,
        'order_p': 2,
        'order_q': 2,
        'method_tol': 1.0e-18,
        'method_opts': { 'maxiter': 10, },
        }

    
def build_header():
  s = ''
  for ki, vi in info.items():
    s = s + '# {:16s} :: {}\n'.format(ki,vi)
  return s

class builder_func_rational_c():

    '''
    Builder of rational functions of defined orders from a plain array of 
    parameters
    '''

    def __init__(self, np, nq):

        '''
        Constructor

        -> in
        np: order of polynomial in numerator
        nq: order of polynomial in denominator
        '''

        # polynomial order
        self.np = np
        self.nq = nq

        return

    def __call__(self, params):
        
        '''
        Build functor for rational function evaluation

        -> in
        params: coefficients for numerator and denominator

        <- out
        functor object for evaluation
        '''

        assert( len(params) == (self.np + self.nq + 1) )
        
        cp = numpy.zeros(self.np + 1)
        cq = numpy.zeros(self.nq + 1)

        cp = params[:self.np + 1]
        cq[:self.nq] = params[self.np + 1:self.np + self.nq + 1]

        cq[-1] = 1.0e+00
        
        return rational.rational_c.cbuild(cp,cq)


def select_point(x, fx, filter):

    '''
    Select a pair of values
    '''

    k = 0
    for i in range(1, len(x)):
        if filter(x[k], x[i]):
            k = i
    return x[k], fx[k]


def select_point_left(x, fx):

    '''
    Return left pair of values
    '''

    return select_point(x, fx, lambda x0, x: (x < x0))


def select_point_right(x, fx):

    '''
    Return right pair of values
    '''

    return select_point(x, fx, lambda x0, x: (x > x0))


def params_guess(x, fx, np, nq):

    '''
    Guess for fitting paramers, p(x) = a0 + a1*x (linear) and q(x) = 1.0
    '''

    # number of coefficients
    num_cp = np + 1
    num_cq = nq

    # edge values for the fitting
    x1, f1 = select_point_left(x, fx)
    x2, f2 = select_point_right(x, fx)

    # initialization, empty arrays
    p = [0.0e+00, ] * num_cp
    q = [0.0e+00, ] * num_cq

    # guess for p(x)
    if np >= 1:
        # guess for p(x), linear
        p[1] = (f2 - f1) / (x2 - x1)
        p[0] = f1 - p[1] * x1
    else:
        p[0] = f1

    # guess for q(x), constant
    q[0] = 1.0e+00
    
    return p + q


def dofitting(x, fx, np, nq):

    '''
    Fit function parameters to minimize residual
    '''

    assert(np >= 0 and nq >= 0)

    # create residual functor
    func_res = residual.residual_c(x, fx, builder_func_rational_c(np, nq))
    
    # initial guess
    param_init = params_guess(x,fx,np,nq)
    #print('# param_init :: {}'.format(param_init))

    # minimize error
    #method_name = 'Nelder-Mead'
    method_name = 'Powell'
    #method_name = 'BFGS'
    #method_name = 'SLSQP'
    #method_name = 'COBYLA'
    res = scipy.optimize.minimize(func_res.peval, param_init, method=method_name, tol=defs['method_tol'], options=defs['method_opts'], )

    return func_res.peval(res.x), res.x, func_res.build_func(res.x)


def load_data(file_name, cx=0, cf=1, ilo=0, ihi=-1):
    
    '''
    Load data from data file

    -> in
    file_name: path to the file with the data. Data must be arranged in columns
    cx: column with xi
    cf: column with fi
    ilo, ihi: consider values in rows between index ilo and ihi

    <- out
    array with data, dimension ( N, 2, )
    '''
    
    data = numpy.loadtxt(file_name)
    assert(ilo < data.shape[0])
    return data[ilo:ihi, cx], data[ilo:ihi, cf]


def get_program_args():
    
    '''
    Read command line arguments and defined allowed program options
    '''

    eplg = '{}, {}'.format(info['author'],info['license'])
    
    parser = argparse.ArgumentParser(description=info['description'], epilog=eplg)

    parser.add_argument('inp', help='input file name', type=str, )
    parser.add_argument('--col_x', help='column with values xi in input file, default "{}"'.format(
        defs['col_x']), default=defs['col_x'], type=int, )
    parser.add_argument('--col_f', help='column with values fi in input file, default "{}"'.format(
        defs['col_f']), default=defs['col_f'], type=int, )
    parser.add_argument('--ilo', help='ignore initial N points given by this argument, default "{}"'.format(
        defs['ilo']), default=defs['ilo'], type=int, )
    parser.add_argument('--ihi', help='use values up to N points given by this argument, default "{}" (-1 means up to the last point)'.format(
        defs['ihi']), default=defs['ihi'], type=int, )
    parser.add_argument('--order_p', help='set order for numerator polynomial in rational fitting, default "{}"'.format(
        defs['order_p']), default=defs['order_p'], type=int, )
    parser.add_argument('--order_q', help='set order for denominator polynomial in rational fitting, default "{}"'.format(
        defs['order_q']), default=defs['order_q'], type=int, )
    
    return parser.parse_args()


def main():
    
    '''
    Program main function
    '''

    # get command line arguments
    args = get_program_args()
   
    # print program info
    print()
    print(build_header())
    print()

    # print arguments
    print('# command line arguments ::')
    for ai in args.__dict__.items():
      print('# {} = {}'.format(ai[0],ai[1]))
    print()

    # load data
    x, f = load_data(args.inp, args.col_x, args.col_f, args.ilo, args.ihi)
    
    # do fitting
    res, params, func = dofitting(x, f, args.order_p, args.order_q)
    
    # print params
    print('# res :: {:15.7e}'.format(res))
    print()
    for i, pi in enumerate(func.fp.c):
        print('# params :: p{:02d} = {:15.7e}'.format(i, pi))
    print()
    for i, qi in enumerate(func.fq.c):
        print('# params :: q{:02d} = {:15.7e}'.format(i, qi))
    print()
    
    # print data
    for i, ( xi, fi, ) in enumerate(zip(x, f)):
        print(' {:3d} {:15.7e} {:15.7e} {:15.7e}'.format(i, xi, fi, func(xi)))

    return 0

if __name__ == '__main__':
    sys.exit(main())
