
# 
# Copyright (C) 2022 Universidad Politécnica de Madrid
# Author: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
#

import io
import os
import sys
import argparse
import datetime

import pandas


# program info
info = argparse.Namespace(
  name = 'ldcobra',
  desc = 'Load data from COBRA output',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 2, 0, ],
  copyright = 'Copyright (C) 2022 Universidad Politécnica de Madrid',
)


# program default parameters
defs = argparse.Namespace(
  verbose = 0,
)


# create table for general results
def tab_results( lines, num_nodes, verbose=0, ):
  
  if verbose > 0:
    print( f'# loading table for results ...', )
    sys.stdout.flush()
  
  tab = argparse.Namespace(
    name = 'channel',
    desc = 'channel results',
    num_nodes = num_nodes,
    index_table = -1,
    index_channel = -1,
    line_begin = 0,
    line_extra = 30,
    line_end = 0 + 30 + num_nodes,
    skiph = 24,
    skipf = 7,
  )
  
  load_tab( tab, lines, verbose, )
  
  return tab


# create table for averages
def tab_averages( lines, num_nodes, verbose=0, ):
  
  if verbose > 0:
    print( f'# loading table for averages ...', )
    sys.stdout.flush()
  
  tab = argparse.Namespace(
    name = 'average_total',
    desc = 'Bundle average or total fluid properties  (table B)',
    num_nodes = num_nodes, 
    index_table = -1,
    index_channel = -1,
    line_begin = 80,
    line_extra = 15,
    line_end = 80 + 15 + num_nodes,
    skiph = 13,
    skipf = 4,
  )

  load_tab( tab, lines, verbose, ) 
  
  return tab


# create table for channel
def tab_channel( lines, index_channel, line_begin, num_nodes, par, verbose=0, ):
  
  if verbose > 0:
    print( f'# loading table for channel {index_channel} table {par.index_table} ...', )
    sys.stdout.flush()

  # create new tab descriptor for table 1
  tab = argparse.Namespace(
    name = f'fluid_table{par.index_table}_ch{index_channel+1:03d}',
    desc = f'Fluid properties for channel    {index_channel+1:03d}  (table {par.index_table})',
    num_nodes = num_nodes,
    index_table = par.index_table,
    index_channel = index_channel,
    line_begin = line_begin,
    line_extra = par.lines_extra,
    line_end = line_begin + par.lines_extra + num_nodes,
    skiph = par.lines_head, # skip initial rows in file
    skipf = par.lines_foot, # skip footer
  )
  
  load_tab( tab, lines, verbose, )
  
  return tab


# load table with pandas
def load_tab( tab, lines, verbose=0, ):

  if verbose > 0:
    print( '# lodaing table with pandas ...', )
    sys.stdout.flush()

  # save header and footer for each table
  tab.header = lines[tab.line_begin:tab.line_begin+tab.skiph]
  tab.footer = lines[tab.line_end-tab.skipf+1:tab.line_end]
  
  # create stream with the table data
  buff = io.StringIO( '\n'.join(lines[tab.line_begin + tab.skiph:tab.line_end - tab.skipf+1]), )
        
  # read data
  '''
  tab.data = pandas.read_table( 
    buff,
    header=None, 
    sep='\s+|\t+|\s+\t+|\t+\s+', 
    engine='python', 
  )
  '''
  tab.data = pandas.read_table( buff, header=None, delim_whitespace=True, )

  return


# guess number of nodes based on first table
def guess_num_nodes(lines):
  
  num_nodes = 0
  in_table = False

  for line in lines:
    
    # skip empty lines
    if not line:
      continue

    words = line.split()
    try:
      # if first value accepts cast to float then we are in table row
      _ = float(words[0])
      num_nodes += 1
      in_table = True
    except ValueError:
      if in_table:
        # we already passed the end of the table, break the loop
        break

  # validation
  assert num_nodes > 0, 'wrong number of nodes, must be a number greater than 0'
  
  return num_nodes


# function to read tables from file
def build_tabs( file_name, verbose=0, ):
  
  # open file
  if verbose > 0:
    print( '# loading file ...', )
    sys.stdout.flush()
  # read the whole file and get a list of text lines
  lines = open(file_name,'r').read().splitlines()

  # guess number of nodes
  num_nodes = guess_num_nodes(lines) 
  
  # initialize list of tables 
  list_tabs = [
    tab_results( lines, num_nodes, verbose, ),
    tab_averages( lines, num_nodes, verbose, ),
  ]

  # parameters for each table for channels: tabel index, extra lines, header lines, footer lines
  tab_params = [
    argparse.Namespace( index_table=1, lines_extra=16, lines_head = 14, lines_foot = 3, ),
    argparse.Namespace( index_table=2, lines_extra=16, lines_head = 11, lines_foot = 6, ),
    argparse.Namespace( index_table=3, lines_extra=15, lines_head = 16, lines_foot = 1, ),
    argparse.Namespace( index_table=4, lines_extra=17, lines_head = 15, lines_foot = 4, ),
  ]

  # read data for all available channels
  index_channel = 0
  
  while ( True ):

    try:
      
      if verbose > 0:
        print( f'# loading data for channel {index_channel} ...', )
        sys.stdout.flush()

      for par in tab_params:
        
        # initialize table
        tab = tab_channel( 
          lines, 
          index_channel, 
          list_tabs[-1].line_end, 
          num_nodes, 
          par,
          verbose,
        )
        
        # add to list of available tables
        list_tabs.append( tab, )
      
      # advance counter
      index_channel += 1

    except pandas.errors.EmptyDataError:

      break

  return list_tabs


# helper functions to show messages
def wrdict(d):
  return '\n'.join( [ f'# {ki:>18s} = {vi}' for ki, vi in d.items() ], )


def wrinfo(info):
  return '# info:\n' + wrdict(vars(info))


def wrargs(args):
  return '# args:\n' + wrdict(vars(args))


def wrdate():
  return '# date:\n' + wrdict( { 'now': datetime.datetime.now(), } )


def wrinit(info,args):
  return '\n' + wrinfo(info) + '\n\n' + wrargs(args) + '\n\n' + wrdate() + '\n'


def write_resume(list_tabs):

  num_channels = int((len(list_tabs) - 2)/4)
  num_nodes = list_tabs[0].data.shape[0]

  s = '\n'
  s += f'# num_nodes = {num_nodes}\n'
  s += f'# num_channels = {num_channels}\n'

  return s

# read command line aerguments
def get_args(info,defs):
  
  # create argument parser
  parser = argparse.ArgumentParser( description=f'{info.desc}', epilog=f'{info.copyright} ({info.email})', )

  # define positional arguments (mandatory)
  parser.add_argument( 'file_name', help='Data file name', type=str, )
  
  # define optional arguments
  parser.add_argument( '--verbose', help=f'Define level of verbosity, default{defs.verbose}', default=defs.verbose, type=int, )
  
  # read arguments
  args = parser.parse_args()

  # argument validation
  assert os.path.isfile(args.file_name), f'missing file "{args.file_name}"'

  return args


# main routine, do jopb
def main(info,defs):
  
  args = get_args(info,defs)

  print(wrinit(info,args))

  list_tabs = build_tabs( args.file_name, args.verbose, )
 
  if args.verbose > 0:
    print(write_resume(list_tabs))

  return


# run script
if __name__ == '__main__':
  sys.exit(main(info,defs))
else:
  print( f'# {info.copyright}, hereby disclaims all copyright interest in "{info.name}" (contact {info.email})', )
