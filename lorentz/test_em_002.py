
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Test program for Coulomb field with numeric computation and symbolic 
# computation
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.02 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse
import itertools

import numpy
import sympy

import libem.cphys as cphys
import libem.em as em

# info
desc  = 'Test program for EM field'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'q': 1.0e+00,
         'pos': [ 0.0e+00, 0.0e+00, 0.0e+00, ]
         }

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--q', help='Charge of the poitn particle, default "{}"'.format(defs['q']), type=float, default=defs['q'], )
  parser.add_argument( '--pos', help='Position of the point particle, default "{}"'.format(defs['pos']), nargs=3, type=float, default=defs['pos'], )

  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()
  
  # get parameters
  q = args.q
  pos = numpy.array(args.pos)
  
  # define useful constant
  k = q/4.0e+00/numpy.pi/cphys.vacuum_permitivity.value 
  
  # EM for Coulomb field
  f1 = em.coufield_c(pos,q)

  # EM for general EM field
  func_e = lambda x, t: k*(x - pos)/numpy.power(numpy.linalg.norm(x-pos),3.0)
  func_b = lambda x, t: numpy.zeros( ( 3, ))

  f2 = em.emfield_c(func_e,func_b)

  # EM with symbolic functions
  x, y, z, t = sympy.Symbol('x'), sympy.Symbol('y'), sympy.Symbol('z'), sympy.Symbol('t')
  r = sympy.sqrt( (x - pos[0])*(x - pos[0]) + (y - pos[1])*(y - pos[1]) + (z - pos[2])*(z - pos[2]) )
  
  funcs_e = [ k*(x - pos[0])/r/r/r, k*(y - pos[1])/r/r/r, k*(z - pos[2])/r/r/r, ]
  funcs_b = [ (x - x), (y - y), (z - z), ]

  f3 = em.emfield_sym_c(x,y,z,t,funcs_e,funcs_b)

  # list of functions
  list_func = [ f1, f2, f3, ]

  # print data
  x1 = numpy.linspace(-1.0,1.0,10)
  x2 = numpy.zeros( ( 1, ))
  x3 = numpy.zeros( ( 1, ))
  tau = 0.0e+00
 
  for x1i in x1:
    u = numpy.array( [ x1i, 1.0e+00, 1.0e+00, ])

    '''
    for ui in u:
      print(' {:15.7e}'.format(ui),end='')
    print(end='')
    '''
   
    '''
    e = [ fi.e(u,tau) for fi in list_func] 
    for ei in numpy.nditer(e, flags=["refs_ok"], ):
      for eij in ei:
        print(' {:15.7e}'.format(eij),end='')
    '''
  
    '''
    dxe = [ fi.grad(u,tau)[0] for fi in list_func] 
    for di in numpy.nditer( [ dxe[0], dxe[1], ], flags=["refs_ok"], ):
      for dij in di:
        print(' {:15.7e}'.format(dij),end='')
    '''

    dxe = [ fi.grad(u,tau)[0] for fi in list_func] 
    for i in range(3):
      for di in numpy.nditer(dxe[i]):
        print(' {:15.7e}'.format(di),end='')
      print()
    print()
    
    '''
    dte = [ fi.dt(u,tau)[0] for fi in list_func] 
    for di in numpy.nditer(dte, flags=["refs_ok"], ):
      for dij in di:
        print(' {:15.7e}'.format(dij),end='')
    '''

    print()
  print()

  return 0

if __name__=='__main__':
  sys.exit(main())
