
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Solve trajectories of particles for the Rutherford experiment
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.11 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - improve output
# 2019.03.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy

import libem.units as units
import libem.em as em
import libem.dynamic as dynamic

# info
desc  = 'Trajectories of particles in the Rutherford experiment'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'Zp': 2,
         'Mp': 4.002602,
         'Zt': 72,
         'Mt': 196.96657,
         'b': 1.0e-12, # collision parameter, in m
         'kin': 1.0e+03, # kinetic energy of the projectile, in eV
         }

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--Zp', help='Atomic number of the projectile, default "{}"'.format(defs['Zp']), type=int, default=defs['Zp'], )
  parser.add_argument( '--Mp', help='Mass of the projectile (in amu), default "{}"'.format(defs['Mp']), type=float, default=defs['Mp'], )
  parser.add_argument( '--Zt', help='Atomic number of the target, default "{}"'.format(defs['Zt']), type=int, default=defs['Zt'], )
  parser.add_argument( '--Mt', help='Mass of the target (in amu), default "{}"'.format(defs['Mt']), type=float, default=defs['Mt'], )
  
  parser.add_argument( '--b', help='Collision parameter (in m), default "{}"'.format(defs['b']), type=float, default=defs['b'], )
  parser.add_argument( '--kin', help='Initial kinetic energy of the projectile (in eV), default "{}"'.format(defs['kin']), type=float, default=defs['kin'], )

  return parser.parse_args()

def main():

  '''
  Main program functions
  '''

  # read command line arguments
  args = get_program_args()
  assert( args.Zp>0.0 and args.Mp>0.0 )
  assert( args.Zt>0.0 and args.Mt>0.0 )

  # convert masses and charges to SI
  mass_p = args.Mp*units.mass.amu_to_kg
  q_p = args.Zp*units.charge.z_to_c

  mass_t = args.Mt*units.mass.amu_to_kg
  q_t = args.Zt*units.charge.z_to_c

  # set initial position of target particle
  pos_t = 3*[ 0.0e+00, ]

  # define Coulomb electric field
  func_em = em.coufield_c(pos_t,q_t)

  # build object to integrate the projectile trajectory
  trace = dynamic.trajectory_c(mass_p,q_p,func_em,use_jac=True)

  # define initial conditions
  ene_p = args.kin*units.energy.eV_to_j
  vp = numpy.sqrt(2.0e+00*ene_p/mass_p)

  delta = 1.0e-03
  pos_p = [ -delta, args.b, 0.0e+00, ]
  vel_p = [ vp, 0.0e+00, 0.0e+00, ]

  # set integration time
  t_begin = 0.0e+00
  t_end = 2.0e+00*delta/vp
  t_span = [ t_begin, t_end, ]

  # get trajectory
  t, u = trace.integrate(t_span,pos_p,vel_p)
  
  # print results
  # print header
  print()
  print('# {}'.format(desc))
  print('# {}'.format(eplg))
  print()
  print('# pos_init :: {}'.format(pos_p))
  print('# vel_init :: {}'.format(vel_p))
  print()
  print('# t_span :: {}'.format(t_span))
  print('# t_last t_end :: {:15.7e} {:15.7e}'.format(t[-1],t_span[-1]))
  print()
  print('# column_00 = time, in s')
  print('# column_01 = position x, in m')
  print('# column_02 = position y, in m')
  print('# column_03 = position z, in m')
  print('# column_04 = position vx, in m/s')
  print('# column_05 = position vy, in m/s')
  print('# column_06 = position vz, in m/s')
  print()
  # print results
  for i, ti in enumerate(t):
    print(' {:17.9e}'.format(ti),end='')
    for uij in u[:,i]:
      print(' {:17.9e}'.format(uij),end='')
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
