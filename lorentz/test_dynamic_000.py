
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Test for module dynamic 
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy

import libem.units as units
import libem.cphys as cphys
import libem.em as em
import libem.dynamic as dynamic

# info
desc  = 'Test module dynamic'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'pos': [ 0.0e+00, 0.0e+00, 0.0e+00, ],
         'vel': [ 0.0e+00, 1.0e+05, 1.0e+05, ], # in m/s 
         'bfield': [ 0.0e+00, 0.0e+00, 1.0e+00, ], # in T
         'q': cphys.proton.charge.value, # in C
         'mass': cphys.proton.mass.value, # in kg
         }

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--pos', help='Initial position of the electron (in m), default "{}"'.format(defs['pos']), type=float, default=defs['pos'], nargs=3, )
  parser.add_argument( '--vel', help='Initial velocity of the electron (in m/s), default "{}"'.format(defs['vel']), type=float, default=defs['vel'], nargs=3, )
  parser.add_argument( '--bfield', help='Magnetic field (in T), default "{}"'.format(defs['bfield']), type=float, default=defs['bfield'], nargs=3, )

  parser.add_argument( '--mass', help='Mass of the particle (in kg), default "{}"'.format(defs['mass']), type=float, default=defs['mass'], )
  parser.add_argument( '--q', help='Charge of the particle (in C), default "{}"'.format(defs['q']), type=float, default=defs['q'], )

  return parser.parse_args()

def main():

  '''
  Main program functions
  '''

  # read command line arguments
  args = get_program_args()

  # define electric and magnetic field
  ef = [ 0.0e+00, 0.0e+00, 0.0e+00, ]
  bf = args.bfield
  func_em = em.emfield_uniform_c(ef,bf)

  # build object to integrate the projectile trajectory
  trace = dynamic.trajectory_scaled_c(args.mass,args.q,func_em,use_jac=True)

  # define initial conditions
  pos_init = args.pos
  vel_init = args.vel 

  # set integration time
  t_begin = 0.0e+00
  t_end = 1.0e-06 # must estimate the time span !!!
  t_span = [ t_begin, t_end, ]

  # get trajectory
  t, u = trace.integrate(t_span,pos_init,vel_init)
 
  # print results
  # print header
  print()
  print('# {}'.format(desc))
  print('# {}'.format(eplg))
  print()
  print('# pos_init :: {}'.format(pos_init))
  print('# vel_init :: {}'.format(vel_init))
  print()
  print('# t_span :: {}'.format(t_span))
  print('# t_last t_end :: {:15.7e} {:15.7e}'.format(t[-1],t_span[-1]))
  print()
  print('# column_00 = time, in s')
  print('# column_01 = position x, in m')
  print('# column_02 = position y, in m')
  print('# column_03 = position z, in m')
  print('# column_04 = position vx, in m/s')
  print('# column_05 = position vy, in m/s')
  print('# column_06 = position vz, in m/s')
  print()
  # print data
  for i, ti in enumerate(t):
    print(' {:17.9e}'.format(ti),end='')
    for uij in u[:,i]:
      print(' {:17.9e}'.format(uij),end='')
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
