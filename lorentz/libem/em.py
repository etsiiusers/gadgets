
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module to manage electromagnetic fields
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include class to manage EM field defined with the vector 
#   potential and scalar potential
# - move numerical derivation to a different module
# - include gradient of some analytical models
# 2019.03.05 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add uniform EM field
# 2019.03.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import itertools

import numpy
import scipy.integrate
import scipy.misc
import sympy

try:
  import cphys as cphys
except ImportError:
  import libem.cphys as cphys

try:
  import deriv as deriv
except ImportError:
  import libem.deriv as deriv

# module info
desc = 'Manage EM fields'
author = 'Manuel Cotelo Ferrreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)



class EMFieldError(Exception):

  '''
  Exception class tio manage errors in this module
  '''

  def __init__(self,msg):
    Exception.__init__(self,msg)
    return



class emfield_base_c():

  '''
  Electromagnetic field base object
  '''

  _invalid_field = numpy.array( [ numpy.nan, numpy.nan, numpy.nan, ])

  # number of dimensions
  _num_dim = 3

  def e(self,x,t):
    '''
    Evaluate Electric E field

    -> in
    x: position, in SI
    t: time, in SI

    <- out
    (1) electric field [ ex, ey, ez, ], in SI
    '''
    
    raise EMFieldError('not implemented!!!')
    return self._invalid_field
  
  def b(self,x,t):
    
    '''
    Evaluate Magnetic B field

    -> in
    x: position, in SI
    t: time, in SI

    <- out
    (1) magnetic field [ bx, by, bz, ], in SI
    '''
    
    raise EMFieldError('not implemented!!!')
    return self._invalid_field

  def field(self,x,t):
    
    '''
    Evaluate EM field, see e(x,t) and b(x,t) above for more info
    '''
    
    return self.e(x,t), self.b(x,t)

  def __call__(self,x,t):
    return self.field(x,t)

  def grad(self,u,t):

    '''
    Evauate gradient of the EM field
    
    -> in
    x: position, in SI
    t: time, in SI

    <- out
    (1) grad(E), 3x3 dimension
    (2) grad(B), 3x3 dimension
    '''
    
    func_de = deriv.deriv_vector_c(lambda x: self.e(x,t),self._num_dim)
    func_db = deriv.deriv_vector_c(lambda x: self.b(x,t),self._num_dim)

    return func_de.eval(u), func_db.eval(u)
  
  def dt(self,u,t):
    
    '''
    Derivatives respect to time
    
    -> in
    x: position, in SI
    t: time, in SI

    <- out
    (1) dE/dt, 3-dimension 
    (2) dB/dt, 3 dimension
    '''
    
    func_de = deriv.deriv_vector_c(lambda z: self.e(u,z), self._num_dim)
    func_db = deriv.deriv_vector_c(lambda z: self.b(u,z), self._num_dim)
    
    return func_de.eval(t), func_db.eval(t)



class emfield_c(emfield_base_c):

  '''
  Object to compute general EM field based in user defined functions
  '''

  def __init__(self,func_e,func_b):
    
    '''
    Initialize object

    -> in
    func_e: function to compute E(x,t) field
    func_b: function to compute B(x,t) field
    '''

    self.func_e = func_e
    self.func_b = func_b
    return

  def e(self,x,t):

    '''
    Compute E(x,t) field

    -> in
    x: position, in m
    t: time, in s

    <- out
    (1) E field
    '''
    
    return self.func_e(x,t)

  def b(self,x,t):
    
    '''
    Compute B(x,t) field

    -> in
    x: position, in m
    t: time, in s

    <- out
    (1) B field
    '''
    
    return self.func_b(x,t)



class emfield_sym_c(emfield_base_c):

  '''
  Object to compute general EM field based in symbolic user defined functions
  '''

  def __init__(self,x,y,z,t,func_e,func_b):
    
    '''
    Initialize object

    -> in
    x, y, z: coordinates as free variables
    t: time as free variable
    func_e: function to compute E(x,y,z,t) = (Ex, Ey, Ez) field, 3 components
    func_b: function to compute B(x,y,z,t) = (Bx, By, Bz) field, 3 components
    '''

    # free variables
    self.var = [ x, y, z, t ]
    
    # symbolic functions
    self.func_e = func_e
    self.func_b = func_b
    
    # checks
    assert( len(self.func_e) == self._num_dim )
    assert( len(self.func_b) == self._num_dim )
    
    for fi in self.func_e:
      status = False
      if fi.free_symbols:
        for vi, vj in itertools.product(fi.free_symbols,self.var):
          if vi == vj:
            status = True
            break
      else:
        status = True
      assert( status )

    for fi in self.func_b:
      status = False
      if fi.free_symbols:
        for vi, vj in itertools.product(fi.free_symbols,self.var):
          if vi == vj:
            status = True
            break
      else:
        status = True
      assert( status )

    # precompute symbolic derivatives
    self.func_e_dx = []
    self.func_b_dx = []
    for fei, fbi in zip(self.func_e,self.func_b):
      self.func_e_dx.append([])
      self.func_b_dx.append([])
      for i in range(self._num_dim):
        self.func_e_dx[-1].append(fei.diff(self.var[i]))
        self.func_b_dx[-1].append(fbi.diff(self.var[i]))
    
    self.func_e_dt = []
    self.func_b_dt = []
    for i in range(self._num_dim):
      self.func_e_dt.append(self.func_e[i].diff(self.var[-1]))
      self.func_b_dt.append(self.func_b[i].diff(self.var[-1]))
    
    '''
    for i in range(self._num_dim):
      for j in range(self._num_dim):
        print('# dE_{}/dx_{} = {}'.format(i,j,self.func_e_dx[i][j]))
    '''

    return

  def param(self,x,t):
    return { self.var[0]: x[0], self.var[1]: x[1], self.var[2]: x[2], self.var[3]: t, }
  
  def subst(self,func,x,t):
    
    par = self.param(x,t)
    f = [ float(fi.subs(par)) for fi in func ]

    return numpy.array(f)

  def e(self,x,t):

    '''
    Compute E(x,t) field, see emfield_base_c.e
    '''
    
    return self.subst(self.func_e,x,t)

  def b(self,x,t):
    
    '''
    Compute B(x,t) field, see emfield_base_c.b
    '''
    
    return self.subst(self.func_b,x,t)
  
  def grad(self,x,t):

    '''
    Evauate gradient of the EM field, see emfield_base_c.grad
    '''
    
    par = self.param(x,t)

    de = numpy.zeros( ( self._num_dim, self._num_dim, ))
    db = numpy.zeros( ( self._num_dim, self._num_dim, ))
    
    for i in range(self._num_dim):
      for j in range(self._num_dim):
        de[i,j] = float(self.func_e_dx[i][j].subs(par))
        db[i,j] = float(self.func_b_dx[i][j].subs(par))

    return de, db
  
  def dt(self,x,t):
    
    '''
    Derivatives respect to time, see emfield_base_c.dt
    '''
    
    par = self.param(x,t)

    de = numpy.zeros( ( self._num_dim, ))
    db = numpy.zeros( ( self._num_dim, ))
    
    for i in range(self._num_dim):
      de[i] = float(self.func_e_dt[i].subs(par))
      db[i] = float(self.func_b_dt[i].subs(par))

    return de, db


class emfield_uniform_c(emfield_base_c):
  
  '''
  Object to compute uniform EM field, E = E0 and B = B0
  '''

  def __init__(self,e0,b0):
    
    '''
    Initialize object

    -> in
    e: constant E(x,t) = E0 field
    b: constant B(x,t) = B0 field
    '''

    self.e0 = numpy.array(e0)
    self.b0 = numpy.array(b0)

    self.gx = numpy.zeros( ( self._num_dim, self._num_dim, ))
    self.gt = numpy.zeros( ( self._num_dim, ))
    return

  def e(self,x,t):

    '''
    Compute E(x,t) field, see emfield_base_c.e
    '''
    
    return self.e0

  def b(self,x,t):
    
    '''
    Compute B(x,t) field, see emfield_base_c.b
    '''
    
    return self.b0
  
  def grad(self,u,t):

    '''
    Evauate gradient of the EM field, see emfield_base_c.grad
    '''
    
    return self.gx, self.gx
  
  def dt(self,u,t):
    
    '''
    Derivatives respect to time, see emfield_base_c.dt
    '''
    
    return self.gt, self.gt


class coufield_c(emfield_base_c):

  '''
  Object to compute EM field produced by a static point charge
  '''

  def __init__(self,pos,q):
    
    '''
    Initialization

    -> in
    pos: point charge position, in m
    q: charge, in C
    '''

    # parameters
    self.pos = numpy.array(pos)
    self.q = q

    # auxiliar variables
    self.e0 = self.q/4.0e+00/numpy.pi/cphys.vacuum_permitivity.value
    self.b0 = numpy.zeros( ( self._num_dim, ))
    
    # derivatives
    self.gx = numpy.zeros( ( self._num_dim, self._num_dim, ))
    self.gt = numpy.zeros( ( self._num_dim, ))

    return

  def e(self,x,t):
    
    '''
    Compute E(x,t) field, see emfield_base_c.e
    '''
    
    dx = x - self.pos
    r = numpy.linalg.norm(dx)
    return self.e0*dx/r/r/r

  def b(self,x,t):
    
    '''
    Compute B(x,t) field, see emfield_base_c.b
    '''

    return self.b0
  
  def grad(self,x,t):

    '''
    Evauate gradient of the EM field, see emfield_base_c.grad
    '''
    
    dx = x - self.pos
    r = numpy.linalg.norm(dx)
    
    r3 = r*r*r
    r5 = r3*r*r
    
    de = numpy.zeros( ( self._num_dim, self._num_dim, ))
    for i in range(self._num_dim):
      de[i,:] -= 3.0*dx[i]*dx[:]/r5
      de[i,i] += 1.0e+00/r3
      de[i,:] *= self.e0

    return de, self.gx
  
  def dt(self,x,t):
    
    '''
    Derivatives respect to time, see emfield_base_c.dt
    '''
    
    return self.gt, self.gt


class emlaser_c(emfield_base_c):

  '''
  Object for the EM wave in vacuumm
  '''

  def __init__(self,dir,e0,w,func_epol,x0=0.0e+00,t0=0.0e+00):
    
    '''
    Initialization
    '''

    # parameters
    self.dir = numpy.array(dir)
    assert( numpy.linalg.norm(self.dir) > 0.0e+00 )
    self.dir /=numpy.linalg.norm(self.dir)
    self.e0 = e0
    self.w = w
    self.func_epol = func_epol
    self.x0 = x0
    self.t0 = t0
    # auxiliar variables
    self.k = self.w/cphys.c_light.value # assume positive solution, another solution for -k exists
    self.k = self.k*self.dir
    self.b0 = self.e0/cphys.c_light.value
    self.phase_ref = self.w*self.t0 - numpy.dot(self.k,self.x0)
    return

  def polarization(self,x,t):
    
    '''
    Get polarization vector
    '''

    ep = self.func_epol(x,t)
    assert( numpy.linalg.norm(ep) > 0.0e+00 )
    ep /= numpy.linalg.norm(ep)
    bp = numpy.cross(self.dir,ep)
    bp /= numpy.linalg.norm(bp)
    return ep, bp

  def phase(self,x,t):

    '''
    Get phase of the wave
    '''

    return self.w*(t - self.t0) - numpy.dot(self.k,x-self.x0)
   
  def fwave(self,x,t):

    '''
    Compute wave oscillations
    '''

    return numpy.cos(self.phase(x,t))

  def e(self,x,t):
    
    '''
    Compute E(x,t) field, see emfield_base_c.e
    '''

    ep, _ = self.polarization(x,t)
    return ep*self.e0*self.fwave(x,t)

  def b(self,x,t):
    
    '''
    Compute B(x,t) field, see emfield_base_c.b
    '''
    
    _, bp = self.polarization(x,t)
    return bp*self.b0*self.fwave(x,t)

  def field(self,x,t):
    
    '''
    Evaluate EM field, see emfield_base_c.field
    '''

    ep, bp = self.polarization(x,t)
    f = self.fwave(x,t)
    return ep*self.e0*f, bp*self.b0*f


class emlaser_lp_c(emlaser_c):

  def __init__(self,dir,e0,w,epol,x0=0.0e+00,t0=0.0e+00):
    func_epol = lambda x, t: epol
    emlaser_c.__init__(self,dir,e0,w,func_epol,x0,t0)
    return
  
  def grad(self,x,t):

    '''
    Evauate gradient of the EM field, see emfield_base_c.grad
    '''
    
    de = numpy.zeros( ( self._num_dim, self._num_dim, ))
    db = numpy.zeros( ( self._num_dim, self._num_dim, ))

    ep, bp = self.polarization(x,t)

    for i in range(self._num_dim):
      sp = numpy.sin(self.phase(x,t))
      de[i,:] = - self.k[:]*self.e0*ep[i]*sp
      db[i,:] = - self.k[:]*self.b0*bp[i]*sp

    return de, db
  
  def dt(self,x,t):
    
    '''
    Derivatives respect to time, see emfield_base_c.dt
    '''

    de = numpy.zeros( ( self._num_dim, ))
    db = numpy.zeros( ( self._num_dim, ))

    ep, bp = self.polarization(x,t)

    sp = numpy.sin(self.phase(x,t))
    
    de = self.w*self.e0*ep*sp
    db = self.w*self.b0*bp*sp

    return de, db


class afield_base_c():

  '''
  Electromagnetic field base object

  Computes A (vector potential) and phi (scalar  potential)
  '''

  _invalid_field_sca = numpy.array( [ numpy.nan, ])
  _invalid_field_vec = numpy.array( [ numpy.nan, numpy.nan, numpy.nan, ])

  # number of dimensions
  _num_dim = 3

  def vecp(self,x,t):
    '''
    Evaluate Vector Potential A(x,t)

    -> in
    x: position, in SI
    t: time, in SI

    <- out
    (1) vector potential [ ax, ay, az, ], in SI
    '''
    
    raise EMFieldError('not implemented!!!')
    return self._invalid_field_vec
  
  def scap(self,x,t):
    
    '''
    Evaluate Scalar Potential phi(x,t)

    -> in
    x: position, in SI
    t: time, in SI

    <- out
    (1) scalar potential, phi(x,t), in SI
    '''
    
    raise EMFieldError('not implemented!!!')
    return self._invalid_field_sca

  def field(self,x,t):
    '''
    Evaluate EM field, see vecp(x,t) and scap(x,t) above for more info
    '''
    return self.vecp(x,t), self.scap(x,t)

  def __call__(self,x,t):
    return self.field(x,t)

  def dx(self,u,t):

    '''
    Evauate gradient of the EM field
    
    -> in
    x: position, in SI
    t: time, in SI

    <- out
    (1) grad(E), 3x3 dimension
    (2) grad(B), 3x3 dimension
    '''
    
    func_dv = deriv.deriv_vector_c(lambda x: self.vecp(x,t))
    func_ds = deriv.deriv_scalar_c(lambda x: self.scap(x,t))
    
    return func_dv.eval(u), func_ds.eval(u)
  
  def dt(self,u,t):
    
    '''
    Derivatives respect to time
    
    -> in
    x: position, in SI
    t: time, in SI

    <- out
    (1) dE/dt, 3-dimension 
    (2) dB/dt, 3 dimension
    '''
    
    func_dv = deriv.deriv_vector_c(lambda t: self.vecp(u,t))
    func_ds = deriv.deriv_scalar_c(lambda t: self.scap(u,t))
    
    return func_dv.eval(t), func_ds.eval(t)

class afield_c(afield_base_c):

  '''
  Object to compute general EM field based in user defined functions for vector 
  potential and scalar potential
  '''

  def __init__(self,func_vp,func_sp):
    
    '''
    Initialize object

    -> in
    func_vp: function to compute A(x,t) vector potential
    func_sp: function to compute phi(x,t) scalar potential
    '''

    self.func_vp = func_vp
    self.func_sp = func_sp
    return

  def vecp(self,x,t):

    '''
    Compute A(x,t) vector potential

    -> in
    x: position, in m
    t: time, in s

    <- out
    (1) A vector potential
    '''
    
    return self.func_vecp(x,t)

  def scap(self,x,t):
    
    '''
    Compute phi(x,t) scalar potential

    -> in
    x: position, in m
    t: time, in s

    <- out
    (1) phi scalar potential
    '''
    
    return self.func_b(x,t)


if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
