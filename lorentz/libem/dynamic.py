
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module to integrate the equations of the dynamics of a charged particle
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - improve scaling of the equations of motion
# 2019.03.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

import numpy
import scipy.integrate

try:
  import cphys as cphys
except ImportError:
  import libem.cphys as cphys

# module info
desc = 'Integrate the movement of a charged particle in an electric field'
author = 'Manuel Cotelo Ferrreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)


class trajectory_c():

  '''
  Object for trajectory integration of a charged particle in a EM field using 
  the laboratory frame
  '''

  # auxiliar variables for indices
  _ix1 = 0
  _ix2 = _ix1 + 1
  _ix3 = _ix2 + 1
  _iv1 = _ix3 + 1
  _iv2 = _iv1 + 1
  _iv3 = _iv2 + 1

  # define ODE dimensionality, in 3D
  _num_dim = 3
  _num_var = _num_dim + _num_dim

  # select ODE integration method
  _method_default = 'BDF'
  #_method_default = 'LSODA'
  #_method_default = 'RK45'
  #_method_default = 'Radau'

  # set default tolerances
  _tol_rel = 1.0e-12
  _tol_abs = 1.0e-12

  def __init__(self,mass,q,func_field,func_ext=None,use_jac=False):

    '''
    Object initialization

    -> in
    mass: mass of the particle, in kg
    q: charge of the particle, in C
    func_field: function for EM field evaluation
    func_ext: function to evaluate other extern forces
    use_jac: flag to use ODE solver with access to the jacobian
    '''

    # parameters
    self.mass = mass
    self.q = q
   
    # field functions
    self.func_field = func_field
    self.func_ext = func_ext
    self.func_jac = None
    if use_jac:
      self.func_jac = self.eval_jac

    return
  
  def scale(self,t_delta):
    
    # set scale factors
    self.scale_t = t_delta
    self.scale_v = cphys.c_light.value
    self.scale_x = self.scale_v*self.scale_t
    self.scale_u = self.scale_v*self.scale_t/self.scale_x
    self.scale_w = self.scale_t/self.scale_v
    
    # set scale parameters
    self.scale_wb = self.scale_t*self.q/self.mass
    self.scale_wo = self.scale_t/self.scale_v/self.mass
    self.scale_we = self.q*self.scale_wo

    return
    
  def get_uw(self,s):
    
    '''
    Split ODE state in position and velocity

    -> in
    s: full state

    <- out
    (1) position
    (2) velocity
    '''

    return s[:self._num_dim], s[self._num_dim:]

  def eval_ode(self,z,s):

    '''
    Evaluate ODE functions: du_i/dt = f_i(u,t)

    -> in
    z: scaled time
    s: scaled state

    <- out
    (1) evaluation of functions f_i(u,t)
    '''
  
    # split state
    u, w = self.get_uw(s)

    # scale state to original dimensions
    t = z*self.scale_t
    x = u*self.scale_x

    # build array to store derivatives
    df = numpy.zeros( ( self._num_var, ))
    du, dw = self.get_uw(df)

    # get EM field 
    e, b = self.func_field(x,t)

    wxb = numpy.cross(w,b)

    # get other fields acting over the particle
    fext = numpy.zeros( ( self._num_dim, ))
    if self.func_ext:
      fext = self.func_ext(x,t)

    # evaluate derivatives
    du[:] = w[:]
    dw[:] = self.scale_wb*wxb[:] + self.scale_we*e[:] + self.scale_wo*fext[:]

    return df

  def eval_jac(self,z,s):

    '''
    Evaluate jacobian of the ODE functions: du_i/dt = f_i(u,t), the 
    jacobian J(i,j) = (d f_i)/(d u_j)

    -> in
    z: scaled time
    s: scaled state

    <- out
    (1) jacobian matrix
    '''
    
    # split state
    u, w = self.get_uw(s)

    # scale state to original dimensions
    t = z*self.scale_t
    x = u*self.scale_x
    v = w*self.scale_v
    
    # get EM field 
    e, b = self.func_field(x,t)
    # get EM gradient
    de, db = self.func_field.grad(x,t)

    # get external field
    f  = numpy.zeros( ( self._num_dim))
    df = numpy.zeros( ( self._num_dim, self._num_dim, ))
    if self.func_ext:
      f = self.func_ext(x,t)
    
    # scale gradients
    de *= self.scale_x
    db *= self.scale_x
    df *= self.scale_x
    
    # initialize jacobian matrix
    j = numpy.zeros( ( self._num_var, self._num_var, ))
    ju, jw = self.get_uw(j)

    # fill jacobian
    ju[self._ix1,self._iv1] = 1.0e+00
    ju[self._ix2,self._iv2] = 1.0e+00
    ju[self._ix3,self._iv3] = 1.0e+00

    jw[self._ix1,:self._iv1] = self.scale_we*de[self._ix1,:] + self.scale_wo*df[self._ix1,:] + self.scale_wb*(w[self._ix2]*db[self._ix3,:] - w[self._ix3]*db[self._ix2,:])
    jw[self._ix1,self._iv1] = 0.0e+00
    jw[self._ix1,self._iv2] = self.scale_wb*( b[self._ix3])
    jw[self._ix1,self._iv3] = self.scale_wb*(-b[self._ix2])
    
    jw[self._ix2,:self._iv1] = self.scale_we*de[self._ix2,:] + self.scale_wo*df[self._ix2,:] + self.scale_wb*(w[self._ix3]*db[self._ix1,:] - w[self._ix1]*db[self._ix3,:])
    jw[self._ix2,self._iv1] = self.scale_wb*(-b[self._ix3])
    jw[self._ix2,self._iv2] = 0.0e+00
    jw[self._ix2,self._iv3] = self.scale_wb*( b[self._ix1])
    
    jw[self._ix3,:self._iv1] = self.scale_we*de[self._ix3,:] + self.scale_wo*df[self._ix3,:] + self.scale_wb*(w[self._ix1]*db[self._ix2,:] - w[self._ix2]*db[self._ix1,:])
    jw[self._ix3,self._iv1] = self.scale_wb*( b[self._ix2])
    jw[self._ix3,self._iv2] = self.scale_wb*(-b[self._ix1])
    jw[self._ix3,self._iv3] = 0.0e+00

    return j

  def integrate(self,t_span,pos,vel,size=0):
    
    # set scale parameters
    t_delta = t_span[1] - t_span[0]
    self.scale(t_delta)

    # copy time span
    z_span = numpy.array(t_span)
    
    # copy initial conditions
    s_init = numpy.zeros( ( self._num_var, ))
    u_init, w_init = self.get_uw(s_init)
    u_init[:] = numpy.array(pos)
    w_init[:] = numpy.array(vel)

    # rescale values
    z_span /= self.scale_t
    u_init /= self.scale_x
    w_init /= self.scale_v

    # select evaluation times
    z_sample = None
    if size > 0:
      z_sample = numpy.linspace(z_span[0],z_span[1],size)
   
    # integrate ODE
    ans = scipy.integrate.solve_ivp( self.eval_ode, z_span, s_init, jac=self.func_jac, jac_sparsity=None, t_eval=z_sample, method=self._method_default, rtol=self._tol_rel, atol=self._tol_abs, )

    # scale back
    ans.t *= self.scale_t
    yu, yw = self.get_uw(ans.y)
    yu *= self.scale_x
    yw *= self.scale_v

    return ans.t, ans.y

if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
