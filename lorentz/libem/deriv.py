
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module to manage electromagnetic fields
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

import numpy
import scipy.integrate
import scipy.misc

# module info
desc = 'Numerical derivation of fields'
author = 'Manuel Cotelo Ferrreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)



class DerivError(Exception):

  '''
  Exception class to manage errors in this module
  '''

  def __init__(self,msg):
    Exception.__init__(self,msg)
    return



class deriv_scalar_c():

  '''
  Object for numerical derivation of scalar functions
  '''

  # default parameters
  _delta_default = 1.0e-06
  
  # default order
  _order_default = 1

  # numeric derivation order
  _deriv_order = 5

  # auxiliar class to compute gradient
  class _eval_dx_c():

    '''
    Auxiliar object to evaluate numerical derivatives 
    '''

    def __init__(self,u,ix,func):
      '''
      Initialize object

      -> in
      u: reference coorcinates
      ix: dimension for variations
      func: functor to evaluate in reference coordinates and time
      '''
      
      self.u = numpy.array(u)
      self.ix = ix
      self.func = func

      assert( self.u.shape )

      return

    def __call__(self,x):
      '''
      Call operator

      -> in
      x: variation in the coordinate

      <- out
      (1) evaluation of the function
      '''
      # copy reference coordinates
      u = self.u 
      # modify coordinate with the input value
      u[self.ix] = x
      # evaluate function
      return self.func(u)

  def __init__(self,func):
    self.func = func
    return

  def eval(self,x,ord=_order_default,delta=_delta_default):

    '''
    Evauate gradient
    
    -> in
    x: position, in SI
    delta: step for numerical derivation
    ord: order of the derivative

    <- out
    (1) grad(F), same dimension as x
    '''
    
    # initialize variables
    try:
      u = x
      shape = x.shape
    except AttributeError:
      u = numpy.array( [ x, ])
      shape = u.shape
    
    df = numpy.zeros( shape, )

    # compute derivatives
    for i in range(shape[0]):
        df[i] = scipy.misc.derivative( self._eval_dx_c(u, i, self.func), x0=u[i], dx=delta, n=ord, order=self._deriv_order)
    
    return df



class deriv_vector_c():

  '''
  Object for numerical derivation of scalar functions
  '''

  # default parameters
  _delta_default = 1.0e-09
  
  # default order
  _order_default = 1
  
  class _eval_comp_c():
    
    def __init__(self,func,ic):
      self.func = func
      self.ic = ic
      return

    def __call__(self,x):
      return self.func(x)[self.ic]

  def __init__(self,func,nc):
    
    '''
    Initialization

    -> in 
    func: functor
    nc: number of components
    '''

    # parameters
    self.func = func
    self.nc = nc
    assert( self.nc > 0 )

    # derivative evaluation
    self.func_dx = [ deriv_scalar_c(self._eval_comp_c(self.func,i)) for i in range(self.nc) ]

    return

  def eval(self,x,ord=_order_default,delta=_delta_default):
    
    '''
    Evaluate gradient for all function components
    
    -> in
    x: position, in SI
    delta: step for numerical derivation
    ord: order of the derivative

    <- out
    (1) grad(F), dimension (self.nc) x (u.shape)
    '''
    
    # allocate memory for data
    try:
      u = x
      shape = u.shape
    except AttributeError:
      u = numpy.array( [ x, ])
      shape = u.shape
   
    df = numpy.zeros( ( self.nc, shape[0], ))

    # get gradient for every function component
    for j, func_df in enumerate(self.func_dx):
      df[j,:] = func_df.eval(u)
    
    return df



if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run')
  sys.exit(-1)
