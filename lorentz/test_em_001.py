
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Test program for EM fields
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.04 :: Manuel Cotelo Ferreiro
# - initial version
#

import sys
import argparse

import numpy

import libem.em as em

# info
desc  = 'Test program for EM field'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'w': 1.0e+15,
         'e': 1.0e+00,
         'dir': [ 0.0e+00, 0.0e+00, 1.0e+00, ],
         'pol': [ 1.0e+00, 0.0e+00, 0.0e+00, ],
         }

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--w', help='Frequency of the EM wave (in 1/s), default "{}"'.format(defs['w']), type=float, default=defs['w'], )
  parser.add_argument( '--e', help='Amplitude of electric field in the EM wave (in V/m), default "{}"'.format(defs['e']), type=float, default=defs['e'], )
  parser.add_argument( '--dir', help='Direction of propagation of the wave, default "{}"'.format(defs['dir']), nargs=3, type=float, default=defs['dir'], )
  parser.add_argument( '--pol', help='Polarization of electric field in the EM wave, default "{}"'.format(defs['pol']), nargs=3, type=float, default=defs['pol'], )

  return parser.parse_args()

def main():

  args = get_program_args()

  func_em = em.emlaser_lp_c(args.dir,args.e,args.w,args.pol)

  delta = 1.0e+00

  x_lo = -delta
  x_hi =  delta
  x_size = 100
  x = numpy.linspace(x_lo,x_hi,x_size)
  
  z_lo = -delta
  z_hi =  delta
  z_size = 100
  z = numpy.linspace(z_lo,z_hi,z_size)

  for zk in z:
    for xi in x:
      u = numpy.array( [ xi, 0.0e+00, zk, ])

      e, b = func_em(u,0.0e+00)

      for ui in u:
        print(' {:15.7e}'.format(ui),end='')
      for ei in e:
        print(' {:15.7e}'.format(ei),end='')
      for bi in b:
        print(' {:15.7e}'.format(bi),end='')
      print()

    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
