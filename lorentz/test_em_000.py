
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Test program for EM fields
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.04 :: Manuel Cotelo Ferreiro
# - initial version
#

import sys
import argparse

import numpy

import libem.em as em

# info
desc  = 'Test program for EM field'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'pos': 3*[ 0.0e+00, ],
         'q': 1.0e+00,
         }

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--pos', help='Point charge position (in m), default "{}"'.format(defs['pos']), nargs=3, type=float, default=defs['pos'], )
  parser.add_argument( '--q', help='Charge value (in C), default "{}"'.format(defs['q']), type=float, default=defs['q'], )

  return parser.parse_args()

def main():

  args = get_program_args()

  x_delta = 1.0e+00
  x_lo = -x_delta
  x_hi =  x_delta
  x_size = 1000
  x = numpy.linspace(x_lo,x_hi,x_size)

  func_em = em.coufield_c(args.pos,args.q)

  for xi in x:
    u = args.pos
    u[0] = xi

    e, b = func_em(u,0.0e+00)

    for ui in u:
      print(' {:15.7e}'.format(ui),end='')
    for ei in e:
      print(' {:15.7e}'.format(ei),end='')
    for bi in b:
      print(' {:15.7e}'.format(bi),end='')

    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
