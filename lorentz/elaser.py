
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Solve trajectory of an electron ina plane EM wave
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - include jacobian calculations for trajectory 
# 2019.03.11 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - improve output
# 2019.03.04 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy

import libem.units as units
import libem.cphys as cphys
import libem.em as em
import libem.dynamic as dynamic

# info
desc  = 'Trajectory of an electron in a plane EM wave'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'pos': [ 0.0e+00, 0.0e+00, 0.0e+00, ], # in m
         'vel': [ 0.0e+00, 0.0e+00, 0.0e+00, ], # in m/s 
         'dir': [ 0.0e+00, 0.0e+00, 1.0e+00, ],
         'pol': [ 1.0e+00, 0.0e+00, 0.0e+00, ],
         'amp': 1.0e+08, # in V/m
         'wl': 1.0e-06, # in m
         }

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--dir', help='Propagation direction of the EM wave, default "{}"'.format(defs['dir']), type=float, default=defs['dir'], nargs=3, )
  parser.add_argument( '--pol', help='Polarization of the electric field, default "{}"'.format(defs['pol']), type=float, default=defs['pol'], nargs=3, )
  parser.add_argument( '--amp', help='Amplitude of the electric field (in V/m), default "{}"'.format(defs['amp']), type=float, default=defs['amp'], )
  parser.add_argument( '--wl', help='Wavelength of the EM field (in m), default "{}"'.format(defs['wl']), type=float, default=defs['wl'], )

  parser.add_argument( '--pos', help='Initial position of the electron (in m), default "{}"'.format(defs['pos']), type=float, default=defs['pos'], nargs=3)
  parser.add_argument( '--vel', help='Initial velocity of the electron (in m/s), default "{}"'.format(defs['vel']), type=float, default=defs['vel'], nargs=3)

  return parser.parse_args()

def main():

  '''
  Main program functions
  '''

  # read command line arguments
  args = get_program_args()
  assert( args.wl > 0.0e+00 )

  # define Coulomb electric field
  freq = cphys.c_light.value/args.wl
  func_em = em.emlaser_lp_c(args.dir,args.amp,freq,args.pol)

  # elecron properties
  mass_e = cphys.electron.mass.value
  q_e = cphys.electron.charge.value

  # build object to integrate the projectile trajectory
  trace = dynamic.trajectory_c(mass_e,q_e,func_em,use_jac=True)

  # define initial conditions
  pe = args.pos
  ve = args.vel 

  # set integration time
  t_begin = 0.0e+00
  t_end =  4.0e+01/freq # 40 cycles of the EM wave
  t_span = [ t_begin, t_end, ]

  # get trajectory
  t, u = trace.integrate(t_span,pe,ve)
  
  # print results
  # print header
  print()
  print('# {}'.format(desc))
  print('# {}'.format(eplg))
  print()
  print('# pos_init :: {}'.format(pe))
  print('# vel_init :: {}'.format(ve))
  print()
  print('# t_span :: {}'.format(t_span))
  print('# t_last t_end :: {:15.7e} {:15.7e}'.format(t[-1],t_span[-1]))
  print()
  print('# column_00 = time, in s')
  print('# column_01 = position x, in m')
  print('# column_02 = position y, in m')
  print('# column_03 = position z, in m')
  print('# column_04 = position vx, in m/s')
  print('# column_05 = position vy, in m/s')
  print('# column_06 = position vz, in m/s')
  print()
  # print results
  for i, ti in enumerate(t):
    print(' {:17.9e}'.format(ti),end='')
    for uij in u[:,i]:
      print(' {:17.9e}'.format(uij),end='')
    print()

  return 0

if __name__=='__main__':
  sys.exit(main())
