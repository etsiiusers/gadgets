
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Test program for EM fields
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.01 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse
import itertools

import numpy

import libem.deriv as deriv

# info
desc  = 'Test program for numerical dderivation'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = {}

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)
  
  return parser.parse_args()


def eval_p00_f(x):
  fx = x[0]*(1.0 + x[0])*(1.0 - x[0])
  fy = x[1]*(1.0 + x[1])*(1.0 - x[1])
  return fx*fy

def eval_p00_df(x):
  fx = x[0]*(1.0 + x[0])*(1.0 - x[0])
  fy = x[1]*(1.0 + x[1])*(1.0 - x[1])
  dfx = 1.0 - 3.0*x[0]*x[0] 
  dfy = 1.0 - 3.0*x[1]*x[1] 
  return numpy.array( [ dfx*fy, fx*dfy, ])

def eval_p01_f(x,t):
  return eval_p00_f(x - 1.0*t)

def eval_p01_df(x,t):
  return eval_p00_df(x - 1.0*t)

list_problems = [ ( eval_p00_f, eval_p00_df, ),
                  #( lambda x: eval_p01_f(x,0.0), lambda x: eval_p01_df(x,0.0), ),
                  ]

def driver(prob):
  
  eval_f = prob[0]
  eval_df = prob[1]
      
  func_df = deriv.deriv_scalar_c(eval_f)

  x = numpy.linspace(-1.0,1.0,100)
  y = numpy.linspace(-1.0,1.0,100)
  
  err = 0.0e+00
  for u in itertools.product(x,y):
    u = numpy.array(u)

    df = eval_df(u)
    df0 = func_df.eval(u)
    
    for w1, w2 in numpy.nditer( [ df, df0, ]):
      e = w1 - w2
      if w1 != 0.0:
        e /= w1
      err += e*e
   

  for xi in x:
    for yj in y:
      u = numpy.array( [ xi, yj, ])

      for ui in u:
        print(' {:15.7e}'.format(ui),end='')
      
      df = eval_df(u)
      df0 = func_df.eval(u)
      for w in numpy.nditer( [ df, df0, ]):
        for wi in w:
          print(' {:15.7e}'.format(wi),end='')
      print()
    print()

  return err/x.size/y.size

def main():

  args = get_program_args()

  err = list(map(driver,list_problems))

  for ei in err:
    print('# error :: {}'.format(numpy.sqrt(ei)))

  return 0

if __name__=='__main__':
  sys.exit(main())
