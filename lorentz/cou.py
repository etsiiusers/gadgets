
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Symbolic derivatives of the Coulomb electric field
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.02 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import sympy

# info
desc  = 'Symbolic derivatives of the Coulomb electric field'
author = 'Manuel Cotello Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'q': 1.0e+00,
         'pos': [ 0.0e+00, 0.0e+00, 0.0e+00, ],
         } 

def print_field(e,de,fd=sys.stdout):
 
  # set number of dimensions
  num_dim = len(e)

  # print header
  print(file=fd)
  print('# desc = {}'.format(desc),file=fd)
  print('# eplg = {}'.format(eplg),file=fd)
  print(file=fd)

  # print Coulomb electric field
  for i in range(num_dim):
    print('# E_{} = {}'.format(i,e[i]),file=fd)
  print(file=fd)

  # derivatives of the Coulomb electric field
  for i in range(num_dim):
    for j in range(num_dim):
      print('# dE_{}/dx_{} = {}'.format(i,j,de[i][j]),file=fd)
  print(file=fd)
  
  return

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--q', help='Charge of the point particle, default "{}"'.format(defs['q']), type=float, default=defs['q'], )
  parser.add_argument( '--pos', help='Position of the point particle, default "{}"'.format(defs['pos']), nargs=3, type=float, default=defs['pos'], )
  
  return parser.parse_args()

def main():

  '''
  Main program functions
  '''

  num_dim = 3

  # read command line arguments
  args = get_program_args()

  # define symbols for coordinates
  x = [ sympy.Symbol('x'), sympy.Symbol('y'), sympy.Symbol('z'), ]

  # define symbols for parameters
  q = sympy.Symbol('q')
  x0 = [ sympy.Symbol('x0'), sympy.Symbol('y0'), sympy.Symbol('z0'), ]
  
  # define Coulomb electric field
  dx = [ (xi - xi0) for xi, xi0 in zip(x,x0) ]
  r = sympy.sqrt(dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2])
  r3 = r*r*r
  e = ( q*dx[0]/r3, q*dx[1]/r3, q*dx[2]/r3, )
  
  # derivatives of the Coulomb electric field
  de = []
  for i in range(num_dim):
    de.append([])
    for j in range(num_dim):
      de[-1].append(e[i].diff(x[j]))

  # print results
  print_field(e,de)

  return 0

if __name__=='__main__':
  sys.exit(main())
