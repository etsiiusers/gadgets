
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Numeric derivation
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.13 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

# program info
info = { 'description': 'Numeric derivation',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }

class func_deriv():

  def __init__(self,func,delta,k):
    self.func = numpy.vectorize(func)
    self.delta = delta
    self.k = k
    return

  def eval_func(self,x):
    return self.func(x)

  def _deval(self,x,ds):

    dim = ds.shape[0]

    b = self.eval_func(x + ds) - self.eval_func(x)
    a = numpy.zeros( ( dim, dim, ))
    
    q = numpy.array([ numpy.math.factorial(i+1) for i in range(dim) ])
    for j in range(dim):
      dj = ds[j]
      for i in range(dim):
        a[j,i] = numpy.power(dj,i+1)/q[i]

    df = numpy.linalg.solve(a,b)
    res = numpy.allclose(numpy.dot(a,df),b)

    return df, res

  def eval_derv(self,x):
    
    ds = []
    for i in range(self.k):
      di = (i+1)*self.delta
      ds.append( di)
      ds.append(-di)
    ds = numpy.array(ds)

    return self._deval(x,ds)


def test():

  func = lambda x: (((((3.0*x + 2.0)*x + 1.0)*x - 4.0)*x + 3.0)*x - 2.0)*x + 1.0

  dfunc = func_deriv(func,1.0e-01,3)

  x = 0.0e+00

  df, res = dfunc.eval_derv(x)

  print('# res = {:15.7e}'.format(res))
  for i, di in enumerate(df):
    print(' {:3d} {:15.7e}'.format(i,di))

  return

if __name__=='__main__':
  sys.exit(test())
