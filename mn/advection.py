
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Numeric methods for advection equation
#
# u = u(x,t)
#
# du/dt + a*du/dx = 0
#
# u(x,0) = u0(x) for 0 <= x <= 1, initial conditions known
#
# BC's, open boundaries
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.17 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

# program info
info = { 'description': 'Numeric methods for advection equation',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }

# function to define initial conditions
def initial_state(x):
  '''
  Initial conditions, in this case u(x,0) = x
  '''
  u =  x
  return return u
  
#
# functions to advance the state
#
def update_simple(u0,a,dx,dt):
  u = u0
  return u

def update_upwind(u0,a,dx,dt):
  u = u0
  return u

def update_downwind(u0,a,dx,dt):
  u = u0
  return u

def update_lf(u0,a,dx,dt):
  u = u0
  return u

def update_lw(u0,a,dx,dt):
  u = u0
  return u

def update_bw(u0,a,dx,dt):
  u = u0
  return u
  
list_funcs = { 'simple': update_simple,
               'upwind':update_upwind,
               'downwind': update_downwind,
               'lax-friedrichs': update_lf,
               'lax-wendroff': update_lw,
               'beam-warming': update_bw,
               }

# driver function
def main():
  
  # define equation parameters
  a = 1.0e+00

  # define an uniform mesh
  nx = 100

  xlo = 0.0e+00
  xhi = 1.0e+00

  x = numpy.linspace(xlo,xhi,nx,endpoint=True)
  h = (xhi - xlo)/(nx - 1)

  # define initial conditions
  u0 = initial_state(nx,1.0e+00,0.0e+00)
  u = u0

  # integration parameters
  num_steps = 100
  dt = 1.0e-01

  # select method
  name_model = 'simple'
  func_update = list_funcs[name_model]

  # print initial state
  print_state(x,u0)

  # integration loop
  for i in range(num_steps):
    u = func_update(u0,a,h,dt)
    
  return


main()
