
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Numeric derivation
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.18 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - ejercicio de ejemplo realizado en clase
#

import numpy

def eval_f(x):
  return ((2.0*x - 2.0)*x + 3.0)*x - 3.0

def eval_df(x):
  return 6.0*x*x - 4.0*x + 3.0

def main():
  
  # creación de malla
  nx = 100 # número de puntos
  xlo = 0.0 # límite inferior
  xhi = 1.0 # límite superior

  h = (xhi - xlo)/(nx - 1) # tamaño de celda

  x = numpy.linspace(xlo,xhi,nx) # puntos de la malla

  # evaluación de la función y su derivada en los puntos de la malla
  fx = eval_f(x)
  dfx_exact = eval_df(x)

  # creación de array para derivadas numéricas
  df_o2 = numpy.zeros( ( nx, ))

  # 
  # cálculo de derivada numérica
  # 
  # derivada en la frontera inferior
  df_o2[0] = (fx[1] - fx[0])/h
  # derivada en los puntos interiores
  for i in range(1,nx-1):
    df_o2[i] = (fx[i+1] - fx[i-1])/2.0/h
  # derivada en la frontera superior
  df_o2[nx-1] = (fx[nx-1] - fx[nx-2])/h

  # cálculo del error relativo 
  err = (dfx_exact - df_o2)/dfx_exact

  for i in range(nx):
    print(' {:15.7e} {:15.7e} {:15.7e} {:15.7e} {:15.7e}'.format(x[i],fx[i],dfx_exact[i],df_o2[i],err[i]))

  return

main()
