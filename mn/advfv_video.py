
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Numeric methods for advection equation using finite volume discretizations
#
# u = u(x,t)
#
# du/dt + a*du/dx = 0, x in [0,1]
#
# Initial conditions:
# u(x,0) = "square function"
#
# Periodic boundary conditions
# u(0,t) = u(1,t)
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.17 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation

# program info
info = { 'description': 'Numeric methods for advection equation using finite volumes',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }

# function to define initial conditions
def initial_state(x):
  '''
  Initial conditions, in this case u(x,0) = x
  '''
  q =  numpy.zeros(x.shape)

  l = x[-1] - x[0]
  l = l/3.0
  xc = 0.5*(x[0] + x[-1])
  xlo = xc - l/2.0
  xhi = xc + l/2.0

  for i in range(x.shape[0]):
    if x[i] > xlo and x[i] < xhi:
      q[i] = 1.0e+00

  return q

# functor for boundary conditions
class bc_c():

  def __init__(self):
    return

  def left(self,t,q):
    return q[-1]

  def right(self,t,q):
    return q[0]

  def eval(self,t,q):
    return self.left(t,q), self.right(t,q)

  def __call__(self,t,q):
    return self.eval(t,q)

# flux function for advection
class flux_c():

  def __init__(self,u):
    self.u = u
    return

  def eval(self,ql,qr):
    if self.u > 0.0e+00:
      return self.u*ql
    else:
      return self.u*qr

  def __call__(self,q):
    return self.eval(q)

# print data
def print_state(time,xl,xc,xr,u,fd=sys.stdout):
  for wi in numpy.nditer( [ xl, xc, xr, u, ]):
    print(' {:15.7e}'.format(time),end='',file=fd)
    for wij in wi:
      print(' {:15.7e}'.format(wij),end='',file=fd)
    print(file=fd)
  print(file=fd)
  #print(file=fd)
  return

#
# functions to advance the state
#

def update_upw(t,u0,a,dx,dt,bc,flux):
  ul, ur = bc.eval(t)
  u = u0
  return u

def update_lxf(t,q0,a,dx,dt,bc,flux):
  # number of cells
  nc = q0.shape[0]
  # get boundary conditions
  ql, qr = bc.eval(t,q0)
  # evaluate scale constant
  k = dt/dx/2.0e+00
  # allocate variable for updated state
  q = q0
  # left border cell
  q[0] = 0.5*(ql + q0[1]) - k*(flux.eval(q0[0],q0[1]) - flux.eval(ql,q[0]))
  # right border cell
  q[-1] = 0.5*(qr + q0[-2]) - k*(flux.eval(q0[-1],qr) - flux.eval(q0[-2],q[-1]))
  # inner cells
  for i in range(1,nc-1,1):
    q[i] = 0.5*(q0[i+1] + q0[i-1]) - k*(flux.eval(q0[i],q0[i+1]) - flux.eval(q0[i-1],q0[i]))
  return q

def update_lxw(t,u0,a,dx,dt,bc,flux):
  ul, ur = bc.eval(t)
  u = u0
  return u

def update_bmw(t,u0,a,dx,dt,bc,flux):
  ul, ur = bc.eval(t)
  u = u0
  return u
  
list_funcs = { 'upw':update_upw,
               'lxf': update_lxf,
               'lxw': update_lxw,
               'bmw': update_bmw,
               }

# driver function
def main():
  
  # define equation parameters
  a = 1.0e+00

  # set number of cells
  ncell = 80

  # define domain limits
  xlo = 0.0e+00
  xhi = 1.0e+00

  # set cell size
  dx = (xhi - xlo)/ncell

  # define grid
  xl = dx*numpy.array(range(ncell))
  xr = xl + dx
  xc = 0.5*(xl + xr)
  
  # define initial conditions
  q0 = initial_state(xc)
  q = q0

  # integration parameters
  num_steps = 100
  time_begin = 0.0e+00
  dt = 3.0e-03

  # CFL condition
  cfl = a*dt/dx
  print('# clf = {:15.7e}'.format(cfl))

  # define boundary conditions
  func_bc = bc_c()

  # define flux function
  func_flux = flux_c(a)

  # select method
  name_model = 'lxf'
  func_update = list_funcs[name_model]

  # initialize plot
  fig = plt.figure()
  l, = plt.plot( [], [], )
  plt.xlim(0.0,1.0)
  plt.ylim(0.0,1.0)

  FFMpegWriter = manimation.writers['ffmpeg']
  metadata = dict(title='Advection of a square pulse', artist='Manuel Cotelo Ferreiro',
                  comment='Using Lax-Friedrichs method')
  writer = FFMpegWriter(fps=15, metadata=metadata)

  # start recording movie
  with writer.saving(fig, 'adv.mp4', num_steps+1):
    # integration loop
    time = time_begin
    for i in range(num_steps):
      # print current state
      #print_state(time,xl,xc,xr,q)
      # add frame to movie
      l.set_data(xc,q)
      writer.grab_frame()
      # update state
      q0 = q
      q  = func_update(time,q0,a,dx,dt,func_bc,func_flux)
      # update time
      time = time + dt
  
    # print last time update
    #print_state(time,xl,xc,xr,q)
    # add frame to movie
    l.set_data(xc,q)
    writer.grab_frame()
    
  return

if __name__=='__main__':
  sys.exit(main())
