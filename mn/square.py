
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Square function
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.10.03 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

# declare public objects of this module
__all__ = [ 'info', 'square_c', 'wsquare_c', 'wsquare_pbc_c', ]

# program info
info = { 'description': 'Square function',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }


class square_c():

  '''
  Define a square
  '''

  # define some constants
  pos_lo = -1
  pos_hi =  1
    
  # area of a triangle
  func_trg = lambda x, y: 0.5e+00*numpy.abs(x[0]*y[1] - x[1]*y[0])

  def __init__(self,lo,hi):
    '''
    Initialize object

    -> in
    lo: ower corner
    hi: upper corner
    '''
    
    self.lo = numpy.array(lo)
    self.hi = numpy.array(hi)

    self.dx = self.hi - self.lo

    self.area = self.dx[0]*self.dx[1]
  
    return

  @staticmethod
  def build(x1,x2):
    '''
    Create object from the coordinates of two opposite corners

    -> in
    x1: first corner coordinates
    x2: opposite corner coordinates
    '''

    lo = []
    hi = []

    for a, b in zip(x1,x2):
      lo.append(min(a,b))
      hi.append(max(a,b))
    
    return square_c(lo,hi)
  
  @staticmethod
  def _sanitize(x,lo=0.0,hi=1.0,tol=1.0e-15):

    l = numpy.abs(hi - lo)
  
    if l != 0.0e+00:

      xs = [] 

      for xi in x:

        rl = numpy.abs(xi-lo)/l
        rh = numpy.abs(xi-hi)/l
      
        if rl < tol:
          xs.append(lo)
        elif rh < tol:
          xs.append(hi)
        else:
          xs.append(xi)

      return numpy.array(xs)

    else:

      return x

  @staticmethod
  def _select_side(x,lo=0.0,hi=1.0):
    s = ''
    if x[0] == lo:
      s = 'l'
    elif x[0] == hi:
      s = 'r'
    elif x[1] == lo:
      s = 'b'
    elif x[1] == hi:
      s = 't'
    return s

  def inside(self,x):
    '''
    Check if point is inside square, incude boundary

    -> in
    x: point coordinates

    <- out 
    1) bool variable, True if the point is inside the square
    '''
    
    return (self.lo[0]<=x[0] and self.hi[0]>=x[0]) and (self.lo[1]<=x[1] and self.hi[1]>=x[1])

  def section(self,x1,x2):
    '''
    Divide the square in two by a segment defined by two points and return the 
    area of the square above that segment

    -> in
    x1: first point of the segment
    x2: ssecond point fo the segment

    <- out
    1) area of the square over the segment
    '''

    class _store_c():
      pass
    
    # get target points
    ps = []
   
    delta = x2 - x1

    if ( delta[0] != 0.0 ):
      
      x = _store_c()
      x.l = (self.lo[0] - x1[0])/delta[0]
      x.pos = [ self.lo[0], x1[1] + x.l*delta[1], ]
      ps.append(x)
    
      x = _store_c()
      x.l = (self.hi[0] - x1[0])/delta[0]
      x.pos = [ self.hi[0], x1[1] + x.l*delta[1], ]
      ps.append(x) 
    
    if ( delta[1] != 0.0 ):

      x = _store_c()
      x.l = (self.lo[1] - x1[1])/delta[1]
      x.pos = [ x1[0] + x.l*delta[0], self.lo[1], ]
      ps.append(x) 
    
      x = _store_c()
      x.l = (self.hi[1] - x1[1])/delta[1]
      x.pos = [ x1[0] + x.l*delta[0], self.hi[1], ]
      ps.append(x)
    
    # order points from left to right
    ps = sorted(ps,key=lambda x: x.l)

    # take the points in the middle
    if len(ps) == 4:
      ps = [ ps[1], ps[2], ]

    # get parameters and sanitize
    ps[0].par = square_c._sanitize((ps[0].pos - self.lo)/self.dx)
    ps[1].par = square_c._sanitize((ps[1].pos - self.lo)/self.dx)

    ps[0].side = square_c._select_side(ps[0].par) 
    ps[1].side = square_c._select_side(ps[1].par) 

    
    case = ps[0].side + ps[1].side
    
    # get area
    w = 0.0e+00

    if   case == 'lb' or case == 'bl':
      w = 1.0e+00 - square_c.func_trg(ps[0].par,ps[1].par)
    elif case == 'lr' or case == 'rl':
      w = 1.0e+00 - 0.5*(ps[0].par[1] + ps[1].par[1])
    elif case == 'lt' or case == 'tl':
      u = ps[0].par - numpy.array( [ 0.0, 1.0, ])
      v = ps[1].par - numpy.array( [ 0.0, 1.0, ])
      w = square_c.func_trg(u,v)
    elif case == 'tb' or case == 'bt':
      w = 0.5*(ps[0].par[0] + ps[1].par[0])
      if ps[0].par[0] < ps[1].par[0]:
        if ps[0].par[1] > 0.5:
          w = 1.0e+00 - w
      else:
        if ps[0].par[1] < 0.5:
          w = 1.0e+00 - w
    elif case == 'br' or case == 'rb':
      u = ps[0].par - numpy.array( [ 1.0, 0.0, ])
      v = ps[1].par - numpy.array( [ 1.0, 0.0, ])
      w = 1.0e+00 - square_c.func_trg(u,v)
    elif case == 'tr' or case == 'rt':
      u = ps[0].par - numpy.array( [ 1.0, 1.0, ])
      v = ps[1].par - numpy.array( [ 1.0, 1.0, ])
      w = square_c.func_trg(u,v)
    
    return w*self.area


class wsquare_c():

  '''
  Square wave function
  '''

  def __init__(self,width,center=0.0e+00,value_lo=0.0e+00,value_hi=1.0e+00,u=0.0e+00):
    '''
    Object initialization

    -> in
    width: width of the square function
    center: position of the center of the square function
    value_lo: function low value
    value_hi: function high value
    u: speed
    '''

    # store parameters
    self.wh = width/2.0e+00
    self.c = center
    self.u = u
    self.fl = value_lo
    self.fh = value_hi
    
    # define imits of the square for t = 0
    self.pl = self.c - self.wh
    self.pr = self.c + self.wh
    
    # function vectorization
    self.func_eval = numpy.vectorize(self._func_eval)

    return

  def to_ref(self,x,t):
    '''
    Coordinate change, x0 = x - u*t
    '''
    
    return x - self.u*t

  def from_ref(self,x0,t):
    '''
    Coordinate change, x = x0 + u*t
    '''

    return x0 + self.u*t
  
  def pos(self,t):
    '''
    Position of the square at time t
    '''

    return self.from_ref(self.pl,t), self.from_ref(self.pr,t)
  
  def _func_eval(self,x,t):
    '''
    Function evaluation for a single (x,t) point
    '''

    # length from point to the center of the square
    l = numpy.abs(self.to_ref(x,t) - self.c)
    # check if point is inside the square
    if l < self.wh:
      return self.fh
    else:
      return self.fl

  def eval(self,x,t):
    return self.func_eval(x,t)

  def __call__(self,x,t):
    return self.eval(x,t)

  def integx(self,x1,x2,t):
    '''
    Integrate square function for fixed time
    '''
    
    ans = 0.0e+00

    if x1 != x2:
      p1 = self.to_ref(min(x1,x2),t)
      p2 = self.to_ref(max(x1,x2),t)

      q1, q2 = self.pos(t)

      w = 0.0e+00
      if p2>q1 and p1<q2:
        r1 = max(p1,q1)
        r2 = min(p2,q2)
        w = (r2 - r1)/numpy.abs(p2 - p1)

      ans = w*self.fh + (1.0e+00 - w)*self.fl
      if x1 > x2:
        ans = -ans
    
    return ans

  def integt(self,x,t1,t2):
    '''
    Integrate square function for fixed position. 

    Convert time domain into space domain using the relation: x - u*t = K
    '''
    x1 = self.to_ref(x,t1)
    x2 = self.to_ref(x,t2)
    return self.integ(x1,x2,0.0e+00)

  def integ(self,x1,x2,t1,t2):

    # build a square with the integration domain
    sq = square_c([x1,t1],[x2,t2])

    # get intersection points of the limit of the square wave with the 
    # integration domain
    p11, p12 = self.pos(t1) # position of square for t1
    p21, p22 = self.pos(t2) # position of square for t2

    # get areas above the segments produces by the square limits
    au1 = sq.section( [ p11, t1, ], [ p21, t2, ], )
    au2 = sq.section( [ p12, t1, ], [ p22, t2, ], )

    # integration
    w = numpy.abs(au1-au2)/sq.area
    ans = (w*self.fh + (1.0-w)*self.fl)

    return ans


class wsquare_pbc_c():
  
  '''
  Square wave function with periodic boundary conditions
  '''

  def __init__(self,width,center=0.0e+00,value_lo=0.0e+00,value_hi=1.0e+00,u=0.0e+00,lo=0.0e+00,hi=1.0e+00):
    '''
    Object initialization
    '''

    # build common square function
    self.sq = square_c(width,center,value_lo,value_hi,u)

    # limits of the periodic domain
    self.lo = min(lo,hi)
    self.hi = max(lo,hi)

    # check parameters
    assert( self.lo < self.hi )
    
    # length of the domain
    self.l = self.hi - self.lo

    return

  def to_frame(self,x):
    return self.lo + numpy.remainder(x-self.lo,self.l)

  def eval(self,x,t):
    # position from reference
    x0 = self.sq.to_ref(x,t)
    # position respect to the domain of periodicity
    xl = self.to_frame(x0)
    return self.sq.eval(xl,0.0e+00)

  def integx(self,x1,x2,t):
    
    q1 = self.sq.to_ref(x1,t)
    q2 = self.sq.to_ref(x2,t)

    p1 = self.to_frame(q1)
    p2 = self.to_frame(q2)

    return self.sq.integx(p1,p2,0.0e+00)

  def integt(self,x,t1,t2):
    raise 'not implemented !!!'
    return 

  def integ(self,x1,x2,t1,t2):
    raise 'not implemented !!!'
    return



def _print_func(x_size,t_size,x,fxt,fd=sys.stdout):
  for i in range(x_size):
    print(' {:11.3e}'.format(x[i]),end='',file=fd)
    for j in range(t_size):
      print(' {:11.3e}'.format(fxt[i,j]),end='',file=fd)
    print(file=fd)
  return


def test_000(num_sample=400):
  
  lo = numpy.array([ 0.0, 0.0, ])
  hi = numpy.array([ 1.0, 1.0, ])
  
  sq = square_c.build(lo,hi)

  p_ref = sq.lo
  ang = numpy.linspace(0.0,2.0e+00*numpy.pi,num_sample)

  for i, ai in numpy.ndenumerate( [ ang, ]):
    pi = p_ref + numpy.array([ numpy.cos(ai), numpy.sin(ai), ])

    s = sq.section(p_ref,pi)

    print(' {:5d} {:15.7e} {:15.7e}'.format(i[1],ai,s))
  print()

  return


def test_001(num_sample=400):
  
  lo = numpy.array([ 0.0, 0.0, ])
  hi = numpy.array([ 1.0, 1.0, ])
  
  sq = square_c.build(lo,hi)

  p_ref = numpy.array([ 0.5, 0.0, ])
  ang = numpy.linspace(0.0,2.0e+00*numpy.pi,num_sample)

  for i, ai in numpy.ndenumerate( [ ang, ]):
    pi = p_ref + numpy.array([ numpy.cos(ai), numpy.sin(ai), ])

    s = sq.section(p_ref,pi)

    print(' {:5d} {:15.7e} {:15.7e}'.format(i[1],ai,s))
  print()

  return


def test_002(num_sample=400):
  
  lo = numpy.array([ 0.0, 0.0, ])
  hi = numpy.array([ 1.0, 1.0, ])
  
  sq = square_c.build(lo,hi)

  p_ref = 0.5*(sq.lo + sq.hi)
  ang = numpy.linspace(0.0,2.0e+00*numpy.pi,num_sample)

  for i, ai in numpy.ndenumerate( [ ang, ]):
    pi = p_ref + numpy.array([ numpy.cos(ai), numpy.sin(ai), ])

    s = sq.section(p_ref,pi)

    print(' {:5d} {:15.7e} {:15.7e}'.format(i[1],ai,s))
  print()

  return


def test_100():

  l = 1.0e+00

  # define space grid
  x_lo = -l
  x_hi =  l
  x_size = 200

  x = numpy.linspace(x_lo,x_hi,x_size)

  # defien time steps
  t_lo = 0.0e+00
  t_hi = 1.0e+00
  t_size = 40

  t = numpy.linspace(t_lo,t_hi,t_size)

  # create a 2D grid
  xm, tm = numpy.meshgrid(x,t,indexing='ij')

  # build squares
  width = l/3.0e+00
  center = 0.0e+00
  vel = 2.0e+00
  list_func = [ ( wsquare_c(width,center,u=vel), 'square', ),
                ( wsquare_pbc_c(width,center,u=vel,lo=x_lo,hi=x_hi), 'square_pbc', ),
                ]

  # evaluate functions
  for fi, name in list_func:
    
    # get values
    fxt = fi.eval(xm,tm)
    
    # print results
    _print_func(x_size,t_size,x,fxt,open('{}.txt'.format(name),'w'))

  return


def main():
  
  num_sample = 1000
  test_000(num_sample)
  test_001(num_sample)
  test_002(num_sample)

  #test_100()

  return

if __name__=='__main__':
  sys.exit(main())
