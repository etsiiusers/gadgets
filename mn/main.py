
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Some numeric methods for ODE's. Solving this equation:
#
# du/dt = u , with initial conditions u(t0) = u0
#
# The exact solutuion is u(t) = u0*exp(t-t0)
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.11 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

import ode

# program info
info = { 'description': 'Some numerical methods for ODE\'s',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }

# analytical solution to the ODE, use a functor (class that behaves like a 
# class and like a function)
class func_exp():
  
  def __init__(self,u0,t0):
    self.u0 = u0
    self.t0 = t0
    return

  def eval(self,t):
    return self.u0*numpy.exp(t - self.t0)

  def __call__(self,t):
    return self.eval(t)


# function to evaluate the rates
def rates(t,u):
  return u

# driver function
def main():
  
  # set initial conditions
  t0 = 0.0e+00
  u0 = 1.0e+00

  # create function to evaluate exact solution
  func_exact = func_exp(u0,t0)

  # define methods
  list_funcs = [ ode.euler_explicit(rates),
                 ode.euler_implicit(rates),
                 ode.euler_midpoint(rates),
                 ode.pece(rates,2),
                 ode.pece(rates,3),
                 ode.pece(rates,4),
                 ]
  
  # add initial state to the list of states
  s_init = { 'time': t0,
             'value': u0,
             'up': [],
             }
  for fi in list_funcs:
    s_init['up'].append(u0)

  # create state variables
  s = [ s_init, ]

  # set numerical parameters
  num_steps = 80
  t_delta = 5.0e-01

  # integration
  for i in range(num_steps):

    # set initial time
    t_begin = s[-1]['time']
    t_end   = t_begin + t_delta 

    # create new advanced state
    si = { 'time': t_end, 
           'value': func_exact(t_end),
           'up': [],
           }
    
    for fi, ui in zip(list_funcs,s[-1]['up']):
      si['up'].append(fi(ui,t_begin,t_delta))

    # append updated state to the list of states
    s.append(si)

  # print results
  for si in s:
    print(' {time:15.7e} {value:15.7e}'.format(** si),end='')
    for ui in si['up']:
      print(' {:15.7e}'.format(ui),end='')
    print()

  return

main()
