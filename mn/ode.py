
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Some numeric methods for ODE's.
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.12 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - split file, just keeep ODE integrators
# 2019.09.11 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy
import scipy.optimize

# program info
info = { 'description': 'Some numerical methods for ODE\'s',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }

# base class for ODE solver
class _base():

  def __init__(self,func_rates):
    self.func_rates = func_rates
    return

  def rates(self,t,u):
    return self.func_rates(t,u)
  
  def update(self,u0,t0,t_delta):
    raise 'not implemented!!!'
    return numpy.nan

  def __call__(self,u0,t0,t_delta):
    return self.update(u0,t0,t_delta)

# euler method
class euler(_base):
  
  def __init__(self,func_rates,weight):
    _base.__init__(self,func_rates)
    assert( weight >= 0.0e+00 and weight <= 1.0e+00 )
    self.w1 = weight
    self.w2 = 1.0e+00 - self.w1
    return

  def residual(self,t0,u0,t,u):
    w = (u - u0) - (t - t0)*(self.w1*self.rates(t0,u0) + self.w2*self.rates(t,u))
    if ( u0 != 0.0 ):
      w = w/u0
    return w

  def update(self,u0,t0,t_delta):
    func_eval = lambda u: self.residual(t0,u0,t0+t_delta,u)
    u_guess = u0 + self.rates(t0,u0)*t_delta
    return scipy.optimize.newton(func_eval,x0=u_guess,maxiter=200)

# explicit euler method
class euler_explicit(euler):
  
  def __init__(self,func_rates):
    euler.__init__(self,func_rates,1.0e+00)
    return
  
  def update(self,u0,t0,t_delta):
    return u0 + self.rates(t0,u0)*t_delta

# implicit euler method
class euler_implicit(euler):
  
  def __init__(self,func_rates):
    euler.__init__(self,func_rates,0.0e+00)
    return

# midpoint rule
class euler_midpoint(euler):
  
  def __init__(self,func_rates):
    euler.__init__(self,func_rates,0.5e+00)
    return

# PE(CE)^k method
class pece(_base):

  def __init__(self,func_rates,k):
    _base.__init__(self,func_rates)
    self.k = k
    assert( self.k > 0 )
    self.update_init = euler_explicit(self.func_rates)
    return

  def update(self,u0,t0,t_delta):
    t1 = t0
    t2 = t0 + t_delta

    u = self.update_init(u0,t0,t_delta)
    for i in range(self.k):
      u = u0 + 0.5*t_delta*(self.rates(t1,u0) + self.rates(t2,u))

    return u

if __name__=='__main__':
  print("This file is not intended for standalone run.")
  sys.exit()
