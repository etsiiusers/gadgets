
import os
import sys

import numpy

import deriv

#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Some numeric methods for root finding.
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.13 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

# program info
info = { 'description': 'Some numerical methods for ODE\'s',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }

class solver():

  def __init__(self,func,tol=1.0e-09,iter_max=100):
    self.func = func
    self.tol = numpy.abs(tol)
    self.iter_max = iter_max
    return

  def bisec(self,xl,xr):
   
    # initialize variables
    x1 = xl
    x2 = xr

    f1 = self.func(x1)
    f2 = self.func(x2)
    
    if ( f1*f2 <= 0.0e+00 ):
      
      for i in range(self.iter_max):
        
        # evaluate new point in interval
        x = 0.5*(x1 + x2)
        f = self.func(x)
        
        # update solution
        if f*f1 <= 0.0e+00:
          f2 = f
          x2 = x
        else:
          f1 = f
          x1 = x
        
        # residual control
        res = 2.0*numpy.abs(x1-x2)/numpy.abs(x1+x2)
        if res < self.tol:
          break

      return 0.5*(x1+x2)
    else:
      print('impossible to find root, fl*fr > 0.0')
      return numpy.nan

  def newton(self,x):
    
    # create numerical derivatives evaluator
    dfunc = deriv.func_deriv(self.func,1.0e-02,3)

    # initialize variables
    x1 = x
    x2 = x
    w = self.func(x1)

    for i in range(self.iter_max):
      
      # save previous solution
      x1 = x2

      # evaluate numerical derivative
      df, res = dfunc.eval_derv(x)

      # update root
      if df[0] != 0.0:
        x2 = x1 - w/df[0]
      else:
        x2 = 0.5*(x1 + x2)
      
      # check convergence criteria
      w = self.func(x2)
      if ( numpy.abs(w) < self.tol ):
        break

    return x2

def test():

  func = lambda x: x - numpy.cos(x)

  fzero = solver(func)

  xn = fzero.newton(0.0e+00)
  xb = fzero.bisec(0.0e+00,numpy.pi/2.0e+00)

  print('# x = {:15.7e}'.format(xn))
  print('# res = {:15.7e}'.format(func(xn)))
  print('# x = {:15.7e}'.format(xb))
  print('# res = {:15.7e}'.format(func(xb)))

  return

if __name__=='__main__':
  sys.exit(test())
