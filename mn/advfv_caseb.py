
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Numeric methods for advection equation using finite volume discretizations
#
# u = u(x,t)
#
# du/dt + a*du/dx = 0, x in [0,1]
#
# Initial conditions:
# u(x,0) = "square function"
#
# Periodic boundary conditions
# u(0,t) = u(1,t)
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.17 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

# program info
info = { 'description': 'Numeric methods for advection equation using finite volumes',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }

# function to define initial conditions
def initial_state(xl,xr,value_lo=0.0,value_hi=1.0):
  '''
  Initial conditions, in this case u(x,0) = x
  '''
  q =  numpy.zeros(xl.shape)

  l = xr[-1] - xl[0]
  l = l/3.0
  xc = 0.5*(xl[0] + xr[-1])
  lo = xc - l/2.0
  hi = xc + l/2.0

  for i in range(q.shape[0]):
    
    li = xl[i]
    ri = xr[i]
    
    u = lo - li
    v = hi - ri

    wl = u*(lo - ri)
    wh = (hi - li)*v

    if ( wl < 0.0 and wh < 0.0 ):
      z = (ri - li)/l
      q[i] = z*value_hi + (1.0 - z)*value_lo
    elif wl < 0.0:
      z = (lo - li)/l
      q[i] = z*value_hi + (1.0 - z)*value_lo
    elif wh < 0.0:
      z = (ri - hi)/l
      q[i] = z*value_hi + (1.0 - z)*value_lo
    else:
      if u > 0.0 or v < 0.0:
        q[i] = value_lo
      else:
        q[i] = value_hi

  return q

# functor for boundary conditions
class bc_c():

  def __init__(self):
    return

  def left(self,t,q):
    return q[-1]

  def right(self,t,q):
    return q[0]

  def eval(self,t,q):
    return self.left(t,q), self.right(t,q)

  def __call__(self,t,q):
    return self.eval(t,q)

# flux function for advection
class flux_c():

  def __init__(self,u):
    self.u = u
    return

  def eval(self,ql,qr):
    if self.u > 0.0e+00:
      return self.u*ql
    else:
      return self.u*qr

  def __call__(self,q):
    return self.eval(q)

# print data
def print_state(time,xl,xc,xr,u,fd=sys.stdout):
  for wi in numpy.nditer( [ xl, xc, xr, u, ]):
    print(' {:15.7e}'.format(time),end='',file=fd)
    for wij in wi:
      print(' {:15.7e}'.format(wij),end='',file=fd)
    print(file=fd)
  print(file=fd)
  #print(file=fd)
  return

#
# functions to advance the state
#

def update_upw_cell(k,ql,qc,qr,flux):
  '''
  Update cell value using Upwind method

  -> in
  ql: left cell state, i-1
  qc: current cell state, i
  qr: right cell state, i+1
  flux: flux function based on values at both sides, F_{i-1/2) = F(Q_{i-1},Q_{i})

  <- out
  1) updated cell state
  '''
  
  return qc - k*(flux.eval(qc,qr) - flux.eval(ql,qc))

def update_upw(t,q0,a,dx,dt,bc,flux):
  '''
  Update state using the Upwind method, assume uniform mesh

  -> in
  t: time
  q0: current state
  a: advection velocity
  dx: cell size
  dt: time step
  bc: boundary conditions function
  flux: flux function, F_{i-1/2} = F(Q_{i-1},Q_{i})

  <- out
  1) updated state
  '''

  # number of cells
  nc = q0.shape[0]
  # get boundary conditions
  ql, qr = bc.eval(t,q0)
  # evaluate scale constant
  k = dt/dx
  # allocate variable for updated state
  q = q0
  # left border cell
  q[0] = update_upw_cell(k,ql,q0[0],q0[1],flux)
  # right border cell
  q[-1] = update_upw_cell(k,q0[-2],q0[-1],qr,flux)
  # inner cells
  for i in range(1,nc-1,1):
    q[i] = update_upw_cell(k,q0[i-1],q0[i],q0[i+1],flux)
  return q

def update_lxf_cell(k,ql,qc,qr,flux):
  '''
  Update cell value using Lax-Friedrichs method

  -> in
  ql: left cell state, i-1
  qc: current cell state, i
  qr: right cell state, i+1
  flux: flux function based on values at both sides, F_{i-1/2) = F(Q_{i-1},Q_{i})

  <- out
  1) updated cell state
  '''
  
  return 0.5*(ql + qr) - k*(flux.eval(qc,qr) - flux.eval(ql,qc))

def update_lxf(t,q0,a,dx,dt,bc,flux):
  '''
  Update state using the Lax-Friedrichs method, assume uniform mesh

  -> in
  t: time
  q0: current state
  a: advection velocity
  dx: cell size
  dt: time step
  bc: boundary conditions function
  flux: flux function, F_{i-1/2} = F(Q_{i-1},Q_{i})

  <- out
  1) updated state
  '''

  # number of cells
  nc = q0.shape[0]
  # get boundary conditions
  ql, qr = bc.eval(t,q0)
  # evaluate scale constant
  k = dt/dx/2.0e+00
  # allocate variable for updated state
  q = q0
  # left border cell
  q[0] = update_lxf_cell(k,ql,q0[0],q0[1],flux)
  # right border cell
  q[-1] = update_lxf_cell(k,q0[-2],q0[-1],qr,flux)
  # inner cells
  for i in range(1,nc-1,1):
    q[i] = update_lxf_cell(k,q0[i-1],q0[i],q0[i+1],flux)
  return q

def update_lxw(t,u0,a,dx,dt,bc,flux):
  ul, ur = bc.eval(t)
  u = u0
  return u

def update_bmw(t,u0,a,dx,dt,bc,flux):
  ul, ur = bc.eval(t)
  u = u0
  return u
  
list_funcs = { 'upw':update_upw,
               'lxf': update_lxf,
               'lxw': update_lxw,
               'bmw': update_bmw,
               }

# driver function
def main():
  
  # define equation parameters
  a = 1.0e00

  # set number of cells
  ncell = 160

  # define domain limits
  xlo = 0.0e+00
  xhi = 1.0e+00

  # set cell size
  dx = (xhi - xlo)/ncell

  xl = dx*numpy.array(range(ncell))
  xr = xl + dx
  xc = 0.5*(xl + xr)
  
  # define initial conditions
  q0 = initial_state(xl,xr)
  q = q0

  # cfl condition
  cfl_target = 0.95

  # integration parameters
  time_begin = 0.0e+00
  time_end   = (xhi - xlo)/a
  time_delta = time_end - time_begin

  dt = cfl_target*(0.5*dx/a)
  
  num_steps = int(numpy.ceil(time_delta/dt)) 
  
  # change a bit the dt, then the last frame should overlap with initial 
  # conditions
  dt = time_delta/num_steps

  # define boundary conditions
  func_bc = bc_c()

  # define flux function
  func_flux = flux_c(a)

  # select method
  name_model = 'upw'
  #name_model = 'lxf'
  func_update = list_funcs[name_model]

  # integration loop
  time = time_begin
  for i in range(num_steps):
    # print current state
    print_state(time,xl,xc,xr,q)
    # update state
    q0 = q
    q  = func_update(time,q0,a,dx,dt,func_bc,func_flux)
    # update time
    time = time + dt
  
  # print last time update
  print_state(time,xl,xc,xr,q)
    
  return


main()
