
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Numeric methods for advection equation using finite volume discretizations
#
# u = u(x,t)
#
# du/dt + a*du/dx = 0, x in [0,1]
#
# Initial conditions:
# u(x,0) = 0.0 
#
# Boundary conditions:
# u(0,t) = gl(t) = exp(-(t-t0)^2/sigma^2/2)
# u(1,t) = gr(t) = 0.0
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.09.17 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

# program info
info = { 'description': 'Numeric methods for advection equation using finite volumes',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }

# function to define initial conditions
def initial_state(x):
  '''
  Initial conditions, in this case u(x,0) = x
  '''
  u =  numpy.zeros(x.shape)
  return u

# functor for boundary conditions
class bc_c():
  
  '''
  Object to evaluate boundary condition

  Gaussian function, f(t) = N(mu,sigma)
  '''

  def __init__(self):
    self.mu    = 3.0e+00
    self.sigma = 1.0e+00
    return

  def func(self,t):
    w = (t - self.mu)/self.sigma
    return numpy.exp(- w*w/2.0)

  def left(self,t):
    return self.func(t)

  def right(self,t):
    return 0.0e+00

  def eval(self,t):
    return self.left(t), self.right(t)

  def __call__(self,t):
    return self.eval(t)

# flux function for advection equation
class flux_c():

  def __init__(self,u):
    self.u = u
    return

  def eval(self,ql,qr):
    if self.u > 0.0e+00:
      return self.u*ql
    else:
      return self.u*qr

  def __call__(self,q):
    return self.eval(q)

# print data
def print_state(time,xl,xc,xr,u,fd=sys.stdout):
  for wi in numpy.nditer( [ xl, xc, xr, u, ]):
    print(' {:15.7e}'.format(time),end='',file=fd)
    for wij in wi:
      print(' {:15.7e}'.format(wij),end='',file=fd)
    print(file=fd)
  print(file=fd)
  #print(file=fd)
  return

#
# functions to advance the state
#

def update_upwind(t,u0,a,dx,dt,bc,flux):
  ul, ur = bc.eval(t)
  u = u0
  return u

def update_lxf(t,q0,a,dx,dt,bc,flux):
  # number of cells
  nc = q0.shape[0]
  # get boundary conditions
  ql, qr = bc.eval(t)
  # evaluate scale constant
  k = dt/dx/2.0e+00
  # allocate variable for updated state
  q = q0
  # left border cell
  q[0] = 0.5*(ql + q0[1]) - k*(flux.eval(q0[0],q0[1]) - flux.eval(ql,q[0]))
  # right border cell
  q[-1] = 0.5*(qr + q0[-2]) - k*(flux.eval(q0[-1],qr) - flux.eval(q0[-2],q[-1]))
  # inner cells
  for i in range(1,nc-1,1):
    q[i] = 0.5*(q0[i+1] + q0[i-1]) - k*(flux.eval(q0[i],q0[i+1]) - flux.eval(q0[i-1],q0[i]))
  return q

def update_lxw(t,u0,a,dx,dt,bc,flux):
  ul, ur = bc.eval(t)
  u = u0
  return u

def update_bw(t,u0,a,dx,dt,bc,flux):
  ul, ur = bc.eval(t)
  u = u0
  return u
  
list_funcs = { 'upwind':update_upwind,
               'lxf': update_lxf,
               'lxw': update_lxw,
               'beam-warming': update_bw,
               }

# driver function
def main():
  
  # define equation parameters
  a = 1.0e-01

  # set number of cells
  ncell = 50

  # define domain limits
  xlo = 0.0e+00
  xhi = 1.0e+00

  # set cell size
  dx = (xhi - xlo)/ncell

  xl = dx*numpy.array(range(ncell))
  xr = xl + dx
  xc = 0.5*(xl + xr)
  
  # define initial conditions
  u0 = initial_state(xc)
  u = u0

  # integration parameters
  num_steps = 100
  time_begin = 0.0e+00
  dt = 1.0e-01

  # define boundary conditions
  func_bc = bc_c()

  # define flux function
  func_flux = flux_c(a)

  # select method
  name_model = 'lxf'
  func_update = list_funcs[name_model]

  # integration loop
  time = time_begin
  for i in range(num_steps):
    # print current state
    print_state(time,xl,xc,xr,u)
    # update state
    u0 = u
    u  = func_update(time,u0,a,dx,dt,func_bc,func_flux)
    # update time
    time = time + dt
  
  # print last time update
  print_state(time,xl,xc,xr,u)
    
  return


main()
