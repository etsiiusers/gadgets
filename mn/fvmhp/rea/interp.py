
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Interpolation for Finite Volume Methods
#
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.10.02 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

import rea.interp as interp

# program info
info = { 'description': 'Interpolation for Finite Volume Methods',
         'author': 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)',
         'license': 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
         }


# base class for interpolators

class interp_base_c():
  '''
  Base abstract class for interpolators

  This functor interpolates the values of the M state variables at the given 
  points in 1D.
  '''

  # set name of the interpolator
  name = 'undef'

  def __init__(self,x,fx):
    '''
    Constructor

    -> in
    x: grid points ( N+1, )
    fx: cell avreages ( N, M, )
    '''
    self.x  = x
    self.fx = fx
    return

  def __call__(self,x):
    '''
    Evaluate interpolation

    -> in
    x: array of points, ( P, )

    <- out
    1) array with interpolated values in points, ( P, M, )
    '''
    return self.eval(x)

  def inside(self,x):
    '''
    Check if point is inside the grid

    -> in
    x: point

    <- out:
    1) True if point is inside the domain (including boundaries), False 
       othercase
    '''
    raise 'Not implemented!!!'

  def eval(self,x):
    '''
    Evaluation of the interpolation

    -> in
    x: array of points

    <- out
    1) array of inteprolated values
    '''
    raise 'Not implemented!!!'

#
# interpolators
#

class interp_pw_c(interp_base_c):
  
  name = 'piecewise'

  def __init__(self,x,fx):
    super().__init__(x,fx)
    return

  def eval(self,x):
    return

class interp_linear_c(interp_base_c):

  name = 'linear'

  def __init__(self,x,fx):
    super().__init__(x,fx)
    return

  def eval(self,x):
    return

class interp_quad_c(interp_base_c):
  
  name = 'quadratic'

  def __init__(self,x,fx):
    super().__init__(x,fx)
    return

  def eval(self,x):
    return

class interp_ppm_c(interp_base_c):

  name = 'ppm'

  def __init__(self,x,fx):
    super().__init__(x,fx)
    return

  def eval(self,x):
    return

#
# interpolator selector
#

list_func_interp = { interp_pw_c.name: interp_pw_c,
                     interp_linear_c.name: interp_linear_c,
                     interp_quad_c.name: interp_quad_c,
                     interp_ppm_c.name: interp_ppm_c,
                     }

def build_interp(type,x,fx):
  try:
    return list_func_interp[type](x,fx)
  except KeyError:
    func = interp_pw_c(x,fx)
    print('# error :: interpolator type "{}" does not exist, using default "{}" interpolator',format(type,func.name))
    return func

# driver function
def main():
  return

if __name__=='__main__':
  sys.exit(main())
