
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Program to get data for the PVPC (Precio Voluntario para el 
# Pequeño Consumidor) from REE (Red Eléctrica de España)
#
# Copyright (C) 2022 Universidad Politécnica de Madrid
# Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
#
# Version control:
# 2022.10.20 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys
import argparse
import time
import datetime

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException, TimeoutException

import printer

# program info
info = argparse.Namespace(
  name = 'pvpc',
  desc = 'Get PVPC data from REE',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = '2022',
  version = [ 0, 1, 0, ],
  copyright = 'Copyright (C) 2022 Manuel Cotelo Ferreiro',
)

# program default parameters
defs = argparse.Namespace(
  date_init = '2020-06-01', # first available date
  date_end = f'{datetime.date.today()}', # today
  url = 'https://www.esios.ree.es/es/pvpc',
  verbose = 1, 
)

# define default printer
pr_empty = printer.build_empty()

# get elements from DOM
def get_elem_by( driver, locator, key, delay=2., ):
  #return WebDriverWait( driver, delay, ).until( ec.visibility_of_element_located( ( locator, key, ), ), )
  return driver.find_element( locator, key, )

def get_elem_by_xpath( driver, key, delay=2., ):
  return get_elem_by( driver, By.XPATH, key, delay, )

def get_elems_by( driver, locator, key, delay=2., ):
  return driver.find_elements( locator, key, )

def get_elems_by_xpath( driver, key, delay=2., ):
  return get_elems_by( driver, By.XPATH, key, delay, )

# define web driver options
def get_options( pr=pr_empty, ):
  
  _ = pr.child( 'set web driver options', )
  
  # driver options
  list_opts = [
    #'--headless', 
    '--disable-notifications',
    '--no-sandbox',
    '--verbose',
    '--mute-audio',
    #'--window-size=1920x1080',
    #'--disable-gpu',
    #'--disable-software-rasterizer',
  ]
  
  # user preferences
  list_prefs = { 
    'download.default_directory': os.getcwd(),
    'profile.default_content_settings.popups': 0,
    #'directory_upgrade': True,
    'download.prompt_for_download': False,
    'profile.default_content_setting_values.automatic_downloads': 2,
    #'download_restrictions': 1,
    'safebrowsing.enabled': False,
  }
  
  # create options object
  opts = Options()
  
  # load options
  for op in list_opts:
    opts.add_argument(op)
    
  # load preferences
  opts.add_experimental_option( 'prefs', list_prefs)
  
  return opts

# create web driver
def get_driver( pr=pr_empty, time_wait=10, ):
  _ = pr.child( 'open web driver', )
  driver = webdriver.Chrome( options=get_options( pr, ), )
  driver.implicitly_wait(time_wait)
  return driver

# navigate
def driver_to_url( driver, url, pr=pr_empty, ):
  _ = pr.child( 'get url', )
  driver.get( url, )
  return

def driver_to_menu( driver, pr=pr_empty, ):
  _ = pr.child( 'open menu', )
  get_elem_by_xpath( driver, '//*[@id="pvpcContent"]/div/div[1]/a[2]', 20, ).click()
  return

def driver_to_date( driver, the_date, pr=pr_empty, ):
 
  def navigate_to_year( driver, pr=pr_empty, ):
    _ = pr.child( 'navigate to year', )
    return get_elem_by( driver, By.CLASS_NAME, 'ui-datepicker-year', )
    
  def select_year( driver, the_date, pr=pr_empty, ):
    pr_child = pr.child( f'select year {the_date.year}', )
    Select( navigate_to_year( driver, pr_child, ), ).select_by_value( f'{the_date.year}', )
    return
  
  def navigate_to_month( driver, pr=pr_empty, ):
    return get_elem_by( driver, By.CLASS_NAME, 'ui-datepicker-month', )
    
  def select_month( driver, the_date, pr=pr_empty, ):
    pr_child = pr.child( f'select month {the_date.month}', )
    Select( navigate_to_month( driver, pr_child, ), ).select_by_value( f'{the_date.month-1}', ) 
    return
  
  def navigate_to_day( driver, day, pr=pr_empty, ):
    _ = pr.child( 'navigate to day', )
    for elem in get_elems_by( driver, By.TAG_NAME, 'td', ):
      if elem.get_attribute("data-handler") == 'selectDay':
        try:
          elem_child = get_elem_by( elem, By.TAG_NAME, 'a', )
          if int(elem_child.text) == day:
            return elem_child
        except ValueError:
          pass
    return None
  
  def select_day( driver, the_date, pr=pr_empty, ):
    pr_child = pr.child( f'select day {the_date.day}', )
    navigate_to_day( driver, the_date.day, pr_child, ).click()
    return

  pr_child = pr.child( f'get data for {the_date}', )
  
  print( driver.page_source, file=open( 'page.html', 'w', ), )

  select_year( driver, the_date, pr_child, )
  select_month( driver, the_date, pr_child, )
  select_day( driver, the_date, pr_child, )
    
  return

def count_files( path, filter=lambda: True, ):
  nf = 0
  for fi in os.listdir( path, ):
    if filter(fi):
      nf += 1
  return nf

def down( driver, the_date, pr=pr_empty, ):
  
  def navigate_to_down( driver, pr=pr_empty, ):
    _ = pr.child( 'navigate to download', )
    return get_elem_by_xpath( driver, '/html/body/div[5]/div[1]/div[2]/div/div[2]/div[3]/div/a', )
    '''
    for elem in get_elems_by( driver, By.TAG_NAME, 'td', ):
      if elem.text.tolower().strip() == 'descargar':
        return elem
    return None
    '''
  
  def wait_to_down( func, path, pr=pr_empty, num_tries=5, ):
    
    _ = pr.child( 'wait till download starts', )
    
    counter = lambda: count_files( path, filter=lambda file_name: file_name.endswith('.xls'), )
    
    nf_ref = counter()
    func()
    
    j = 0
    while counter() == nf_ref:
      time.sleep( 0.5, )
      j += 1
      if j ==  10:
        print( '\n# go on...', )
        break

    return

  pr_child = pr.child( f'download data for {the_date}', )
  
  #try:
  driver_to_date( driver, the_date, pr_child, )
  navigate_to_down( driver, pr_child, ).click()
  time.sleep( 5., )
  #elem = navigate_to_down( driver, pr_child, )
  #wait_to_down( lambda: elem.click(), './', pr_child, )
  #except:
  #  print( f'# impossible to download data for {the_date}', file=sys.stderr, )
  
  return

# select range of dates
def range_dates( date_init, date_end, pr=pr_empty, ):
  _ = pr.child( 'selecting dates', )
  ls = [ date_init, ]
  while ls[-1] < date_end:
    ls.append( ls[-1] + datetime.timedelta( days=1, ), )
  ls.pop()
  return ls

# read command line arguments
def get_args(info,defs):

  # create argument parser
  parser = argparse.ArgumentParser( description=f'{info.desc}', epilog=f'{info.copyright} ({info.email})', )
  
  # define optional arguments
  parser.add_argument( '--date_init', help='', default=defs.date_init, type=str, )
  parser.add_argument( '--date_end', help='', default=defs.date_end, type=str, )
  parser.add_argument( '--url', help='', default=defs.url, type=str, )
  parser.add_argument( '--verbose', help='', default=defs.verbose, type=int, )

  # get command line arguemnts
  args = parser.parse_args()
  
  # get dates as a datetime object
  args.date_init = datetime.date.fromisoformat(args.date_init)
  args.date_end = datetime.date.fromisoformat(args.date_end)
  
  # validations

  # swap values
  if args.date_init > args.date_end:
    args.date_init, args.date_end = args.date_end, args.date_init

  return args

# driver routine
def main(info,defs):
  
  # get command line arguments
  args = get_args(info,defs)

  # define info printer
  bl = printer.select_builder( args.verbose, )
  pr = bl( 'program starts', )
  
  # get web driver
  driver = get_driver( pr, )
 
  driver_to_url( driver, args.url, pr, )
  
  driver_to_menu( driver, pr, )
  
  for the_date in range_dates( args.date_init, args.date_end, pr, ):
    down( driver, the_date, pr, )
  
  return 0

if __name__=='__main__':
  sys.exit(main(info,defs))
