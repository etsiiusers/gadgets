
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
# Program to automatic download data from OMIE
#
# Copyright (C) 2022 Universidad Politécnica de Madrid
# Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
#
# Version control:
# 2022.10.25 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - add multiprocessing
# 2022.10.20 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys
import argparse
import datetime
import urllib.request
import urllib.parse
import multiprocessing
import itertools

import pandas

# program info
info = argparse.Namespace(
  name = 'omie',
  desc = 'Make request to OMIE server to obtain hourly electricity prizes',
  author = 'Manuel Cotelo Ferreiro',
  email = 'manuel.cotelo@upm.es',
  year = 2022,
  version = [ 1, 0, 0, ],
  copyright = 'Copyright (C) 2022 Universidad Politécnica de Madrid',
)

# program default parameters
defs = argparse.Namespace(
  url = 'https://www.omie.es/es/file-download',
  first = datetime.date(2022,1,1).isoformat(),
  last = datetime.date.today().isoformat(),
  num_proc = 4,
  verbose = 1, 
)

def get_args(info,defs):

  # create argument parser
  parser = argparse.ArgumentParser( description=f'{info.desc}', epilog=f'{info.copyright} ({info.email})', )
  
  # define optional arguments
  parser.add_argument( '--url', help=f'Base URL to make requests to OMIE, default "{defs.url}"', default=defs.url, type=str, )
  parser.add_argument( '--first', help=f'First date to retrieve data, default "{defs.first}"', default=defs.first, type=str, )
  parser.add_argument( '--last', help=f'Last date to retrieve data, default "{defs.last}"', default=defs.last, type=str, )
  parser.add_argument( '--num_proc', help=f'NBumber of process to use, default "{defs.num_proc}"', default=defs.num_proc, type=int, )
  parser.add_argument( '--verbose', help=f'Define level of verbosity, default "{defs.verbose}"', default=defs.verbose, type=int, )
  
  # read command line arguments
  args = parser.parse_args()
  
  # validation
  assert args.num_proc > 0, 'bad number of processes'
  
  return args 

def get_file_name(date):
  return f'marginalpdbc_{date.year:4d}{date.month:02d}{date.day:02d}.1'

def get_url_full(url,date):
  #url = 'https://www.omie.es/es/file-download?parents%5B0%5D=marginalpdbc&filename=marginalpdbc_20220427.1
  return f'{url}?parents%5B0%5D=marginalpdbc&filename={get_file_name(date)}'

# do job 
def down( url, the_date, verbose=1, ):
  if verbose > 0:
    print( f'# downloading data for {the_date.isoformat()} ... ', file=sys.stdout, )
    sys.stdout.flush()
  file_csv = f'{get_file_name(the_date)}.csv'
  try:
    urllib.request.urlretrieve( get_url_full( url, the_date, ), file_csv, )
  except FileNotFoundError:
    file_csv = ''
  return file_csv

# def create list of dates in range
def range_dates( date_begin, date_end, ):

  list_dates = []

  date_curr = date_begin
  while date_curr < date_end:
    # add date to list
    list_dates.append( date_curr, )
    # advance date to tomorrow
    date_curr = date_curr + datetime.timedelta(1)

  return list_dates 
  
# loop dates
def helper_get_file( params, ):
  return down( * params, )

def get_files( url, list_dates, num_proc = 1, ):
  pool = multiprocessing.Pool( num_proc, )
  return pool.map( helper_get_file, zip(itertools.repeat(url),list_dates,) )

# main program
def main(info,defs):
  
  # get command line arguments
  args = get_args(info,defs)

  # define range of dates
  date_first = datetime.date.fromisoformat(args.first)
  date_last  = datetime.date.fromisoformat(args.last)
  # swap dates
  if date_last < date_first:
    date_first, date_last = date_last, date_first

  # download files
  list_files = get_files( args.url, range_dates( date_first, date_last, ), args.num_proc, ) 

  # define table loader
  read_omie = lambda file_name: pandas.read_csv( file_name, index_col=None, header=None, skiprows=1, skipfooter=1, engine='python', sep=';', encoding='utf-8', )

  # join files
  frame = pandas.concat( [ read_omie( file_name, ) for file_name in list_files if file_name ], axis=0, ignore_index=True, )
  
  # write to file
  frame.to_csv( 'omie.csv', encoding='utf-8', index=False, sep=';', header=False, )

  return

# run script
if __name__ == '__main__':
  sys.exit(main(info,defs))
