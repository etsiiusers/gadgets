#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Example program of the bisection method to solve a non-linear equation
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
#
# Version control:
# 2019.02.23 :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

# program description
desc   = 'Solve a non-linear equation with the bisection method'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
eplg   = '{}, 2018'.format(author)

# example equation (15) and (16) from Phys. Plasmas 14, 043301 (2007), Drake et al.

# function to compute Q parameter
def q_parameter(us,r0):
  R = 8.314
  R4 = R*R*R*R
  sigma = 5.67e-08
  u5 = numpy.power(us,5.0)
  return 2.0*sigma*u5/r0/R4

class func_obj_c():
  
  '''
  Functor for function evaluation
  '''

  def __init__(self,q,gamma):
    '''
    Construct functor, save function parameters
    '''
    # parameters
    self.q = q
    self.gamma = gamma
    self.dx_min = 1.0e-15
    # some auxiliar coefficients
    a = 3.0 - self.gamma
    b = self.gamma + 1.0
    b = numpy.power(b,7.0)
    b = a*b
    b = b/16.0/self.q
    c = self.gamma - 1.0
    b = b/c/c/c/c
    self.a4 = a*a*a*a
    self.b  = b
    return
  
  def eval(self,x):
    '''
    Function evaluation
    '''
    dx = (1.0e+00 - x)
    fs4 = (1.0e+00 - 1.2e+00*x*self.a4)/max(dx,self.dx_min)
    return dx + 1.2e+00*x*self.a4 - fs4 - self.b
  
  def __call__(self,x):
    '''
    Overload call operator for function evaluation
    '''
    return self.eval(x)

# function to solve function root using the bisection method
def solve_bisect(func,x1,x2,eps_min=1.0e-09,iter_max=1000):
  '''
  Solve equation using the bisection method
  '''

  iter_count = 0

  x_lo = x1
  x_hi = x2

  f_lo = func(x_lo)
  f_hi = func(x_hi)

  if ( f_lo*f_hi ) > 0.0e+00:
    print('# warning :: limits of bisection have the same sign, not sure to find a zero')

  eps = 1.0e+00
  while ( eps < eps_min ):
    x = (x_lo + x_hi)/2.0e+00
    f = func(x)

    if ( f*f_lo ) > 0.0e+00:
      x_lo = x
      f_lo = f
    else:
      x_hi = x
      f_hi = f

    eps = 2.0e+00*numpy.abs((x_hi-x_lo)/(x_hi+x_lo))
    
    iter_count = iter_count + 1
    if iter_count > iter_max:
      print('# error :: cannot find zero')
      break

  return x

def main():

  us    = 20.0e+03
  r0    = 1.0e+00

  args = { 'Q': q_parameter(us,r0),
           'gamma': 4.0e+00/3.0e+00,
           }

  func = func_obj_c(args['Q'],args['gamma'])

  x = solve_bisect(func,0.0e+00,1.0e+00)

  print('# x = {:17.9e}'.format(x))

  return 0

if __name__=='__main__':
  sys.exit(main())

