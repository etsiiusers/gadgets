
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Program to compute the laser intensity based in Gaussian laser profile
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.01  :: Manuel Cotelo Ferreiro
# - initial version
#

import os
import sys

import argparse

import numpy
import scipy.special

# program description
desc = 'Program to compute the laser intensity based in Gaussian laser profile'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
eplg = '{}, 2019'.format(author)

# default parameters
defs = {'ene': 1.0e-06,  # in J
        'dt': 100.0e-15,  # in s
        'spot': 25.0e-06,  # in m
        }

# conversion from FWHM to parameters sigma (standard deviation of the gaussian
# distribution)
_k_sigma_to_fwhm = 2.0e+00 * numpy.sqrt(2.0e+00 * numpy.log(2.0e+00))


def fwhm2sigma(fwhm):
    return fwhm / _k_sigma_to_fwhm


def sigma2fwhm(sigma):
    return sigma * _k_sigma_to_fwhm


class gauss_c():

    def __init__(self, mu, sigma):
        self.mu = mu
        self.sigma = sigma
        return

    def __call__(self, x):
        w = (x - self.mu) / self.sigma
        return numpy.exp(-0.5 * w * w)


class laser_profile_c():

    '''
    Class for laser profile
    '''

    def __init__(self, energy, fwhm_r, fwhm_t, t0=0.0e+00):
        '''
        Build object for laser intensity profile
        '''

        sigma_t = fwhm2sigma(fwhm_t)
        sigma_r = fwhm2sigma(fwhm_r)
        # parameters
        self.func_t = gauss_c(t0, sigma_t)
        self.func_r = gauss_c(0.0e+00, sigma_r)
        self.energy = energy
        # auxiliar variables
        self.i0 = 1.0e+00
        t_lo = self.func_t.mu - 8.0e+00 * self.func_t.sigma
        t_hi = self.func_t.mu + 8.0e+00 * self.func_t.sigma
        r_lo = 0.0e+00
        r_hi = self.func_r.mu + 8.0e+00 * self.func_r.sigma
        w = self.integ((t_lo, t_hi), (r_lo, r_hi))
        self.i0 = (self.energy / w) * self.i0
        return

    def intens(self, t, r):
        return self.i0 * self.func_t(t) * self.func_r(r)

    def func_integ(self, t, r):
        return self.intens(t, r) * 2.0e+00 * numpy.pi * r

    def integ(self, t_span, r_span):
        return scipy.integrate.dblquad(self.func_integ, r_span[0], r_span[1], gfun=lambda x: t_span[0], hfun=lambda x: t_span[1])[0]


def get_program_args():
    '''
    Read command line arguments and defined allowed program options
    '''

    parser = argparse.ArgumentParser(description=desc, epilog=eplg)

    parser.add_argument('--ene', help='Select laser energy (in J), default {:13.5e}'.format(
        defs['ene']), type=float, default=defs['ene'], )
    parser.add_argument('--dt', help='Laser pulse duration (in s), default {:13.5e}'.format(
        defs['dt']), type=float, default=defs['dt'], )
    parser.add_argument('--spot', help='Laser spot size (diameter in m), default {:13.5e}'.format(
        defs['spot']), type=float, default=defs['spot'], )

    return parser.parse_args()


def main():
    '''
    Program main function
    '''

    # get command line arguments
    args = get_program_args()

    # create laser profile
    func_laser = laser_profile_c(args.ene, args.spot, args.dt)

    print('# laser peak intensity :: {:15.7e} W/m2'.format(func_laser.i0))

    return 0

if __name__ == '__main__':
    sys.exit(main())
