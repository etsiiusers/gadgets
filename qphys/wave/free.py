
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Particle wave package moving freely
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.integrate

# info
desc  = 'Particle in a box'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

info = '''
'''

# program default parameters
defs = { 'kin': 1.0e+00,
         }


class gauss_c():

  _fdelta = 5.0e+00

  def __init__(self,sigma=1.0e+00,mu=0.0e+00,k=2.0e+00,area=1.0e+00):
    # parameters
    self.sigma = sigma
    self.mu = mu
    self.k = k
    self.area = area
    # vectorize functions to improve performance
    self.func_pdf = numpy.vectorize(self._pdf)
    self.func_cdf = numpy.vectorize(self._cdf)
    # limit support of the gaussian function
    self.x_supp = ( self.mu - self._fdelta*self.sigma, self.mu + self._fdelta*self.sigma, )
    # normalization factor
    self.f0 = 1.0e+00
    self.f0 = self.f0/self.cdf(self.x_supp[1])
    return
  
  def supp(self):
    return self.x_supp

  def scale(self,x):
    return (x - self.mu)/self.sigma

  def _pdf(self,x):
    if x > self.x_supp[0] and x < self.x_supp[1]:
      w = self.scale(x)
      w = numpy.power(numpy.abs(w),self.k)
      return self.f0*numpy.exp(-w)
    else:
      return 0.0e+00
  
  def _cdf(self,x):
    if x < self.x_supp[0]:
      return 0.0e+00
    elif x > self.x_supp[1]:
      return 1.0e+00
    else:
      return scipy.integrate.romberg(self.pdf,self.x_supp[0],x,divmax=20)
  
  def pdf(self,x):
    return self.func_pdf(x)
  
  def cdf(self,x):
    return self.func_cdf(x)

  def __call__(self,x):
    return self.area*self.pdf(x)


class plane_wave_c():

  def __init__(self,w):
    self.w = w
    self.k = numpy.sqrt(2.0e+00*self.w)
    return

  def scale(self,x,t):
    return self.k*x - self.w*t

  def __call__(self,x,t):
    return numpy.exp(-1.0j*self.scale(x,t))


class gauss_wave_c():

  def __init__(self,w):
    # plane wave function
    self.func_wave = plane_wave_c(w)
    # restirct plane wave to a gaussian profile
    k = self.func_wave.k
    self.func_profile = gauss_c(5.0e-01*k,k)
    return

  def supp(self):
    return self.func_profile.supp()

  def __call__(self,x,t):
    fw = self.func_wave(x,t)
    fp = self.func_profile(self.func_wave.scale(x,t))
    return fw*fp


class wave_c():

  _k_size = 2000

  def __init__(self,w,func_mom):
    # parameters
    self.w  = w
    self.func_mom = func_mom
    # auxiliar variables
    s = func_mom.supp()
    self.k = numpy.linspace(s[0],s[1],self._k_size)
    self.k_delta = (self.k[-1] - self.k[0])/self.k.shape[0]
    return

  def eval(self,x,t):
    g = self.func_mom(self.k,0.0e+00)
    f = numpy.exp(1.0j*(self.k*x - self.w*t))
    fg = numpy.multiply(g,f)
    h = scipy.integrate.simps(fg,dx=self.k_delta)
    return h

  def wave(self,t):
    #xc = numpy.sqrt(self.w/2.0e+00)*t
    xc = 0.0e+00
    x_lo = xc - 4.0e+01
    x_hi = xc + 4.0e+01
    x_size = 400
    x = numpy.linspace(x_lo,x_hi,x_size)
    x_delta = (x[-1] - x[0])/x.shape[0]

    f  = []
    for xi in x:
      f.append(self.eval(xi,t))
    f = numpy.array(f)

    f2 = numpy.multiply(numpy.conj(f),f)

    a2 = scipy.integrate.simps(f2,dx=x_delta)

    f /= numpy.sqrt(numpy.abs(a2))

    return x, f

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--kin', help='Kinetic energy of the particle (in Ry), default "{}"'.format(defs['kin']), type=float, default=defs['kin'], )
  
  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()
  assert( args.kin > 0.0e+00 )

  # get wave parameters, using atomic unit systems
  kin = args.kin
  w = kin # E = hbar*w and in atomic units hbar = 1.0

  # create momentum distribution
  pk = gauss_wave_c(w)

  # create wave package generator
  wf = wave_c(w,pk)

  # create time mesh
  t_lo = 0.0e+00
  t_hi = 1.0e+05
  t_size = 20
  t = numpy.linspace(t_lo,t_hi,t_size)
  
  # compute waves
  for i, ti in enumerate(t):
    x, f = wf.wave(ti)
    # print wave
    for xj, fj in zip(x,f):
      fr = numpy.real(fj)
      fi = numpy.imag(fj)
      f2 = numpy.absolute(fj)
      f2 = f2*f2
      print(' {:17.9e} {:17.9e} {:17.9e} {:17.9e} {:17.9e}'.format(ti,xj,fr,fi,f2))
    print()

  return

if __name__=='__main__':
  sys.exit(main())
