
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Wave package
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.integrate

import matplotlib.pyplot as plt

# info
desc  = 'Wave package'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

info = '''
'''

# program default parameters
defs = { 'k': 1.0e+00,
         'w': 1.0e+00,
         'pos': 0.0e+00,
         'width': 1.0e+01,
         }


class plane_wave_c():

  def __init__(self,w,k):
    self.k = k
    self.w = w
    return

  def scale(self,x,t):
    return self.k*x + self.w*t

  def eval(self,x,t):
    return numpy.exp(1.0j*self.scale(x,t))

  def __call__(self,x,t):
    return self.eval(x,t)


class gauss_c():

  _f_supp = 4.0e+00

  def __init__(self,sigma=1.0e+00,mu=0.0e+00,k=2.0e+00,area=1.0e+00):
    # parameters
    self.sigma = sigma
    self.mu = mu
    self.k = k
    self.area = area
    # vectorized functions
    self.func_pdf = numpy.vectorize(self._pdf)
    self.func_cdf = numpy.vectorize(self._cdf)
    # define support
    self.x_supp = [ self.mu - self._f_supp*self.sigma, self.mu + self._f_supp*self.sigma, ]
    # auxiliar variables
    self.f0 = 1.0e+00
    self.f0 = self.f0/self.cdf(self.hi())
    return

  def supp(self):
    return self.x_supp
  def lo(self):
    return self.x_supp[0]
  def hi(self):
    return self.x_supp[1]

  def scale(self,x):
    return (x - self.mu)/self.sigma

  def _pdf(self,x):
    if x > self.lo() and x < self.hi():
      w = self.scale(x)
      w = numpy.power(numpy.abs(w),self.k)
      return self.f0*numpy.exp(-w)
    else:
      return 0.0e+00

  def _cdf(self,x):
    if x < self.lo():
      return 0.0e+00
    elif x > self.hi():
      return 1.0e+00
    else:
      return scipy.integrate.romberg(self.pdf,self.lo(),x,divmax=12)

  def pdf(self,x):
    return self.func_pdf(x)
  
  def cdf(self,x):
    return self.func_cdf(x)

  def eval(self,x):
    return self.area*self.pdf(x)
  
  def integ(self,x1,x2):
    return self.area*(self.cfd(x2) - self.cdf(x1))

  def __call__(self,x):
    return self.pdf(x)


class wave_pack_c():

  def __init__(self,pos,width,k,w):
    self.func_pw = plane_wave_c(w,k)
    self.func_prof = gauss_c(width,pos)
    return

  def eval(self,x,t):
    return self.func_prof(x)*self.func_pw(x,t)
  

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--k', help='Wave number, default "{}"'.format(defs['k']), type=float, default=defs['k'], )
  parser.add_argument( '--w', help='Angular frequency, default "{}"'.format(defs['w']), type=float, default=defs['w'], )
  
  parser.add_argument( '--pos', help='Position of the maximum of the profile, default "{}"'.format(defs['pos']), type=float, default=defs['pos'], )
  parser.add_argument( '--width', help='Width of the profile, default "{}"'.format(defs['width']), type=float, default=defs['width'], )
  
  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()

  # create plane wave
  wf = wave_pack_c(args.pos,args.width,args.k,args.w)

  # define some scale
  #x_delta = numpy.pi/wf.func_pw.k
  x_delta = args.width
  t_delta = numpy.pi/wf.func_pw.w

  # define x-t mesh
  x_lo = -5.0e+00*x_delta
  x_hi =  5.0e+00*x_delta
  x_size = 400
  x = numpy.linspace(x_lo,x_hi,x_size)
  
  t_lo = 0.0e+00
  t_hi = 6.0e+00*t_delta
  t_size = 400
  t = numpy.linspace(t_lo,t_hi,t_size)

  mx, mt = numpy.meshgrid(x,t)

  # evaluate wavefunction
  f = wf.eval(mx,mt)
  e = wf.func_prof(mx)
  fr = numpy.real(f)
  fi = numpy.imag(f)
  f2 = numpy.real(numpy.conj(f)*f)

  # print image
  plt.figure()
  plt.xlabel('position, in au')
  plt.ylabel('time, in au')
  plt.imshow(fi,extent=(x_lo,x_hi,t_hi,t_lo),aspect='auto')
  plt.show()

  # print some frames
  num_frame = 4
  for i in range(num_frame+1):
    
    frame = int(i*(t.shape[0]-1)/num_frame)

    plt.figure()
    plt.plot(x,fr[frame,:])
    plt.plot(x,fi[frame,:])
    plt.plot(x,e[frame,:])
    plt.show()

  return

if __name__=='__main__':
  sys.exit(main())
