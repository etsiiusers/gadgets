
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Wave in 2D saved to HDF5 file
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.21 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

import h5py

# info
desc  = 'Wave in 2D saved to HDF5 file'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

info = '''
'''

class wave_2d_c():

  def __init__(self,w,k,amp=1.0e+00):
    self.w = w
    self.k = k
    self.amp = amp
    return

  def phase(self,x,y,t):
    return (self.k[0]*x + self.k[1]*y) - self.w*t

  def __call__(self,x,y,t):
    return self.amp*numpy.exp(-1.0j*self.phase(x,y,t))


def main():

  # set output file name
  file_name = 'wave_2d.h5'

  # set wave parameters
  k = numpy.array( [ 1.0e+00, 1.0e+00, ])
  w = 1.0e+00

  wf = wave_2d_c(w,k)

  # set some scale parameters
  x_scale = 2.0e+00*numpy.pi/k[0]
  y_scale = 2.0e+00*numpy.pi/k[1]
  t_scale = 2.0e+00*numpy.pi/w

  x_delta = x_scale/40
  y_delta = y_scale/40
  t_delta = t_scale/40

  # define x-t mesh
  x_lo = -4.0*x_scale
  x_hi =  4.0*x_scale
  x_size = int((x_hi - x_lo)/x_delta)
  x = numpy.linspace(x_lo,x_hi,x_size)
  
  y_lo = -4.0*y_scale
  y_hi =  4.0*y_scale
  y_size = int((y_hi - y_lo)/y_delta)
  y = numpy.linspace(y_lo,y_hi,y_size)
 
  t_lo = 0.0e+00
  t_hi = 4.0*t_scale
  t_size = int((t_hi - t_lo)/t_delta)
  t = numpy.linspace(t_lo,t_hi,t_size)

  mx, my, mt = numpy.meshgrid(x,y,t)

  # evaluate wave
  f = wf(mx,my,mt)

  # open HDF5 file
  h5f = h5py.File(file_name,'w')
  
  ds = h5f.create_dataset('x',data=x,compression='gzip',)
  ds = h5f.create_dataset('y',data=y,compression='gzip',)
  ds = h5f.create_dataset('t',data=t,compression='gzip',)
  ds = h5f.create_dataset('f',data=f,compression='gzip',)
    
  h5f.close()

  return

if __name__=='__main__':
  sys.exit(main())
