
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Wave package
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.15 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - selection of resolution based in wavenumber 
# - selection of time step based on angular frequency
# 2019.03.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.integrate

import matplotlib.pyplot as plt
import matplotlib.animation as ani

# info
desc  = 'Wave package'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

info = '''
'''

# program default parameters
defs = { 'k': 0.5e+00,
         'w': 1.0e+00,
         'pos': 0.0e+00,
         'width': 1.0e+01,
         'vel': 2.0e+00,
         }


class plane_wave_c():

  '''
  Object to describe a plane wave
  '''

  def __init__(self,w,k):
    
    '''
    Object initialization

    -> in
    w: angular frequency
    k: wave number
    '''

    self.k = k
    self.w = w
    return

  def phase(self,x,t):
    
    '''
    -> in
    x: position, in au
    t: time, in au

    <- out
    (1) phase
    '''

    return self.k*x + self.w*t

  def eval(self,x,t):
    
    '''
    Function evaluation

    -> in
    x: position, in au
    t: time, in au

    <- out
    (1) function
    '''

    return numpy.exp(-1.0j*self.phase(x,t))

  def __call__(self,x,t):
    
    '''
    Overload call operator, see eval
    '''

    return self.eval(x,t)


class gauss_c():

  '''
  Object to describe a gaussian function
  '''

  # factor to compute the limits of the gaussian profile
  _f_supp = 4.0e+00

  def __init__(self,sigma=1.0e+00,mu=0.0e+00,k=2.0e+00,area=1.0e+00,wk=0.0e+00):
    
    '''
    Object initialization

    -> in
    sigma: width of the gaussian (it is not the STD)
    mu: position of the maximum
    k: exponent
    area: area under the gaussian
    wk: velocity of the maximum of the gaussian
    '''

    # parameters
    self.sigma = sigma
    self.mu = mu
    self.k = k
    self.area = area
    self.wk = wk
    
    # vectorized functions
    self.func_pdf = numpy.vectorize(self._pdf)
    self.func_cdf = numpy.vectorize(self._cdf)
    
    # define support
    self.x_supp = numpy.array([ - self._f_supp*self.sigma, self._f_supp*self.sigma, ])
    
    # auxiliar variables
    self.f0 = 1.0e+00
    self.f0 = self.f0/self.cdf(self.hi(0.0e+00),0.0e+00)
    return

  def ref(self,t):
    
    '''
    Evaluate reference point, position of the maximum of the gaussian

    -> in
    t: time

    <- out
    (1) position of the reference point
    '''

    return self.mu + self.wk*t

  def supp(self,t):
    xc = self.ref(t)
    return self.ref(t) + self.x_supp
  def lo(self,t):
    return self.supp(t)[0]
  def hi(self,t):
    return self.supp(t)[1]
  
  def scale(self,x,t):
    
    '''
    Scale position

    -> in
    x: position
    t: time

    <- out
    (1) scaled position
    '''

    return (x - self.ref(t))/self.sigma

  def _pdf(self,x,t):

    '''
    Evaluate PDF

    -> in
    x: position
    t: time

    <- out
    (1) evaluation of probability density
    '''

    if x > self.lo(t) and x < self.hi(t):
      w = self.scale(x,t)
      w = numpy.power(numpy.abs(w),self.k)
      return self.f0*numpy.exp(-w)
    else:
      return 0.0e+00

  def _cdf(self,x,t):

    '''
    Evaluate CDF
    
    -> in
    x: position
    t: time

    <- out
    (1) evaluation of probability, CDF(x0) = P(x<x0)
    '''

    if x < self.lo(t):
      return 0.0e+00
    elif x > self.hi(t):
      return 1.0e+00
    else:
      func = lambda x: self.pdf(x,t)
      return scipy.integrate.romberg(func,self.lo(t),x,divmax=12)

  def pdf(self,x,t):
    return self.func_pdf(x,t)
  
  def cdf(self,x,t):
    return self.func_cdf(x,t)

  def eval(self,x):
    return self.area*self.pdf(x)
  
  def integ(self,x1,x2,t):
    return self.area*(self.cfd(x2,t) - self.cdf(x1,t))

  def __call__(self,x,t):
    return self.pdf(x,t)


class wave_pack_c():

  '''
  Object to describe a wave package
  '''

  def __init__(self,pos,width,vel,k,w):
    
    '''
    Object initialization
    '''

    self.func_pw = plane_wave_c(w,k)
    self.func_prof = gauss_c(width,pos,wk=vel)
    return

  def eval(self,x,t):
    
    '''
    Wave evaluation

    -> in
    x: position
    t: time

    <- out
    (1) value of the wave 
    '''

    return self.func_prof(x,t)*self.func_pw(x,t)

class record_c():

  '''
  Helper object to crate animations
  '''

  def __init__(self,t,x,list_func):
    
    '''
    Object initialization

    -> in
    x: coordinate of the function evaluations
    list_func: list of function evaluations [ f1, f2, f3, ...], fij = fi(xj)
    '''

    # save data
    self.t = t
    self.x = x
    self.list_func = list_func
    # initialize figure
    self.fig, self.ax = plt.subplots()
    # initialize lines
    self.ln = []
    for _ in self.list_func:
      ln = plt.plot( [], [], )[0]
      self.ln.append(ln)
    return

  def init(self):
    
    '''
    Initialize graph

    <- out
    (1) list of Line2D
    '''

    # get largest value
    f = 0.0e+00
    for fi in self.list_func:
      f_curr = 1.10*numpy.amax(numpy.abs(fi))
      f = max(f,f_curr)
    # set axis
    self.ax.set_xlim(self.x[0],self.x[-1])
    self.ax.set_ylim(-f,f)
    # initialize lines
    for li in self.ln:
      li.set_data( [], [], )
    return self.ln,

  def update(self,frame):
    
    '''
    Set data of the current frame

    -> in
    frame: index of the frame

    <- out
    (1) list of Line2D
    '''
    
    # set plot data
    for li, fi in zip(self.ln,self.list_func):
      li.set_data(self.x,fi[frame,:])

    # set legend
    self.ln[0].set_label('time = {:10.2e}'.format(self.t[frame]))
    self.ax.legend(loc='lower left')

    return self.ln


def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--k', help='Wave number, default "{}"'.format(defs['k']), type=float, default=defs['k'], )
  parser.add_argument( '--w', help='Angular frequency, default "{}"'.format(defs['w']), type=float, default=defs['w'], )
  
  parser.add_argument( '--pos', help='Position of the maximum of the profile, default "{}"'.format(defs['pos']), type=float, default=defs['pos'], )
  parser.add_argument( '--width', help='Width of the profile, default "{}"'.format(defs['width']), type=float, default=defs['width'], )
  parser.add_argument( '--vel', help='Position of the maximum of the profile, default "{}"'.format(defs['vel']), type=float, default=defs['vel'], )
  
  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()
  
  # define some scale
  num_nodes_per_cycle = 16
  x_delta = numpy.abs(numpy.pi/args.k)
  t_delta = numpy.abs(numpy.pi/args.w)
  
  # define x-t mesh
  x_lo = -5.0e+00*args.width
  x_hi =  5.0e+00*args.width
  x_size = num_nodes_per_cycle*int(numpy.ceil((x_hi - x_lo)/x_delta))
  x = numpy.linspace(x_lo,x_hi,x_size)
  
  t_lo = 0.0e+00
  t_hi = 6.0e+00*t_delta
  t_size = num_nodes_per_cycle*int(numpy.ceil((t_hi - t_lo)/t_delta))
  t = numpy.linspace(t_lo,t_hi,t_size)

  mx, mt = numpy.meshgrid(x,t)

  # create plane wave
  pos_init = args.pos - 0.5*args.vel*(t_hi - t_lo) 
  wf = wave_pack_c(pos_init,args.width,args.vel,args.k,args.w)

  # evaluate wavefunction
  f = wf.eval(mx,mt)
  e = wf.func_prof(mx,mt)
  fr = numpy.real(f)
  fi = numpy.imag(f)
  f2 = numpy.real(numpy.conj(f)*f)

  # print image
  plt.figure()
  plt.xlabel('position, in au')
  plt.ylabel('time, in au')
  plt.imshow(fi,extent=(x_lo,x_hi,t_hi,t_lo),aspect='auto')
  plt.show()
  plt.imshow(e,extent=(x_lo,x_hi,t_hi,t_lo),aspect='auto')
  plt.show()
  
  # print some frames
  num_frame = 4
  for i in range(num_frame+1):
    
    frame = int(i*(t.shape[0]-1)/num_frame)

    plt.figure()
    plt.plot(x,fr[frame,:])
    plt.plot(x,fi[frame,:])
    plt.plot(x,e[frame,:])
    plt.show()

  # make animation
  play = record_c(t,x,[ fr, fi, e, ])
  mov = ani.FuncAnimation(play.fig,play.update,frames=t.shape[0],interval=1,init_func=play.init,blit=False)
  mov.save('packv.mp4',fps=30,)
  plt.show()

  return

if __name__=='__main__':
  sys.exit(main())
