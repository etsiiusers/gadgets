
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Wave in 1D
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.21 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import numpy

import h5py

# info
desc  = 'Wave package'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

info = '''
'''

class wave_1d_c():

  def __init__(self,w,k,amp=1.0e+00):
    self.w = w
    self.k = k
    self.amp = amp
    return

  def phase(self,x,t):
    return self.k*x - self.w*t

  def __call__(self,x,t):
    return self.amp*numpy.exp(-1.0j*self.phase(x,t))


def main():

  # set output file name
  file_name = 'wave.h5'

  # set wave parameters
  k = 1.0e+00
  w = 1.0e+00

  wf = wave_1d_c(w,k)

  # set some scale parameters
  x_scale = 2.0e+00*numpy.pi/k
  t_scale = 2.0e+00*numpy.pi/w

  x_delta = x_scale/50
  t_delta = t_scale/50

  # define x-t mesh
  x_lo = -4.0*x_scale
  x_hi =  4.0*x_scale
  x_size = int((x_hi - x_lo)/x_delta)
  x = numpy.linspace(x_lo,x_hi,x_size)
  
  t_lo = 0.0e+00
  t_hi = 4.0*t_scale
  t_size = int((t_hi - t_lo)/t_delta)
  t = numpy.linspace(t_lo,t_hi,t_size)

  mx, mt = numpy.meshgrid(x,t)

  # evaluate wave
  f = wf(mx,mt)

  # open HDF5 file
  h5f = h5py.File(file_name,'w')
  
  for i, ti in numpy.ndenumerate(t):
    name = 'step_{:06d}'.format(i[0])
    
    q = numpy.zeros( (x.shape[0], 3, ))
    q[:,0] = x[:]
    q[:,1] = numpy.real(f[i[0],:])
    q[:,2] = numpy.imag(f[i[0],:])

    ds = h5f.create_dataset(name,data=q,compression='gzip',)
    
    ds.attrs['step'] = i
    ds.attrs['time'] = ti
    ds.attrs['column_00'] = 'position' 
    ds.attrs['column_01'] = 'real(f)' 
    ds.attrs['column_02'] = 'imag(f)'

  h5f.close()

  return

if __name__=='__main__':
  sys.exit(main())
