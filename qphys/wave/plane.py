
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Plane wave
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.integrate

import matplotlib.pyplot as plt

# info
desc  = 'Particle in a box'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

info = '''
'''

# program default parameters
defs = { 'k': 1.0e+00,
         'w': 1.0e+00,
         }

class plane_wave_c():

  def __init__(self,w,k):
    self.k = k
    self.w = w
    return

  def scale(self,x,t):
    return self.k*x + self.w*t

  def eval(self,x,t):
    return numpy.exp(1.0j*self.scale(x,t))

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--k', help='Wave number, default "{}"'.format(defs['k']), type=float, default=defs['k'], )
  parser.add_argument( '--w', help='Angular frequency, default "{}"'.format(defs['w']), type=float, default=defs['w'], )
  
  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()

  # create plane wave
  pw = plane_wave_c(args.k,args.w)

  # define some scale
  x_delta = numpy.pi/pw.k
  t_delta = numpy.pi/pw.w

  # define x-t mesh
  x_lo = -3.0e+00*x_delta
  x_hi =  3.0e+00*x_delta
  x_size = 400
  x = numpy.linspace(x_lo,x_hi,x_size)
  
  t_lo = 0.0e+00
  t_hi = 6.0e+00*t_delta
  t_size = 400
  t = numpy.linspace(t_lo,t_hi,t_size)

  mx, mt = numpy.meshgrid(x,t)

  # evaluate wavefunction
  f = pw.eval(mx,mt)
  fr = numpy.real(f)
  fi = numpy.imag(f)
  f2 = numpy.real(numpy.conj(f)*f)

  # print image
  plt.figure()
  plt.xlabel('position, in au')
  plt.ylabel('time, in au')
  plt.imshow(fi,extent=(x_lo,x_hi,t_hi,t_lo),aspect='auto')
  plt.show()

  # print some frames
  num_frame = 4
  for i in range(num_frame+1):
    
    frame = int(i*(t.shape[0]-1)/num_frame)

    plt.figure()
    plt.plot(x,fr[frame,:])
    plt.plot(x,fi[frame,:])
    plt.plot(x,f2[frame,:])
    plt.show()

  return

if __name__=='__main__':
  sys.exit(main())
