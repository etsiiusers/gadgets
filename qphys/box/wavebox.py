
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module to manage a wavefunction in a box
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Pyrhon program to solve the quantum mechanics problem of a particle in a 
# 1D box. The solution of this problem is wellknown
# See https://en.wikipedia.org/wiki/Particle_in_a_box)
#
# Version control:
# 2019.03.12 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.integrate

# info
desc  = 'Wavefunction of a particle in a box'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

class wave_function_c():

  '''
  Object to describe a wavefunction in a 1D box

  This description uses the atomic units system
  '''

  def __init__(self,n,pos,len,tref=0.0e+00):
    
    '''
    Initialize object

    -> in
    n: principal quantum number
    pos: position of the center of the box, in au
    len: size of the box, in au
    tref: time reference, in au
    '''

    # save parameters
    self.pos = pos
    self.len = len
    self.n = n
    self.tref = tref

    # vectorized functions for fast evaluation
    self.func_eval = numpy.vectorize(self._eval)

    # axuxiliar variables
    self.kn = self.n*numpy.pi/self.len
    self.wn = self.kn*self.kn/2.0e+00
    self.f0 = numpy.sqrt(2.0e+00/self.len) # normalization factor
    self.xref = self.pos - self.len/2.0e+00
    self.xlim = [ self.pos - self.len/2.0e+00, self.pos + self.len/2.0e+00, ]
    
    '''
    for ki, vi in self.__dict__.items():
      print('# {} :: {}'.format(ki,vi))
    print()
    '''

    return

  def _eval(self,x,t):
    
    '''
    Auxiliar function for wavefunction evaluation

    -> in
    x: position, in au
    t: time, in au

    <- out
    (1) Psi(x,t)
    '''

    if x > self.xlim[0] and x < self.xlim[1]:
      fx = self.f0*numpy.sin(self.kn*(x - self.xref))
      ft = numpy.exp(1.0j*self.wn*(t - self.tref))
      return fx*ft
    else:
      return 0.0e+00j

  def eval(self,x,t):
    
    '''
    Wavefunction evaluation, see "_eval" in this class
    '''

    return self.func_eval(x,t)


class projection_c():

  '''
  Object to make projections of the wavefunction in the base space
  '''

  def __init__(self,x,f):
    
    '''
    Initialize object 

    -> in
    x: position, in au
    f: wavefunction values for that positions
    '''

    # save data
    self.data = [ x, f, ]
    self.data = numpy.array(self.data)
    
    # auxiliar variables
    self.pos = 0.5*(self.data[0,0] + self.data[0,-1])
    self.len = self.data[0,-1] - self.data[0,0]
    self.tref = 0.0e+00
    
    return

  def eval(self,n):
    
    '''
    Evaluate projection coefficient

    -> in
    n: principal quantum number

    <- out
    (1) Cn, projection
    '''

    assert( n > 0 )
    # create wavefunction descriptor
    wf = wave_function_c(n,self.pos,self.len,self.tref)
    # compute integrand values
    g = self.data[1]*numpy.conj(wf.eval(self.data[0],self.tref))
    # integrate using Simpson's rule
    cn = scipy.integrate.simps(g,x=self.data[0])
    return cn


class func_expansion_c():

  '''
  Object to manage wavefunction expansio in the basis
  '''

  _n_max_default = 8

  def __init__(self,x,f,n_max=_n_max_default):
    
    '''
    Initialize object
    
    -> in
    x: position, in au
    f: wavefunction values for that positions
    n_max: maximum principal quantum number for the expansion
    '''

    # save data
    self.data = [ x, f, ]
    self.data = numpy.array(self.data)
    self.n_max = n_max
    assert( self.n_max > 0 )

    # auxiliar variables
    self.pos = 0.5*(self.data[0,0] + self.data[0,-1])
    self.len = self.data[0,-1] - self.data[0,0]
    self.tref = 0.0e+00
    
    # define coefficients and WF for the expansion
    func_proj = projection_c(x,f)
    self.params = []
    for i in range(self.n_max):
      n = i + 1
      cn = func_proj.eval(n)
      wf = wave_function_c(n,self.pos,self.len,self.tref)
      self.params.append( ( n, cn, wf, ))

    return

  def eval(self,x,t):

    '''
    Evaluate expansion

    -> in
    x: position, in au
    t: time in au

    <- out
    (1) Psi(x,t)
    '''

    f = 0.0e+00
    for ni, ci, wi in self.params:
      f += ci*wi.eval(x,t)

    return f

if __name__=='__main__':
  print('# warning :: this file is not intended for standalone run.')
  sys.exit(-1)
