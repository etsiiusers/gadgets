
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Test avefunction for a particle in a box
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Pyrhon program to solve the quantum mechanics problem of a particle in a 
# 1D box. The solution of this problem is wellknown
# See https://en.wikipedia.org/wiki/Particle_in_a_box)
#
# Version control:
# 2019.03.14 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy

import wavebox

# info
desc  = 'Test avefunction for a particle in a box'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

info = '''
Potential energy

\\begin{align}
V(x) = \\left\{ \\begin{array}{ll}
  0, & x_{c} - L/2 < x < x_{c} + L/2 \\\\
  \\infty, & \\text{otherwise}
\\end{array} \\right.
\\end{align}

Using the atomic units system the Schrödinger equation takes the form

i \\frac{\\partial}{\\partial t} \\psi(x,t) = \\frac{1}{2} \\frac{\\partial^{2}}{\\partial x^{2}} \\psi(x,t) + V(x) \\psi(x,t)

Solutions take the form

\\begin{align}
\\psi_{n}(x,t) = \\left\{ \\begin{array}{ll}
  A \\cdot \\left( k_{n} \\left( x - x_{c} + \\frac{L}{2}\\right) \\right) \cdot e^{- i \\omega_{n} t}
  0, & \\text{otherwise} \\\\
\\end{array} \\right.
\\end{align}

where

\\begin{align}
  k_{n} = \\frac{n \\pi}{L} \\\\
  \\omega_{n} = \\frac{n^{2} \\pi^{2}}{2 L^{2}}
\\end{align}

Remember, in the atomic units system $\\hbar = 1$ and then $E_{n} = \\omega_{n}$.
'''

# program default parameters
defs = { 'pos': 0.0e+00,
         'len': 1.0e+00, 
         'n_min': 1,
         'n_max': 4,
         }

def wfprint(t,x,f,fd=sys.stdout):

  print(file=fd)
  print('# {}'.format(desc),file=fd)
  print('# {}'.format(eplg),file=fd)
  print(file=fd)
  print('# time = {:17.9e}'.format(t),file=fd)
  print(file=fd)
 
  print('# column_00 = index',file=fd)
  print('# column_01 = position, in au',file=fd)
  for i, _ in enumerate(f):
    print('# column_{:02d} = Re[\Psi_n(x,t)], n = {}'.format(2+3*i,i),file=fd)
    print('# column_{:02d} = Img[\Psi_n(x,t)], n = {}'.format(2+3*i+1,i),file=fd)
    print('# column_{:02d} = |\Psi_n(x,t)|^{{2}}, n = {}'.format(2+3*i+2,i),file=fd)
  print(file=fd)

  for j, xj in enumerate(x):
    print(' {:5d} {:15.7e}'.format(j,xj),end='',file=fd)
    for fi in f:
      fij = fi[j]

      fre = numpy.real(fij)
      fim = numpy.imag(fij)
      f2 = numpy.absolute(fij)
      f2 *= f2

      print(' {:15.7e} {:15.7e} {:15.7e}'.format(fre,fim,f2),end='',file=fd)

    print(file=fd)
  
  return

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--pos', help='Position of the box center, default "{}"'.format(defs['pos']), type=float, default=defs['pos'], )
  parser.add_argument( '--len', help='Size of the box, default "{}"'.format(defs['len']), type=float, default=defs['len'], )
  
  parser.add_argument( '--n_min', help='Minimum quantum number, default "{}"'.format(defs['n_min']), type=int, default=defs['n_min'], )
  parser.add_argument( '--n_max', help='Maximum quantum number, default "{}"'.format(defs['n_max']), type=int, default=defs['n_max'], )

  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()

  # check command line arguments
  assert( args.len > 0.0e+00 )
  assert( args.n_min > 0 )
  assert( args.n_max >= args.n_min)

  # create wave functions
  list_wf = []
  for n in range(args.n_min,args.n_max+1):
    list_wf.append(wavebox.wave_function_c(n,args.pos,args.len))

  # define time
  t =  0.0e+00

  # define space mesh
  x_lo = args.pos - args.len/2.0e+00
  x_hi = args.pos + args.len/2.0e+00
  x_size = 200
  x = numpy.linspace(x_lo,x_hi,x_size)

  # evaluate wavefunctions in mesh (x,t)
  f = []
  for wf in list_wf:
    f.append(wf.eval(x,t))

  # print results to screen
  wfprint(t,x,f)

  return

if __name__=='__main__':
  sys.exit(main())
