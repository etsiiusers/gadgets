
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Particle in a box
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Pyrhon program to solve the quantum mechanics problem of a particle in a 
# 1D box. The solution of this problem is wellknown
# See https://en.wikipedia.org/wiki/Particle_in_a_box)
#
# Version control:
# 2019.03.12 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.integrate

import wavebox

# info
desc  = 'Particle in a box'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

info = '''
Potential energy

\\begin{align}
V(x) = \\left\{ \\begin{array}{ll}
  0, & x_{c} - L/2 < x < x_{c} + L/2 \\\\
  \\infty, & \\text{otherwise}
\\end{array} \\right.
\\end{align}

Using the atomic units system the Schrödinger equation takes the form

i \\frac{\\partial}{\\partial t} \\psi(x,t) = \\frac{1}{2} \\frac{\\partial^{2}}{\\partial x^{2}} \\psi(x,t) + V(x) \\psi(x,t)

Solutions take the form

\\begin{align}
\\psi_{n}(x,t) = \\left\{ \\begin{array}{ll}
  A \\cdot \\left( k_{n} \\left( x - x_{c} + \\frac{L}{2}\\right) \\right) \cdot e^{- i \\omega_{n} t}
  0, & \\text{otherwise} \\\\
\\end{array} \\right.
\\end{align}

where

\\begin{align}
  k_{n} = \\frac{n \\pi}{L} \\\\
  \\omega_{n} = \\frac{n^{2} \\pi^{2}}{2 L^{2}}
\\end{align}

Remember, in the atomic units system $\\hbar = 1$ and then $E_{n} = \\omega_{n}$.
'''

# program default parameters
defs = { 'pos': 0.0e+00,
         'len': 1.0e+00, 
         'n_max': 48,
         'kin': 1.0e+01,
         'num_x': 400,
         'num_t': 1600,
         }


class wave_box_c():

  '''
  Object to manage a wave inside a box
  '''

  def __init__(self,k,pos,len,order=0):

    '''
    Initialize object
    '''

    # parameters
    self.k = k
    self.pos = pos
    self.len = len
    self.order = 2*order + 1
    # vectorize function
    self.func_eval = numpy.vectorize(self._eval)
    # auxiliar parameters
    self.w = self.order*numpy.pi/self.len
    # wave support
    self.x_lim = [ self.pos - 0.5*self.len, self.pos + 0.5*self.len, ]
    # normalization factor
    self.f0 = 1.0e+00
    self.f0 = self.f0/scipy.integrate.romberg(self.eval,self.lo(),self.hi(),divmax=12)
    return

  def lo(self):
    return self.x_lim[0]
  def hi(self):
    return self.x_lim[1]

  def _eval(self,x):
    if x > self.lo() and x < self.hi():
      dx = x - self.pos
      return numpy.exp(1.0j*self.k*x)*numpy.cos(self.w*dx)
    else:
      return 0.0j

  def eval(self,x):
    return self.func_eval(x)


def wfprint(t,x,fxt,fexp,fd=sys.stdout):
  
  print(file=fd)
  print('# {}'.format(desc),file=fd)
  print('# {}'.format(eplg),file=fd)
  print(file=fd)

  print('# function expansion :: ',file=fd)
  for n, cn, fn in fexp.params:
    print('# c_{:03d} = {:17.9e} {:17.9e}j'.format(n,numpy.real(cn),numpy.imag(cn)),file=fd)
  print(file=fd)
  
  print('# column_00 = time, in au',file=fd)
  print('# column_01 = position, in au',file=fd)
  print('# column_02 = Re[\Phi(x,t)]',file=fd)
  print('# column_03 = Imag[\Phi(x,t)]',file=fd)
  print('# column_04 = |\Phi(x,t)|^{{2}}',file=fd)
  print(file=fd)
  
  for i, ti in enumerate(t):
    for j, xj in enumerate(x):
      
      fij = fxt[i,j]

      values = [ ti, xj, numpy.real(fij), numpy.imag(fij), numpy.real(numpy.conj(fij)*fij), ]
      for vi in values:
        print(' {:17.9e}'.format(vi),end='',file=fd)
      print(file=fd)

    print(file=fd)
  return

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--pos', help='Position of the box center, default "{}"'.format(defs['pos']), type=float, default=defs['pos'], )
  parser.add_argument( '--len', help='Size of the box, default "{}"'.format(defs['len']), type=float, default=defs['len'], )
  parser.add_argument( '--kin', help='Kinetic energy of the particle in atomic units, default "{}"'.format(defs['kin']), type=float, default=defs['kin'], )
  
  parser.add_argument( '--n_max', help='Maximum quantum number, default "{}"'.format(defs['n_max']), type=int, default=defs['n_max'], )

  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()

  # check command line arguments
  assert( args.len > 0.0e+00 )
  assert( args.n_max > 0 )

  # space mesh
  x_lo = args.pos - 0.5*args.len
  x_hi = args.pos + 0.5*args.len
  x_size = defs['num_x']
  x = numpy.linspace(x_lo,x_hi,x_size)

  # define initial conditions of WF
  w = args.kin
  k = numpy.sqrt(2.0*w)
  func_init = wave_box_c(k,args.pos,args.len)
  f = func_init.eval(x)

  # WF expansion in the basis
  fexp = wavebox.func_expansion_c(x,f,args.n_max)
 
  '''
  g = fexp.eval(x,0.0e+00)
  for xi, fi, gi in zip(x,f,g):
    values = [ xi,
               numpy.real(fi), numpy.imag(fi), numpy.absolute(numpy.conj(fi)*fi), 
               numpy.real(gi), numpy.imag(gi), numpy.absolute(numpy.conj(gi)*gi), 
               ]
    for vi in values:
      print(' {:17.9e}'.format(vi),end='')
    print()
  sys.exit()
  '''

  # time mesh
  t_lo = 0.0e+00
  t_hi = args.n_max*4.0e+00*(args.len/k/2.0e+00)
  t_size = defs['num_t']
  t = numpy.linspace(t_lo,t_hi,t_size)

  mx, mt = numpy.meshgrid(x,t)

  fxt = fexp.eval(mx,mt)
  den = numpy.conj(fxt)*fxt

  
  # print results
  wfprint(t,x,fxt,fexp)

  return

if __name__=='__main__':
  sys.exit(main())
