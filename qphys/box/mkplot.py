
import os
import sys

import shlex
import subprocess

cmmd_common = '''
set terminal pngcairo enhanced size 1200,960

set xlabel 'position'
set ylabel '|{{/Symbol P}}(x,t)|'

set xrange [{x_lo}:{x_hi}]
set yrange [{y_lo}:{y_hi}]
'''

cmmd_base = '''
set output "{file_trash}"
plot "{file_inp}" every :::{index}::{index} u 1:(0.0/0.0)

set output "{file_out}"
plot "{file_inp}" every :::{index}::{index} u 2:5 w l lw 2.0 t sprintf("time = %13.5e",GPVAL_DATA_X_MIN)
'''

file_data = './log'
file_trash = 'trash.png'

# common picture options
common_opts = { 'x_lo': -0.5,
                'x_hi': 0.5,
                'y_lo': 0.0,
                'y_hi': 5.0,
                }

# number of pictures
t_size = 400

# create a list with the parameters for each picture
list_jobs = []
for i in range(t_size):
  # set parameters
  params = { 'index': i,
             'file_inp': file_data,
             'file_out': './pics/pic_{:06d}.png'.format(i),
             'file_trash': file_trash,
             }
  # add to list of pictures to do
  list_jobs.append(params)

# open GNUPLOT
proc = subprocess.Popen(shlex.split('gnuplot'),stdin=subprocess.PIPE)

# send common commands
cmmd = cmmd_common.format(** common_opts)
proc.stdin.write(cmmd.encode())

# send commands to GNUPLOT
for ji in list_jobs:
  cmmd = cmmd_base.format(** ji)
  proc.stdin.write(cmmd.encode())

os.remove(file_trash)
