
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Spherical harmonics
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.03.13 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.special

# info
desc  = 'Spherical harmonics'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 'num_theta': 100,
         'num_phi': 100,
         'l_max': 3,
         }


def print_sph(theta,phi,sph,fd=sys.stdout):

  count = 0

  print('# column_{:03d} = theta, in radians'.format(count),file=fd)
  count += 1
  print('# column_{:03d} = phi, in radians'.format(count),file=fd)
  count += 1

  for si in sph:
    print('# column_{:03d} = Re[ Y_{{{}}}^{{{}}}(\\theta,\\phi) ]'.format(count,si[0],si[1]),file=fd)
    count += 1
    print('# column_{:03d} = Imag[ Y_{{{}}}^{{{}}}(\\theta,\\phi) ]'.format(count,si[0],si[1]),file=fd)
    count += 1
    print('# column_{:03d} = | Y_{{{}}}^{{{}}}(\\theta,\\phi) |'.format(count,si[0],si[1]),file=fd)
    count += 1
  print(file=fd)

  for i, ti in numpy.ndenumerate(theta):
    for j, pj in numpy.ndenumerate(phi):
      
      values = [ ti, pj, ]
      
      for si in sph:
        values.append(numpy.real(si[2][j,i][0]))
        values.append(numpy.imag(si[2][j,i][0]))
        values.append(numpy.linalg.norm(si[2][i,j]))
      
      for vi in values:
        print(' {:15.7e}'.format(vi),end='',file=fd)
      print(file=fd)

    print(file=fd)

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--num_theta', help='Number of divisions of theta angle, default "{}"'.format(defs['num_theta']), type=int, default=defs['num_theta'], )
  parser.add_argument( '--num_phi', help='Number of divisions of phi angle, default "{}"'.format(defs['num_phi']), type=int, default=defs['num_phi'], )
  
  parser.add_argument( '--l_max', help='Maximum L number, default "{}"'.format(defs['l_max']), type=int, default=defs['l_max'], )

  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()
  assert( args.num_theta > 1 )
  assert( args.num_phi > 1 )
  assert( args.l_max >= 0 )

  # theta-phi mesh
  t_lo = 0.0e+00
  t_hi = numpy.pi
  t_size = args.num_theta
  t = numpy.linspace(t_lo,t_hi,t_size)

  p_lo = 0.0e+00
  p_hi = 2.0e+00*numpy.pi
  p_size = args.num_phi
  p = numpy.linspace(p_lo,p_hi,p_size)

  mt, mp = numpy.meshgrid(t,p)

  # compute values
  list_items = []
  for l in range(args.l_max+1):
    for m in range(-l,l+1):
      list_items.append( ( l, m, scipy.special.sph_harm(m,l,mp,mt), ))
  
  # print results
  fd = sys.stdout

  print(file=fd)
  print('# {}'.format(desc),file=fd)
  print('# {}'.format(eplg),file=fd)
  print(file=fd)
  
  print_sph(t,p,list_items,fd)

  return

if __name__=='__main__':
  sys.exit(main())
