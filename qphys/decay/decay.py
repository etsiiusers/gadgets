
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Decay chain
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Pyrhon program to solve the quantum mechanics problem of a particle in a 
# 1D box. The solution of this problem is wellknown
# See https://en.wikipedia.org/wiki/Particle_in_a_box)
#
# Version control:
# 2019.03.12 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import argparse

import numpy
import scipy.integrate

# info
desc  = 'Decay chain'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# program default parameters
defs = { 't1': 1.0e+00, # mean life of isotope 1
         't2': 1.0e+02, # mean life of isotope 2
         'den1': 1.0e+24,
         'den2': 0.0e+00,
         }

class decay_chain_c():

  '''
  Object to compute rates of decay of a chain of unstable isotopes

  X1 -> X2 -> X3

  Isotope 1 decays to isotope 2
  Isotope 2 decays to isotope 3
  Isotope 3 is stable
  '''

  def __init__(self,l1,l2):
    
    '''
    Object initialization

    -> in
    t1: mean time for isotope 1, in s
    t2: mean time for isotope 2, in s
    '''

    self.l1 = l1
    self.l2 = l2
    return

  def func_ode(self,t,u):

    '''
    Compute decay rates for each isotope

    -> in
    t: time, in s
    u: isotope concentrations, in 1/m3

    <- out
    (1) rates
    '''

    # initialize array for rates
    udot = numpy.zeros(u.shape)

    # desintegration rates
    w12 = self.l1*u[0]
    w23 = self.l2*u[1]

    # apply rates balance
    udot[0] = -w12
    udot[1] = -w23 + w12
    udot[2] = w23

    return udot

  def func_jac(self,t,u):
    
    '''
    Jacobian matrix of the decay rates

    -> in
    t: time, in s
    u: isotope concentrations, in 1/m3

    <- out
    (1) jacobian
    '''

    j = numpy.zeros( ( u.shape[0], u.shape[0], ))
    
    j[0,0] = - self.l1

    j[1,0] = self.l1
    j[1,1] = - self.l2

    j[2,1] = self.l2

    return j

def get_program_args():

  '''
  Define command line arguments adn parse them
  '''

  parser = argparse.ArgumentParser(description=desc,epilog=eplg)

  parser.add_argument( '--t1', help='Mean life for parent unstable isotope, default "{}"'.format(defs['t1']), type=float, default=defs['t1'], )
  parser.add_argument( '--t2', help='Mean life for child unstable isotope, default "{}"'.format(defs['t2']), type=float, default=defs['t2'], )
  
  parser.add_argument( '--den1', help='Initial density of particles of parent, default "{}" 1/m3'.format(defs['den1']), type=float, default=defs['den1'], )
  parser.add_argument( '--den2', help='Initial density of particles of child, default "{}" 1/m3'.format(defs['den2']), type=float, default=defs['den2'], )

  return parser.parse_args()

def main():

  # read command line arguments
  args = get_program_args()

  # check command line arguments
  assert( args.t1 > 0.0e+00 )
  assert( args.t2 > 0.0e+00 )

  # define decay chain
  dc = decay_chain_c(1.0/args.t1,1.0/args.t2)

  # initial conditions
  t_span = [ 0.0e+00, 8.0*max(args.t1,args.t2), ]
  n_init = [ args.den1, args.den2, 0.0e+00, ]
  ans = scipy.integrate.solve_ivp(dc.func_ode, t_span, n_init, method='BDF', jac=dc.func_jac, )
 
  # print results
  print()
  print('# {}'.format(desc))
  print('# {}'.format(eplg))
  print()
  print('# column_00 = time, in s')
  print('# column_01 = concentration of isotope 1 (parent), in 1/m3')
  print('# column_02 = concentration of isotope 2 (child), in 1/m3')
  print('# column_03 = concentration of isotope 3 (stable), in 1/m3')
  print()
  for i, ti in enumerate(ans.t):
    print(' {:17.9e}'.format(ti),end='')
    for xij in ans.y[:,i]:
      print(' {:17.9e}'.format(xij),end='')
    print()  

  return

if __name__=='__main__':
  sys.exit(main())
