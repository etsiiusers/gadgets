

#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Program to solve astrophysics analytical problems
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

# try to impor anlp, if it does not work try to set path
try:
    import anlp
except ImportError:
    sys.path.append('../../')

import math

import numpy
import scipy.optimize as sci_opt
import scipy.integrate as sci_int

import anlp.util as util
import anlp.eospy as eos
import anlp.opapy as opa

import anlp.probs.putil as putil

# program description
desc = 'Solve Radiative Shock problem in LTE'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author, license)

# restrict from * imports
__all__ = ['prob_key', 'prob_desc', 'prob_ref', 'prob_dict',
           'solve', 'desc', 'author', 'license', 'eplg', ]

# problem description
prob_key = 'rs_eqdiff'
prob_desc = 'Radiative Shock in the equilibrium diffusion limit'
prob_ref = '''
\"Radiative shock solutions in the equilibrium diffusion limit\",
R. B. Lowrie, R. M. Rauenzahn,
Shock Waves (2007)
dx.doi.org/10.1007/s00193-007-0081-2
'''

# solver for specific problems


def solve(args):
    '''
    Solver for the Radiative Shock with equilibrium diffusion problem. 
    '''
    if args.eos == 'glaw':
        data = solve_glaw(args)
    else:
        data = solve_real(args)

    count = 0

    print()
    print('# colum_{:03d} = position'.format(count))
    count += 1
    print('# colum_{:03d} = density'.format(count))
    count += 1
    print('# colum_{:03d} = temperature'.format(count))
    count += 1
    print('# colum_{:03d} = velocity'.format(count))
    count += 1
    print()

    for xi in data[:]:
        for xij in xi:
            print(' {:17.9e}'.format(xij), end='')
        print()
    
    return data


def solve_glaw(args):
    '''
    Solver for the Radiative Shock with equilibrium diffusion problem 
    using a gamma-law EOS model. The parameters needed must be passed 
    by the namespace args:
            gamma: EOS gamma coefficient
            M0: pre-shock Mach number
            P0: hydro-rad coupling coeffcient
            opa: opacity model
    '''
    # define parameters
    g = args.gamma
    p0 = args.P0
    m0 = args.M0
    kappa = opa.select_opa_model(args)
    # pre-shock properties
    r0 = 1.0
    t0 = 1.0
    v0 = m0
    # post-shock temperature estimation
    t1_0 = tem_for_P0_null(g, m0)
    t1_inf = tem_for_P0_inf(p0, m0)
    # post-shock properties: root finding
    t1 = sci_opt.newton(ojump, t1_0, fprime=ojump_drv,
                        args=(g, p0, m0), maxiter=1000)
    r1 = rho1(t1, g, p0)
    v1 = r0 * m0 / r1
    m1 = v1 * r1
    # isothermal shock conditions
    nu_1 = 1.0 / r1
    nu_max_zr = 1.0 / (4.0 + math.sqrt(2.0 + (g + 1.0) / (g - 1.0)))
    nu_max = sci_opt.newton(
        fcond,
        nu_max_zr,
        fprime=fcond_drv,
        args=(g, p0, m0))
    r_limit = 1.0 / nu_max
    # precursor porperties
    tp = t1
    rp = rhop(tp, g, p0, m0)
    vp = m0 / rp
    mp = vp / math.sqrt(tp)
    # radiative precursor profile: ode integration
    nx = 500
    x_begin = 0.0
    x_end = -0.25
    x = numpy.linspace(x_begin, x_end, nx)
    if nu_1 > nu_max:
        t_begin = 0.5 * (tp + t0)
        nxp = 500
        xp_begin = 0.0
        xp_end = 0.25
        xp = numpy.linspace(xp_begin, xp_end, nx)
        tp_init = numpy.array([t_begin])
        txp = sci_int.odeint(
            odefunc,
            tp_init,
            xp,
            args=(g, p0, m0, kappa),
            Dfun=odefunc_drv)
    else:
        t_begin = tp
        nxp = 2
        xp_begin = 0.0
        xp_end = 0.25
        xp = numpy.linspace(xp_begin, xp_end, nx)
        txp = numpy.linspace(t_begin, t_begin, nx)

    t_init = numpy.array([t_begin])
    tx = sci_int.odeint(
        odefunc,
        t_init,
        x,
        args=(g, p0, m0, kappa),
        Dfun=odefunc_drv)

    ans = []
    for i in range(len(x) - 1, 0, -1):
        r = rhop(txp[i], g, p0, m0)
        v = r0 * m0 / r
        ans.append([xp[i], r, txp[i], v])
    for i in range(0, len(x), 1):
        r = rhop(tx[i], g, p0, m0)
        v = r0 * m0 / r
        ans.append([x[i], r, tx[i], v])
    # zel'dovich spike
    if nu_1 > nu_max:  # isothermal shock
        rs = rhos(rp, g, mp)
        vs = m0 / rs
        ts = tems(tp, g, mp)
    else:			# no shock, continuous state
        rs = r1
        vs = v1
        ts = t1
    print('# Pre-shock state  :: r0 = {:12e}  t0 = {:12e}  v0 = {:12e}  m0 = {:12e}'.
          format(r0, t0, v0, m0))
    print('# Post-shock state :: r1 = {:12e}  t1 = {:12e}  v1 = {:12e}  m1 = {:12e}'.
          format(r1, t1, v1, m1))
    print('# Precursor state  :: rp = {:12e}  tp = {:12e}  vp = {:12e}  mp = {:12e}'.
          format(rp, tp, vp, mp))
    return numpy.array(ans)


def solve_real(args):
    '''
    Solver for the Radiative Shock with equilibrium diffusion problem 
    using a general EOS model. The parameters needed must be passed by 
    the namespace args:
            gamma: EOS gamma coefficient (for some initial approximations)
            M0: pre-shock Mach number
            P0: hydro-rad coupling coeffcient
            opa: opacity model
            eos: EOS model
    '''
    # define parameters
    gamma = args.gamma
    p0 = args.P0
    m0 = args.M0
    kappa = opa.select_opa_model(args)
    geteos = eos.select_eos_model(args)
    geteos_ene = geteos[0]
    geteos_pre = geteos[1]
    geteos_cs = geteos[2]
    # pre-shock conditions
    r0 = 1.0
    t0 = 1.0
    v0 = m0
    # post-shock temperature estimation using gamma-law EOS
    gamma = 1.666
    t1_0 = tem_for_P0_null(gamma, m0)
    r1_0 = rho1(t1_0, gamma, p0)
    t1_inf = tem_for_P0_inf(p0, m0)
    # post-shock conditions: root finding
    ans = sci_opt.root(
        jfunc,
        [r1_0, t1_0],
        jac=jfunc_drv,
        args=(r0, t0, p0, m0, geteos))
    if not ans.success:
        raise ProbError('root findind not succeded')
    r1 = ans.x[0]
    t1 = ans.x[1]
    v1 = r0 * m0 / r1
    m1 = v1 * r1
    # isothermal shock conditions
    nu_1 = 1.0 / r1
    nu_max_zr = 1.0 / (4.0 + math.sqrt(2.0 + (gamma + 1.0) / (gamma - 1.0)))
    # precursor properties
    tp = t1
    rp = momcons_rho(tp, r0, geteos_pre(r0, t0), p0, m0)
    vp = m0 / rp
    mp = vp * geteos_cs(r0, t0) / geteos_cs(rp, tp)
    # precursor profile
    if rp < r1:  # there is isothermal shock
        t_begin = tp
        nxp = 2
        xp_begin = 0.0
        xp_end = 0.25
        xp = numpy.linspace(xp_begin, xp_end, nxp)
        txp = numpy.linspace(t_begin, t_begin, nxp)
    else:			# no shock, variables are continuos
        t_begin = 0.5 * (tp + t0)
        nxp = 500
        xp_begin = 0.0
        xp_end = 0.25
        xp = numpy.linspace(xp_begin, xp_end, nxp)
        t_init = numpy.array([t_begin])
        txp = sci_int.odeint(
            pfunc,
            t_init,
            xp,
            args=(r0, t0, p0, m0, kappa, geteos),
            Dfun=pfunc_drv)
    nx = 500
    x_begin = 0.0
    x_end = -0.25
    x = numpy.linspace(x_begin, x_end, nx)
    t_init = numpy.array([t_begin])
    tx = sci_int.odeint(
        pfunc,
        t_init,
        x,
        args=(r0, t0, p0, m0, kappa, geteos),
        Dfun=pfunc_drv)
    # precursor profile
    ans = []
    for i in range(len(x) - 1, 0, -1):
        r = momcons_rho(txp[i], r0, p0, param0, m0)
        v = r0 * m0 / r
        ans.append([xp[i], r, txp[i], v])
    for i in range(0, len(x), 1):
        r = momcons_rho(tx[i], r0, p0, param0, m0)
        v = r0 * m0 / r
        ans.append([x[i], r, tx[i], v])
    # zel'dovich spike
    print('# Pre-shock state  :: r0 = {:12e}  t0 = {:12e}  v0 = {:12e}  m0 = {:12e}'.
          format(r0, t0, v0, m0))
    print('# Post-shock state :: r1 = {:12e}  t1 = {:12e}  v1 = {:12e}  m1 = {:12e}'.
          format(r1, t1, v1, m1))
    print('# Precursor state  :: rp = {:12e}  tp = {:12e}  vp = {:12e}  mp = {:12e}'.
          format(rp, tp, vp, mp))
    return numpy.array(ans)

# auxiliar functions for gamma-law problem


def m(t, g, p0, m0):
    return 0.5 * (g * m0 * m0 + 1.0) + g * p0 * (1.0 - t * t * t * t) / 6.0


def m_drv(t, g, p0, m0):
    return -4.0 * g * p0 * t * t * t / 6.0


def rhop(t, g, p0, m0):
    mr = m(t, g, p0, m0)
    disc = mr * mr - g * t * m0 * m0
    if disc < 0.0:
        raise ProbError('fail trying to compute precursor density!!!')
    return (mr - math.sqrt(disc)) / t


def rhop_drv(t, g, p0, m0):
    m02 = m0 * m0
    mr = m(t, g, p0, m0)
    dmr = m_drv(t, g, p0, m0)
    disc = mr * mr - g * t * m0 * m0
    if disc < 0.0:
        raise ProbError(
            'fail trying to compute precursor density derivative!!!')
    return (dmr - (mr * dmr - 0.5 * g * m02) / math.sqrt(disc)) / t - rhop(t, g, p0, m0) / t


def f1(t, g, p0):
    return 3.0 * (g + 1.0) * (t - 1.0) - p0 * g * (g - 1.0) * (7.0 + t * t * t * t)


def f1_drv(t, g, p0):
    return 3.0 * (g + 1.0) - 4.0 * p0 * g * (g - 1.0) * t * t * t


def f2(t, g, p0):
    return 12.0 * (g - 1.0) * (g - 1.0) * t * (3.0 + g * p0 * (1.0 + 7.0 * t * t * t * t))


def f2_drv(t, g, p0):
    return 12.0 * (g - 1.0) * (g - 1.0) * (3.0 + g * p0 + 35.0 * p0 * g * t * t * t * t)


def f3(t, r, g, p0, m0):
    r2 = r * r
    return 6.0 * r2 * (t - 1.0) / (g - 1.0) + 3.0 * (1.0 - r2) * m0 * m0 + \
        8.0 * p0 * (t * t * t * t - r) * r


def f3_drv(t, r, g, p0, m0):
    dr = rhop_drv(t, g, p0, m0)
    return (12.0 * r * (t - 1) / (g - 1.0)) * dr + \
        6.0 * r * r / (g - 1.0) - \
        6.0 * r * m0 * m0 * dr + \
        8.0 * p0 * (dr * (t * t * t * t - r) + r * (4.0 * t * t * t - dr))


def rho1(t, g, p0):
    x1 = f1(t, g, p0)
    x2 = f2(t, g, p0)
    return (x1 + math.sqrt(x1 * x1 + x2)) / (6.0 * (g - 1.0) * t)


def rho1_drv(t, g, p0):
    x1 = f1(t, g, p0)
    x2 = f2(t, g, p0)
    dx1 = f1_drv(t, g, p0)
    dx2 = f2_drv(t, g, p0)
    # x1*x1 + x2 always positive defined, no error exception checking
    disc = math.sqrt(x1 * x1 + x2)
    return (dx1 + (x1 * dx1 + 0.5 * dx2) / (disc)) / (6.0 * (g - 1.0) * t) - \
           (x1 + disc) / (6.0 * (g - 1.0) * t * t)


def rhos(rp, g, mp):
    mp2 = mp * mp
    return rp * ((g + 1.0) * mp2) / (2.0 + (g - 1.0) * mp2)


def tems(tp, g, mp):
    mp2 = mp * mp
    return tp * (1.0 - g + 2.0 * g * mp2) * (2.0 + (g - 1.0) * mp2) / ((g + 1.0) * (g + 1.0) * mp2)


def tem_for_P0_null(g, m0):
    m02 = m0 * m0
    return (1.0 - g + 2.0 * g * m02) * (2.0 + (g - 1.0) * m02) / ((g + 1.0) * (g + 1.0) * m02)


def tem_for_P0_inf(p0, m0):
    a0star = math.sqrt(4.0 * p0 / 9.0)
    m0star = m0 / a0star
    return math.pow((8.0 * m0star * m0star - 1.0) / 7.0, 0.25)
# function to be solved for the gamma-law EOS model


def ojump(t, g, p0, m0):
    '''
    Overall jump condition equation
    '''
    r1 = rho1(t, g, p0)
    return 3.0 * r1 * (r1 * t - 1.0) + g * p0 * r1 * (t * t * t * t - 1.0) - 3.0 * g * (r1 - 1.0) * m0 * m0


def ojump_drv(t, g, p0, m0):
    '''
    Overall jump condition derivative
    '''
    r = rho1(t, g, p0)
    dr = rho1_drv(t, g, p0)
    return 3.0 * (2.0 * t * r * dr + r * r - dr * dr) + \
        g * p0 * (t * t * t * (t * dr + 4.0 * r) - dr) + \
        3.0 * g * m0 * m0 * dr


def odefunc(t, x, g, p0, m0, kappa):
    '''
    Precursor temperature ODE system
    '''
    r = rhop(t, g, p0, m0)
    return m0 * f3(t, r, g, p0, m0) / (24.0 * kappa(r, t) * r * r * t * t * t)


def odefunc_drv(t, x, g, p0, m0, kappa):
    '''
    Precursor temperature jacobian
    '''
    r = rhop(t, g, p0, m0)
    r_dt = rhop_drv(t, g, p0, m0)
    k = kappa(r, t)
    k_dr = kappa(r, t, ['dr'])
    k_dt = kappa(r, t, ['dt'])
    f = f3(t, r, g, p0, m0)
    f_dt = f3_drv(t, r, g, p0, m0)
    dnm = 24.0 * k * r * r * t * t * t
    return (m0 * f_dt) / (dnm) - \
           (m0 * f) * (
        24.0 * (k_dr * r_dt + k_dt) * r * r * t * t * t +
        48.0 * k * r * r_dt * t * t * t +
        72.0 * k * r * r * t * t
    ) / (math.pow(dnm, 2.0))


def fcond(nu, g, p0, m0):
    alpha = g * p0 / 3.0
    a0 = 1.0 + alpha + g * m0 * m0
    a1 = -2.0 * g * m0 * m0
    a8 = -alpha * g * g * g * g
    return a0 + a1 * nu + a8 * math.pow(m0 * nu, 8.0)


def fcond_drv(nu, g, p0, m0):
    alpha = g * p0 / 3.0
    a0 = 1.0 + alpha + g * m0 * m0
    a1 = -2.0 * g * m0 * m0
    a8 = -alpha * g * g * g * g * math.pow(m0, 8.0)
    return a1 + 8.0 * a8 * math.pow(nu, 7.0)


def frho(t, r, g, p0, m0):
    return rhop(t, g, p0, m0) - r


def frho_drv(t, r, g, p0, m0):
    return rhop_drv(t, g, p0, m0)

# auxiliar functions for real EOS model


def jump1(r, p, e, r0, p0, e0):
    return (e - e0) + 0.5 * (p + p0) * (1.0 / r - 1.0 / r0)


def jump1_dr(r, p, e, r0, p0, e0, dp_dr, de_dr):
    return de_dr + 0.5 * (dp_dr * (1.0 / r - 1.0 / r0) - (p0 + p) / (r * r))


def jump1_dt(r, p, e, r0, p0, e0, dp_dt, de_dt):
    return de_dt + 0.5 * dp_dt * (1.0 / r - 1.0 / r0)


def jump2(r, p, r0, p0, m0):
    return (p - p0) / (r - r0) - m0 * m0 * (r0 / r)


def jump2_dr(r, p, r0, p0, dp_dr, m0):
    delta_r = r - r0
    return (dp_dr - (p - p0) / delta_r) / delta_r + m0 * m0 * r0 / (r * r)


def jump2_dt(r, p, r0, p0, dp_dt, m0):
    return dp_dt / (r - r0)


def jfunc(x, r0, t0, param0, m0, get_eos=eos.eos_models['real']):
    r = x[0]
    t = x[1]
    eos_get_ene = get_eos[0]
    eos_get_pre = get_eos[1]
    # jump conditions system of equations
    t04 = t0 * t0 * t0 * t0
    t4 = t * t * t * t
    p0 = eos_get_pre(r0, t0) + param0 * t04 / 3.0
    e0 = eos_get_ene(r0, t0) + param0 * t04 / r0
    p = eos_get_pre(r, t) + param0 * t4 / 3.0
    e = eos_get_ene(r, t) + param0 * t4 / r
    return [jump1(r, p, e, r0, p0, e0), jump2(r, p, r0, p0, m0)]


def jfunc_drv(x, r0, t0, param0, m0, get_eos=eos.eos_models['real']):
    r = x[0]
    t = x[1]
    eos_get_ene = get_eos[0]
    eos_get_pre = get_eos[1]
    # jacobian of the jump conditions
    t04 = t0 * t0 * t0 * t0
    t3 = t * t * t
    t4 = t3 * t
    p0 = eos_get_pre(r0, t0) + param0 * t04 / 3.0
    e0 = eos_get_ene(r0, t0) + param0 * t04 / r0
    p = eos_get_pre(r, t) + param0 * t4 / 3.0
    e = eos_get_ene(r, t) + param0 * t4 / r
    dp_dr = eos_get_pre(r, t, 'dr')
    de_dr = eos_get_ene(r, t, 'dr') - param0 * t4 / (r * r)
    dp_dt = eos_get_pre(r, t, 'dt') + 4.0 * param0 * t3 / 3.0
    de_dt = eos_get_ene(r, t, 'dt') + 4.0 * param0 * t3 / r
    return [[jump1_dr(r, p, e, r0, p0, e0, dp_dr, de_dr),
             jump1_dt(r, p, e, r0, p0, e0, dp_dt, de_dt)],
            [jump2_dr(r, p, r0, p0, dp_dr, m0),
             jump2_dt(r, p, r0, p0, dp_dt, m0)]]


def mfunc(r, t, p0, param0, m0, get_eos=eos.eos_models['real']):
    # momentum conservation function
    eos_get_pre = get_eos[1]
    p = eos_get_pre(r, t)
    return p - p0 - m0 * m0 * (1.0 - 1.0 / r) - param0 * (1.0 - t * t * t * t) / 3.0


def mfunc_drv(r, t, p0, param0, m0, get_eos=eos.eos_models['real']):
    # derivative of the momentum conservation function
    eos_get_pre = get_eos[1]
    p_dr = eos_get_pre(r, t, 'dr')
    return p_dr - m0 * m0 / (r * r)


def momcons_rho(t, r0, p0, param0, m0):
    r_init = r0
    return sci_opt.newton(
        mfunc,
        r_init,
        fprime=mfunc_drv,
        args=(t, p0, param0, m0))


def pfunc(t, x, r0, t0, param0, m0, kappa, get_eos=eos.eos_models['real']):
    # EOS functions
    eos_get_ene = get_eos[0]
    eos_get_pre = get_eos[1]
    # auxiliar variables
    t3 = t * t * t
    t4 = t3 * t
    p0 = eos_get_pre(r0, t0)
    e0 = eos_get_ene(r0, t0)
    r = momcons_rho(t, r0, p0, param0, m0)
    k = kappa(r, t)
    p = eos_get_pre(r, t)
    e = eos_get_ene(r, t)
    # precursor ode functions
    # using RHS formula from eq. 20 of the main reference
    v = m0 / r
    rhs20_1 = (r * e + p + 0.5 * r * v * v + 4.0 *
               param0 * t * t * t * t / 3.0) * v
    rhs20_2 = (e0 + p0 + 0.5 * m0 * m0 + 4.0 * param0 / 3.0) * m0
    return (rhs20_1 - rhs20_2) / (4.0 * k * t3)


def pfunc_drv(t, x, r0, t0, param0, m0, kappa, get_eos=eos.eos_models['real']):
    # EOS functions
    eos_get_ene = get_eos[0]
    eos_get_pre = get_eos[1]
    # auxiliar variables
    m02 = m0 * m0
    t2 = t * t
    t3 = t2 * t
    t4 = t3 * t
    p0 = eos_get_pre(r0, t0)
    e0 = eos_get_ene(r0, t0)
    r = momcons_rho(t, r0, p0, param0, m0)
    r2 = r * r
    p = eos_get_pre(r, t)
    p_dr = eos_get_pre(r, t, 'dr')
    p_dt = eos_get_pre(r, t, 'dt')
    e = eos_get_ene(r, t)
    e_dr = eos_get_ene(r, t, 'dr')
    e_dt = eos_get_ene(r, t, 'dt')
    k = kappa(r, t)
    k_dr = kappa(r, t, 'dr')
    k_dt = kappa(r, t, 'dt')
    r_dt = (p_dt + 4.0 * param0 * t3) / (m02 / r2 - p_dr)
    v = m0 / r
    v_dr = (-m0 / r2)
    # prefursor ode function derivative
    a = 4.0 * k * t3
    bstar = (r * e + p + 0.5 * r * v * v + 4.0 * param0 * t4 / 3.0) * v
    b0 = (e0 + p0 + 0.5 * m02 + 4.0 * param0 / 3.0) * m0
    b = bstar - b0
    da = 4.0 * (k_dr * r_dt + k_dt) * t3 + 12.0 * k * t2
    db = (
        e * r_dt + r * (e_dr * r_dt + e_dt) +
        (p_dr * r_dt + p_dt) +
        0.5 * v * v * r_dt + r * v * v_dr * r_dt +
        16.0 * param0 * t3 / 3.0
    ) * v + (bstar / v) * v_dr * r_dt
    return db / a - da * b / (a * a)

prob_dict = {
    'key': prob_key,
    'desc': prob_desc,
    'ref': prob_ref,
    'solve': solve,
}


def main():
    util.printStandAloneMode(__file__)
    putil.printProbInfo(prob_dict)
    return

if __name__ == '__main__':
    sys.exit(main())
