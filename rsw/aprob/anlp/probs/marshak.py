

#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Solve Marshak wave problem
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

# try to impor anlp, if it does not work try to set path
try:
	import anlp
except ImportError:
	sys.path.append('../../')

import anlp.util as util
import anlp.eospy as eos
import anlp.opapy as opa

import anlp.probs.putil as putil

# program description
desc = 'Solve Marshak wave problem'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# from* imports
__all__ = ['prob_key','prob_desc','prob_ref','prob_dict','solve']

# problem description
prob_key = 'marshak'
prob_desc = 'Marshak Wave'
prob_ref = '''
\"High Energy Density Physics: fundamentals, Inertial Fusion, and Experimental Astrophysics\"
P. Drake,
Springer, Shock Wave and High Pressure Phenomena (2006)
ISBN-10: 3-540-29314-0
ISBN-13: 978-3-540-29314-9
'''

# solve this problem
def solve(args):
	'''
	Solver for the Marshak problem. The parameters needed must be passed 
	by the namespace args:
		M0 : pre-shock conditions
	'''
	# auxiliar functions
	# parameters
	g = args.gamma
	return 0

prob_dict = { 
	'key': prob_key,
	'desc': prob_desc,
	'ref': prob_ref,
	'solve': solve,
	}

def main():
	util.printStandAloneMode(__file__)
	putil.printProbInfo(prob_dict)
	return

if __name__=='__main__' :
	sys.exit(main())
