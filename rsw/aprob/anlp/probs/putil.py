
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Auxiliar functions to solve problems
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

# try to impor anlp, if it does not work try to set path
try:
	import anlp
except ImportError:
	sys.path.append('../../')

import anlp.util as util

# program description
desc = 'Auxiliar functions to solve problems'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

coorsys_coeff = { 'cart': 0.0,
		  'cyl': 1.0,
		  'sph': 2.0,
		  }

# problem dependent error while getting the solution
class ProbError(util.Error):
	'''
	Problem dependent exception class
	'''
	pass

class ProbWarn(util.Warn):
	'''
	Problem dependent exception class
	'''
	pass

def printProbInfo(dct):
	print('')
	printFromDict(dct,'problem key','key')
	printFromDict(dct,'problem description','desc')
	printFromDict(dct,'problem reference','ref')
	print('')
	return

def printFromDict(dct,name,key):
	try:
		print('{:20} :: {}'.format(name,dct[key]))
	except KeyError:
		pass
	return

def strformat(len=1,base_format=' {:14.6e}'):
	if len <= 0:
		raise ProbError('format for numerical values needs at least one value')
	return len*base_format

def main():
	util.printStanAloneMode(__file__)
	return 0

if __name__=='__main__':
	sys.exit(main())
