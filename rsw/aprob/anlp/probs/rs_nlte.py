
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Module to solve a radiative shock with grey nonequilibrium diffusion
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import bisect

# try to impor anlp, if it does not work try to set path
try:
    import anlp
except ImportError:
    sys.path.append('../../')

import numpy as np

import scipy.optimize
import scipy.integrate

import anlp.util as util
import anlp.eospy as eos
import anlp.opapy as opa

import anlp.probs.putil as putil
import anlp.probs.rs_lte as rs_lte

# program description
desc = 'Module to solve a radiative shock with grey nonequilibrium diffusion'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author, license)

# restrict from * imports
__all__ = ['prob_key', 'prob_desc', 'prob_ref', 'prob_dict',
           'solve', 'desc', 'author', 'license', 'eplg', ]

# problem description
prob_key = 'rs_grey_neqdiff'
prob_desc = 'Radiative Shock with grey nonequilibrium diffusion'
prob_ref = '''
\"Radiative shock solutions with grey nonequilibrium diffusion\",
R. B. Lowrie, J. D. Edwards,
Shock Waves (2008)
dx.doi.org/10.1007/s00193-008-0143-0
'''

# solve this problem


def solve(args):
    '''
    Solver for the Radiative Shock with grey nonequilibrium diffusion 
    problem. 
    '''
    if args.eos == 'glaw':
        data = solve_glaw(args)
    else:
        data = solve_real(args)

    count = 0

    print()
    print('# colum_{:03d} = position'.format(count))
    count += 1
    print('# colum_{:03d} = density'.format(count))
    count += 1
    print('# colum_{:03d} = temperature'.format(count))
    count += 1
    print('# colum_{:03d} = velocity'.format(count))
    count += 1
    print()

    for xi in data[:]:
        for xij in xi:
            print(' {:17.9e}'.format(xij), end='')
        print()

    return data


def solve_glaw(args):
    '''
    Solver for the Radiative Shock with grey nonequilibrium diffusion 
    problem using a gamma-law EOS model. The parameters needed must be 
    passed by the namespace args:
            gamma: EOS gamma coefficient (for some initial approximations)
            M0: pre-shock Mach number
            P0: hydro-rad coupling coeffcient
            opa: opacity model
            eos: EOS model
    '''
    # A) set parameters
    # A.1 parameters
    g = args.gamma
    p0 = args.P0
    m0 = args.M0
    #kappa = opa.select_opa_model(args)
    # sa    = 1.0 # absorption XS
    kappa = 1.0
    sa = 1.0e+06
    # A.2 initial state
    r0 = 1.0  # density
    t0 = 1.0  # material temperature
    # radiation temperature (same as material temperature, equilibrium)
    th0 = t0
    v0 = m0  # material velocity, same as pre-shock Mach number

    # B) overall jump conditions
    # B.1 post_shock temperature estimation
    t1_0 = rs_lte.tem_for_P0_null(g, m0)
    t1_inf = rs_lte.tem_for_P0_inf(p0, m0)
    # B.2 post-shock properties: root finding
    try:
        t1 = scipy.optimize.newton(
            rs_lte.ojump, t1_0, fprime=rs_lte.ojump_drv, args=(g, p0, m0))
    except RuntimeError:
        print('scipy.optimize.newton :: problems of convergence, increasing maximum number of iterations')
        t1 = scipy.optimize.newton(
            rs_lte.ojump, t1_0, fprime=rs_lte.ojump_drv, args=(g, p0, m0), maxiter=200)
    # B.3 post-shock state
    r1 = rs_lte.rho1(t1, g, p0)
    th1 = t1
    v1 = m0 / r1
    m1 = v1 / np.sqrt(t1)

    # C) hydrodynamic jump conditions: compute precursor and relaxation
    #    region
    # C.1 get the adiabatic sonic point, ASP
    masp = 1.0
    try:
        #tasp = scipy.optimize.newton(nzeta_asp,t0,fprime=nzeta_asp_drv,args=(g,m0,p0,kappa,sa))
        tasp = scipy.optimize.newton(
            nzeta_asp, t0, args=(g, m0, p0, kappa, sa))
    except RuntimeError:
        print('scipy.optimize.newton :: problems of convergence, increasing maximum number of iterations')
        #tasp = scipy.optimize.newton(nzeta_asp,t0,fprime=nzeta_asp_drv,args=(g,m0,p0,kappa,sa),maxiter=200)
        tasp = scipy.optimize.newton(
            nzeta_asp, t0, args=(g, m0, p0, kappa, sa), maxiter=200)
    vasp = masp * np.sqrt(tasp)
    rasp = m0 / vasp
    thasp = thetamt(masp, tasp, g, m0, p0)
    eps = 1.0e-06
    # C.4 integrate the precursor region
    wp, fp = integrate_precursor(
        t0, th0, r0, g, m0, p0, kappa, sa, eps, nstep=100)
    # C.5 integrate the relaxiation region
    wr, fr = integrate_relaxation(
        t1, th1, r1, g, m0, p0, kappa, sa, eps, nstep=100)

    # D) check condition for ISP
    misp = 1.0 / np.sqrt(g)
    # D.1 guess for the ISP
    #     low nu approximation seems to get closer results to the real one
    n0 = fisp_for_nu_low(g, m0, p0)
    ntay = fisp_taylor(g, m0, p0)
    # D.2 solve the density equation for the ISP
    try:
        nisp = scipy.optimize.newton(
            fisp, n0, fprime=fisp_drv, args=(g, m0, p0))
    except RuntimeError:
        print('scipy.optimize.newton :: problems of convergence, increasing maximum number of iterations')
        nisp = scipy.optimize.newton(
            fisp, n0, fprime=fisp_drv, args=(g, m0, p0), maxiter=200)
    # D.3 get the full IPS state
    risp = 1.0 / nisp
    tisp = g * nisp * nisp * m0 * m0
    thisp = thetamt(misp, tisp, g, m0, p0)
    visp = m0 / (risp * np.sqrt(tisp))

    # E) connect precursor and relaxation regions
    is_shock = False
    try:
        # E.1 continuous case
        (w, f) = join_continuous(wp, fp, wr, fr, tasp)
    except putil.ProbError:
        is_shock = True
        # E.2 embedded shock
        (w, f) = join_embedded(wp, fp, wr, fr, g, m0, p0, kappa, sa)
        # E.3 hydrodynamic shock states, shock at x=0.0
        (thp, tp, rp) = state_prec(w, f, pos=0.0)
        vp = m0 / rp
        mp = vp / np.sqrt(tp)
        (thr, tr, rr) = state_relax(w, f, pos=0.0)
        vr = m0 / rr
        mr = vr / np.sqrt(tr)
        # E.4 check hydrodynamic shock conditions
        err = jump_hydroshock(thp, tp, rp, thr, tr, rr,
                              f, g, m0, p0, kappa, sa)
    '''
	is_shock = check_shock(wp,fp,wr,fr,g,m0,p0,kappa,sa)
	if is_shock:
		print('# RS_NLTE :: there is a shock')
		# E.1 embedded shock
		# E.1.a locate shock and join precursor and relaxation regions
		(w,f) = join_embedded(wp,fp,wr,fr,g,m0,p0,kappa,sa)
		# E.1.b hydrodynamic shock states, shock at x=0.0
		(thp,tp,rp) = state_prec(w,f,pos=0.0)
		vp = m0/rp
		mp = vp/np.sqrt(tp)
		(thr,tr,rr) = state_relax(w,f,pos=0.0)
		vr = m0/rr
		mr = vr/np.sqrt(tr)
		# E.1.c check hydrodynamic shock conditions
		err = jump_hydroshock(thp,tp,rp,thr,tr,rr,f,g,m0,p0,kappa,sa)
	else:
		print('# RS_NLTE :: there is an ASP, continuous solution')
		# E.2 continuous case
		(w,f) = join_continuous(wp,fp,wr,fr,tasp)
	'''

    # F) print output
    # F.1 print singular states
    print('# Pre-shock state  :: r = {:12.5e}  t = {:12.5e}  th = {:12.5e}  v = {:12.5e}  m = {:12.5e}'.
          format(r0, t0, th0, v0, m0))
    print('# Post-shock state :: r = {:12.5e}  t = {:12.5e}  th = {:12.5e}  v = {:12.5e}  m = {:12.5e}'.
          format(r1, t1, th1, v1, m1))
    print('# ISP state        :: r = {:12.5e}  t = {:12.5e}  th = {:12.5e}  v = {:12.5e}  m = {:12.5e}'.
          format(risp, tisp, thisp, visp, misp))
    print('# ASP state        :: r = {:12.5e}  t = {:12.5e}  th = {:12.5e}  v = {:12.5e}  m = {:12.5e}'.
          format(rasp, tasp, thasp, vasp, masp))
    if is_shock:
        print('# Precursor state  :: r = {:12.5e}  t = {:12.5e}  th = {:12.5e}  v = {:12.5e}  m = {:12.5e}'.
              format(rp, tp, thp, vp, mp))
        print('# Relaxation state :: r = {:12.5e}  t = {:12.5e}  th = {:12.5e}  v = {:12.5e}  m = {:12.5e}'.
              format(rr, tr, thr, vr, mr))
        print('# Error in hydro jump conditions ::')
        print('#   - mass eq.            err = {:13.6e}'.format(err[0]))
        print('#   - momentum eq.        err = {:13.6e}'.format(err[1]))
        print('#   - energy eq.          err = {:13.6e}'.format(err[2]))
        print('#   - radtiation flux eq. err = {:13.6e}'.format(err[3]))
    # F.2 print profiles
    print()
    for wi, fi in zip(w, f):
        print(putil.strformat(4).format(wi, fi[0], fi[1], fi[2]))
    return


def solve_real(args):
    '''
    Solver for the Radiative Shock with grey nonequilibrium diffusion 
    problem using a general EOS model. The parameters needed must be 
    passed by the namespace args:
            gamma: EOS gamma coefficient (for some initial approximations)
            M0: pre-shock Mach number
            P0: hydro-rad coupling coeffcient
            opa: opacity model
            eos: EOS model
    '''
    print('developement :: this option is not implemented yet, try to use the glaw EOS model instead the real EOS model')
    return


def integrate_precursor(t0, th0, r0, g, m0, p0, kappa, sa, eps=1.0e-06, nstep=200, eta=1.0e-03):
    '''
    Integrate ODE system for the prescursor, setting epsión state at 
    x = 0 and then computing x0 state as initial point
    '''
    # assume equilibrium in pre-shock state, t = th
    theps = th0 + eps
    dtth = dtdth(t0, th0, g, m0, p0, kappa, sa, t0, th0, r0)
    if dtth < 0.0:
        print('# ODE precursor :: warning!!! dt/dth < 0.0, it must be positive, using abs value')
        #dtth = np.fabs(dtth)
    teps = max(t0, t0 + eps * dtth)
    xeps = 0.0
    # get eps state
    reps = rhotth_minus(teps, theps, g, m0, p0)
    veps = m0 / reps
    meps = veps / np.sqrt(teps)
    print('# ODE precursor :: eps state, xeps = {}  teps = {}  theps = {}  reps = {}  veps = {}   meps = {}'.format(
        xeps, teps, theps, reps, veps, meps))
    # get initial conditions point
    thprime = dthdx(teps, theps, reps, veps, g, m0, p0, kappa)
    x0 = xeps - eps / thprime
    # add pre-shock state
    w = [np.array([x0, ]), ]
    f = [np.array([np.array([th0, t0, r0, ]), ]), ]
    # estimate ode step, empiric
    delta = np.fabs(eta) * np.sqrt(m0 - 1.0)
    # integrate precursor
    x_begin = xeps
    x_end = x_begin + delta
    f_init = np.array([theps, teps, reps, ])
    w.append(np.linspace(x_begin, x_end, nstep))
    try:
        f.append(scipy.integrate.odeint(odepr_thtr, f_init,
                                        w[-1], args=(g, m0, p0, kappa, sa)))
    except RuntimeError:
        print('# scipy.integrate.odeint :: problems integration precursor ODE system')
    # check if the integration have run till the end
    while not converg_odepr_prec(w[-1], f[-1]):
        x_begin = x_end
        x_end = x_begin + delta
        f_init = f[-1][-1]
        w.append(np.linspace(x_begin, x_end, nstep))
        try:
            f.append(scipy.integrate.odeint(odepr_thtr, f_init,
                                            w[-1], args=(g, m0, p0, kappa, sa)))
        except RuntimeError:
            print('# scipy.integrate.odeint :: problems integration precursor ODE system')
    w_clean, f_clean = clean_prec(w, f)
    if len(w_clean) == 0 or len(f_clean) == 0:
        raise putil.ProbError('ODE precursor :: missing solution!!!')
    return w_clean, f_clean


def integrate_relaxation(t1, th1, r1, g, m0, p0, kappa, sa, eps=1.0e-06, nstep=200, eta=1.0e-03):
    '''
    Integrate ODE system for the prescursor, setting epsión state at 
    x = 0 and then computing x0 state as initial point
    '''
    # assume equilibrium in pre-shock state, t = th
    theps = th1 - eps
    dtth = dtdth(t1, th1, g, m0, p0, kappa, sa, t1, th1, r1)
    if dtth < 0.0:
        print('# ODE relaxation :: warning!!! dt/dth < 0.0, it must be positive, using abs value')
        dtth = np.fabs(dtth)
    teps = t1 - eps * dtth
    xeps = 0.0
    # get eps state
    reps = rhotth(teps, theps, g, m0, p0, t1, th1, r1)
    veps = m0 / reps
    meps = veps / np.sqrt(teps)
    print('# ODE relaxation :: eps state, xeps = {}  teps = {}  theps = {}  reps = {}  veps = {}   meps = {}'.format(
        xeps, teps, theps, reps, veps, meps))
    # get initial conditions point
    thprime = dthdx(teps, theps, reps, veps, g, m0, p0, kappa)
    x1 = xeps - eps / thprime
    # add post-shock state
    w = [np.array([x1, ]), ]
    f = [np.array([np.array([th1, t1, r1, ]), ]), ]
    # estimate ode step, empiric
    delta = -np.fabs(eta) * (m0 - 1.0)
    # integrate precursor
    x_begin = xeps
    x_end = x_begin + delta
    f_init = np.array([theps, teps, reps, ])
    w.append(np.linspace(x_begin, x_end, nstep))
    try:
        f.append(scipy.integrate.odeint(odepr_thtr, f_init,
                                        w[-1], args=(g, m0, p0, kappa, sa)))
    except RuntimeError:
        print(
            '# scipy.integrate.odeint :: problems integration relaxation region ODE system')
    # check if the integration have run till the end
    while not converg_odepr_relx(w[-1], f[-1]):
        x_begin = x_end
        x_end = x_begin + delta
        f_init = f[-1][-1]
        w.append(np.linspace(x_begin, x_end, nstep))
        try:
            f.append(scipy.integrate.odeint(odepr_thtr, f_init,
                                            w[-1], args=(g, m0, p0, kappa, sa)))
        except RuntimeError:
            print(
                '# scipy.integrate.odeint :: problems integration relaxation region ODE system')
    '''
	for j in range(0,len(w),1):
		wj = w[j]
		fj = f[j]
		for i in range(0,len(wj),1):
			print(putil.strformat(4).format(wj[i],fj[i][0],fj[i][1],fj[i][2]))
	'''
    w_clean, f_clean = clean_relax(w, f)
    if len(w_clean) == 0 or len(f_clean) == 0:
        raise putil.ProbError('ODE relaxation :: missing solution!!!')
    return w_clean, f_clean


def converg_odepr_drv(w, f):
    # check derivatives in last step
    wm = w[-3]
    w0 = w[-2]
    wp = w[-1]

    w02 = w0 * w0

    dwp = wp - w0
    dwm = wm - w0

    f1m = f[-3][0]
    f10 = f[-2][0]
    f1p = f[-1][0]
    f2m = f[-3][1]
    f20 = f[-2][1]
    f2p = f[-1][1]
    f3m = f[-3][2]
    f30 = f[-2][2]
    f3p = f[-1][2]

    df1 = ((f1p - f10) * (dwm / dwp) - (f1m - f10) * (dwp / dwm)) / (dwm - dwp)
    df2 = ((f2p - f20) * (dwm / dwp) - (f2m - f20) * (dwp / dwm)) / (dwm - dwp)
    df3 = ((f3p - f30) * (dwm / dwp) - (f3m - f30) * (dwp / dwm)) / (dwm - dwp)

    ddf1 = 2.0 * ((f1p - f10) / dwp - (f1m - f10) / dwm) / (dwp - dwm)
    ddf2 = 2.0 * ((f2p - f20) / dwp - (f2m - f20) / dwm) / (dwp - dwm)
    ddf3 = 2.0 * ((f3p - f30) / dwp - (f3m - f30) / dwm) / (dwp - dwm)

    tol = 1.0e-09
    control = True
    try:
        control = control and \
            np.fabs(df1 * w0 / f10) < tol and \
            np.fabs(ddf1 * w02 / f10) < tol
    except RuntimeWarning:
        control = False
    try:
        control = control and \
            np.fabs(df2 * w0 / f20) < tol and \
            np.fabs(ddf2 * w02 / f20) < tol
    except RuntimeWarning:
        control = False
    try:
        control = control and \
            np.fabs(df3 * w0 / f30) < tol and \
            np.fabs(ddf3 * w02 / f30) < tol
    except RuntimeWarning:
        control = False

    if control:
        print('# ODE convergence :: convergence reached in the derivatives')
    return control


def converg_mono_theta(f):
    # assume theta is monotonic
    return chk_monotonicity(f[:, 0])


def converg_mono_t(f):
    # assume t is monotonic
    return chk_monotonicity(f[:, 1])


def converg_mono_rho(f):
    # assume rho is monotonic
    return chk_monotonicity(f[:, 2])


def converg_min_tem(f):
    # assume the temperature has raised, then t >= 1.0 and theta >= 1.0
    return chk_minimum(f[:, 0], 1.0) or chk_minimum(f[:, 1], 1.0)


def converg_odepr_prec(w, f):
    # check in the array dimension
    if len(w) < 3:
        raise putil.ProbError(
            '# ODE convergence :: integration only uses 3 points, very low resolution to check convergence')
    return converg_odepr_drv(w, f) or converg_mono_rho(f)


def converg_odepr_relx(w, f):
    # check in the array dimension
    if len(w) < 3:
        raise putil.ProbError(
            '# ODE convergence :: integration only uses 3 points, very low resolution to check convergence')
    return converg_odepr_drv(w, f) or converg_mono_rho(f) or converg_min_tem(f)


def chk_monotonicity(x):
    if x[0] - x[1] >= 0.0:
        sign = 1.0
    else:
        sign = -1.0
    for i in range(2, len(x), 1):
        if sign * (x[i - 1] - x[i]) < 0.0:
            print('# ODE convergence :: convergence reached in the density monotonicity')
            return True
    return False


def chk_minimum(data, xmin):
    for x in data:
        if x < xmin:
            return True
    return False


def locate_value(data, val):
    i = bisect.bisect_left(data, val)
    if i == len(data) - 1:
        return i - 1
    return i


def check_shock(wp, fp, wr, fr, g, m0, p0, kappa, sa):
    ip_asp = -1
    for i in range(1, len(wp), 1):
        x1 = m0 / (fp[i - 1][2] * np.sqrt(fp[i - 1][1])) - 1.0
        x2 = m0 / (fp[i][2] * np.sqrt(fp[i][1])) - 1.0
        if x1 * x2 < 0.0:
            ip_asp = i
    ir_asp = -1
    for i in range(1, len(wr), 1):
        x1 = m0 / (fr[i - 1][2] * np.sqrt(fr[i - 1][1])) - 1.0
        x2 = m0 / (fr[i][2] * np.sqrt(fr[i][1])) - 1.0
        if x1 * x2 < 0.0:
            ir_asp = i
    if ip_asp >= 0 or ir_asp >= 0:
        # there is an ASP, there is no shock, continuous solution
        return False
    return True


def join_continuous(wp, fp, wr, fr, tasp):
    '''
    Join precursor region with relaxation region for the conituous case
    We move the curves in such a way that the ASP point will be located 
    at x=0.0
    '''
    # get the point for the ASP in the precursor (lineal approx.)
    ip = locate_value([fi[1] for fi in fp], tasp)
    if ip == len(wp):
        raise putil.ProbError(
            'There is no ASP in the precursor region for continuous solution')
    m = (fp[ip][1] - fp[ip + 1][1]) / (wp[ip] - wp[ip + 1])
    n = fp[ip][1] - m * wp[ip]
    wp_asp = (tasp - n) / m
    # get the point for the ASP in the relaxation zone (lineal approx.)
    ir = locate_value([fi[1] for fi in fr], tasp) + 1
    if ir == len(wr):
        raise putil.ProbError(
            'There is no ASP in the relaxation region for continuous solution')
    m = (fr[ir][1] - fr[ir + 1][1]) / (wr[ir] - wr[ir + 1])
    n = fr[ir][1] - m * wr[ir]
    wr_asp = (tasp - n) / m
    # joint solution
    w = []
    f = []
    # add precursor
    for i in range(0, ip, 1):
        w.append(wp[i] - wp_asp)
        f.append(fp[i])
    # add relaxation region
    for i in range(ir, len(wr), 1):
        w.append(wr[i] - wr_asp)
        f.append(fr[i])
    '''	
	for i in range(0,len(wp),1):
		print(putil.strformat(4).format(wp[i]-wp_asp,fp[i][0],fp[i][1],fp[i][2]))
	print()
	for i in range(0,len(wr),1):
		print(putil.strformat(4).format(wr[i]-wr_asp,fr[i][0],fr[i][1],fr[i][2]))
	sys.exit()
	'''
    return w, f


def join_embedded(wp, fp, wr, fr, g, m0, p0, kappa, sa):
    '''
    Join precursor region with relaxation region for the shock case
    We move the curves in such a way that the shock will be located
    at x = 0.0
    '''
    # parameter initialization
    err_tht_min = 2.0
    ip_tht_min = 0
    ir_tht_min = 0
    err_flx_min = 2.0
    ip_flx_min = 0
    ir_flx_min = 0
    thr = [fi[0] for fi in fr]
    # look-up of the shock position
    for ip in range(0, len(wp)):
        # locate the closest point in the relaxation point
        ir = locate_value(thr, fp[ip][0])
        if ir == len(wr):
            continue
        # set intersection point
        m = (fr[ir][0] - fr[ir + 1][0]) / (wr[ir] - wr[ir + 1])
        n = fr[ir][0] - m * wr[ir]
        xr0 = (fp[ip][0] - n) / m
        r = (xr0 - wr[ir]) / (wr[ir + 1] - wr[ir])
        fstar = np.array([(1.0 - r) * fa + r * fb for fa,
                          fb in zip(fr[ir], fr[ir + 1])])
        # continuity of the flux in lab-fram
        flx0 = radflux(fp[ip][2], fp[ip][1], fp[ip][
                       0], m0 / fp[ip][2], g, m0, p0)
        flxstar = radflux(fstar[2], fstar[1], fstar[
                          0], m0 / fstar[2], g, m0, p0)
        err_tht_curr = 2.0 * \
            np.fabs((fr[ir][0] - fp[ip][0]) / (fr[ir][0] + fp[ip][0]))
        err_flx_curr = 2.0 * np.fabs((flx0 - flxstar) / (flx0 + flxstar))
        # look for minumum error
        if err_flx_curr < err_flx_min:
            ip_flx_min = ip
            ir_flx_min = ir
            err_flx_min = err_flx_curr
        if err_tht_curr < err_tht_min:
            ip_tht_min = ip
            ir_tht_min = ir
            err_tht_min = err_tht_curr
    # select the shock point
    tol = 1.0e-03
    if (1.0 - err_flx_min / 2.0) < tol:
        ip0 = ip_tht_min
        ir0 = ir_tht_min
    else:
        ip0 = ip_flx_min
        ir0 = ir_flx_min
    # set up the solution
    w = []
    f = []
    for i in range(0, ip0, 1):
        w.append(wp[i] - wp[ip0])
        f.append(fp[i])
    for i in range(ir0, len(wr), 1):
        w.append(wr[i] - wr[ir0])
        f.append(fr[i])
    '''	
	for i in range(0,len(wp),1):
		print(putil.strformat(4).format(wp[i]-wp[ip0],fp[i][0],fp[i][1],fp[i][2]))
	for i in range(0,len(wr),1):
		print(putil.strformat(4).format(wr[i]-wr[ir0],fr[i][0],fr[i][1],fr[i][2]))
	sys.exit(-1)
	'''
    return w, f


def clean_prec(w0, f0):
    '''
    Clean precursor profile assuming density in monotone increasing
    '''
    w = []
    f = []
    ref = 0.9
    for j in range(0, len(w0), 1):
        wj = w0[j]
        fj = f0[j]
        try:
            for i in range(0, len(wj), 1):
                if fj[i][2] > ref and fj[i][1] >= 1.0:
                    ref = fj[i][2]
                    w.append(wj[i])
                    f.append(fj[i])
        except IndexError:
            if fj[0, 2] > ref and fj[0, 1] >= 1.0:
                ref = fj[0, 2]
                w.append(wj)
                f.append(fj)
    return w, f


def clean_relax(w0, f0):
    '''
    Clean relaxation region profile assuming density is increasing
    '''
    w = []
    f = []
    ref = 0.9
    for j in range(len(w0) - 1, 0, -1):
        wj = w0[j]
        fj = f0[j]
        try:
            for i in range(len(wj) - 1, 0, -1):
                if fj[i][2] >= ref and fj[i][1] >= 1.0:
                    ref = fj[i][2]
                    w.append(wj[i])
                    f.append(fj[i])
        except IndexError:
            if fj[0, 2] >= ref and fj[0, 1] >= 1.0:
                ref = fj[0, 2]
                w.append(wj)
                f.append(fj)
    return w, f


def state_prec(w, f, pos=0.0):
    i = bisect.bisect_left(w, pos)
    try:
        return f[i][0], f[i][1], f[i][2]
    except:
        return f[i - 1][0], f[i - 1][1], f[i - 1][2]


def state_relax(w, f, pos=0.0):
    i = bisect.bisect_right(w, pos)
    try:
        return f[i][0], f[i][1], f[i][2]
    except:
        return f[i - 1][0], f[i - 1][1], f[i - 1][2]


def jump_hydroshock(thp, tp, rp, thr, tr, rr, f, g, m0, p0, kappa, sa):
    '''
    check eqs. 12 for hydrodynamic jump conditions
    '''
    # full precursor state
    vp = m0 / rp
    pp = rp * tp / g
    ep = tp / (g * (g - 1.0))
    # full relaxation region state
    vr = m0 / rr
    pr = rr * tr / g
    er = tr / (g * (g - 1.0))
    # compute error in jump conditions
    err_mass = np.fabs(1.0 - (rp * vp) / (rr * vr))
    err_mom = np.fabs(1.0 - (rp * vp * vp + pp) / (rr * vr * vr + pr))
    err_ene = np.fabs(1.0 - (vp * (rp * ep + pp)) / (vr * (rr * er + pr)))
    err_rad = np.fabs(1.0 - radflux(rp, tp, thp, vp, g, m0,
                                    p0) / radflux(rr, tr, thr, vr, g, m0, p0))
    # return errors
    return [err_mass, err_mom, err_ene, err_rad, ]

# auxiliar functions for glaw EOS model


def radflux(r, t, th, v, g, m0, p0):
    cp = 1.0 / (g - 1.0)
    th4 = th * th * th * th
    return -4.0 * v * (6.0 * cp * r * (t - 1.0) +
                       3.0 * r * (v * v - m0 * m0) +
                       8.0 * p0 * (th4 - r)
                       ) / (24.0 * p0) + \
        4.0 * v * th4 / 3.0


def dxdm(m, x, t, g, m0, p0, kappa, sa):
    '''
    Reference eq. 37
    '''
    rho = rhomt(m, t, m0)
    return (-6.0) * (m0 * rho * t / ((g + 1.0) * p0 * m)) * \
                    ((m * m - 1.0) / dzeta(m, t, g, m0, p0, kappa, sa))


def dtdm(m, x, t, g, m0, p0, kappa, sa):
    '''
    Reference eq. 38
    '''
    rho = rhomt(m, t, m0)
    return -2.0 * ((g - 1.0) / (g + 1.0)) * (t / m) * \
        (nzeta(m, t, g, m0, p0, kappa, sa) / dzeta(m, t, g, m0, p0, kappa, sa))


def odepr_mt(w, m, g, m0, p0, kappa, sa):
    '''
    ODE system for the precursor/relaxation regions formed by 
    eqs. 37 and 38 from reference
    '''
    t = w[1]
    gp1 = g + 1.0
    gm1 = g - 1.0
    rho = rhomt(m, t, m0)
    zn = nzeta(m, t, g, m0, p0, kappa, sa)
    zd = dzeta(m, t, g, m0, p0, kappa, sa)
    '''
	return [ (-6.0)*(m0*rho*t/(gp1*p0*m))*((m*m-1.0)/zd),
			 (-2.0)*(gm1/gp1)*(t/m)*(zn/zd),
		     ]
	'''

    f1 = (-6.0) * (m0 * rho * t / (gp1 * p0 * m)) * ((m * m - 1.0) / zd)
    f2 = (-2.0) * (gm1 / gp1) * (t / m) * (zn / zd)

    #print(' m = {}  x = {}  t = {} -> zn = {}  zd = {}'.format(m,w[0],w[1],zn,zd))
    #print(' m x t f1 f2 :: {:11e}  {:11e}  {:11e}  {:11e}  {:11e}'.format(m,w[0],w[1],f1,f2))

    return [f1, f2, ]


def odepr_thtr(w, m, g, m0, p0, kappa, sa):
    '''
    ODE build with eqs. 18, 23 and 22, no need to compute density and 
    select "density branch" in eq. 17
    '''
    th = w[0]
    t = max(1.0, w[1])
    r = w[2]
    r2 = r * r
    v = m0 / r
    m = v / np.sqrt(t)
    m2 = m * m
    m02 = m0 * m0
    th3 = th * th * th
    th4 = th3 * th
    t4 = t * t * t * t
    cp = 1.0 / (g - 1.0)

    f1 = v * (6.0 * cp * r * (t - 1.0) +
              3.0 * r * (v * v - m02) +
              8.0 * p0 * (th4 - r)
              ) / (24.0 * p0 * kappa * th3)

    if f1 < 0.0:
        print('# ODE precursor :: warning!!! dth/dx<0.0, correct it to dth/dx=0.0')
        f1 = 0.0

    zn = 4.0 * m0 * th3 * f1 + (g * m2 - 1.0) * 3.0 * r * sa * (th4 - t4)
    f2 = (p0 / (3.0 * cp * r * m0)) * (zn / (m2 - 1.0))
    f3 = r2 * (3.0 * r * f2 + 4.0 * g * p0 * th3 * f1) / \
        (3.0 * (g * m02 - r * r * t))

    #print(' f1 f2 f3 :: {}  {}  {}'.format(f1,f2,f3))

    return [f1, f2, f3, ]


def odepr_thtr_drv(w, m, g, m0, p0, kappa, sa):
    return


def dthdx_tth(t, th, g, m0, p0, kappa, t0, th0, r0):
    '''
    Reference eq. 18 using (t,th) dependence
    '''
    rho = rhotth(t, th, g, m0, p0, t0, th0, r0)
    return dthdx(t, th, rho, m0 / rho, g, m0, p0, kappa)


def dthdx_mt(m, t, g, m0, p0, kappa):
    '''
    Reference eq. 18 but using (m,t) dependance
    '''
    rho = rhomt(m, t, g)
    return dthdx(t, thetamt(m, t, g, m0, p0), rho, m0 / rho, g, m0, p0, kappa)


def dthdx(t, th, rho, v, g, m0, p0, kappa):
    '''
    Reference eq. 18
    '''
    th3 = th * th * th
    th4 = th3 * th
    return v * (6.0 * rho * (t - 1.0) / (g - 1.0) +
                3.0 * rho * (v * v - m0 * m0) +
                8.0 * p0 * (th4 - rho)
                ) / (24.0 * p0 * kappa * th3)


def dtdx(t, th, g, m0, p0, kappa, sa, t0, th0, r0):
    '''
    Reference eq. 23
    '''
    rho = rhotth(t, th, g, m0, p0, t0, th0, r0)
    m = m0 / (rho * np.sqrt(t))
    zn = nzeta(m, t, g, m0, p0, kappa, sa)
    return (p0 * (g - 1.0) / (3.0 * rho * m0)) * (zn / (m * m - 1.0))


def nzeta(m, t, g, m0, p0, kappa, sa):
    '''
    Reference eq. 24
    '''
    thp = dthdx_mt(m, t, g, m0, p0, kappa)
    th4 = theta4mt(m, t, g, m0, p0)
    th3 = np.power(th4, 0.75)
    return 4.0 * m0 * th3 * thp + (g * m * m - 1.0) * (3.0 * rhomt(m, t, m0) * sa * (th4 - t * t * t * t))


def nzeta_dt(m, t, g, m0, p0, kappa, sa):
    return


def nzeta_asp(tasp, g, m0, p0, kappa, sa):
    '''
    Reference eq. 64, Zn(tasp,1.0) = 0.0
    '''
    return nzeta(1.0, tasp, g, m0, p0, kappa, sa)


def nzeta_asp_drv(tasp, g, m0, p0, kappa, sa):
    '''
    Reference eq. 64, Zn(tasp,1.0) = 0.0
    '''
    return nzeta_dt(1.0, tasp, g, m0, p0, kappa, sa)


def dzeta(m, t, g, m0, p0, kappa, sa):
    '''
    Reference eq. 39
    '''
    thp = dthdx_mt(m, t, g, m0, p0, kappa)
    th4 = theta4mt(m, t, g, m0, p0)
    th3 = np.power(th4, 0.75)
    return 4.0 * m0 * th3 * thp + ((g - 1.0) / (g + 1.0)) * (g * m * m + 1.0) * (3.0 * rhomt(m, t, m0) * sa * (th4 - t * t * t * t))


def rhomt(m, t, m0):
    '''
    Reference eq. 40
    '''
    return m0 / (m * np.sqrt(t))


def rhotth_plus(t, th, g, m0, p0):
    '''
    Reference eq. 17 for "+" branch
    '''
    w = km(g, m0, p0) - g * p0 * th * th * th * th
    return (w + np.sqrt(w * w - 36.0 * g * m0 * m0 * t)) / (6.0 * t)


def rhotth_minus(t, th, g, m0, p0):
    '''
    Reference eq. 17 for "-" branch
    '''
    w = km(g, m0, p0) - g * p0 * th * th * th * th
    return (w - np.sqrt(w * w - 36.0 * g * m0 * m0 * t)) / (6.0 * t)


def rhotth(t, th, g, m0, p0, t0, th0, r0):
    '''
    Reference eq. 17 with branch selection
    '''
    tol = 1.0e-06
    if tol > np.fabs(1.0 - rhotth_minus(t0, th0, g, m0, p0) / r0):
        return rhotth_minus(t, th, g, m0, p0)
    else:
        return rhotth_plus(t, th, g, m0, p0)


def rhotth_dt(t, th, g, m0, p0, t0, th0, r0):
    '''
    Reference eq. 61 with branch selection
    '''
    tol = 1.0e-06
    if tol > np.fabs(1.0 - rhotth_minus(t0, th0, g, m0, p0) / r0):
        return rhotth_dt_m(t, th, g, m0, p0)
    else:
        return rhotth_dt_p(t, th, g, m0, p0)
    return


def rhotth_dth(t, th, g, m0, p0, t0, th0, r0):
    '''
    Reference eq. 62 with branch selection
    '''
    tol = 1.0e-06
    if tol > np.fabs(1.0 - rhotth_minus(t0, th0, g, m0, p0) / r0):
        return rhotth_dth_m(t, th, g, m0, p0)
    else:
        return rhotth_dth_p(t, th, g, m0, p0)
    return


def rhotth_dt_m(t, th, g, m0, p0):
    '''
    Reference eq. 61 for "-" branch
    '''
    rho = rhotth_minus(t, th, g, m0, p0)
    b = km(g, m0, p0) - g * p0 * th * th * th * th
    d = np.sqrt(b * b - 36.0 * g * m0 * m0 * t)
    return -(rho - 3.0 * g * m0 * m0 / d) / t


def rhotth_dt_p(t, th, g, m0, p0):
    '''
    Reference eq. 61 for "+" branch
    '''
    rho = rhotth_plus(t, th, g, m0, p0)
    b = km(g, m0, p0) - g * p0 * th * th * th * th
    d = np.sqrt(b * b - 36.0 * g * m0 * m0 * t)
    return -(rho + 3.0 * g * m0 * m0 / d) / t


def rhotth_dth_m(t, th, g, m0, p0):
    '''
    Reference eq. 62 for "-" branch
    '''
    th4 = th * th * th * th
    k = km(g, m0, p0)
    b = k - g * p0 * th4
    d = np.sqrt(b * b - 36.0 * g * m0 * m0 * t)
    return (-2.0 * p0 * g * th * th * th) / (3.0 * t) * (1.0 - (k - g * p0 * th4) / d)


def rhotth_dth_p(t, th, g, m0, p0):
    '''
    Reference eq. 62 for "+" branch
    '''
    th4 = th * th * th * th
    k = km(g, m0, p0)
    b = k - g * p0 * th4
    d = np.sqrt(b * b - 36.0 * g * m0 * m0 * t)
    return (-2.0 * p0 * g * th * th * th) / (3.0 * t) * (1.0 + (k - g * p0 * th4) / d)


def thetamt(m, t, g, m0, p0):
    '''
    Reference eq. 41 
    modified to return theta instead theta^4
    '''
    return np.power(theta4mt(m, t, g, m0, p0), 0.25)


def theta4mt(m, t, g, m0, p0):
    '''
    Reference eq. 41 
    '''
    r = rhomt(m, t, m0)
    return (km(g, m0, p0) - 3.0 * g * m0 * m0 / r - 3.0 * t * r) / (g * p0)


def th3thprime(m, t, g, m0, p0, kappa):
    '''
    Combination of eq. 41 and 18
    '''
    return np.power(theta4mt(m, t, g, m0, p0), 0.75) * dthdx_mt(m, t, g, m0, p0, kappa)


def km(g, m0, p0):
    '''
    Reference eq. 16
    '''
    return 3.0 * (g * m0 * m0 + 1.0) + g * p0


def dtdth(t, th, g, m0, p0, kappa, sa, t0, th0, r0):
    '''
    Reference eq. 54
    '''
    tol = 1.0e-06
    if tol > np.fabs(1.0 - rhotth_minus(t0, th0, g, m0, p0) / r0):
        r = rhotth_minus(t, th, g, m0, p0)
        drt = rhotth_dt_m(t, th, g, m0, p0)
        drth = rhotth_dth_m(t, th, g, m0, p0)

        '''
		dtol = 1.0e-3
		rt_m = rhotth_minus((1.0-dtol)*t,th,g,m0,p0)
		rt_p = rhotth_minus((1.0+dtol)*t,th,g,m0,p0)
		drt0 = (rt_p-rt_m)/(2.0*dtol*t)
		rth_m = rhotth_minus(t,(1.0-dtol)*th,g,m0,p0)
		rth_p = rhotth_minus(t,(1.0+dtol)*th,g,m0,p0)
		drth0 = (rth_p-rth_m)/(2.0*dtol*t)

		print(' err_drt  = {}'.format(1.0-np.fabs(drt0/drt)))
		print(' err_drth = {}'.format(1.0-np.fabs(drth0/drth)))
		'''

        sg = -1.0
    else:
        r = rhotth_plus(t, th, g, m0, p0)
        drt = rhotth_dt_p(t, th, g, m0, p0)
        drth = rhotth_dth_p(t, th, g, m0, p0)
        sg = 1.0
    dft, dfth, dgt, dgth = \
        dtdth_fgdrv(r, t, th, g, m0, p0, kappa, sa, drdt=drt, drdth=drth)
    w = dft - dgth
    # return (w + sg*np.sqrt(w*w + 4.0*dgt*dfth))/(2.0*dgt)
    ans = (w + sg * np.sqrt(w * w + 4.0 * dgt * dfth)) / (2.0 * dgt)

    '''
	print(' r drt drth :: {}  {}  {}'.format(r,drt,drth))
	print(' dft dfth dgt dgth :: {}  {}  {}  {}'.format(dft,dfth,dgt,dgth))
	print(' w sq sg :: {}  {}  {}'.format(w,np.sqrt(w*w + 4.0*dgt*dfth),sg))
	print(' ans :: {}'.format(ans))
	
	dtol = 1.0e-09
	
	ft_m = dtdx((1.0-dtol)*t,th,g,m0,p0,kappa,sa,t0,th0,r0)
	ft_p = dtdx((1.0+dtol)*t,th,g,m0,p0,kappa,sa,t0,th0,r0)
	dft0 = (ft_p-ft_m)/(2.0*dtol*t)
	
	fth_m = dtdx(t,(1.0-dtol)*th,g,m0,p0,kappa,sa,t0,th0,r0)
	fth_p = dtdx(t,(1.0+dtol)*th,g,m0,p0,kappa,sa,t0,th0,r0)
	dfth0 = (fth_p-fth_m)/(2.0*dtol*t)
	
	gt_m = dthdx_tth((1.0-dtol)*t,th,g,m0,p0,kappa,t0,th0,r0)
	gt_p = dthdx_tth((1.0+dtol)*t,th,g,m0,p0,kappa,t0,th0,r0)
	dgt0 = (gt_p-gt_m)/(2.0*dtol*t)
	
	gth_m = dthdx_tth(t,(1.0-dtol)*th,g,m0,p0,kappa,t0,th0,r0)
	gth_p = dthdx_tth(t,(1.0+dtol)*th,g,m0,p0,kappa,t0,th0,r0)
	dgth0 = (gth_p-gth_m)/(2.0*dtol*t)
		
	print(' err_dft  = {}'.format(1.0-np.fabs(dft0/dft)))
	print(' err_dfth = {}'.format(1.0-np.fabs(dfth0/dfth)))
	print(' err_dgt  = {}'.format(1.0-np.fabs(dgt0/dgt)))
	print(' err_dgth = {}'.format(1.0-np.fabs(dgth0/dgth)))

	print(' dft dft0 :: {}  {}'.format(dft,dft0))
	'''

    return ans


def dtdth_fgdrv(r, t, th, g, m0, p0, kappa, sa, drdt=None, drdth=None):
    '''
    Reference eqs. 55, 56, 57 & 58
    '''
    # get density derivatives
    if drdt:
        drt = drdt
    else:
        drt = rhotth_drt(t, th, g, m0, p0, t, th, r)
    if drdth:
        drth = drdth
    else:
        drth = rhotth_drth(t, th, g, m0, p0, t, th, r)
    # define some auxiliar variables
    v = m0 / r
    m02 = m0 * m0
    m = v / np.sqrt(t)
    m2 = m * m
    cp = 1.0 / (g - 1.0)
    th3 = th * th * th
    th4 = th3 * th
    # parameters
    c1 = m0 / (24.0 * p0 * kappa * r * r * th3)
    c2 = p0 / (3.0 * cp * m0 * (m2 - 1.0))
    # derivatives of f and g
    dgt = c1 * (6.0 * cp * r * (2.0 * drt * (t - 1.0) + r) -
                6.0 * m02 * r * drt +
                8.0 * p0 * drt * (th4 - 2.0 * r))
    dgth = c1 * (12.0 * cp * drth * r * (t - 1.0) -
                 6.0 * m02 * r * drth +
                 8.0 * p0 * (drth * (th4 - 2.0 * r) + 4.0 * r * th3))
    dft = c2 * (4.0 * v * th3 * dgt -
                12.0 * sa * (g * m2 - 1.0) * t * t * t)
    dfth = c2 * (4.0 * v * th3 * dgth +
                 12.0 * sa * (g * m2 - 1.0) * th3)
    return (dft, dfth, dgt, dgth)


def fisp(n, g, m0, p0):
    '''
    Reference eq. 29, all terms to the right hand side, then fisp(n) = 0
    '''
    a = g * p0 / 3.0
    m02 = m0 * m0
    return 1.0 + a + (1.0 - 2.0 * n) * g * m02 - a * np.power(g * n * n * m02, 4.0)


def fisp_drv(n, g, m0, p0):
    a = g * p0 / 3.0
    w = n * m0
    return -2.0 * g * m0 * m0 - 8.0 * a * g * g * g * g * m0 * np.power(n * m0, 7.0)


def fisp_for_nu_low(g, m0, p0):
    return min(1.0, 0.5 * (1.0 + (1.0 + g * p0 / 3.0) / (g * m0 * m0)))


def fisp_taylor(g, m0, p0):
    m02 = m0 * m0
    ex = np.power(g * m02, 4.0)
    f1 = 1.0 + g * p0 / 3.0 - g * m02 - g * p0 * (ex) / 3.0
    df1 = -2.0 * g * m02 - 8.0 * g * p0 * (ex) / 3.0
    return min(1.0, 1.0 - f1 / df1)

prob_dict = {
    'key': prob_key,
    'desc': prob_desc,
    'ref': prob_ref,
    'solve': solve,
}


def main():
    util.printStandAloneMode(__file__)
    putil.printProbInfo(prob_dict)
    return 0

if __name__ == '__main__':
    sys.exit(main())
