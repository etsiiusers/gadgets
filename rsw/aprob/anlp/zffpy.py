

#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Manage ionization calculations
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

try:
	import anlp
except ImportError:
	sys.path.append('../')

import anlp.util as util

# program description
desc = 'Manage ionization calculations'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# EOS calculations error class
class ZffError(util.Error):
	'''
	Charge state exception class based in the base Error class defined in util
	'''
	pass

# charge state 
zff_const = 1.0

# get charge state and derivatives for const ionization
def get_zff_const(d,t,opt=None):
	global zff_const
	if opt:
		if   'dr' in opt:
			return 0.0
		elif 'dt' in opt:
			return 0.0
		else:
			return zff_const
	else:
		return zff_const

# get charge state and derivatives for the approximate Thomas-Fermi 
# model
def get_zff_atf(den,tem,opt=None):
	if opt:
		if   'dr' in opt:
			return zff_atf_dr(den,tem)
		elif 'dt' in opt:
			return zff__atf_dt(den,tem)
		else:
			return zff_atf(den,tem)
	else:
		return zff_atf(den,tem)

# specific charge state functions
def zff_atf(d,t):
	return 0.0
def zff_atf_dr(d,t):
	return 0.0
def zff_atf_dt(d,t):
	return 0.0

# get charge state and derivatives for the approximate Thomas-Fermi 
# model
def get_zff_real(den,tem,opt=None):
	if opt:
		if   'dr' in opt:
			return zff_real_dr(den,tem)
		elif 'dt' in opt:
			return zff_real_dt(den,tem)
		else:
			return zff_real(den,tem)
	else:
		return zff_real(den,tem)

# specific charge state functions
def zff_real(d,t):
	return 0.0
def zff_real_dr(d,t):
	return 0.0
def zff_real_dt(d,t):
	return 0.0

# select charge state functions from a defined ionization model
def select_zff_model(args):
	'''
	Select EOS callable set of functions from eos_models dictionary
	'''
	global zff_models
	try:
		return zff_models[args.zff]
	except (AttributeError, KeyError):
		return zff_models['default']

# charge state models avaliable
zff_models = { 'const': get_zff_const, 
               'atf': get_zff_atf,
               'real': get_zff_real,
               'default': get_zff_atf,
			 }

def main():
	util.printStandAloneMode(__file__)
	return 0

if __name__=='__main__' :
	sys.exit(main())
