

#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Manage opacity calculations
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys
import math

try:
	import anlp
except ImportError:
	sys.path.append('../')

import anlp.util as util

# program description
desc = 'Manage opacity calculations'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# opacity calculation error class
class OpaError(util.Error):
	'''
	Opacity managing exception class
	'''
	pass

# opacity calculations
def opa_kappa_const(r,t,opt=None):
	'''
	Set constant opacity
	'''
	fopa = lambda r,t: 1.0e-04
	if opt:
		if 'dr' in opt:
			return 0.0
		elif 'dt' in opt:
			return 0.0
		else:
			return fopa(r,t)
	else:
		return fopa(r,t)

# use a power law formula for opacity
def opa_kappa_powlaw(r,t,opt=None):
	'''
	Use power law formula for opacity
	'''
	fopa = lambda r,t: (4.0e-09)*math.pow(t,3.5)/r
	if opt:
		if 'dr' in opt:
			return -fopa(r,t)/r
		elif 'dt' in opt:
			return 3.5*fopa(r,t)/t
		else:
			return fopa(r,t)
	else:
		return fopa(r,t)

# get opacity and derivatives data from tables
def opa_kappa_real(r,t,opt=None):
	'''
	Use opacity data tables
	'''
	if opt:
		if 'dr' in opt:
			return opacity_kappa_dkdr(r,t)
		elif 'dt' in opt:
			return opacity_kappa_dkdt(r,t)
		else:
			return opacity_kappa(r,t)
	else:
		return opacity_kappa(r,t)

# get mean free path
def opa_mfp_real(r,t,opt=None):
	return 1.0/(r*opa_kappa_real(r,t,opt))

# specific opacity functions: WARNING!!! using analytic version instead EOS tables
def opacity_kappa(r,t):
	return opa_kappa_analytic(r,t)
def opacity_kappa_dkdr(r,t):
	return opa_kappa_analytic(r,t,'dr')
def opacity_kappa_dkdt(r,t):
	return opa_kappa_analytic(r,t,'dt')

# select opacity functions from a defined opacity model
def select_opa_model(args):
	'''
	Select opacity callable function from opa_models dictionary
	'''
	global opa_models
	try:
		return opa_models[args.opa]
	except (AttributeError, KeyError):
		# default use opacity data tables
		return opa_models['default']

# Opacity models avaliable
opa_models = { 'const': opa_kappa_const, 
               'powerlaw': opa_kappa_powlaw, 
			   'real': opa_kappa_real,
			   'default': opa_kappa_real,
			 }

def main():
	util.printStandAloneMode(__file__)
	return 0

if __name__=='__main__':
	sys.exit(main())
