
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Program to solve astrophysics analytical problems
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

# try to include whole anlp module
try:
	import anlp
except ImportError:
	sys.path.append('../')

import math
import anlp.util as util

# EOS calculations error class
class EosError(util.Error):
    '''
    EOS exception class based in the base Error class defined in util
    '''
    pass

# program description
desc = 'EOS manipulations'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# global EOS gamma coefficient
eos_gamma = 1.666	

# get energy and derivatives for the gamma-la EOS
def eos_ene_glaw(den,tem,opt=None):
	global eos_gamma 
	if opt:
		if   'dr' in opt:
			return 0.0
		elif 'dt' in opt:
			return 1.0/(eos_gamma*(eos_gamma-1.0))
		else:
			return tem/(eos_gamma*(eos_gamma-1.0))
	else:
		return tem/(eos_gamma*(eos_gamma-1.0))

# get pressure and derivatives for the gamma-la EOS
def eos_pre_glaw(den,tem,opt=None):
	global eos_gamma 
	if opt:
		if   'dr' in opt:
			return tem/eos_gamma
		elif 'dt' in opt:
			return den/eos_gamma
		else:
			return den*tem/eos_gamma
	else:
		return den*tem/eos_gamma

# get energy and derivatives from EOS tables
def eos_ene_real(den,tem,opt=None):
	if opt:
		if   'dr' in opt:
			return eos_energy_dedr(den,tem)
		elif 'dt' in opt:
			return eos_energy_dedt(den,tem)
		else:
			return eos_energy(den,tem)
	else:
		return eos_energy(den,tem)

# get pressure and derivatives from EOS tables
def eos_pre_real(den,tem,opt=None):
	if opt:
		if   'dr' in opt:
			return eos_pressure_dpdr(den,tem)
		elif 'dt' in opt:
			return eos_pressure_dpdt(den,tem)
		else:
			return eos_pressure(den,tem)
	else:
		return eos_pressure(den,tem)

# get sound speed for a gamma-law EOS
def eos_cs_glaw(den,tem,opt=None):
	# using sound speed in H
	return eos_sound(den,tem,aw=1.0)
# get sound speed from EOS tables
def eos_cs_real(den,tem,opt=None):
	# using gamma-law for H !!!
	return eos_sound(den,tem,aw=1.0)	

def eos_sound(den,tem,gamma=1.666,aw=float(1.0)):	# default for H
	r = float(8.31e+00)*1.0e+03/aw
	return math.sqrt( gamma*r*tem )

# specific EOS functions
def eos_pressure(d,t):
	global eos_gamma 
	return d*t/eos_gamma
def eos_pressure_dpdr(d,t):
	global eos_gamma
	return t/eos_gamma
def eos_pressure_dpdt(d,t):
	global eos_gamma
	return d/eos_gamma

def eos_energy(d,t):
	global eos_gamma
	return t/(eos_gamma*(eos_gamma-1.0))
def eos_energy_dedr(d,t):
	return 0.0
def eos_energy_dedt(d,t):
	global eos_gamma
	return 1.0/(eos_gamma*(eos_gamma-1.0))

# select EOS functions from a defined EOS model
def select_eos_model(args):
	'''
	Select EOS callable set of functions from eos_models dictionary
	'''
	global eos_models
	try:
		return eos_models[args.eos]
	except (AttributeError, KeyError):
		return eos_models['default']

# EOS model avaliable
eos_models = { 'glaw': (eos_ene_glaw, eos_pre_glaw, eos_cs_glaw) , 
               'real': (eos_ene_real, eos_pre_real, eos_cs_real),
               'default': (eos_ene_glaw, eos_pre_glaw, eos_cs_glaw),
			 }

def main():
	util.printStandAloneMode(__file__)
	return 0

if __name__=='__main__':
	sys.exit(main())
