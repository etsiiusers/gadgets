

#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Program to solve astrophysics analytical problems, helper functions
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import sys

try:
	import anlp
except ImportError:
	sys.path.append('../')

# program description
desc = 'Program to solve astrophysics analytical problems, helper functions'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author,license)

# base error class
class Error(Exception):
	'''
	Base exception class
	'''
	def __init__(self,value=''):
		self.value = value
		return
	def __str__(self):
		return str(self.value)

# base error class
class Warn(Warning):
	'''
	Base warning class
	'''
	def __init__(self,value=''):
		self.value = value
		return
	def __str__(self):
		return str(self.value)

class MainError(Error):
	pass

class MainWarn(Warn):
	pass

def ndprint(lst,format_string ='  {0:13.6e}',newl=True):
	for item in lst:
		try:
			for x in item:
				print(format_string.format(x),end='')
		except TypeError:
			print(format_string.format(item),end='')
	if newl:
		print()
	return

def printStandAloneMode(fname):
	if not fname:
		name = 'unknown'
	else:
		name = fname
	print('{}: nothing to do in standalone mode'.format(name))
	return

def main():
	printStandAloneMode(__file__)
	return 0

if __name__=='__main__' :
	sys.exit(main())
