
#
# This file is part of GADGETS.
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GADGETS.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Program to solve astrophysics analytical problems
# Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID
#
# Version control:
# 2019.04.22 :: Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)
# - initial version
#

import os
import sys

import argparse
import math

import anlp.util as util
import anlp.eospy as eos
import anlp.opapy as opa
import anlp.zffpy as zff

import anlp.probs.marshak as marshak
import anlp.probs.rs_lte as rs_lte
import anlp.probs.rs_nlte as rs_nlte
#import anlp.probs.coggeshall as cogg

# program description
desc = 'Analytical solutions for coupled hydro-rad problems'
author = 'Manuel Cotelo Ferreiro (manuel.cotelo@upm.es)'
license = 'Copyright (C) 2019  UNIVERSIDAD POLITÉCNICA DE MADRID'
eplg = '{}, {}'.format(author, license)

# dictionary with all problems
prob_names = {marshak.prob_key: (marshak.solve, marshak.prob_desc, marshak.prob_ref),
              rs_lte.prob_key: (rs_lte.solve, rs_lte.prob_desc, rs_lte.prob_ref),
              rs_nlte.prob_key: (rs_nlte.solve, rs_nlte.prob_desc, rs_nlte.prob_ref),
              # cogg.prob_key: (cogg.solve, cogg.prob_desc, cogg.prob_ref),
              }

# select problem and call solver


def solve_problem(args, fd=sys.stdout):
    try:
        prob = prob_names[args.prob]
    except KeyError:
        print('Error :: unknown problem \"{}\"'.format(args.prob))
        return None
    except AttributeError:
        print('Error :: problem to solve is not defined')
        return None
    print_problem_info(prob, fd)
    if args.dry:
        print_program_options(args, fd)
        return
    return prob[0](args)


def print_problem_info(prob, fd=sys.stdout):
    '''
    Print problem information
    '''
    print()
    print('# desc    :: {}'.format(desc))
    print('# autjor  :: {}'.format(author))
    print('# license :: {}'.format(license))
    print()
    print('# Problem :: {}'.format(prob[1]), file=fd)
    print('# Reference :: {}'.format(prob[2]), file=fd)
    return


def print_program_options(args, fd=sys.stdout):
    '''
    Print argument parser options
    '''
    print('Program options :', file=fd)
    dargs = vars(args)
    for key in dargs.keys():
        print(' {:10} :: {}'.format(key, dargs[key]), file=fd)
    return

# set default parameters
args_default = {'prob': 'rs_eqdiff',
                'gamma': 1.666,
                'P0': 1.0e-04,
                'M0': 1.0e+01,
                'eos_model': 'glaw',
                'opa_model': 'const',
                'zff_model': 'atf',
                'dry': False,
                'dim': 1,
                'coord': 'x',
                }


def getProgramArgs(parser, adef=args_default):
    '''
    Specific program argument parser, it reads input problem parameters, 
    selects problem to be solved, and sets up EOS and opacity models.
    '''
    parser.add_argument('--prob', help='Problem to solve',
                        choices=prob_names.keys(), type=str, default=adef['prob'])
    parser.add_argument(
        '--gamma', help='Gamma coefficient for gamma-law EOS', type=float, default=adef['gamma'])
    parser.add_argument(
        '--M0', help='Mach number for the pre-shock state', type=float, default=adef['M0'])
    parser.add_argument(
        '--P0', help='Hydro-rad coupling parameter (P0=0.0 means pure hydro)', type=float, default=adef['P0'])
    parser.add_argument('--eos',
                        help='select EOS model',
                        choices=eos.eos_models.keys(),
                        type=str,
                        default=adef['eos_model'])
    parser.add_argument('--opa',
                        help='select opacity model',
                        choices=opa.opa_models.keys(),
                        type=str,
                        default=adef['opa_model'])
    parser.add_argument('--zff',
                        help='select opacity model',
                        choices=zff.zff_models.keys(),
                        type=str,
                        default=adef['zff_model'])
    parser.add_argument('--dry',
                        help='Not solving problem, only printing info',
                        action='store_true',
                        default=adef['dry'])
    parser.add_argument('--dim',
                        help='Problem dimension',
                        type=int,
                        default=adef['dim'])
    parser.add_argument('--coord',
                        help='Coordinate system',
                        type=str,
                        default=adef['coord'])

    return parser.parse_args()


def main():
    args = getProgramArgs(argparse.ArgumentParser(
        description=desc, epilog=eplg))
    solve_problem(args)
    return 0

if __name__ == '__main__':
    sys.exit(main())
